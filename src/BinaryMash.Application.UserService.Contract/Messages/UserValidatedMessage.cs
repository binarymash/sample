﻿namespace BinaryMash.Application.UserService.Contract.Messages
{
    using System;
    using NServiceBus;

    public class UserValidatedMessage : IMessage
    {
        public Guid UserRegistrationSagaId { get; set; }
    }
}
