﻿namespace BinaryMash.Application.UserService.Contract.Messages
{
    using System;
    using NServiceBus;

    public class UserValidationReceivedMessage : IMessage
    {
        public Guid UserValidationSagaId { get; set; }
    }
}
