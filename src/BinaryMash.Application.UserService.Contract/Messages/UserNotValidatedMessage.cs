﻿namespace BinaryMash.Application.UserService.Contract.Messages
{
    using System;
    using NServiceBus;

    public class UserNotValidatedMessage : IMessage
    {
        public Guid UserRegistrationSagaId { get; set; }
    }
}
