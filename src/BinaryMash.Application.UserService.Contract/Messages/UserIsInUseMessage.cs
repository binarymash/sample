﻿namespace BinaryMash.Application.UserService.Contract.Messages
{
    using Domain;
    using NServiceBus;

    public class UserIsInUseMessage : IMessage
    {
        public UserCredential UserCredential { get; set; }
    }
}
