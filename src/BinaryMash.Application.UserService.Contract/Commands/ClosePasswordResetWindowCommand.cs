﻿namespace BinaryMash.Application.UserService.Contract.Commands
{
    using System;
    using NServiceBus;

    public class ClosePasswordResetWindowCommand : IMessage
    {
        public Guid PasswordResetWindowSagaId { get; set; }
    }
}
