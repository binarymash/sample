﻿namespace BinaryMash.Application.UserService.Contract.Commands
{
    using System;
    using NServiceBus;

    public class RegisterUserCommand : ICommand
    {
        public Guid UserId { get; set; }

        public Guid AccountId { get; set; }

        public string Email { get; set; }

        public string HashedPassword { get; set; }

        public string Salt { get; set; }

        public string FirstName { get; set; }

        public string LastName;

    }
}
