﻿namespace BinaryMash.Application.UserService.Contract.Commands
{
    using System;
    using NServiceBus;

    //TODO: change to event?
    public class UserLogInCommand : ICommand
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public DateTime Time { get; set; }
    }
}
