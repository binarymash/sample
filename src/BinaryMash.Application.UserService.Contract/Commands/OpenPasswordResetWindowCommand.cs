﻿namespace BinaryMash.Application.UserService.Contract.Commands
{
    using System;
    using NServiceBus;

    public class OpenPasswordResetWindowCommand : ICommand
    {
        public Guid LoginId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
    }
}
