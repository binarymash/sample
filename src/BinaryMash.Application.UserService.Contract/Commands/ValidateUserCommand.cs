﻿namespace BinaryMash.Application.UserService.Contract.Commands
{
    using System;
    using NServiceBus;

    public class ValidateUserCommand : ICommand
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public Guid UserRegistrationSagaId { get; set; }
    }
}
