﻿namespace BinaryMash.Application.UserService.Contract.Events
{
    using Domain;
    using NServiceBus;

    public class UserActivated : IEvent
    {
        public UserCredential UserCredential { get; set; }
    }
}
