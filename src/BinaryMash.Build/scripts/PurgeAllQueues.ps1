$queueNameFilter = "private$\BinaryMash.*"

$queues = [Messaging.MessageQueue]::GetPrivateQueuesByMachine(".") | Where-Object {$_.QueueName -like $queueNameFilter}

foreach($queue in $queues)
{
$queuename = $queue.QueueName
$messagecount = $queue.GetAllMessages().Length 
$colour = @{$true="Green";$false="Yellow"}[$messagecount -eq 0] 
$queue.Purge() 
Write-Host "$queuename has been purged of $messagecount messages." -ForegroundColor $colour
}