﻿namespace BinaryMash.DataAccess.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;
    using BinaryMash.DataAccess.EF;
    using Contract;

    public class UserRepository : EFRepository<User>, IUserRepository
    {
        public UserRepository(BirdhouseContext context) : base(context)
        {            
        }

        public User Get(string email)
        {
            return QueryByEmail(email).FirstOrDefault();
        }

        public IQueryable<User> Me(IIdentity identity)
        {
            var id = IdentityConverter.ExtractUserId(identity.Name);
            return DbContext.Set<Login>().AsNoTracking()
                            .Where(l => l.User.Id == id)
                            .Select(l => l.User);
        }

        public IQueryable<User> All(IIdentity identity)
        {
            return AvailableToUser(identity, DbContext.Set<User>());
        }

        private IQueryable<User> QueryByEmail(string email)
        {
            return DbContext.Set<User>()
                .Where(user => user.Email == email);            
        }

        private IQueryable<User> AvailableToUser(IIdentity identity, IQueryable<User> query)
        {
            if (!identity.IsAuthenticated)
            {
                return Enumerable.Empty<User>().AsQueryable();
            }

            var user = User(identity);
            if (user == null)
            {
                return Enumerable.Empty<User>().AsQueryable();

            }

            return query.Where(u => (u.AccountId == user.AccountId));
        }

        private User User(IIdentity identity)
        {
            if (_user == null)
            {
                lock (_lock)
                {
                    if (_user == null)
                    {
                        var id = IdentityConverter.ExtractUserId(identity.Name);
                        _user = DbContext.Set<Login>().AsNoTracking()
                                        .Where(l => l.User.Id == id)
                                        .Include(l => l.User).AsNoTracking()
                                        .Select(l => l.User)
                                        .FirstOrDefault();
                    }
                }
            }

            return _user;
        }

        private volatile User _user;
        private readonly object _lock = new object();
    }
}
