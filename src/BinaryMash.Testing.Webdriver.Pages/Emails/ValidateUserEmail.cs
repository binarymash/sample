﻿
namespace BinaryMash.Testing.Webdriver.Pages.Emails
{
    using System.Net.Mail;
    using Environment;
    using Utilities;
    using Web.Account;

    public class ValidateUserEmail : EmailContent
    {
        public ValidateUserEmail(Sandbox sandbox, MailMessage message) : base(sandbox, message)
        {
        }

        public ActivatePage ClickLink()
        {
            var url = HrefUrlExtractor.From(Message.Body);
            Sandbox.Browser.Session.Visit(url);
            return new ActivatePage(Sandbox);
        }
    }
}
