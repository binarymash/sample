﻿namespace BinaryMash.Testing.Webdriver.Pages.Emails
{
    using System.Net.Mail;
    using Environment;

    public abstract class EmailContent : Content
    {
        public MailMessage Message { get; private set; }

        protected EmailContent(Sandbox sandbox, MailMessage message) : base(sandbox)
        {
            Message = message;
        }
    }
}
