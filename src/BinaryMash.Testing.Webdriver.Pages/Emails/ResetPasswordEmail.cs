﻿
namespace BinaryMash.Testing.Webdriver.Pages.Emails
{
    using System.Net.Mail;
    using Environment;
    using Utilities;
    using Web.Account;

    public class ResetPasswordEmail : EmailContent
    {
        public ResetPasswordEmail(Sandbox sandbox, MailMessage message) : base(sandbox, message)
        {
        }

        public ResetPasswordPage ClickLink()
        {
            var url = HrefUrlExtractor.From(Message.Body);
            Sandbox.Browser.Session.Visit(url);
            return new ResetPasswordPage(Sandbox);
        }
    }
}
