﻿namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using System;
    using Coypu;
    using BinaryMash.Testing.Environment;
    using OpenQA.Selenium;

    public abstract class WebContent<T> : Content
    {
        protected WebContent (Sandbox sandbox) : base(sandbox)
        {
        }

        public BrowserSession BrowserSession { get { return Sandbox.Browser.Session; } }

        protected abstract T This { get; }

        public T WaitForNoToasts()
        {
            BrowserSession.HasNoCss("#toast-container", new Options { Timeout = TimeSpan.FromSeconds(20) });
            return This;
        }

        public T RemoveAllToasts()
        {
            if (string.Equals("false", Sandbox.Browser.Session.ExecuteScript("return window.document.notifier === undefined;"), StringComparison.OrdinalIgnoreCase))
            {
                Sandbox.Browser.Session.ExecuteScript("return window.document.notifier.removeAll();");
            }
            return WaitForNoToasts();
        }

        public void ScrollUntilFillInWith(ElementScope element, string value, int maxAttempts = 10, bool up = true)
        {
            try
            {
                element.FillInWith(value);
            }
            catch
            {
                if (maxAttempts <= 0)
                {
                    throw new Exception("Couldn't scroll to the requested element");
                }

                int amount = up ? -25 : 25;
                var script = string.Format("window.scrollBy(0,{0});", amount);
                BrowserSession.ExecuteScript(script);
                ScrollUntilFillInWith(element, value, --maxAttempts, up);
            }
        }

        public void ScrollUntilClick(ElementScope element, int maxAttempts, bool up = true)
        {
            try
            {
                element.Click();
            }
            catch
            {
                if (maxAttempts <= 0)
                {
                    throw new Exception("Couldn't scroll to the requested element");
                }

                int amount = up ? 25 : -25;
                var script = string.Format("window.scrollBy(0,{0});", amount);
                BrowserSession.ExecuteScript(script);
                ScrollUntilClick(element, --maxAttempts, up);
            }
        }

    }
}
