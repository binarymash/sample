﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Authentication
{
    using Account;
    using BinaryMash.Testing.Environment;
    using User;

    public class IndexPage : WebPage<IndexPage>
    {
        public IndexPage(Sandbox sandbox) : base(sandbox)
        {
            BrowserSession.Visit("Authentication");
        }

        public HomePage LogInAs(string email, string password)
        {
            FillInLoginDetailsAndSubmit(email, password);
            WaitForPageWithTitle("Questions");
            return new HomePage(Sandbox);
        }

        public IndexPage LogInAsExpectingError(string email, string password)
        {
            FillInLoginDetailsAndSubmit(email, password);
            return this;
        }

        public ForgottenPasswordPage ForgottenPassword
        {
            get
            {
                BrowserSession.ClickLink("I've forgotten my password");
                return new ForgottenPasswordPage(Sandbox);
            }
        }
        
        public CreatePage Create
        {
            get
            {
                BrowserSession.ClickLink("Create an account");
                return new CreatePage(Sandbox);
            } 
        }

        protected void FillInLoginDetailsAndSubmit(string email, string password)
        {
            BrowserSession.FillIn("Email").With(email);
            BrowserSession.FillIn("Password").With(password);

            BrowserSession.ClickButton("Log in");
        }

        protected override IndexPage This
        {
            get { return this; }
        }
    }
}
