﻿namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using System;
    using System.Globalization;
    using Coypu;
    using Environment;

    public class AsyncTrackerItemComponent : ScopedWebContent
    {
        public AsyncTrackerItemComponent(Sandbox sandbox, ElementScope scope)
            : base(sandbox, scope)
        {
            Timestamp = UInt64.Parse(Scope.FindXPath("td[1]").Text, CultureInfo.InvariantCulture);
            Key = Scope.FindXPath("td[2]").Text;
            Value = Scope.FindXPath("td[3]").Text;
        }

        public UInt64 Timestamp { get; set; }

        public string Key { get; private set; }

        public string Value { get; private set; }

    
    }
}
