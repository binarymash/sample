﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.User
{
    using BinaryMash.Testing.Environment;
    using Coypu;

    public class SummaryPage : AsyncWebPage<SummaryPage>
    {
        public SummaryPage(Sandbox sandbox) : base(sandbox)
        {
        }

        public ChangeDetailsPage ChangeDetails
        {
            get
            {
                BrowserSession.ClickLink(ChangeDetailsText);
                WaitForPageWithTitle("Edit User");
                return new ChangeDetailsPage(Sandbox).WaitForContentToLoad();
            }
        }

        public ChangeEmailPage ChangeEmail
        {
            get 
            { 
                BrowserSession.ClickLink(ChangeEmailText);
                WaitForPageWithTitle("Change Email");
                return new ChangeEmailPage(Sandbox).WaitForContentToLoad();
            }
        }

        public ChangePasswordPage ChangePassword
        {
            get
            {
                BrowserSession.ClickLink(ChangePasswordText);
                WaitForPageWithTitle("Change Password");
                return new ChangePasswordPage(Sandbox).WaitForMetadataToLoad();
            }
        }

        public string Name
        {
            get { return NameElement.Text; }
        }

        public string DisplayName
        {
            get { return DisplayNameElement.Text; }
        }

        public string Email
        {
            get { return EmailElement.Text; }
        }

        public string Password
        {
            get { return PasswordElement.Text; }
        }

        protected override SummaryPage This
        {
            get { return this; }
        }

        protected const string ChangeDetailsText = "Change details"; 
        protected const string ChangeEmailText = "Change email";
        protected const string ChangePasswordText = "Change password";

        protected ElementScope EmailElement
        {
            get { return BrowserSession.FindCss(".f-email"); }
        }

        protected ElementScope PasswordElement
        {
            get { return BrowserSession.FindCss(".f-password"); }
        }

        protected ElementScope NameElement
        {
            get { return BrowserSession.FindCss(".f-name"); }
        }

        protected ElementScope DisplayNameElement
        {
            get { return BrowserSession.FindCss(".f-displayname"); }
        }
    
    }
}
