﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.User
{
    using BinaryMash.Testing.Environment;

    public class ChangeEmailPage : AsyncWebPage<ChangeEmailPage>
    {
        public ChangeEmailPage(Sandbox sandbox) : base(sandbox)
        {
        }

        protected override ChangeEmailPage This
        {
            get { return this; }
        }

        public ChangeEmailPage SetEmail(string email)
        {
            BrowserSession.FillIn("Email").With(email);
            return this;
        }

        public SummaryPage Cancel
        {
            get
            {
                BrowserSession.ClickButton(CancelText);
                WaitForPageWithTitle("User Summary");
                return new SummaryPage(Sandbox).WaitForContentToLoad();                
            }
        }

        public SummaryPage Done
        {
            get
            {
                BrowserSession.ClickButton(DoneText);
                WaitForPageWithTitle("User Summary");
                return new SummaryPage(Sandbox).WaitForContentToLoad();
            }
        }

        public ChangeEmailPage DoneExpectingErrors
        {
            get
            {
                BrowserSession.ClickButton(DoneText);
                AsyncTracker.WaitForAndRemove("SaveFailed");
                return this;
            }
        }

        public bool UserHasBeenNotifiedThatSavingWasUnsuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("ErrorNotification", "There are validation errors - please fix these. Your changes have NOT been saved"); }
        }

        protected const string DoneText = "Done";
        protected const string CancelText = "Cancel";

    }
}
