﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.User
{
    using BinaryMash.Testing.Environment;

    public class HomePage : WebPage<HomePage>
    {
        public HomePage(Sandbox sandbox) : base(sandbox)
        {
        }

        public string WelcomeMessage
        {
            get
            {
                return string.Empty;
            }
        }

        protected override HomePage This
        {
            get { return this; }
        }
    }
}
