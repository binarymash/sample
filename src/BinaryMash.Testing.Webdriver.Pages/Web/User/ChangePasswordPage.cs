﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.User
{
    using System.Globalization;
    using BinaryMash.Testing.Environment;
    using Coypu;
    using Models;

    public class ChangePasswordPage : AsyncWebPage<ChangePasswordPage>
    {
        public ChangePasswordPage(Sandbox sandbox) : base(sandbox)
        {
        }

        public ChangePasswordPage SetData(ChangeUserPasswordPageModel model)
        {
            if (model.CurrentPassword != null)
            {
                CurrentPasswordElement.FillInWith(model.CurrentPassword);
            }

            if (model.NewPassword != null)
            {
                NewPasswordElement.FillInWith(model.NewPassword);
            }

            if (model.NewPasswordConfirmation != null)
            {
                NewPasswordConfirmationElement.FillInWith(model.NewPasswordConfirmation);
            }

            return this;
        }

        public SummaryPage Cancel
        {
            get
            {
                BrowserSession.ClickButton(CancelText);
                WaitForPageWithTitle("User Summary");
                return new SummaryPage(Sandbox).WaitForContentToLoad();
            }
        }

        public SummaryPage Done
        {
            get
            {
                BrowserSession.ClickButton(DoneText);
                WaitForPageWithTitle("User Summary");
                return new SummaryPage(Sandbox).WaitForContentToLoad();
            }
        }

        public ChangePasswordPage DoneExpectingErrors
        {
            get
            {
                BrowserSession.ClickButton(DoneText);
                AsyncTracker.WaitForAndRemove("SaveFailed");
                return this;
            }
        }

        public bool UserHasBeenNotifiedThatSavingWasUnsuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("ErrorNotification", "An error occurred when changing the password. Your changes have NOT been saved"); }
        }

        public bool UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved
        {
            get { return AsyncTracker.WaitForAndRemove("ErrorNotification", "There are validation errors - please fix these. Your changes have NOT been saved"); }
        }

        protected override ChangePasswordPage This
        {
            get { return this; }
        }

        protected const string DoneText = "Done";
        protected const string CancelText = "Cancel";

        protected ElementScope CurrentPasswordElement { get { return BrowserSession.FindCss("#Password"); } }
        protected ElementScope NewPasswordElement { get { return BrowserSession.FindCss("#NewPassword"); } }
        protected ElementScope NewPasswordConfirmationElement { get { return BrowserSession.FindCss("#NewPasswordConfirmation"); } }
    }
}
