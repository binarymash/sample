﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.User
{
    using BinaryMash.Testing.Environment;
    using Coypu;
    using Models;

    public class ChangeDetailsPage : AsyncWebPage<ChangeDetailsPage>
    {
        public ChangeDetailsPage(Sandbox sandbox) : base(sandbox)
        {
        }

        public ChangeDetailsPage SetData(ChangeUserDetailsPageModel model)
        {
            if (model.FirstName != null)
            {
                FirstNameElement.FillInWith(model.FirstName);
            }

            if (model.LastName != null)
            {
                LastNameElement.FillInWith(model.LastName);
            }

            if (model.DisplayName != null)
            {
                DisplayNameElement.FillInWith(model.DisplayName);
            }

            return this;
        }

        public SummaryPage Cancel
        {
            get
            {
                BrowserSession.ClickButton(CancelText);
                WaitForPageWithTitle("User Summary");
                return new SummaryPage(Sandbox).WaitForContentToLoad();                
            }
        }

        public SummaryPage Done
        {
            get
            {
                BrowserSession.ClickButton(DoneText);
                WaitForPageWithTitle("User Summary");
                return new SummaryPage(Sandbox).WaitForContentToLoad();
            }
        }

        public ChangeDetailsPage DoneExpectingErrors
        {
            get
            {
                BrowserSession.ClickButton(DoneText);
                AsyncTracker.WaitForAndRemove("SaveFailed");
                return this;
            }
        }

        public bool UserHasBeenNotifiedThatSavingWasUnsuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("ErrorNotification", "There are validation errors - please fix these. Your changes have NOT been saved"); }
        }

        protected override ChangeDetailsPage This
        {
            get { return this; }
        }

        protected const string DoneText = "Done";
        protected const string CancelText = "Cancel";

        protected ElementScope FirstNameElement
        {
            get { return BrowserSession.FindCss("#FirstName"); }
        }

        protected ElementScope LastNameElement
        {
            get { return BrowserSession.FindCss("#LastName"); }
        }

        protected ElementScope DisplayNameElement
        {
            get { return BrowserSession.FindCss("#DisplayName"); }
        }

    }
}
