﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Candidate
{
    using BinaryMash.Testing.Environment;
    using Coypu;

    public class WelcomePage : WebPage<WelcomePage>
    {
        public WelcomePage(Sandbox sandbox) : base(sandbox)
        {}

        public string Message
        {
            get { return MessageElement.Text; }
        }

        protected override WelcomePage This
        {
            get { return this; }
        }

        protected ElementScope MessageElement
        {
            get { return BrowserSession.FindCss("#summary"); }
        }
    }
}
