﻿namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Coypu;
    using Environment;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class AsyncTrackerComponent : ScopedWebContent
    {
        public AsyncTrackerComponent(Sandbox sandbox)
            : base(sandbox, null)
        {
            Show();
        }

        #region visibility

        public void Show()
        {
            for (var i = 0; i < 50; i++)
            {
                if(string.Equals("false", Sandbox.Browser.Session.ExecuteScript("return window.document.asyncTracker === undefined;"), StringComparison.OrdinalIgnoreCase))
                {
                    Sandbox.Browser.Session.ExecuteScript("window.document.asyncTracker.isVisible(true);");

                    var wait = new WebDriverWait((IWebDriver)Sandbox.Browser.Session.Native, new TimeSpan(0, 0, 10));
                    wait.Until(x => x.FindElements(By.CssSelector("#asyncTracker")).Any());                    
                    Scope = Sandbox.Browser.Session.FindCss("#asyncTracker");
                    return;
                }
                System.Threading.Thread.Sleep(100);
            }

            throw new Exception("Unable to make the async tracker visible");
        }

        public void Hide()
        {
            for (var i = 0; i < 10; i++)
            {
                if (string.Equals("false", Sandbox.Browser.Session.ExecuteScript("return window.document.asyncTracker === undefined;"), StringComparison.OrdinalIgnoreCase))
                {
                    Sandbox.Browser.Session.ExecuteScript("window.document.asyncTracker.isVisible(false);");
                    Scope = null;
                    return;
                }
                System.Threading.Thread.Sleep(100);
            }

            throw new Exception("Unable to make the async tracker invisible");
        }

        #endregion

        #region items

        public ICollection<AsyncTrackerItemComponent> SnapshotItems
        {
            get
            {
                var items = new Collection<AsyncTrackerItemComponent>();

                if (Scope != null)
                {
                    var elements = Scope.FindAllCss("tbody tr");
                    foreach (var element in elements)
                    {
                        items.Add(new AsyncTrackerItemComponent(Sandbox, element));
                    }
                }

                return items;
            }
        }

        public AsyncTrackerItemComponent WaitForSnapshotItem(string key, string value, int maxIterations = 10)
        {
            for (int iteration = 0; iteration < maxIterations; iteration++)
            {
                var item = SnapshotItems.FirstOrDefault(i => (i.Key == key && i.Value == (value ?? string.Empty)));
                if (item != null)
                {
                    return item;
                }
                System.Threading.Thread.Sleep(500);
            }

            throw new Exception(string.Format(CultureInfo.InvariantCulture, "Timed out waiting for async tracker item with key {0} and value {1}", key, value));
        }

        public bool WaitForAndRemove(string key, string value = null, int maxIterations = 10)
        {
            var item = WaitForSnapshotItem(key, value, maxIterations);
            return Remove(item);
        }

        public bool Remove(AsyncTrackerItemComponent item)
        {
            var result = Sandbox.Browser.Session.ExecuteScript(String.Format(CultureInfo.InvariantCulture, "return window.document.asyncTracker.remove({0},'{1}','{2}');", item.Timestamp, item.Key, item.Value));
            return (String.Equals(result, "true", StringComparison.OrdinalIgnoreCase));
        }

        public void RemoveAll()
        {
            Sandbox.Browser.Session.ExecuteScript("window.document.asyncTracker.removeAll();");
        }

        #endregion

        #region Elements

        public ElementScope Tracker
        {
            get { return Sandbox.Browser.Session.FindCss("#asyncTracker", new Options{Timeout = new TimeSpan(0,0,0,10)}); }
        }

        #endregion

    }
}
