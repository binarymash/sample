﻿namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BinaryMash.Testing.Environment;
    using Home;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public abstract class WebPage<T> : WebContent<T>
    {
        protected WebPage (Sandbox sandbox) : base(sandbox)
        {
        }

        public List<string> ErrorSummary
        {
            get
            {
                return Sandbox.Browser.Session
                    .FindAllCss(".validation-summary-errors")
                    .Select(element => element.Text)
                    .ToList();
            }
        }

        public List<string> ErrorMessages
        {
            get
            {
                var elements = Sandbox.Browser.Session.FindAllCss(".field-validation-error");
                var errors = elements.Select(element => element.Text).ToList();
                return errors;
            }
        }

        public void WaitForErrorsWithoutPageLoad()
        {
            var wait = new WebDriverWait((IWebDriver) BrowserSession.Native, new TimeSpan(0, 0, 10));
            wait.Until(x => x.FindElements(By.CssSelector(".field-validation-error")).Any());
        }

        public void WaitForPageWithTitle(string title)
        {
            var wait = new WebDriverWait((IWebDriver)BrowserSession.Native, new TimeSpan(0,0,0,10));
            wait.Until(x => x.Title == title);
        }

        public void WaitForPageWithTitleStartingWith(string title)
        {
            var wait = new WebDriverWait((IWebDriver)BrowserSession.Native, new TimeSpan(0, 0, 0, 10));
            wait.Until(x => x.Title.StartsWith(title));
        }
        
        public MenuPage Menu
        {
            get { return new MenuPage(Sandbox); }
        }

        public IndexPage LogOut()
        {
            return Menu.LogOut();
        }

        public bool ContainsContent(string content)
        {
            return BrowserSession.HasContent(content);
        }


    }
}
