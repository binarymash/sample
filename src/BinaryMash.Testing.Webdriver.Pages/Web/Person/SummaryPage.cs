﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Person
{
    using BinaryMash.Testing.Environment;
    using Coypu;

    public class SummaryPage : AsyncWebPage<SummaryPage>
    {

        public SummaryPage(Sandbox sandbox) : base(sandbox)
        {
        }

        protected override SummaryPage This
        {
            get { return this; }
        }

        public string Name
        {
            get
            {
                return NameElement.Text;
            }
        }

        public CreateEditPage EditDetails
        {
            get
            {
                BrowserSession.ClickButton(EditDetailsText);
                return new CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        public Session.CreateEditPage ArrangeATest
        {
            get
            {
                BrowserSession.ClickButton(ArrangeATestText);
                return new Session.CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        protected ElementScope NameElement
        {
            get { return BrowserSession.FindCss(".f-name"); }
        }

        private string EditDetailsText = "Edit details";
        private string ArrangeATestText = "Arrange a test";
    }
}
