﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Person
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Coypu;
    using Domain;

    public class EmailChannelEditComponent : CommunicationChannelEditComponent
    {
        public EmailChannelEditComponent(BrowserSession browserSession, SnapshotElementScope scope) : base(browserSession, scope)
        {
        }

        public EmailType Type
        {
            get
            {
                string typeString = TypeElement.Value;
                EmailType type;
                if (EmailType.TryParse(typeString, true, out type))
                {
                    return type;
                }

                return EmailType.Other;
            }
        }

        public CommunicationChannelEditComponent SetType(EmailType type)
        {
            var typeString = type.ToString();
            return SetType(typeString);
        }

        protected override ElementScope TypeElement
        {
            get { return _scope.FindCss(".emailChannel .f-type"); }
        }

        protected override ElementScope ValueElement
        {
            get { return _scope.FindCss(".emailChannel .f-value"); }
        }

        protected override ElementScope DeleteElement
        {
            get { return _scope.FindCss(".glyphicon-trash"); }
        }

    }
}
