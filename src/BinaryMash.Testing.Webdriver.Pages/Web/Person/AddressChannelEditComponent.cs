﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Person
{
    using System;
    using System.Globalization;
    using Coypu;
    using Domain;

    public class AddressChannelEditComponent : CommunicationChannelEditComponent
    {

        public AddressChannelEditComponent(BrowserSession browserSession, Coypu.SnapshotElementScope scope) : base(browserSession, scope)
        {
        }

        public AddressType Type
        {
            get
            {
                string typeString = TypeElement.Value;
                AddressType type;
                if (AddressType.TryParse(typeString, true, out type))
                {
                    return type;
                }

                return AddressType.Other;
            }
        }

        public CommunicationChannelEditComponent SetType(AddressType type)
        {
            var typeString = type.ToString();
            return SetType(typeString);
        }

        protected override ElementScope TypeElement
        {
            get { return _scope.FindCss(".addressChannel .f-type"); }
        }
        
        protected override ElementScope ValueElement
        {
            get { return _scope.FindCss(".addressChannel .f-value"); }
        }

        protected override ElementScope DeleteElement
        {
            get { return _scope.FindCss(".glyphicon-trash"); }
        }

    }
}
