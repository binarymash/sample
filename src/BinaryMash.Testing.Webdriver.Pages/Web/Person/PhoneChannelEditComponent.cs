﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Person
{
    using System;
    using System.Globalization;
    using Coypu;
    using Domain;

    public class PhoneChannelEditComponent : CommunicationChannelEditComponent
    {

        public PhoneChannelEditComponent(BrowserSession browserSession, SnapshotElementScope scope) : base(browserSession, scope)
        {
        }

        public PhoneType Type
        {
            get
            {
                string typeString = TypeElement.Value;
                PhoneType type;
                if (PhoneType.TryParse(typeString, true, out type))
                {
                    return type;
                }

                return PhoneType.Other;
            }
        }

        public CommunicationChannelEditComponent SetType(PhoneType type)
        {
            var typeString = type.ToString();
            return SetType(typeString);
        }

        protected override ElementScope TypeElement
        {
            get { return _scope.FindCss(".phoneChannel .f-type"); }
        }

        protected override ElementScope ValueElement
        {
            get { return _scope.FindCss(".phoneChannel .f-value"); }
        }

        protected override ElementScope DeleteElement
        {
            get { return _scope.FindCss(".glyphicon-trash"); }
        }
    }
}
