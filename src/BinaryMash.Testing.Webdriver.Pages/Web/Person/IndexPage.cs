﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Person
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BinaryMash.Testing.Environment;

    public class IndexPage : WebPage<IndexPage>
    {
        public IndexPage(Sandbox sandbox) : base(sandbox)
        {}

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        public int NumberOfPeople
        {
            get
            {
                var quantity = BrowserSession.FindCss("#quantity").Text;
                if (string.Equals(quantity, "no", StringComparison.OrdinalIgnoreCase))
                {
                    return 0;
                }

                return Convert.ToInt32(quantity);
            }
        }

        public CreateEditPage AddANewPerson
        {
            get
            {
                BrowserSession.ClickLink("Add a new person");
                return new CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        public IList<PersonSummaryComponent> People
        {
            get
            {
                var summaryElements = BrowserSession.FindAllCss(".personSummary");
                return summaryElements.Select(element => new PersonSummaryComponent(Sandbox, element)).ToList();
            }
        }

        protected override IndexPage This
        {
            get { return this; }
        }
    }
}
