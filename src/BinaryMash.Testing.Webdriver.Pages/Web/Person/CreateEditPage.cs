﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Person
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Coypu;
    using BinaryMash.Testing.Environment;
    using Question;

    public class CreateEditPage : AsyncWebPage<CreateEditPage>
    {

        public CreateEditPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        public int PersonId
        {
            get
            {
                var options = new Options
                {
                    ConsiderInvisibleElements = true
                };
                var element = BrowserSession.FindCss("#personId", options);
                return !element.Exists() ? 0 : Int32.Parse(element.Value, CultureInfo.InvariantCulture);
            }
        }

        public SummaryPage CancelExpectingSummary()
        {
            BrowserSession.ClickButton("Cancel");
            WaitForPageWithTitle("Person Summary");
            return new SummaryPage(Sandbox);
        }
        
        public IndexPage CancelExpectingIndex()
        {
            BrowserSession.ClickButton("Cancel");
            WaitForPageWithTitle("People");
            return new IndexPage(Sandbox);
        }

        public SummaryPage Done()
        {
            BrowserSession.ClickButton("Done");
            WaitForPageWithTitle("Person Summary");
            return new SummaryPage(Sandbox);
        }

        public CreateEditPage Save()
        {
            BrowserSession.ClickButton("Save");
            if (!AsyncTracker.WaitForAndRemove("SaveSucceeded"))
            {
                throw new Exception("Async status failed");
            }
            RemoveAllToasts();
            return this;
        }

        public CreateEditPage SaveExpectingError
        {
            get
            {
                BrowserSession.ClickButton("Save");
                if (!AsyncTracker.WaitForAndRemove("SaveFailed"))
                {
                    throw new Exception("Async status failed");
                }
                RemoveAllToasts(); 
                return this;
            }
        }

        public string Name
        {
            get { return BrowserSession.FindId("Name").Value; }
        }

        public string Notes
        {
            get { return BrowserSession.FindId("Notes").Value; }
        }

        public List<CommunicationChannelEditComponent> CommunicationChannels 
        { 
            get
            {
                var channels = new List<CommunicationChannelEditComponent>();

                EmailChannels.ForEach(channels.Add);
                PhoneChannels.ForEach(channels.Add);
                AddressChannels.ForEach(channels.Add);

                return channels;
            }
        }

        public List<EmailChannelEditComponent> EmailChannels
        {
            get
            {
                var emailChannelElements = BrowserSession.FindAllCss(".emailChannel");
                return emailChannelElements.Select(channel => new EmailChannelEditComponent(BrowserSession, channel)).ToList();
            }
        }

        public List<PhoneChannelEditComponent> PhoneChannels
        {
            get
            {
                var phoneChannelElements = BrowserSession.FindAllCss(".phoneChannel");
                return phoneChannelElements.Select(channel => new PhoneChannelEditComponent(BrowserSession, channel)).ToList();
            }
        }

        public List<AddressChannelEditComponent> AddressChannels
        {
            get
            {
                var addressChannelElements = BrowserSession.FindAllCss(".addressChannel");
                return addressChannelElements.Select(channel => new AddressChannelEditComponent(BrowserSession, channel)).ToList();
            }
        }

        public CreateEditPage SetName(string name)
        {
            BrowserSession.FillIn("Name").With(name);
            return this;
        }

        public CreateEditPage SetNotes(string notes)
        {
            BrowserSession.FillIn("Notes").With(notes);
            return this;
        }

        public CreateEditPage AddEmail()
        {
            BrowserSession.ClickButton("Add email");
            //TODO: wait for email to be added
            return this;
        }

        public CreateEditPage AddPhone()
        {
            BrowserSession.ClickButton("Add phone");
            //TODO: wait for phone to be added
            return this;
        }

        public CreateEditPage AddAddress()
        {
            BrowserSession.ClickButton("Add address");
            //TODO: wait for address to be added
            return this;
        }

        public bool UserHasBeenNotifiedThatTheChangesHaveBeenCancelled
        {
            get { return AsyncTracker.WaitForAndRemove("SuccessNotification", "All unsaved changes have been reverted"); }
        }

        public bool UserHasBeenNotifiedThatSavingWasSuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("SuccessNotification", string.Format(CultureInfo.InvariantCulture, "Saved the changes to {0}", this.Name)); }
        }

        public bool UserHasBeenNotifiedThatSavingWasUnsuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("ErrorNotification", "There are validation errors - please fix these. Your changes have NOT been saved"); }
        }

        protected override CreateEditPage This
        {
            get { return this; }
        }
    }
}
