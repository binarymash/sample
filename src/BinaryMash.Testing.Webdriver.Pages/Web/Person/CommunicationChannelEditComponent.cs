﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Person
{
    using System.Collections.Generic;
    using System.Linq;
    using Coypu;
    using OpenQA.Selenium;

    public abstract class CommunicationChannelEditComponent
    {
        protected readonly BrowserSession _browserSession;
        protected readonly Coypu.SnapshotElementScope _scope;

        protected CommunicationChannelEditComponent(BrowserSession browserSession, SnapshotElementScope scope)
        {
            _browserSession = browserSession;
            _scope = scope;
        }

        public string Value
        {
            get { return ValueElement.Value; }
        }

        public string TypeAsString
        {
            get { return TypeElement.Text; }
        }

        public CommunicationChannelEditComponent SetType(string value)
        {
            TypeElement.Click();
            TypeInputElement.FillInWith(value);
            TypeInputElement.SendKeys(Keys.Enter);
            return this;
        }

        public CommunicationChannelEditComponent SetValue(string value)
        {
            ValueElement.FillInWith(value);
            return this;
        }

        public void Remove()
        {
            DeleteElement.Click();
        }

        public ICollection<string> TypeOptions
        {
            get 
            {
                TypeElement.Click();
                var elements = TypeInputOptionElements;
                var options = elements.Select(element => element.Text).ToList();
                SelectedTypeInputOptionElement.Click();
                return options;
            }
        }

        public string SelectedTypeOption
        {
            get
            {
                TypeElement.Click();
                var option = SelectedTypeInputOptionElement.Text;
                SelectedTypeInputOptionElement.Click(); 
                return option;
            }
        }

        #region elements

        protected abstract ElementScope TypeElement { get; }

        protected abstract ElementScope ValueElement { get; }

        protected abstract ElementScope DeleteElement { get; }

        protected ElementScope TypeInputElement
        {
            get { return _browserSession.FindCss(".select2-input"); }
        }

        protected IEnumerable<ElementScope> TypeInputOptionElements
        {
            get { return _browserSession.FindAllCss(".select2-result"); }
        }

        protected ElementScope SelectedTypeInputOptionElement
        {
            get { return _browserSession.FindCss(".select2-result.select2-highlighted"); }
        }

        #endregion

    }
}
