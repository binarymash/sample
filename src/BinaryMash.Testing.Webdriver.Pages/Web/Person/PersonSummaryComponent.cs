﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Person
{
    using BinaryMash.Testing.Environment;
    using Coypu;
    using Question;

    public class PersonSummaryComponent
    {
        private readonly Sandbox _sandbox;
        private readonly Coypu.SnapshotElementScope _scope;

        public PersonSummaryComponent(Sandbox sandbox, Coypu.SnapshotElementScope scope)
        {
            _sandbox = sandbox;
            _scope = scope;
        }

        public string Name 
        { 
            get
            {
                return NameElement.Text;
            }
        }

        public SummaryPage ViewSummary
        {
            get
            {
                NameElement.Click();
                return new SummaryPage(_sandbox).WaitForContentToLoad();
            }
        }

        public string Updated
        {
            get
            {
                return UpdatedElement.Text;
            }
        }

        public string UpdatedBy
        {
            get
            {
                return UpdatedByElement.Text;                
            }
        }
    
        private ElementScope NameElement
        {
            get { return _scope.FindCss("h5 a"); }
        }

        private ElementScope UpdatedElement
        {
            get { return _scope.FindCss(".updated"); }
        }

        private ElementScope UpdatedByElement
        {
            get { return _scope.FindCss(".updatedBy"); }
        }

    }
}
