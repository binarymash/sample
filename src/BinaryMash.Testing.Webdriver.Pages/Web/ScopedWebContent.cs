﻿namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using Coypu;
    using BinaryMash.Testing.Environment;

    public abstract class ScopedWebContent : Content
    {
        protected ElementScope Scope { get; set; }

        protected ScopedWebContent (Sandbox sandbox, ElementScope scope) : base(sandbox)
        {
            Scope = scope;
        }

    }
}
