﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Account
{
    using System;
    using System.Linq;
    using Authentication;
    using BinaryMash.Testing.Environment;
    using Coypu;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class ActivatePage : AsyncWebPage<ActivatePage>
    {
        public ActivatePage(Sandbox sandbox) : base(sandbox)
        {
        }

        public string ActivatingMessage
        {
            get { return ActivatingMessageElement.Text; }
        }

        public string ActivatedMessage
        {
            get { return ActivatedMessageElement.Text; }
        }

        public ActivatePage WaitForActivation()
        {
            AsyncTracker.WaitForAndRemove("Activated", null, 70);
            //var wait = new WebDriverWait((IWebDriver)BrowserSession.Native, new TimeSpan(0, 0, 3000));
            //wait.Until(x => x.FindElements(By.Id("authenticate")).Any());
            return this;
        }

        public IndexPage LogIn()
        {
            LogInButton.Click();
            return new IndexPage(Sandbox);
        }

        protected override ActivatePage This
        {
            get { return this; }
        }

        protected ElementScope LogInButton
        {
            get { return BrowserSession.FindCss("#authenticate"); }
        }

        protected ElementScope ActivatingMessageElement
        {
            get { return BrowserSession.FindCss("#activating"); }
        }

        protected ElementScope ActivatedMessageElement
        {
            get { return BrowserSession.FindCss("#activated"); }            
        }
    }
}
