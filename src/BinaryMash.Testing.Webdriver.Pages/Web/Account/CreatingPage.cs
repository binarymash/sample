﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Account
{
    using BinaryMash.Testing.Webdriver.Pages.Emails;
    using BinaryMash.Testing.Environment;

    public class CreatingPage : WebPage<CreatingPage>
    {
        public CreatingPage(Sandbox sandbox) : base(sandbox)
        {
        }

        public string Message
        {
            get
            {
                //System.Diagnostics.Trace.Write(BrowserSession);
                var element = BrowserSession.FindId("message");
                return element.Text;
            }
        }

        public ValidateUserEmail ReadValidateUserEmail
        {
            get
            {
                return new ValidateUserEmail(Sandbox, Sandbox.Email.GetFirstEmail());
            }
        }

        protected override CreatingPage This
        {
            get { return this; }
        }
    }
}
