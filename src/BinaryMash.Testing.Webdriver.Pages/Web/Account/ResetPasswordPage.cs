﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Account
{
    using BinaryMash.Testing.Environment;
    using BinaryMash.Testing.Webdriver.Pages.Models;

    public class ResetPasswordPage : WebPage<ResetPasswordPage>
    {
        public ResetPasswordPage(Sandbox sandbox) : base(sandbox)
        {            
        }

        public ConfirmationOfPasswordResetPage Reset(ResetPasswordPageModel model)
        {
            FillInFieldsAndReset(model);
            WaitForPageWithTitle("Password Reset");
            return new ConfirmationOfPasswordResetPage(Sandbox);
        }

        public ResetPasswordPage ResetExpectingErrors(ResetPasswordPageModel model)
        {
            FillInFieldsAndReset(model);
            WaitForErrorsWithoutPageLoad();
            return this;
        }

        protected void FillInFieldsAndReset(ResetPasswordPageModel model)
        {
            if (model.Email != null)
            {
                BrowserSession.FillIn("Email").With(model.Email);                
            }

            if (model.Password != null)
            {
                BrowserSession.FillIn("Password").With(model.Password);
            }

            if (model.PasswordConfirmation != null)
            {
                BrowserSession.FillIn("PasswordConfirmation").With(model.PasswordConfirmation);
            }

            BrowserSession.ClickButton("Reset my password");
        }

        protected override ResetPasswordPage This
        {
            get { return this; }
        }
    }
}
