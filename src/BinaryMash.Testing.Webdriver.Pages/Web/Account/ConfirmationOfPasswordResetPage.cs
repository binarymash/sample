﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Account
{
    using Authentication;
    using BinaryMash.Testing.Environment;
    using BinaryMash.Testing.Webdriver.Pages.Models;

    public class ConfirmationOfPasswordResetPage : WebPage<ConfirmationOfPasswordResetPage>
    {
        public ConfirmationOfPasswordResetPage(Sandbox sandbox)
            : base(sandbox)
        {            
        }

        public string Message
        {
            get
            {
                var element = BrowserSession.FindId("message");
                return element.Text;
            }
        }

        public IndexPage LogIn
        {
            get 
            { 
                BrowserSession.ClickLink("Log in");
                return new IndexPage(Sandbox);
            }    
        }

        protected override ConfirmationOfPasswordResetPage This
        {
            get { return this; }
        }
    }
}
