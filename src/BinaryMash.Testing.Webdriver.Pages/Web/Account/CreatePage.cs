﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Account
{
    using BinaryMash.Testing.Environment;
    using BinaryMash.Testing.Webdriver.Pages.Models;

    public class CreatePage : WebPage<CreatePage>
    {
        public CreatePage(Sandbox sandbox) : base(sandbox)
        {            
        }

        public CreatingPage Create(CreateAccountPageModel model)
        {
            FillInFieldsAndSave(model);
            WaitForPageWithTitle("Creating account");
            return new CreatingPage(Sandbox);
        }

        public CreatePage CreateExpectingErrors(CreateAccountPageModel model)
        {
            FillInFieldsAndSave(model);
            WaitForErrorsWithoutPageLoad();
            return this;
        }

        protected void FillInFieldsAndSave(CreateAccountPageModel model)
        {
            if (model.FirstName != null)
            {
                BrowserSession.FillIn("FirstName").With(model.FirstName);
            }

            if (model.LastName != null)
            {
                BrowserSession.FillIn("LastName").With(model.LastName);
            }

            if (model.Email != null)
            {
                BrowserSession.FillIn("Email").With(model.Email);
            }

            if (model.Password != null)
            {
                BrowserSession.FillIn("Password").With(model.Password);
            }

            if (model.PasswordConfirmation != null)
            {
                BrowserSession.FillIn("PasswordConfirmation").With(model.PasswordConfirmation);
            }

            if (model.AcceptTermsAndConditions.HasValue)
            {
                if (model.AcceptTermsAndConditions.Value)
                {
                    BrowserSession.Check("TermsAccepted");
                }
            }
            BrowserSession.ClickButton("Create my account");
        }

        protected override CreatePage This
        {
            get { return this; }
        }
    }
}
