﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Account
{
    using BinaryMash.Testing.Environment;
    using BinaryMash.Testing.Webdriver.Pages.Models;

    public class ForgottenPasswordPage : WebPage<ForgottenPasswordPage>
    {
        public ForgottenPasswordPage(Sandbox sandbox) : base(sandbox)
        {            
        }

        public ResettingPasswordPage Reset(string email)
        {
            FillInFieldsAndReset(email);
            WaitForPageWithTitle("Resetting Password");
            return new ResettingPasswordPage(Sandbox);
        }

        public ForgottenPasswordPage ResetExpectingErrors(string email)
        {
            FillInFieldsAndReset(email);
            WaitForErrorsWithoutPageLoad();
            return this;
        }

        protected void FillInFieldsAndReset(string email)
        {
            BrowserSession.FillIn("Email").With(email ?? string.Empty);
            BrowserSession.ClickButton("Reset my password");
        }

        protected override ForgottenPasswordPage This
        {
            get { return this; }
        }
    }
}
