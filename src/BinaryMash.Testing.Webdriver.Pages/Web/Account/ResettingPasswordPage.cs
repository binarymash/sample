﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Account
{
    using BinaryMash.Testing.Webdriver.Pages.Emails;
    using BinaryMash.Testing.Environment;

    public class ResettingPasswordPage : WebPage<ResettingPasswordPage>
    {
        public ResettingPasswordPage(Sandbox sandbox) : base(sandbox)
        {
        }

        public string Message
        {
            get
            {
                var element = BrowserSession.FindId("message");
                return element.Text;
            }
        }

        public ResetPasswordEmail ReadResetPasswordEmail
        {
            get
            {
                return new ResetPasswordEmail(Sandbox, Sandbox.Email.GetFirstEmail(EmailSubject));
            }
        }

        protected override ResettingPasswordPage This
        {
            get { return this; }
        }

        private const string EmailSubject = "Password Reset";
    }
}
