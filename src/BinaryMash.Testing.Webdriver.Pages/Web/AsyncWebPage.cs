﻿namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using System;
    using BinaryMash.Testing.Environment;

    public abstract class AsyncWebPage<T> : WebPage<T>
    {
        protected AsyncWebPage (Sandbox sandbox) : base(sandbox)
        {
        }

        public AsyncTrackerComponent AsyncTracker
        {
            get
            {
                return new AsyncTrackerComponent(Sandbox);
            }
        }

        public T WaitForMetadataToLoad()
        {
            if (!AsyncTracker.WaitForAndRemove("MetadataLoaded"))
            {
                throw new Exception("Async status failed");
            }
            return This;
        }

        public T WaitForContentToLoad()
        {
            if (!AsyncTracker.WaitForAndRemove("ContentLoaded"))
            {
                throw new Exception("Async status failed");
            }
            return This;
        }

        public T WaitForContentLoadToFail()
        {
            if (!AsyncTracker.WaitForAndRemove("ContentLoadedFailed"))
            {
                throw new Exception("Async status failed");
            }
            return This;
        }

    }
}
