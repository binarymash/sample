﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Home
{
    using Account;
    using BinaryMash.Testing.Environment;

    public class IndexPage : WebPage<IndexPage>
    {
        public IndexPage(Sandbox sandbox) : base(sandbox)
        {
            BrowserSession.Visit("");
        }

        public CreatePage SignUp()
        {
            BrowserSession.ClickLink("Sign up");
            return new CreatePage(Sandbox);
        }

        public Authentication.IndexPage LogIn()
        {
            BrowserSession.ClickLink("Log in");
            return new Authentication.IndexPage(Sandbox);
        }

        protected override IndexPage This
        {
            get { return this; }
        }
    }
}
