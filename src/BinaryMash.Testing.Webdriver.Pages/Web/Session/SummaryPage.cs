﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Session
{
    using System;
    using System.Linq;
    using BinaryMash.Testing.Environment;
    using Coypu;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class SummaryPage : AsyncWebPage<SummaryPage>
    {

        public SummaryPage(Sandbox sandbox) : base(sandbox)
        {
        }


        protected override SummaryPage This
        {
            get { return this; }
        }

        public CreateEditPage EditDetails
        {
            get
            {
                BrowserSession.ClickButton(EditDetailsText);
                return new CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        public CreateEditPage ArrangeATest
        {
            get
            {
                BrowserSession.ClickButton(ArrangeATestText);
                return new CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        public TakeTestConfirmationDialog TakeTheTestNowOnThisDevice
        {
            get
            {
                BrowserSession.ClickButton(TakeTheTestNowText);

                return new TakeTestConfirmationDialog(Sandbox, TakeTestConfirmationDialogElement);
            }
        }

        private const string EditDetailsText = "Edit details";
        private const string ArrangeATestText = "Arrange a test";
        private const string TakeTheTestNowText = "Take test now on this device";

        private ElementScope TakeTestConfirmationDialogElement
        {
            get
            {
                var wait = new WebDriverWait((IWebDriver)Sandbox.Browser.Session.Native, new TimeSpan(0, 0, 0, 10));
                wait.Until(x => x.FindElement(By.CssSelector("#takeNowModal")).Displayed); 
                return BrowserSession.FindCss("#takeNowModal");
            }
        }

    }
}
