﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Session
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BinaryMash.Testing.Environment;

    public class IndexPage : WebPage<IndexPage>
    {
        public IndexPage(Sandbox sandbox) : base(sandbox)
        {}

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        public CreateEditPage ScheduleASession
        {
            get
            {
                BrowserSession.ClickLink("Schedule a session");
                return new CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        public UnauthenticatedPage ScheduleASessionExpectingUnauthenticated
        {
            get
            {
                BrowserSession.ClickLink("Schedule a session");
                return new UnauthenticatedPage(Sandbox);
            }
        }

        public int NumberOfSessions
        {
            get
            {
                var quantity = BrowserSession.FindCss("#quantity").Text;
                if (string.Equals(quantity, "no", StringComparison.OrdinalIgnoreCase))
                {
                    return 0;
                }

                return Convert.ToInt32(quantity);
            }
        }

        public IList<SessionSummaryComponent> Sessions
        {
            get
            {
                var summaryElements = BrowserSession.FindAllCss(".sessionSummary");
                return summaryElements.Select(element => new SessionSummaryComponent(Sandbox, element)).ToList();
            }
        }

        protected override IndexPage This
        {
            get { return this; }
        }
    }
}
