﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Session
{
    using Coypu;
    using BinaryMash.Testing.Environment;
    using Test;

    public class TakeTestConfirmationDialog : ScopedWebContent
    {
        public TakeTestConfirmationDialog(Sandbox sandbox, ElementScope scope) : base(sandbox, scope)
        {            
        }

        public SummaryPage DontTakeTheTest()
        {
            Scope.ClickButton(DontTakeTestText);
            var page = new SummaryPage(Sandbox);
            page.WaitForPageWithTitle("Session Summary");
            return page;
        }

        public Candidate.WelcomePage ConfirmToTakeTheTest
        {
            get
            {
                Scope.ClickButton(TakeTestText);
                var page = new Candidate.WelcomePage(Sandbox);
                page.WaitForPageWithTitleStartingWith("Test for ");
                return page;
            }
        }

        protected string TakeTestText { get { return "Yes, take the test"; } }
        protected string DontTakeTestText { get { return "No, don't take the test"; } }

    }
}
