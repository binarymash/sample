﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Session
{
    using BinaryMash.Testing.Environment;
    using Coypu;

    public class SessionSummaryComponent
    {
        private readonly Sandbox _sandbox;
        private readonly Coypu.SnapshotElementScope _scope;

        public SessionSummaryComponent(Sandbox sandbox, Coypu.SnapshotElementScope scope)
        {
            _sandbox = sandbox;
            _scope = scope;
        }

        public string Header 
        { 
            get
            {
                var nameElement = _scope.FindCss("h5 a");
                return nameElement.Text;
            }
        }

        public string State
        {
            get
            {
                var stateElement = _scope.FindCss(".state");
                return stateElement.Text;
            }
        }

        public string Updated
        {
            get
            {
                var updatedElement = _scope.FindCss(".updated");
                return updatedElement.Text;
            }
        }

        public string UpdatedBy
        {
            get
            {
                var updatedByElement = _scope.FindCss(".updatedBy");
                return updatedByElement.Text;                
            }
        }

        public SummaryPage ViewSummary()
        {
            var header = _scope.FindCss("h5");
            var link = header.FindCss("a");
            link.Click();

            var summaryPage = new SummaryPage(_sandbox);
            summaryPage.WaitForPageWithTitle("Test Session Summary");
            return summaryPage.WaitForContentToLoad();            
        }

    }
}
