﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Session
{
    using System;
    using System.Globalization;
    using Coypu;
    using BinaryMash.Testing.Environment;

    public class CreateEditPage : AsyncWebPage<CreateEditPage>
    {

        public CreateEditPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        public int SessionId
        {
            get
            {
                var options = new Options
                {
                    ConsiderInvisibleElements = true
                };
                var element = BrowserSession.FindCss("#sessionId", options);
                return !element.Exists() ? 0 : Int32.Parse(element.Value, CultureInfo.InvariantCulture);
            }
        }

        public void Save()
        {
                BrowserSession.ClickButton("Save");
        }

        public CreateEditPage SaveExpectingError
        {
            get
            {
                BrowserSession.ClickButton("Save");
                if (!AsyncTracker.WaitForAndRemove("SaveFailed"))
                {
                    throw new Exception("Async status failed");
                }
                return this;
            }
        }

        public CreateEditPage RevertChanges()
        {
            BrowserSession.ClickButton("Revert changes");
            //TODO: wait for changes to be cancelled
            return this;
        }

        public bool UserHasBeenNotifiedThatTheChangesHaveBeenCancelled
        {
            get { return AsyncTracker.WaitForAndRemove("SuccessNotification", "All unsaved changes have been reverted"); }
        }

        public bool UserHasBeenNotifiedThatSavingWasSuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("SuccessNotification", string.Format(CultureInfo.InvariantCulture, "Saved the changes to the session")); }
        }

        public bool UserHasBeenNotifiedThatSavingWasUnsuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("ErrorNotification", "There are validation errors - please fix these. Your changes have NOT been saved"); }
        }

        protected override CreateEditPage This
        {
            get { return this; }
        }
    }
}
