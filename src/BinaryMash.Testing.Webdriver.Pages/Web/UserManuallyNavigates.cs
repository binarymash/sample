﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using Authentication;
    using Coypu;
    using Environment;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class UserManuallyNavigates
    {
        private Sandbox _sandbox;

        public UserManuallyNavigates(Sandbox sandbox)
        {
            _sandbox = sandbox;
        }

        public void To(Uri uri)
        {
            _sandbox.Browser.Session.Visit(uri.AbsolutePath);
        }

        public Question.IndexPage ToQuestionsTaggedWith(string tagName)
        {
            _sandbox.Browser.Session.Visit("Question/Tagged/" + tagName);
            return new Question.IndexPage(_sandbox);
        }

        #region Question

        public Question.CreateEditPage ToCreateQuestion
        {
            get
            {
                _sandbox.Browser.Session.Visit("Question/Create");
                var page = new Question.CreateEditPage(_sandbox);
                page.WaitForContentToLoad();
                return page;
            }
        }

        public Question.UnauthenticatedPage ToCreateQuestionExpectingUnauthenticated
        {
            get
            {
                _sandbox.Browser.Session.Visit("Question/Create");
                return new Question.UnauthenticatedPage(_sandbox);
            }
        }

        public Question.CreateEditPage ToEditQuestion(int id)
        {
            return ToEditQuestion(id, true);
        }

        public Question.CreateEditPage ToEditQuestion(int id, bool expectContentLoad)
        {
            _sandbox.Browser.Session.Visit("Question/Edit/" + id);
            var createEditPage = new Question.CreateEditPage(_sandbox);
            if (expectContentLoad)
            {
                return createEditPage.WaitForContentToLoad();
            }
            return createEditPage.WaitForContentLoadToFail();
        }

        public Question.PreviewPage ToPreviewOfQuestion(int id)
        {
            _sandbox.Browser.Session.Visit("Question/Preview/" + id);
            return new Question.PreviewPage(_sandbox);
        }

        public Question.PreviewNotFoundPage ToPreviewOfQuestionExpectingNotFound(int id)
        {
            _sandbox.Browser.Session.Visit("Question/Preview/" + id);
            return new Question.PreviewNotFoundPage(_sandbox);
        }

        #endregion

        #region Person

        public Person.CreateEditPage ToCreatePerson
        {
            get
            {
                _sandbox.Browser.Session.Visit("Person/Create");
                var page = new Person.CreateEditPage(_sandbox);
                page.WaitForContentToLoad();
                return page;
            }
        }

        public Person.UnauthenticatedPage ToCreatePersonExpectingUnauthenticated
        {
            get
            {
                _sandbox.Browser.Session.Visit("Person/Create");
                return new Person.UnauthenticatedPage(_sandbox);
            }
        }

        public Person.CreateEditPage ToEditPerson(int id)
        {
            return ToEditPerson(id, true);
        }

        public Person.CreateEditPage ToEditPerson(int id, bool expectContentLoad)
        {
            _sandbox.Browser.Session.Visit("Person/Edit/" + id);
            var createEditPage = new Person.CreateEditPage(_sandbox);
            if (expectContentLoad)
            {
                return createEditPage.WaitForContentToLoad();
            }
            return createEditPage.WaitForContentLoadToFail();

        }

        #endregion

        #region Tests

        public Test.CreateEditPage ToCreateTest
        {
            get
            {
                _sandbox.Browser.Session.Visit("Test/Create");
                var page = new Test.CreateEditPage(_sandbox);
                page.WaitForContentToLoad();
                return page;
            }
        }

        public Test.UnauthenticatedPage ToCreateTestExpectingUnauthenticated
        {
            get
            {
                _sandbox.Browser.Session.Visit("Test/Create");
                return new Test.UnauthenticatedPage(_sandbox);
            }
        }

        public Test.CreateEditPage ToEditTest(int id)
        {
            return ToEditTest(id, true);
        }

        public Test.CreateEditPage ToEditTest(int id, bool expectContentLoad)
        {
            _sandbox.Browser.Session.Visit("Test/Edit/" + id);
            var createEditPage =  new Test.CreateEditPage(_sandbox);
            if (expectContentLoad)
            {
                return createEditPage.WaitForContentToLoad();
            }
            return createEditPage.WaitForContentLoadToFail();
        }

        public Account.ResetPasswordPage ToResetPassword(Guid id)
        {
            _sandbox.Browser.Session.Visit("Account/ResetPassword/" + id);
            return new Account.ResetPasswordPage(_sandbox);
        }

        #endregion

        #region Sessions

        public Session.CreateEditPage ToCreateSession
        {
            get
            {
                _sandbox.Browser.Session.Visit("Session/Create");
                var page = new Session.CreateEditPage(_sandbox);
                page.WaitForContentToLoad();
                return page;
            }
        }

        public Session.UnauthenticatedPage ToCreateSessionExpectingUnauthenticated
        {
            get
            {
                _sandbox.Browser.Session.Visit("Session/Create");
                return new Session.UnauthenticatedPage(_sandbox);
            }
        }

        public Session.CreateEditPage ToEditSession(Guid id)
        {
            return ToEditSession(id, true);
        }

        public Session.CreateEditPage ToEditSession(Guid id, bool expectContentLoad)
        {
            _sandbox.Browser.Session.Visit("Session/Edit/" + id);
            var createEditPage = new Session.CreateEditPage(_sandbox);
            if (expectContentLoad)
            {
                return createEditPage.WaitForContentToLoad();
            }
            return createEditPage.WaitForContentLoadToFail();
        }

        public User.SummaryPage ToUserSummary(Guid id)
        {
            _sandbox.Browser.Session.Visit("User/Summary/" + id);
            WaitForPageWithTitle("User Summary");
            return new User.SummaryPage(_sandbox).WaitForContentToLoad();            
        }

        public User.ChangeEmailPage ToChangeUserEmailAddress(Guid id)
        {
            _sandbox.Browser.Session.Visit("User/ChangeEmail/" + id);
            WaitForPageWithTitle("Change Email");
            return new User.ChangeEmailPage(_sandbox).WaitForContentToLoad();
        }

        public IndexPage ToChangeUserEmailAddressExpectingUnauthenticated(Guid id)
        {
            _sandbox.Browser.Session.Visit("User/ChangeEmail/" + id);
            WaitForPageWithTitle("Login");
            return new IndexPage(_sandbox);
        }

        public Candidate.WelcomePage ToChangeUserEmailAddressExpectingCandidateWelcome(Guid id)
        {
            _sandbox.Browser.Session.Visit("User/ChangeEmail/" + id);
            var page = new Candidate.WelcomePage(_sandbox);
            page.WaitForPageWithTitleStartingWith("Test for ");
            return page;
        }

        public User.ChangePasswordPage ToChangeUserPassword()
        {
            _sandbox.Browser.Session.Visit("User/ChangePassword");
            WaitForPageWithTitle("Change Password");
            return new User.ChangePasswordPage(_sandbox);
        }

        public IndexPage ToChangeUserPasswordExpectingUnauthenticated()
        {
            _sandbox.Browser.Session.Visit("User/ChangePassword");
            WaitForPageWithTitle("Login");
            return new IndexPage(_sandbox);
        }

        public Candidate.WelcomePage ToChangeUserPasswordExpectingCandidateWelcome()
        {
            _sandbox.Browser.Session.Visit("User/ChangePassword");
            var page = new Candidate.WelcomePage(_sandbox);
            page.WaitForPageWithTitleStartingWith("Test for ");
            return page;
        }

        public User.ChangeDetailsPage ToChangeUserDetails(Guid id)
        {
            _sandbox.Browser.Session.Visit("User/Edit/" + id);
            WaitForPageWithTitle("Edit User");
            return new User.ChangeDetailsPage(_sandbox).WaitForContentToLoad();
        }

        public IndexPage ToChangeUserDetailsExpectingUnauthenticated(Guid id)
        {
            _sandbox.Browser.Session.Visit("User/Edit/" + id);
            WaitForPageWithTitle("Login");
            return new IndexPage(_sandbox);
        }

        public Candidate.WelcomePage ToChangeUserDetailsExpectingCandidateWelcome(Guid id)
        {
            _sandbox.Browser.Session.Visit("User/Edit/" + id);
            var page = new Candidate.WelcomePage(_sandbox);
            page.WaitForPageWithTitleStartingWith("Test for ");
            return page;
        }
        
        //TODO: get rid of this; get from page instead
        public void WaitForPageWithTitle(string title)
        {
            var wait = new WebDriverWait((IWebDriver)_sandbox.Browser.Session.Native, new TimeSpan(0, 0, 0, 10));
            wait.Until(x => x.Title == title);
        }    

        #endregion

    }
}
