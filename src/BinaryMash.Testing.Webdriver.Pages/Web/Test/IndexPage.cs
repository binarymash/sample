﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Environment;

    public class IndexPage : WebPage<IndexPage>
    {
        public IndexPage(Sandbox sandbox) : base(sandbox)
        {}

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        public CreateEditPage CreateANewTest
        {
            get
            {
                BrowserSession.ClickLink("Create a new test");
                return new CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        public int NumberOfTests
        {
            get
            {
                var quantity = BrowserSession.FindCss("#quantity").Text;
                if (string.Equals(quantity, "no", StringComparison.OrdinalIgnoreCase))
                {
                    return 0;
                }

                return Convert.ToInt32(quantity);
            }
        }

        public IList<TestSummaryComponent> Tests
        {
            get
            {
                var summaryElements = BrowserSession.FindAllCss(".testVersionSummary");
                return summaryElements.Select(element => new TestSummaryComponent(Sandbox, element)).ToList();
            }
        }

        protected override IndexPage This
        {
            get { return this; }
        }
    }
}
