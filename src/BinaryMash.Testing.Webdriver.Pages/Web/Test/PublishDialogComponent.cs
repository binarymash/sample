﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Test
{
    using Coypu;
    using BinaryMash.Testing.Environment;

    public class PublishDialog : ScopedWebContent
    {
        public PublishDialog(Sandbox sandbox, ElementScope scope) : base(sandbox, scope)
        {            
        }

        public SummaryPage DontPublish()
        {
            Scope.ClickButton(DontPublishText);
            var page = new SummaryPage(Sandbox);
            page.WaitForPageWithTitle("Test Summary");
            return page;
        }

        public PublishedPage Publish()
        {
            Scope.ClickButton(PublishText);            
            var page = new PublishedPage(Sandbox);
            page.WaitForPageWithTitle("Test published");
            return page;
        }

        protected string PublishText { get { return "Yes"; } }
        protected string DontPublishText { get { return "No"; } }

    }
}
