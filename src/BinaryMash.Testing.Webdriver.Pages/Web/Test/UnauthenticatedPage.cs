﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Test
{
    using BinaryMash.Testing.Environment;

    public class UnauthenticatedPage : AsyncWebPage<UnauthenticatedPage>
    {

        public UnauthenticatedPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        protected override UnauthenticatedPage This
        {
            get { return this; }
        }
    }
}
