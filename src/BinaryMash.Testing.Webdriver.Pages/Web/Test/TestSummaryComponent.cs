﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Test
{
    using BinaryMash.Testing.Environment;

    public class TestSummaryComponent
    {
        private readonly Sandbox _sandbox;
        private readonly Coypu.SnapshotElementScope _scope;

        public TestSummaryComponent(Sandbox sandbox, Coypu.SnapshotElementScope scope)
        {
            _sandbox = sandbox;
            _scope = scope;
        }

        public string Name 
        { 
            get
            {
                var nameElement = _scope.FindCss("h5 a");
                return nameElement.Text;
            }
        }

        public string Version
        {
            get
            {
                var nameElement = _scope.FindCss(".version");
                return nameElement.Text;
            }
        }

        public string Updated
        {
            get
            {
                var updatedElement = _scope.FindCss(".updated");
                return updatedElement.Text;
            }
        }

        public string UpdatedBy
        {
            get
            {
                var updatedByElement = _scope.FindCss(".updatedBy");
                return updatedByElement.Text;
                
            }
        }
    
    }
}
