﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Test
{
    using BinaryMash.Testing.Environment;
    using Coypu;

    public class SummaryPage : AsyncWebPage<SummaryPage>
    {

        public SummaryPage(Sandbox sandbox) : base(sandbox)
        {
        }

        protected override SummaryPage This
        {
            get { return this; }
        }

        public string Name
        {
            get
            {
                return NameElement.Text;
            }
        }

        public CreateEditPage EditDetails
        {
            get
            {
                BrowserSession.ClickButton(EditText);
                return new CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        protected ElementScope NameElement
        {
            get { return BrowserSession.FindCss(".f-name"); }
        }

        private const string EditText = "Edit";
    }
}
