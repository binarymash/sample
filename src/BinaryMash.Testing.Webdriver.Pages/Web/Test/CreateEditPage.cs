﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Test
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Coypu;
    using BinaryMash.Testing.Environment;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    public class CreateEditPage : AsyncWebPage<CreateEditPage>
    {

        public CreateEditPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        public int TestId
        {
            get
            {
                var options = new Options
                {
                    ConsiderInvisibleElements = true
                };
                var element = BrowserSession.FindCss("#testId", options);
                return !element.Exists() ? 0 : Int32.Parse(element.Value, CultureInfo.InvariantCulture);
            }
        }

        public SummaryPage CancelExpectingSummary()
        {
            BrowserSession.ClickButton(CancelText);
            WaitForPageWithTitle("Test Summary");
            return new SummaryPage(Sandbox);
        }

        public IndexPage CancelExpectingIndex()
        {
            BrowserSession.ClickButton(CancelText);
            WaitForPageWithTitle("Tests");
            return new IndexPage(Sandbox);
        }

        public PublishDialog Done()
        {
            BrowserSession.ClickButton(DoneText);
            return new PublishDialog(Sandbox, PublishDialogElement);
        }

        public CreateEditPage Save
        {
            get
            {
                BrowserSession.ClickButton(SaveText);
                if (!AsyncTracker.WaitForAndRemove("SaveSucceeded"))
                {
                    throw new Exception("Async status failed");
                }
                RemoveAllToasts();
                return this;
            }
        }

        public CreateEditPage SaveExpectingError
        {
            get
            {
                BrowserSession.ClickButton(SaveText);
                if (!AsyncTracker.WaitForAndRemove("SaveFailed"))
                {
                    throw new Exception("Async status failed");
                }
                RemoveAllToasts();
                return this;
            }
        }

        public string Name
        {
            get { return BrowserSession.FindId("Name").Value; }
        }

        public CreateEditPage SetName(string name)
        {
            BrowserSession.FillIn("Name").With(name);
            return this;
        }


        public bool UserHasBeenNotifiedThatSavingWasSuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("SuccessNotification", "Saved a draft of the test"); }
        }

        public bool UserHasBeenNotifiedThatSavingWasUnsuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("ErrorNotification", "There are validation errors - please fix these. Your changes have NOT been saved"); }
        }

        public List<TestQuestionVersionEditComponent> Questions
        {
            get
            {
                var questionVersionElements = BrowserSession.FindAllCss(".questionVersion");
                return questionVersionElements.Select(e => new TestQuestionVersionEditComponent(e)).ToList();
            }
        }

        protected override CreateEditPage This
        {
            get { return this; }
        }

        protected ElementScope PublishDialogElement
        {
            get
            {
                var wait = new WebDriverWait((IWebDriver)Sandbox.Browser.Session.Native, new TimeSpan(0, 0, 0, 10));
                wait.Until(x => x.FindElement(By.CssSelector("#publishModal")).Displayed); 
                return BrowserSession.FindCss("#publishModal");
            }
        }

        protected string SaveText { get { return "Save"; } }
        protected string CancelText { get { return "Cancel"; } }
        protected string DoneText { get { return "Done"; } }
    }
}
