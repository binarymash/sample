﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Role
{
    using BinaryMash.Testing.Environment;

    public class IndexPage : WebPage<IndexPage>
    {
        public IndexPage(Sandbox sandbox) : base(sandbox)
        {}

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        protected override IndexPage This
        {
            get { return this; }
        }
    }
}
