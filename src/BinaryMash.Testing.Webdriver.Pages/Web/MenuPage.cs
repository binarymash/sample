﻿namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using BinaryMash.Testing.Environment;
    using Home;

    public class MenuPage : WebContent<MenuPage>
    {
        public MenuPage (Sandbox sandbox) : base(sandbox)
        {
        }

        public bool ShowsUserIdentity
        {
            get
            {
                return Sandbox.Browser.Session.HasCss("#userId");
            }
            
        }
        public bool ShowsLogIn
        {
            get
            {
                return Sandbox.Browser.Session.HasCss("#logIn");
            }
        }

        public bool ShowsLogOut
        {
            get
            {
                
                return Sandbox.Browser.Session.HasCss("#logOut");
            }
        }

        public Test.IndexPage Tests
        {
            get
            {
                RemoveAllToasts();
                BrowserSession.ClickLink("Tests");
                return new Test.IndexPage(Sandbox);
            }
        }

        public Question.IndexPage Questions
        {
            get
            {
                RemoveAllToasts();
                BrowserSession.ClickLink("Questions");
                return new Question.IndexPage(Sandbox);
            }
        }

        public Role.IndexPage Roles
        {
            get
            {
                RemoveAllToasts();
                BrowserSession.ClickLink("Roles");
                return new Role.IndexPage(Sandbox);
            }
        }

        public Person.IndexPage People
        {
            get
            {
                RemoveAllToasts();
                BrowserSession.ClickLink("People");
                return new Person.IndexPage(Sandbox);
            }        
        }

        public Session.IndexPage Sessions
        {
            get
            {
                RemoveAllToasts();
                BrowserSession.ClickLink("Sessions");
                return new Session.IndexPage(Sandbox);
            }
        }


        public IndexPage LogOut()
        {
            RemoveAllToasts();
            BrowserSession.ClickLink("Log out");
            return new IndexPage(Sandbox);
        }

        public User.SummaryPage MyAccount
        {
            get
            { 
                RemoveAllToasts();
                BrowserSession.ClickLink("My account");
                var page = new User.SummaryPage(Sandbox);
                page.WaitForPageWithTitle("User Summary");
                return page.WaitForContentToLoad();
            }
        }

        public string UserIdentity
        {
            get
            {
                RemoveAllToasts();
                return Sandbox.Browser.Session.FindId("userId").Text;
            }
        }

        protected override MenuPage This
        {
            get { return this; }
        }
    }
}
