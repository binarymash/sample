﻿namespace BinaryMash.Testing.Webdriver.Pages.Web
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Environment;

    public class PagerComponent : WebContent<PagerComponent>
    {
        public PagerComponent(Sandbox sandbox) : base(sandbox)
        {}

        public bool IsDisplayed
        {
            get { return BrowserSession.HasCss(".pagination"); }
        }

        public ICollection<int> DisplayedPages
        {
            get
            {
                //TODO: make this better!
                var pages = new List<int>();
                var elements = BrowserSession.FindAllCss(".pagination li");
                var uniqueTextValues = from element in elements
                                     group element by element.Text
                                     into elementGroup
                                     select elementGroup.Key;

                foreach (var textValue in uniqueTextValues)
                {
                    int pageNumber;
                    if (Int32.TryParse(textValue, out pageNumber))
                    {
                        pages.Add(pageNumber);
                    }
                }
                return pages;
            }
        }

        public int CurrentPageNumber
        {
            get
            {
                var element = BrowserSession.FindCss(".pagination .active");
                var value = element.Text;
                return Convert.ToInt32(value, CultureInfo.InvariantCulture);
            }
        }
        public bool PageNumberIsActive(int pageNumber)
        {
            var element = BrowserSession.FindCss("#p" + pageNumber);
            var isActive = element["class"].Contains("active");
            return isActive;
        }

        public bool FirstPageLinkIsEnabled
        {
            get { return ElementIsEnabled("#pf"); }
        }

        public bool PreviousPageLinkIsEnabled
        {
            get { return ElementIsEnabled("#pp"); }
        }

        public bool NextPageLinkIsEnabled
        {
            get { return ElementIsEnabled("#pn"); }
        }

        public bool LastPageLinkIsEnabled
        {
            get { return ElementIsEnabled("#pl"); }
        }

        private bool ElementIsEnabled(string cssSelector)
        {
            var element = BrowserSession.FindCss(cssSelector);
            return !element["class"].Contains("disabled");
        }


        protected override PagerComponent This
        {
            get { return this; }
        }
    }
}
