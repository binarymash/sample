﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    public class AnswerContentUserSuppliedPreviewComponent : AnswerContentPreviewComponent
    {

        public AnswerContentUserSuppliedPreviewComponent(Coypu.ElementScope scope) : base(scope)
        {
        }

        public string AnswerPlaceholder
        {
            get
            {
                return _scope.FindCss(".atext")["placeholder"];                
            }
        }

        public string Notes
        {
            get
            {
                
                return _scope.FindCss(".anotes").Text;
            }
        }
    
    }
}
