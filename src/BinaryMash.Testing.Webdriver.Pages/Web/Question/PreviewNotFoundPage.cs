﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using Environment;

    public class PreviewNotFoundPage : WebPage<PreviewNotFoundPage>
    {

        public PreviewNotFoundPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Summary
        {
            get { return BrowserSession.FindCss("#summary").Text; }
        }

        protected override PreviewNotFoundPage This
        {
            get { return this; }
        }
    }
}
