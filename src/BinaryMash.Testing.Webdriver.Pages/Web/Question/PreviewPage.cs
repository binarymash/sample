﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Coypu;
    using Environment;

    public class PreviewPage : WebPage<PreviewPage>
    {

        public PreviewPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Title
        {
            get { return BrowserSession.FindCss(".qtitle").Text; }
        }

        public int Version
        {
            get { return Convert.ToInt32(VersionElement.Text); }
        }

        public string State
        {
            get { return StateElement.Text; }
        }

        public List<QuestionAndAnswerSectionPreviewComponent> Sections
        {
            get
            {
                var sectionElements = BrowserSession.FindAllCss(".qandasection");
                return sectionElements.Select(sectionElement => new QuestionAndAnswerSectionPreviewComponent(sectionElement)).ToList();
            }
        }

        protected override PreviewPage This
        {
            get { return this; }
        }

        protected ElementScope VersionElement
        {
            get { return BrowserSession.FindCss(".f-version"); }
        }

        protected ElementScope StateElement
        {
            get { return BrowserSession.FindCss(".f-state"); }
        }
    }
}
