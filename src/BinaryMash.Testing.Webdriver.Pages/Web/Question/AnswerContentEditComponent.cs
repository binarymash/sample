﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using Coypu;

    public class AnswerContentEditComponent
    {
        protected readonly Coypu.SnapshotElementScope _scope;

        protected AnswerContentEditComponent(Coypu.SnapshotElementScope scope)
        {
            _scope = scope;
        }

        public string Notes
        {
            get { return NotesElement.Value; }
        }

        public AnswerContentEditComponent SetNotes(string notes)
        {
            NotesElement.FillInWith(notes);
            return this;
        }

        public void Remove()
        {
            _scope.ClickLink("Remove");
            //TODO: wait for removal
        }

        #region elements

        private ElementScope NotesElement
        {
            get { return _scope.FindCss(".answerNotes textarea"); }
        }

        #endregion
    }
}
