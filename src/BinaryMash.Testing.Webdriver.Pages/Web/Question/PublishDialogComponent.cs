﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using Coypu;
    using BinaryMash.Testing.Environment;

    public class PublishDialog : ScopedWebContent
    {
        public PublishDialog(Sandbox sandbox, ElementScope scope) : base(sandbox, scope)
        {            
        }

        public PreviewPage DontPublish()
        {
            Scope.ClickButton(DontPublishButtonText);
            var page = new PreviewPage(Sandbox);
            page.WaitForPageWithTitle("Preview");
            return page;
        }

        public PublishedPage Publish()
        {
            Scope.ClickButton(PublishButtonText);            
            var page = new PublishedPage(Sandbox);
            page.WaitForPageWithTitle("Question published");
            return page;
        }


        protected string PublishButtonText { get { return "Yes"; } }
        protected string DontPublishButtonText { get { return "No"; } }

    }
}
