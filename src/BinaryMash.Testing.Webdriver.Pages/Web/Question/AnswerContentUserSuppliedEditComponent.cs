﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using System.Linq;
    using Coypu;

    public class AnswerContentUserSuppliedEditComponent : AnswerContentEditComponent
    {
        public AnswerContentUserSuppliedEditComponent(Coypu.SnapshotElementScope scope)
            : base(scope)
        {
        }

        public string MaxLength
        {
            get { return MaxLengthElement.Value; }
        }

        public AnswerContentUserSuppliedEditComponent SetMaxLength(string maxLength)
        {
            MaxLengthElement.FillInWith(maxLength);
            return this;
        }

        private ElementScope MaxLengthElement
        {
            get { return _scope.FindCss(".answerMaxLength input"); }
        }

    
    }
}
