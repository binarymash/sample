﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Coypu;
    using Domain;

    public class QuestionAndAnswerSectionEditComponent
    {
        private readonly Coypu.SnapshotElementScope _scope;
        private readonly string _id;

        public QuestionAndAnswerSectionEditComponent(Coypu.SnapshotElementScope scope)
        {
            _scope = scope;
            _id = _scope.FindXPath("div").Id;
        }

        public string Text
        {
            get
            {
                return _scope.FindCss(".sectionText textarea").Value;
            }
        }

        public QuestionAndAnswerSectionEditComponent SetText(string text)
        {
            var element = _scope.FindCss(".sectionText textarea");
            element.FillInWith(text);
            return this;
        }

        public string Notes
        {
            get { return _scope.FindCss(".sectionNotes textarea").Value; }
        }

        public QuestionAndAnswerSectionEditComponent SetNotes(string notes)
        {
            var element = _scope.FindCss(".sectionNotes textarea");
            element.FillInWith(notes);
            return this;
        }

        public QuestionAndAnswerSectionEditComponent SelectAnswerType(AnswerContainerType type)
        {
            var typeValue = Enum.GetName(typeof(AnswerContainerType), type);
 
            var element = _scope.FindCss(".answerType select");
            var options = element.FindAllCss("option");
            foreach (var option in options)
            {
                if (option.Value != typeValue)
                {
                    continue;
                }
                
                option.Click();
                break;
            }
            return this;
        }

        public AnswerContainerType SelectedAnswerType
        {
            get
            {
                var type = _scope.FindCss(".answerType select").Value;
                if (string.IsNullOrWhiteSpace(type))
                {
                    return AnswerContainerType.Unset;
                }
                
                return (AnswerContainerType)Enum.Parse(typeof (AnswerContainerType), type);
            }
        }

        public IList<AnswerContentEditComponent> AnswerContents
        {
            get
            {
                var contents = new List<AnswerContentEditComponent>();
                var elements = _scope.FindAllCss(".answerContainer .answerContent");
                var type = SelectedAnswerType;

                foreach (var element in elements)
                {
                    switch (type)
                    {
                        case AnswerContainerType.UserSupplied:
                            contents.Add(new AnswerContentUserSuppliedEditComponent(element));
                            break;
                        case AnswerContainerType.Checkbox:
                        case AnswerContainerType.Radio:
                            contents.Add(new AnswerContentQuestionerSuppliedEditComponent(element));
                            break;
                        default:
                            throw new Exception("Unsupported type");
                    }
                }
                return contents;
            }
        }

        public void AddAnswer()
        {
            _scope.ClickButton("Add an answer to this section");

        }

        public void Remove()
        {
            _scope.ClickButton("Remove this section");
            //TODO: wait for removal
        }
    }
}
