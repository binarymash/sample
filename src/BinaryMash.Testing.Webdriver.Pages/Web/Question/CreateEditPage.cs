﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Coypu;
    using Environment;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class CreateEditPage : AsyncWebPage<CreateEditPage>
    {

        public CreateEditPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        public int QuestionId
        {
            get
            {
                var options = new Options
                {
                    ConsiderInvisibleElements = true
                };
                var element = BrowserSession.FindCss("#questionId", options);
                return !element.Exists() ? 0 : Int32.Parse(element.Value, CultureInfo.InvariantCulture);
            }
        }

        public PreviewPage CancelExpectingPreview()
        {
            BrowserSession.ClickButton(CancelButtonText);
            WaitForPageWithTitle("Preview");
            return new PreviewPage(Sandbox);
        }

        public IndexPage CancelExpectingIndex()
        {
            BrowserSession.ClickButton(CancelButtonText);
            WaitForPageWithTitle("Questions");
            return new IndexPage(Sandbox);
        }

        public PublishDialog Done()
        {
            BrowserSession.ClickButton(DoneButtonText);            
            return new PublishDialog(Sandbox, PublishDialogElement);
        }

        public CreateEditPage Save
        {
            get
            {
                BrowserSession.ClickButton(SaveButtonText);
                if (!AsyncTracker.WaitForAndRemove("SaveSucceeded"))
                {
                    throw new Exception("Async status failed");
                }
                RemoveAllToasts();
                return this;
            }
        }

        public CreateEditPage SaveExpectingError
        {
            get
            {
                BrowserSession.ClickButton(SaveButtonText);
                if (!AsyncTracker.WaitForAndRemove("SaveFailed"))
                {
                    throw new Exception("Async status failed");
                }
                RemoveAllToasts();
                return this;
            }
        }

        public string Title
        {
            get { return BrowserSession.FindId("Title").Value; }
        }

        public List<QuestionAndAnswerSectionEditComponent> Sections 
        { 
            get 
            {
                var sectionElements = BrowserSession.FindAllCss(".qandasection");

                return sectionElements.Select(sectionElement => new QuestionAndAnswerSectionEditComponent(sectionElement)).ToList();
            }
        }

        public CreateEditPage SetTitle(string title)
        {
            ScrollUntilFillInWith(TitleElement, title); 
            return this;
        }

        public CreateEditPage AddSection()
        {
            BrowserSession.ClickButton("Add a section to the question");
            //TODO: wait for section to be added
            return this;
        }

        public bool UserHasBeenNotifiedThatSavingTheDraftWasSuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("SuccessNotification", "Saved a draft of the question"); }
        }

        public bool UserHasBeenNotifiedThatSavingTheDraftWasUnsuccessful
        {
            get { return AsyncTracker.WaitForAndRemove("ErrorNotification", "There are validation errors - please fix these. Your changes have NOT been saved"); }
        }

        public PublishedPage Publish()
        {
            BrowserSession.ClickButton("Publish");
            WaitForPageWithTitle("Question published");
            return new PublishedPage(Sandbox);
        }

        protected override CreateEditPage This
        {
            get { return this; }
        }

        protected ElementScope PublishDialogElement
        {
            get
            {
                var wait = new WebDriverWait((IWebDriver)Sandbox.Browser.Session.Native, new TimeSpan(0, 0, 0, 10));
                wait.Until(x => x.FindElement(By.CssSelector("#publishModal")).Displayed); 
                return BrowserSession.FindCss("#publishModal");
            }
        }

        protected ElementScope TitleElement
        {
            get { return BrowserSession.FindCss("#Title"); }
        }

        protected string SaveButtonText { get { return "Save"; } }
        protected string CancelButtonText { get { return "Cancel"; } }
        protected string DoneButtonText { get { return "Done"; } }
    }
}
