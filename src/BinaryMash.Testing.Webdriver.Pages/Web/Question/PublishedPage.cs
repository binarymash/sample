﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using BinaryMash.Testing.Environment;

    public class PublishedPage : WebPage<PublishedPage>
    {

        public PublishedPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        protected override PublishedPage This
        {
            get { return this; }
        }
    }
}
