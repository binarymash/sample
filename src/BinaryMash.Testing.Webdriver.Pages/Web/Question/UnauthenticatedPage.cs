﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Coypu;
    using Environment;

    public class UnauthenticatedPage : AsyncWebPage<UnauthenticatedPage>
    {

        public UnauthenticatedPage(Sandbox sandbox) : base(sandbox)
        {       
        }

        public string Summary
        {
            get
            {
                var element = Sandbox.Browser.Session.FindCss("#summary");
                return element.Text;
            }
        }

        protected override UnauthenticatedPage This
        {
            get { return this; }
        }
    }
}
