﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using System;
    using System.Globalization;
    using Coypu;
    using Domain;

    public class QuestionAndAnswerSectionPreviewComponent
    {
        private readonly Coypu.SnapshotElementScope _scope;
        private readonly string _id;

        public QuestionAndAnswerSectionPreviewComponent(Coypu.SnapshotElementScope scope)
        {
            _scope = scope;
            _id = _scope.FindXPath("div").Id;
        }

        public string Text
        {
            get { return _scope.FindCss(".qtext").Text; }
        }

        public string Notes
        {
            get { return _scope.FindCss(".qnotes").Text; }
        }


        public AnswerContentPreviewComponent AnswerContent
        {
            get
            {
                return new AnswerContentUserSuppliedPreviewComponent(_scope.FindCss(".qanswer"));
            }
        }
    
    }
}
