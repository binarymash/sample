﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Environment;

    public class IndexPage : WebPage<IndexPage>
    {
        public PagerComponent Pager { get; private set; }

        public IndexPage(Sandbox sandbox) : base(sandbox)
        {       
            Pager = new PagerComponent(sandbox);
        }

        public int NumberOfQuestions
        {
            get
            {
                var quantity = BrowserSession.FindCss("#quantity").Text;
                if (string.Equals(quantity, "no", StringComparison.OrdinalIgnoreCase))
                {
                    return 0;
                }

                return Convert.ToInt32(quantity);
            }
        }

        public CreateEditPage WriteANewQuestion
        {
            get
            {
                BrowserSession.ClickLink("Write a new question");
                return new CreateEditPage(Sandbox).WaitForContentToLoad();
            }
        }

        public IList<QuestionSummaryComponent> Questions
        {
            get
            {
                var summaryElements = BrowserSession.FindAllCss(".questionSummary");
                return summaryElements.Select(element => new QuestionSummaryComponent(Sandbox, element)).ToList();
            }
        }

        public List<QuestionSummaryComponent> QuestionsWithTitle(string title)
        {
            var questions = from q in Questions
                           where q.Title == title
                           select q;

            return questions.ToList();
        }

        protected override IndexPage This
        {
            get { return this; }
        }
    }
}
