﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using Coypu;

    public class AnswerContentQuestionerSuppliedEditComponent : AnswerContentEditComponent
    {
        public AnswerContentQuestionerSuppliedEditComponent(Coypu.SnapshotElementScope scope)
            : base(scope)
        {
        }

        public string Text
        {
            get { return TextElement.Value; }
        }

        public AnswerContentQuestionerSuppliedEditComponent SetText(string text)
        {
            TextElement.FillInWith(text);
            return this;
        }

        private ElementScope TextElement
        {
            get { return _scope.FindCss(".answerText textarea"); }
        }

    
    }
}
