﻿namespace BinaryMash.Testing.Webdriver.Pages.Web.Question
{
    using Environment;

    public class QuestionSummaryComponent
    {
        private readonly Sandbox _sandbox;
        private readonly Coypu.SnapshotElementScope _scope;

        public QuestionSummaryComponent(Sandbox sandbox, Coypu.SnapshotElementScope scope)
        {
            _sandbox = sandbox;
            _scope = scope;
        }

        public string Title 
        { 
            get
            {
                var titleElement = _scope.FindCss("h5 a");
                return titleElement.Text;
            }
        }

        public PreviewPage Preview()
        {
            var header = _scope.FindCss("h5");
            var link = header.FindCss("a");
            link.Click();
            return new PreviewPage(_sandbox);
        }
    
    }
}
