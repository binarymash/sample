﻿namespace BinaryMash.Testing.Webdriver.Pages.Models
{

    public class ChangeUserPasswordPageModel
    {
        public string CurrentPassword { get; set; }

        public string NewPassword { get; set; }

        public string NewPasswordConfirmation { get; set; }
    }
}
