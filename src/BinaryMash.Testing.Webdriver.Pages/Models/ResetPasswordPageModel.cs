﻿namespace BinaryMash.Testing.Webdriver.Pages.Models
{

    public class ResetPasswordPageModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string PasswordConfirmation { get; set; }
    }
}
