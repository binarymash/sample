﻿namespace BinaryMash.Testing.Webdriver.Pages.Models
{

    public class ChangeUserDetailsPageModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string DisplayName { get; set; }
    }
}
