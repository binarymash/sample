﻿
namespace BinaryMash.Testing.Webdriver.Pages.Utilities
{
    using System.Web;

    public static class HrefUrlExtractor
    {
        public static string From(string text)
        {
            //TODO: oof! this is horrible
            var token = "href=3D\"";
            var indexStartOfLink = text.IndexOf(token);
            if (indexStartOfLink == -1)
            {
                token = "href=\"";
                indexStartOfLink = text.IndexOf(token); 
                if (indexStartOfLink == -1)
                {
                    return null;
                }
            }

            var url = text.Substring(indexStartOfLink + token.Length, text.Length - (indexStartOfLink + token.Length));
            var indexEndOfLink = url.IndexOf("\"");
            url = url.Substring(0, indexEndOfLink);
            url = url.Replace("=3D", "=");
            return url;             
        }

    }
}
