﻿namespace BinaryMash.Testing.Webdriver.Pages
{
    using Environment;

    public class Content
    {
        public Content (Sandbox sandbox)
        {
            Sandbox = sandbox;
        }

        protected Sandbox Sandbox { get; private set; }

    }
}
