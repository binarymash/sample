﻿namespace BinaryMash.Testing.Tests.Core.Security.Hashing
{
    using NUnit.Framework;
    using Shouldly;

    [TestFixture]
    public class SaltFactory
    {

        [Test]
        public void CreateNominal()
        {
            var saltFactory = new BinaryMash.Core.Security.Hashing.SaltFactory();
            var salt = saltFactory.Create("");
            salt.ShouldNotBeEmpty();
        }

        //[Test]
        //public void CreateMultipleTimesOnSameInstanceGivesDifferentSalts()
        //{
        //    var saltFactory = new BinaryMash.Core.Security.Hashing.SaltFactory();
        //    var salt1 = saltFactory.Create;
        //    var salt2 = saltFactory.Create;
        //    var salt3 = saltFactory.Create;
        //    salt1.ShouldNotBeSameAs(salt2);
        //    salt1.ShouldNotBeSameAs(salt3);
        //    salt2.ShouldNotBeSameAs(salt3);
        //}

        //[Test]
        //public void CreateOnDifferentInstancesGivesDifferentSalts()
        //{
        //    var saltFactory1 = new BinaryMash.Core.Security.Hashing.SaltFactory();
        //    var salt1 = saltFactory1.Create;
        //    var saltFactory2 = new BinaryMash.Core.Security.Hashing.SaltFactory();
        //    var salt2 = saltFactory2.Create;

        //    salt1.ShouldNotBeSameAs(salt2);
        //}

    }
}
