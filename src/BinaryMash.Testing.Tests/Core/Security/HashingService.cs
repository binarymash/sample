﻿namespace BinaryMash.Testing.Tests.Core.Security
{
    using BinaryMash.Core.Security;
    using BinaryMash.Core.Security.Hashing;
    using BinaryMash.Core.Security.Hashing.Algorithms;
    using BinaryMash.Core.Security.Interfaces;
    using Moq;
    using NUnit.Framework;
    using Shouldly;

    [TestFixture]
    public class HashingService
    {
        private Mock<ISaltFactory> _saltFactory = new Mock<ISaltFactory>();
        private Mock<IAlgorithmFactory> _algorithmFactory = new Mock<IAlgorithmFactory>();
        private Mock<IAlgorithm> _algorithm = new Mock<IAlgorithm>();
 
        [Test]
        public void ValidateNominal()
        {
            const string saltSeed = "dave.smith@binarymash.com";
            const string plainText = "my plaintext";
            const string salt = "26658328";
            const string hash = "F212345678";

            _saltFactory.Setup(exp => exp.Create(saltSeed))
                .Returns(salt);

            _algorithm.Setup(exp => exp.Hash(plainText, salt))
                .Returns(hash);

            _algorithmFactory.Setup(exp => exp.Create(hash))
                .Returns(_algorithm.Object);

            var hashingService = new BinaryMash.Core.Security.HashingService(_algorithmFactory.Object, _saltFactory.Object);
            var response = hashingService.Validate(new ValidateRequest {Hash = hash, PlainText = plainText, SaltSeed = saltSeed});

            response.Status.ShouldBe(StatusCode.Ok);
            response.IsValid.ShouldBe(true);
        }

        [Test]
        public void ValidateWhenAlgorithmReturnsADifferentHash()
        {
            const string plainText = "my plaintext";
            const string salt = "11223344";
            const string hashIn = "F212345678";
            const string hashOut = "F2ABCDEF12";

            _algorithm.Setup(exp => exp.Hash(plainText, salt))
                .Returns(hashOut);

            _algorithmFactory.Setup(exp => exp.Create(hashIn))
                .Returns(_algorithm.Object);

            var hashingService = new BinaryMash.Core.Security.HashingService(_algorithmFactory.Object, _saltFactory.Object);
            var response = hashingService.Validate(new ValidateRequest { Hash = hashIn, PlainText = plainText, SaltSeed = salt });

            response.Status.ShouldBe(StatusCode.Ok);
            response.IsValid.ShouldBe(false);
        }

        [Test]
        public void ValidateWhenHashedWithUnsupportedAlgorithm()
        {
            const string plainText = "my plaintext";
            const string salt = "11223344";
            const string hash = "AB12CD3456";

            _algorithmFactory.Setup(exp => exp.Create(hash))
                .Returns((IAlgorithm)null);

            var hashingService = new BinaryMash.Core.Security.HashingService(_algorithmFactory.Object, _saltFactory.Object);
            var response = hashingService.Validate(new ValidateRequest { Hash = hash, PlainText = plainText, SaltSeed = salt });

            response.Status.ShouldBe(StatusCode.UnsupportedAlgorithm);
            response.IsValid.ShouldBe(false);
        }

        [Test]
        public void HashNominal()
        {
            const string saltSeed = "dave.smith@binarymash.com";
            const string plainText = "my plaintext";
            const string salt = "11223344";
            const string hash = "AB12CD3456";

            _saltFactory.Setup(exp => exp.Create(saltSeed))
                .Returns(salt);

            _algorithm = new Mock<IAlgorithm>();
            _algorithm.Setup(exp => exp.Hash(plainText, salt))
                .Returns(hash);

            _algorithmFactory = new Mock<IAlgorithmFactory>();
            _algorithmFactory.Setup(exp => exp.Default)
                .Returns(_algorithm.Object);

            var hashingService = new BinaryMash.Core.Security.HashingService(_algorithmFactory.Object, _saltFactory.Object);
            var response = hashingService.Hash(new HashRequest {PlainText = plainText, SaltSeed = saltSeed});

            response.Hash.ShouldBe(hash);
            response.Salt.ShouldBe(salt);
        }

    }
}
