﻿namespace BinaryMash.Testing.Tests.Utilities
{
    using System.Security.Principal;

    public class TestIdentity : IIdentity
    {
        public string Name { get; set; }
        public string AuthenticationType { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}
