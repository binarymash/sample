﻿namespace BinaryMash.Testing.Tests.Application.UserService.Sagas
{
    using System;
    using System.Globalization;
    using BinaryMash.Application.EmailService.Contract.Commands;
    using BinaryMash.Application.UserService.Contract.Commands;
    using BinaryMash.Application.UserService.Contract.Messages;
    using BinaryMash.Application.UserService.Sagas;
    using NServiceBus.Testing;
    using NUnit.Framework;

    [TestFixture]
    public class TestUserValidationSaga
    {

        [Test]
        public void WhenAValidateUserCommandIsReceivedFollowedByAUserValidatedReceivedMessageThenWeReplyWithUserValidatedToTheOriginatorAndTheSagaCompletes()
        {
            //TODO: move to generator
            const string email = "dave.smith@binarymash.com";
            const string firstName = "Dave";
            const string fromAddress = "accounts@binarymash.com";
            const string toAddress = "dave.smith@binarymash.com";
            const string encodedToAddress = "dave.smith%40binarymash.com";
            const string subject = "Activate your account";
            var sagaId = Guid.NewGuid();
            string body = string.Format(CultureInfo.InvariantCulture, "<p>Hello Dave.</p> <p>Your account needs to be activated before it can be used. Please click <a href=\"http://localhost:64023/Account/Activate/{0}?email={1}\">here</a>, or paste the link below into your browser.</p><p>http://localhost:64023/Account/Activate/{0}?email={1}</p>", sagaId, encodedToAddress);
            var userRegistrationSagaId = Guid.NewGuid();

            Test.Initialize();

            Test.Saga<UserValidationSaga>(sagaId)

                .ExpectSend<SendEmailCommand>(m =>
                    m.FromAddress == fromAddress &&
                    m.ToAddress == toAddress &&
                    m.Subject == subject &&
                    m.Body == body)

                .When(s => s.Handle(new ValidateUserCommand
                {
                    Email = email,
                    FirstName = firstName,
                    UserRegistrationSagaId = userRegistrationSagaId
                }))


                .ExpectReplyToOrginator<UserValidatedMessage>(m =>
                    m.UserRegistrationSagaId == userRegistrationSagaId)

                .When(s => s.Handle(new UserValidationReceivedMessage
                {
                    UserValidationSagaId = sagaId
                }))

                .AssertSagaCompletionIs(true);
        }

        //[Test]
        //public void WhenAUserValidatedReceivedMessageOnANonExistentSagaIdThen()
        //{
        //    //TODO: move to generator
        //    const string email = "dave.smith@binarymash.com";
        //    const string firstName = "Dave";
        //    const string fromAddress = "accounts@binarymash.com";
        //    const string toAddress = "dave.smith@binarymash.com";
        //    const string subject = "Activate your account";
        //    var sagaId = Guid.NewGuid();
        //    string body = string.Format(CultureInfo.InvariantCulture, "Hello Dave, please click <a href=\"http://localhost:64023/Account/Activate/{0}\">here</a> to activate your account", sagaId);
        //    var userRegistrationSagaId = Guid.NewGuid();

        //    Test.Initialize();

        //    Test.Saga<UserValidationSaga>(sagaId)

        //        .ExpectSend<SendEmailCommand>(m =>
        //            m.FromAddress == fromAddress &&
        //            m.ToAddress == toAddress &&
        //            m.Subject == subject &&
        //            m.Body == body)

        //        .When(s => s.Handle(new ValidateUserCommand
        //        {
        //            Email = email,
        //            FirstName = firstName,
        //            UserRegistrationSagaId = userRegistrationSagaId
        //        }))


        //        .ExpectReplyToOrginator<UserValidatedMessage>(m =>
        //            m.UserRegistrationSagaId == userRegistrationSagaId)

        //        .When(s => s.Handle(new UserValidationReceivedMessage
        //        {
        //            UserValidationSagaId = Guid.NewGuid()
        //        }))

        //        .AssertSagaCompletionIs(true);
        //}

        [Test]
        public void WhenAValidateUserCommandIsReceivedWithAnEmailThatAlreadyHasARunningSaga()
        {
            //TODO: move to generator
            const string email = "dave.smith@binarymash.com";
            const string firstName = "Dave";
            const string fromAddress = "accounts@binarymash.com";
            const string toAddress = "dave.smith@binarymash.com";
            const string encodedToAddress = "dave.smith%40binarymash.com"; 
            const string subject = "Activate your account";
            var sagaId = Guid.NewGuid();
            string body = string.Format(CultureInfo.InvariantCulture, "<p>Hello Dave.</p> <p>Your account needs to be activated before it can be used. Please click <a href=\"http://localhost:64023/Account/Activate/{0}?email={1}\">here</a>, or paste the link below into your browser.</p><p>http://localhost:64023/Account/Activate/{0}?email={1}</p>", sagaId, encodedToAddress);
            var userRegistrationSagaId = Guid.NewGuid();

            Test.Initialize();

            Test.Saga<UserValidationSaga>(sagaId)                

                .ExpectSend<SendEmailCommand>(m =>
                    m.FromAddress == fromAddress &&
                    m.ToAddress == toAddress &&
                    m.Subject == subject &&
                    m.Body == body)

                .When(s =>
                          {
                              s.Handle(new ValidateUserCommand
                                           {
                                               Email = email,
                                               FirstName = firstName,
                                               UserRegistrationSagaId = userRegistrationSagaId
                                           });

                              s.Handle(new ValidateUserCommand
                              {
                                  Email = email,
                                  FirstName = firstName,
                                  UserRegistrationSagaId = userRegistrationSagaId
                              });
                          });

        }

        [Test]
        public void WhenUserIsNotValidatedWithinTheSpecifiedTimeThenWeReplyToOriginatorWithUserNotValidatedAndTheSagaCompletes()
        {
            //TODO: move to generator
            const string email = "dave.smith@binarymash.com";
            const string firstName = "Dave";
            const string fromAddress = "accounts@binarymash.com";
            const string toAddress = "dave.smith@binarymash.com";
            const string encodedToAddress = "dave.smith%40binarymash.com";
            const string subject = "Activate your account";
            var sagaId = Guid.NewGuid();
            string body = string.Format(CultureInfo.InvariantCulture, "<p>Hello Dave.</p> <p>Your account needs to be activated before it can be used. Please click <a href=\"http://localhost:64023/Account/Activate/{0}?email={1}\">here</a>, or paste the link below into your browser.</p><p>http://localhost:64023/Account/Activate/{0}?email={1}</p>", sagaId, encodedToAddress);
            var userRegistrationSagaId = Guid.NewGuid();

            Test.Initialize();

            Test.Saga<UserValidationSaga>(sagaId)

                .ExpectSend<SendEmailCommand>(m =>
                    m.FromAddress == fromAddress &&
                    m.ToAddress == toAddress &&
                    m.Subject == subject &&
                    m.Body == body)

                .When(s => s.Handle(new ValidateUserCommand
                {
                    Email = email,
                    FirstName = firstName,
                    UserRegistrationSagaId = userRegistrationSagaId
                }))

                
                .ExpectReplyToOrginator<UserNotValidatedMessage>(m =>
                    m.UserRegistrationSagaId == userRegistrationSagaId)

                .WhenSagaTimesOut()

                .AssertSagaCompletionIs(true);
        }

    
    }
}
