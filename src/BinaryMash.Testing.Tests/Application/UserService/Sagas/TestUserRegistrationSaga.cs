﻿//namespace BinaryMash.Testing.Tests.Application.UserService.Sagas
//{
//    using System;
//    using BinaryMash.Application.UserService.Contract.Commands;
//    using BinaryMash.Application.UserService.Contract.Messages;
//    using BinaryMash.Application.UserService.Sagas;
//    using BinaryMash.DataAccess.Db.Contract;
//    using BinaryMash.Testing.Data;
//    using Moq;
//    using NServiceBus.Testing;
//    using NUnit.Framework;

//    [TestFixture]
//    public class TestUserRegistrationSaga
//    {

//        [Test]
//        [Ignore("Temporary ignore this until I've proven the build is stable")]
//        public void WhenARegisterUserCommandIsReceivedFollowedByAUserValidatedMessageThenAUserIsInUseMessageIsPublishedAndTheSagaCompletes()
//        {
//            //var user = UserFactory.Default.Create();

//            //var userRepository = new Mock<IUserIdentityRepository>();

//            //userRepository.Setup(mock => 
//            //    mock.Add(It.Is<User>(u => 
//            //        u.Id == user.Id &&
//            //        u.AccountId == user.AccountId &&
//            //        u.Email == user.Email &&
//            //        u.FirstName == user.FirstName &&
//            //        u.LastName == user.LastName &&
//            //        u.HashedPassword == user.HashedPassword &&
//            //        u.Salt == user.Salt)));


//            //Test.Initialize();

//            //Test.Saga<UserRegistrationSaga>()
//            //    //.WithExternalDependencies(s => s.UserRepository = userRepository.Object)

//            //    .ExpectSend<ValidateUserCommand>(m => 
//            //        m.Email == user.Email && 
//            //        m.FirstName == user.FirstName
//            //        //TODO: m.UserRegistrationSagaId
//            //        )

//            //    .When(s => s.Handle(new RegisterUserCommand
//            //        {
//            //            UserId = user.Id,
//            //            AccountId = user.AccountId,
//            //            Email = user.Email,
//            //            HashedPassword = user.HashedPassword,
//            //            Salt = user.Salt,
//            //            FirstName = user.FirstName,
//            //            LastName = user.LastName,
//            //        }))
                
//            //    .ExpectPublish<UserIsInUseMessage>(m => 
//            //        m.UserCredential.Email == user.Email && 
//            //        m.UserCredential.HashedPassword == user.HashedPassword)
                
//            //    .When(s => s.Handle(new UserValidatedMessage
//            //        {
//            //            UserRegistrationSagaId = s.Data.Id
//            //        }))
                    
//            //    .AssertSagaCompletionIs(true);
//        }

//        [Test]
//        [Ignore("User Repository not yet implemented")]
//        public void WhenARegisterUserCommandIsReceivedFollowedByAUserValidatedMessageThenTheUserRepositoryThrowsAnExceptionTheSagaIsErrored()
//        {
//        //    var user = UserFactory.Default.Create();

//        //    var userRepository = new Mock<IUserIdentityRepository>();

//        //    userRepository
//        //        .Setup(mock => mock.Add(It.Is<User>(u =>
//        //            u.Id == user.Id &&
//        //            u.AccountId == user.AccountId &&
//        //            u.Email == user.Email &&
//        //            u.FirstName == user.FirstName &&
//        //            u.LastName == user.LastName &&
//        //            u.HashedPassword == user.HashedPassword &&
//        //            u.Salt == user.Salt)))
//        //        .Throws(new Exception("boom!"));

//        //    Test.Initialize();

//        //    Test.Saga<UserRegistrationSaga>()
//        //        //.WithExternalDependencies(s => s.UserRepository = userRepository.Object)

//        //        .ExpectSend<ValidateUserCommand>(m =>
//        //            m.Email == user.Email &&
//        //            m.FirstName == user.FirstName
//        //            //TODO: m.UserRegistrationSagaId
//        //            )

//        //        .When(s => s.Handle(new RegisterUserCommand
//        //        {
//        //            UserId = user.Id,
//        //            AccountId = user.AccountId,
//        //            Email = user.Email,
//        //            HashedPassword = user.HashedPassword,
//        //            Salt = user.Salt,
//        //            FirstName = user.FirstName,
//        //            LastName = user.LastName,
//        //        }))
                
//        //        .ExpectNotPublish<UserIsInUseMessage>(m =>
//        //            m.UserCredential.Email == user.Email &&
//        //            m.UserCredential.HashedPassword == user.HashedPassword)

//        //        .When(s => s.Handle(new UserValidatedMessage
//        //        {
//        //            UserRegistrationSagaId = s.Data.Id
//        //        }))

//        //        .AssertSagaCompletionIs(true);
//        }

//        [Test]
//        [Ignore("Don't know if this is a valid test or not")]
//        public void WhenAValidatedMessageIsReceivedBeforeARegisterUserMessageThenNothingHappens()
//        {
//        //    var user = UserFactory.Default.Create();

//        //    var userRepository = new Mock<IUserIdentityRepository>();

//        //    userRepository
//        //        .Setup(mock => mock.Add(It.Is<User>(u =>
//        //            u.Id == user.Id &&
//        //            u.AccountId == user.AccountId &&
//        //            u.Email == user.Email &&
//        //            u.FirstName == user.FirstName &&
//        //            u.LastName == user.LastName &&
//        //            u.HashedPassword == user.HashedPassword &&
//        //            u.Salt == user.Salt)))
//        //        .Throws(new Exception("boom!"));
            
//        //    Test.Initialize();

//        //    Test.Saga<UserRegistrationSaga>()

//        //        //.WithExternalDependencies(s => s.UserRepository = userRepository.Object)

//        //        .ExpectNotPublish<UserIsInUseMessage>(m => m.UserCredential.Email == user.Email)
//        //        .When(s => s.Handle(new UserValidatedMessage
//        //        {
//        //            UserRegistrationSagaId = Guid.NewGuid()
//        //        }))

//        //        .AssertSagaCompletionIs(true);

//        }

//        //[Test]
//        //public void WhenAUserIsNotValidatedThenAReminderIsSentAndWhenEventuallyValidatedThenAUserCredentialsMessageIsPublishedAndTheSagaCompletes()
//        //{
//        //    var user = UserFactory.Default.Create();

//        //    Test.Initialize();

//        //    Test.Saga<UserRegistrationPolicy>()

//        //        .ExpectPublish<RequestToCreateUserAcceptedEvent>(m =>
//        //            m.Email == user.Email &&
//        //            m.FirstName == user.FirstName &&
//        //            m.LastName == user.LastName)

//        //        .When(s => s.Handle(new RegisterUserMessage
//        //        {
//        //            Email = user.Email,
//        //            HashedPassword = user.HashedPassword,
//        //            FirstName = user.FirstName,
//        //            LastName = user.LastName
//        //        }))


//        //        .ExpectPublish<RequestToCreateUserAcceptedEvent>(m =>
//        //            m.Email == user.Email &&
//        //            m.FirstName == user.FirstName &&
//        //            m.LastName == user.LastName)
                        
//        //        .When(s => s.Timeout(new UserValidationNotReceivedMessage()))


//        //        .ExpectPublish<UserIsInUseMessage>(m =>
//        //            m.Email == user.Email &&
//        //            m.HashedPassword == user.HashedPassword)

//        //        .When(s => s.Handle(new UserValidatedMessage
//        //        {
//        //            UserValidationSagaId = s.Data.Id,
//        //            Email = user.Email
//        //        }))

//        //        .AssertSagaCompletionIs(true);

//        //}

//    }
//}
