﻿namespace BinaryMash.Testing.Tests.Application.EmailService.Handlers
{
    using BinaryMash.Application.EmailService.Contract.Commands;
    using BinaryMash.Application.EmailService.Handlers;
    using BinaryMash.Application.UserService.Contract.Messages;
    using NServiceBus.Testing;
    using NUnit.Framework;

    [TestFixture]
    public class TestSendEmailCommandHandler
    {

        [Test]
        [Ignore("Create to debug mandrill issue")]
        public void WhenSendEmailCommandIsReceived_ARequestIsSentToMandrill()
        {
                Test.Initialize();

                Test.Handler<SendEmailCommandHandler>()                    
                    .OnMessage<SendEmailCommand>(m =>
                        {
                            m.Body = "&lt;p&gt;Hello philip.&lt;/p&gt; &lt;p&gt;Your account needs to be activated before it can be used. Please click &lt;a href=\"http://binarymash-apt.cloudapp.net:8081/Account/Activate/c790b72a-a193-4e0d-addf-a21f010950e0\"&gt;here&lt;/a&gt;, or paste the link below into your browser.&lt;/p&gt;&lt;p&gt;http://binarymash-apt.cloudapp.net:8081/Account/Activate/c790b72a-a193-4e0d-addf-a21f010950e0&lt;/p&gt;";
                            m.FromAddress = "accounts@binarymash.com";
                            m.Subject = "Activate your account";
                            m.ToAddress = "philip.m.wood@googlemail.com";
                        });         
        }
    }
}
