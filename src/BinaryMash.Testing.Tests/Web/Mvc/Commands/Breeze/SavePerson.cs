﻿namespace BinaryMash.Testing.Tests.Web.Mvc.Commands.Breeze
{
    using System.Net;
    using BinaryMash.DataAccess.EF;

    using BinaryMash.Testing.Data;
    using BinaryMash.Testing.Data.Requests;
    using BinaryMash.Web.Mvc4.Commands;
    using NUnit.Framework;
    using Newtonsoft.Json.Linq;
    using Shouldly;
    using Utilities;

    [TestFixture]
    [Ignore("Run manually")]
    public class SavePerson
    {

        #region Message

        [Test]
        public void WhenTryingToChangeMultiplePeople_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Message_HasMultiple_Person");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Request must contain one person");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenNoPerson_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Message_HasNo_Person");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Request must contain one person");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenMessageHasAnUnsupportedEntityType_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Message_HasUnsupportedEntity_AnswerContentUserSupplied");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Unsupported entity AnswerContentUserSupplied");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion

        #region Person

        #region Added

        //TODO: trying to add person to someone elses account, actually added to mine

        #endregion

        #region Modified

        [Test]
        public void WhenTryingToChangeSomeoneElsesQuestion_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Person_Modify_Name");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheAccountIdOnAQuestion_TheChangeIsRejected()
        {
            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Person_Modify_AccountId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheUserIdOnAQuestion_TheChangeIsRejected()
        {
            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Person_Modify_UserId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheCreatedOnAQuestion_TheChangeIsRejected()
        {
            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Person_Modify_Created");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheUpdatedOnAQuestion_TheChangeIsRejected()
        {
            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Person_Modify_Created");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion

        #region Deleted

        [Test]
        public void WhenTryingToDeleteAPerson_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.Person_Delete");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Unsupported EntityState");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion

        #endregion

        #region StandardCommunicationChannel

        #region Added

        [Test]
        public void WhenTryingToAddAStandardCommunicationChannelToSomeoneElsesPerson_TheChangeIsRejected()
        {
            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.StandardCommunicationChannel_Add_ToDifferentPerson");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Invalid PersonId on StandardCommunicationChannel");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Added

        #region Modified

        [Test]
        public void WhenTryingToChangeThePersonIdOnAStandardCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.StandardCommunicationChannel_Modify_PersonId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheTypeIdOnAStandardCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.StandardCommunicationChannel_Modify_TypeId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheIdOnAStandardCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.StandardCommunicationChannel_Modify_Id");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToModifyAStandardCommunicationChannelOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.StandardCommunicationChannel_Modify_Value");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Modified

        #region Deleted

        [Test]
        public void WhenTryingToDeleteAStandardCommunicationChannelOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.StandardCommunicationChannel_Delete");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Deleted

        #endregion

        #region CustomCommunicationChannelSupplied

        #region Added

        [Test]
        public void WhenTryingToAddACustomCommunicationChannelToSomeoneElsesPerson_TheChangeIsRejected()
        {
            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.CustomCommunicationChannel_Add_ToDifferentPerson");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Invalid PersonId on CustomCommunicationChannel");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Added

        #region Modified

        [Test]
        public void WhenTryingToChangeThePersonIdOnACustomCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.CustomCommunicationChannel_Modify_PersonId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheTypeIdOnACustomCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.CustomCommunicationChannel_Modify_TypeId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheIdOnACustomCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.CustomCommunicationChannel_Modify_Id");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToModifyACustomCommunicationChannelOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.CustomCommunicationChannel_Modify_Value");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Modified

        #region Deleted

        [Test]
        public void WhenTryingToDeleteACustomCommunicationChannelOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.CustomCommunicationChannel_Delete");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Deleted

        #endregion

        #region PersonCommunicationChannel

        #region Added

        [Test]
        public void WhenTryingToAddAPersonCommunicationChannelToSomeoneElsesPerson_TheChangeIsRejected()
        {
            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.PersonCommunicationChannel_Add_ToDifferentPerson");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Invalid PersonId on PersonCommunicationChannel");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Added

        #region Modified

        [Test]
        public void WhenTryingToChangeThePersonIdOnAPersonCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.PersonCommunicationChannel_Modify_PersonId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheTypeIdOnAPersonCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.PersonCommunicationChannel_Modify_TypeId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheIdOnAPersonCommunicationChannel_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.PersonCommunicationChannel_Modify_Id");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToModifyAPersonCommunicationChannelOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.PersonCommunicationChannel_Modify_CommsPersonId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Modified

        #region Deleted

        [Test]
        public void WhenTryingToDeleteAPersonCommunicationChannelOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();
            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SavePersonCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SavePerson.PersonCommunicationChannel_Delete");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Deleted

        #endregion

    }
}
