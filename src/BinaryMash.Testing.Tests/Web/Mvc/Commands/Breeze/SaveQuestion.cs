﻿namespace BinaryMash.Testing.Tests.Web.Mvc.Commands.Breeze
{
    using System.Net;
    using BinaryMash.DataAccess.EF;

    using BinaryMash.Testing.Data;
    using BinaryMash.Testing.Data.Requests;
    using BinaryMash.Web.Mvc4.Commands;
    using NUnit.Framework;
    using Newtonsoft.Json.Linq;
    using Shouldly;
    using Utilities;

    [TestFixture]
    [Ignore("Run manually")]
    public class SaveQuestion
    {

        #region Message

        [Test]
        public void WhenTryingToChangeMultipleQuestion_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.Message_HasMultiple_Question");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Request must contain one QuestionVersion");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenNoQuestion_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.Message_HasNo_Question");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Request must contain one QuestionVersion");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenMessageHasAnUnsupportedEntityType_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.Message_HasUnsupportedEntity_Person");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Unsupported entity Person");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion

        #region QuestionVersion

            #region Added

            //TODO: trying to add QuestionVersion to someone elses account, actually added to mine

            #endregion

            #region Modified

        [Test]
        public void WhenTryingToChangeSomeoneElsesQuestion_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.Question_Modify_Title");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToChangeTheAuditIdOnAQuestion_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.Question_Modify_AuditId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

            #endregion

            #region Deleted

        [Test]
        public void WhenTryingToDeleteAQuestion_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.Question_Delete");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Unsupported EntityState");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

            #endregion

        #endregion

        #region QuestionAndAnswerSection

            #region Added



            #endregion Added

            #region Modified


            #endregion Modified

            #region Deleted


            #endregion Deleted

        #endregion QuestionAndAnswerSection

        #region AnswerContentUserSupplied

            #region Added

        [Test]
        public void WhenTryingToAddAnAnswerContentUserSuppliedToASectionOnADifferentQuestion_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.UserSupplied_Add_ToSectionOnAnotherQuestion");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Invalid QuestionId on section");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

            #endregion Added

            #region Modified

        [Test]
        public void WhenTryingToChangeTheQuestionAndAnswerSectionIdOnAnAnswerContentUserSupplied_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.UserSupplied_Modify_SectionId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToModifyAnAnswerContentUserSuppliedOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.UserSupplied_Modify_MaxLength");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Modified

            #region Deleted

        [Test]
        public void WhenTryingToDeleteAnAnswerContentUserSuppliedOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.UserSupplied_Delete");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        #endregion Deleted

        #endregion

        #region AnswerContentQuestionerSupplied

            #region Added

        [Test]
        public void WhenTryingToAddAnAnswerContentQuestionerSuppliedToASectionOnADifferentQuestion_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.QuestionerSupplied_Add_ToSectionOnAnotherQuestion");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Invalid QuestionId on section");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

            #endregion Added

        #region Modified

        [Test]
        public void WhenTryingToChangeTheQuestionAndAnswerSectionIdOnAnAnswerContentQuestionerSupplied_TheChangeIsRejected()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.QuestionerSupplied_Modify_SectionId");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("Fields cannot be modified");
                ex.HttpCode.ShouldBe(HttpStatusCode.BadRequest);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

        [Test]
        public void WhenTryingToModifyAnAnswerContentQuestionerSuppliedOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.QuestionerSupplied_Modify_Text");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

            #endregion Modified

            #region Deleted

        [Test]
        public void WhenTryingToDeleteAnAnswerContentQuestionerOwnedBySomeoneElse_TheChangeIsRejected()
        {

            var user = UserBuilder.DaveSmith.Create();

            var principal = new TestPrincipal { Identity = new TestIdentity { Name = user.Email, IsAuthenticated = true } };

            var command = new BinaryMash.Web.Mvc4.Commands.Breeze.SaveQuestionCommand(principal, new BirdhouseContext());
            var message = RequestLoader.Get("SaveQuestion.QuestionerSupplied_Delete");
            command.SaveBundle = JObject.Parse(message);
            try
            {
                command.Run();
                Assert.Fail("Should have thrown CommandException");
            }
            catch (CommandException ex)
            {
                ex.Message.ShouldBe("User not authorised");
                ex.HttpCode.ShouldBe(HttpStatusCode.Forbidden);
            }
            catch
            {
                Assert.Fail("Should have thrown CommandException");
            }
        }

            #endregion

        #endregion

    }
}
