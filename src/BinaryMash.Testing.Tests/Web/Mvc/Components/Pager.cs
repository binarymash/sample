﻿namespace BinaryMash.Testing.Tests.Web.Mvc.Components
{
    using NUnit.Framework;
    using Shouldly;

    [TestFixture]
    public class Pager
    {

        [Test]
        public void WhenNoItems()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(13, 0, 10, "", 5);
            pager.CurrentPageNumber.ShouldBe(0);
            pager.NextPage.ShouldBe(0);
            pager.NumberOfItems.ShouldBe(0);
            pager.NumberOfPages.ShouldBe(0);
            pager.NumberOfPagesToDisplay.ShouldBe(5);
            pager.PageSize.ShouldBe(10);
            pager.Pages.Count.ShouldBe(0);
            pager.PreviousPage.ShouldBe(0);
        }

        [Test]
        public void When1PageButAskedToDisplay3_OnlyOnePageIsDisplayed()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(1, 5, 10, "", 5);
            pager.CurrentPageNumber.ShouldBe(1);
            pager.PreviousPage.ShouldBe(1);
            pager.NextPage.ShouldBe(1);
            pager.NumberOfPages.ShouldBe(1);
            pager.Pages.Count.ShouldBe(1);
            pager.Pages.ShouldContain(1);
        }

        [Test]
        public void When2PagesButAskedToDisplay3_OnlyTwoPagesAreDisplayed()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(1, 12, 10, "", 5);
            pager.CurrentPageNumber.ShouldBe(1);
            pager.PreviousPage.ShouldBe(1);
            pager.NextPage.ShouldBe(2);
            pager.NumberOfPages.ShouldBe(2);
            pager.Pages.Count.ShouldBe(2);
            pager.Pages.ShouldContain(1);
            pager.Pages.ShouldContain(2);
        }

        [Test]
        public void WhenOddNumberOfItemsToDisplay_CurrentIsCentered()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(13, 237, 10, "", 5);
            pager.PreviousPage.ShouldBe(12);
            pager.NextPage.ShouldBe(14);
            pager.Pages.Count.ShouldBe(5);
            pager.Pages.ShouldContain(11);
            pager.Pages.ShouldContain(12);
            pager.Pages.ShouldContain(13);
            pager.Pages.ShouldContain(14);
            pager.Pages.ShouldContain(15);
        }

        [Test]
        public void WhenEvenNumberOfItemsToDisplay_CurrentIsCenteredOffsetToLeft()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(13, 237, 10, "", 6);
            pager.PreviousPage.ShouldBe(12);
            pager.NextPage.ShouldBe(14);
            pager.Pages.Count.ShouldBe(6);
            pager.Pages.ShouldContain(11);
            pager.Pages.ShouldContain(12);
            pager.Pages.ShouldContain(13);
            pager.Pages.ShouldContain(14);
            pager.Pages.ShouldContain(15);
            pager.Pages.ShouldContain(16);
        }

        [Test]
        public void WhenCurrentIsFirstPageAndShowingFivePages_PagesStartFromFirst()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(1, 237, 10, "", 3);
            pager.PreviousPage.ShouldBe(1);
            pager.NextPage.ShouldBe(2);
            pager.Pages.Count.ShouldBe(3);
            pager.Pages.ShouldContain(1);
            pager.Pages.ShouldContain(2);
            pager.Pages.ShouldContain(3);
        }

        [Test]
        public void WhenCurrentIsSecondPageAndShowingFivePages_PagesStartFromFirstPage()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(2, 237, 10, "", 5);
            pager.PreviousPage.ShouldBe(1);
            pager.NextPage.ShouldBe(3);
            pager.Pages.Count.ShouldBe(5);
            pager.Pages.ShouldContain(1);
            pager.Pages.ShouldContain(2);
            pager.Pages.ShouldContain(3);
            pager.Pages.ShouldContain(4);
            pager.Pages.ShouldContain(5);
        }

        [Test]
        public void WhenCurrentIsThirdPageAndShowingFivePages_PagesStartFromFirstPage()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(3, 237, 10, "", 5);
            pager.PreviousPage.ShouldBe(2);
            pager.NextPage.ShouldBe(4);
            pager.Pages.Count.ShouldBe(5);
            pager.Pages.ShouldContain(1);
            pager.Pages.ShouldContain(2);
            pager.Pages.ShouldContain(3);
            pager.Pages.ShouldContain(4);
            pager.Pages.ShouldContain(5);
        }

        [Test]
        public void WhenCurrentIsFourthPageAndShowingFivePages_PagesStartFromSecondPage()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(4, 237, 10, "", 5);
            pager.PreviousPage.ShouldBe(3);
            pager.NextPage.ShouldBe(5);
            pager.Pages.Count.ShouldBe(5);
            pager.Pages.ShouldContain(2);
            pager.Pages.ShouldContain(3);
            pager.Pages.ShouldContain(4);
            pager.Pages.ShouldContain(5);
            pager.Pages.ShouldContain(6);
        }

        [Test]
        public void WhenCurrentIsLastPage_PagesEndWithLast()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(24, 237, 10, "", 3);
            pager.PreviousPage.ShouldBe(23);
            pager.NextPage.ShouldBe(24);
            pager.Pages.Count.ShouldBe(3);
            pager.Pages.ShouldContain(22);
            pager.Pages.ShouldContain(23);
            pager.Pages.ShouldContain(24);
        }

        [Test]
        public void WhenCurrentIsLastPageMinus1AndShowingFivePages_PagesEndWithLast()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(23, 237, 10, "", 5);
            pager.PreviousPage.ShouldBe(22);
            pager.NextPage.ShouldBe(24);
            pager.Pages.Count.ShouldBe(5);
            pager.Pages.ShouldContain(20);
            pager.Pages.ShouldContain(21);
            pager.Pages.ShouldContain(22);
            pager.Pages.ShouldContain(23);
            pager.Pages.ShouldContain(24);
        }

        [Test]
        public void WhenCurrentIsLastPageMinus2AndShowingFivePages_PagesEndWithLast()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(22, 237, 10, "", 5);
            pager.PreviousPage.ShouldBe(21);
            pager.NextPage.ShouldBe(23);
            pager.Pages.Count.ShouldBe(5);
            pager.Pages.ShouldContain(20);
            pager.Pages.ShouldContain(21);
            pager.Pages.ShouldContain(22);
            pager.Pages.ShouldContain(23);
            pager.Pages.ShouldContain(24);
        }

        [Test]
        public void WhenCurrentIsLastPageMinus2AndShowingFivePages_PagesEndWithLastMinus1()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(21, 237, 10, "", 5);
            pager.PreviousPage.ShouldBe(20);
            pager.NextPage.ShouldBe(22);
            pager.Pages.Count.ShouldBe(5);
            pager.Pages.ShouldContain(19);
            pager.Pages.ShouldContain(20);
            pager.Pages.ShouldContain(21);
            pager.Pages.ShouldContain(22);
            pager.Pages.ShouldContain(23);
        }

        [Test]
        public void BeyondLastPage()
        {
            var pager = new BinaryMash.Web.Mvc4.Components.Pager(57, 237, 10, "", 3);
            pager.PreviousPage.ShouldBe(23);
            pager.NextPage.ShouldBe(24);
            pager.Pages.Count.ShouldBe(3);
            pager.Pages.ShouldContain(22);
            pager.Pages.ShouldContain(23);
            pager.Pages.ShouldContain(24);
        }
    
    }
}
