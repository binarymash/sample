namespace BinaryMash.Application.EmailService 
{
    using Castle.Windsor;
    using Castle.Windsor.Installer;
    using NServiceBus;

	/*
		This class configures this endpoint as a Server. More information about how to configure the NServiceBus host
		can be found here: http://nservicebus.com/GenericHost.aspx
	*/
    public class EndpointConfig : IConfigureThisEndpoint,
        AsA_Server,
        IWantCustomInitialization
    {
        private IWindsorContainer _windsorContainer;

        public void Init()
        {
            BootstrapWindsor();

            Configure.Serialization.Xml();

            Configure.With()
                .CastleWindsorBuilder(_windsorContainer)
                .MsmqSubscriptionStorage();

        }

        private void BootstrapWindsor()
        {
            _windsorContainer = new WindsorContainer()
                .Install(Configuration.FromAppConfig());
        }
    }
}