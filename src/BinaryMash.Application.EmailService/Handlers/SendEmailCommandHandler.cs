﻿namespace BinaryMash.Application.EmailService.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Net.Mail;
    using Castle.Components.DictionaryAdapter;
    using Contract.Commands;
    using Mandrill;
    using NServiceBus;

    public class SendEmailCommandHandler : IHandleMessages<SendEmailCommand>
    {
        public IBus Bus { get; set; }

        public void Handle(SendEmailCommand command)
        {
            if (ConfigurationManager.AppSettings["useMandrill"] == "true")
            {
                SendUsingMandrill(command);
            }
            else
            {
                SendUsingSmtp(command);
            }
        }

        private void SendUsingMandrill(SendEmailCommand command)
        {
            var message = new Mandrill.EmailMessage
            {
                from_email = command.FromAddress,
                to = new List<EmailAddress> {new EmailAddress {email = command.ToAddress}},
                subject = command.Subject,
                html =  command.Body,
                track_clicks = false,
                track_opens = false,
                important =  true,
            };

            //message.merge_vars. = new EditableList<rcpt_merge_var>();
            //message.global_merge_vars.AddRange(new List<merge_var>
            //{
            //        new merge_var{name="FIRSTNAME", content="myfirstname"},
            //        new merge_var{name="HOSTNAME", content="myhost"},
            //        new merge_var{name="GUID", content="myguid"},                                            
            //});

            //message.merge_vars.Add(
            //    new rcpt_merge_var
            //    {
            //        rcpt = command.ToAddress,
            //        vars = new EditableList<merge_var>
            //        {
            //        }
            //    });

            var api = new MandrillApi(ConfigurationManager.AppSettings["mandrillApiKey"]);

            var templateContents = new List<TemplateContent>
            {
                //new TemplateContent {name = "FIRSTNAME", content = "blah"},
                //new TemplateContent {name = "HOSTNAME", content = "hjkhjk"},
                //new TemplateContent {name = "GUID", content = "abcd"}
            };

            var results = api.SendMessage(message);//, "AccountValidation", templateContents);

            foreach (var result in results)
            {
                if (result.Status != EmailResultStatus.Sent && result.Status != EmailResultStatus.Queued)
                {
                    throw new Exception(String.Format("Failed to send message to mandrill: {0}", result.Status));
                }
            }
        }

        private void SendUsingSmtp(SendEmailCommand command)
        {
            using (var mailMessage = new MailMessage())
            {
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = command.Body;
                mailMessage.From = new MailAddress(command.FromAddress);
                mailMessage.Subject = command.Subject;
                mailMessage.To.Add(command.ToAddress);

                using (var smtpServer = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"]))
                {
                    smtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);
                    smtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["smtpUserName"], ConfigurationManager.AppSettings["smtpPassword"]);
                    smtpServer.EnableSsl = string.Equals(ConfigurationManager.AppSettings["smtpSsl"], "true", StringComparison.OrdinalIgnoreCase);

                    smtpServer.Send(mailMessage);
                }
            }            
        }
    }
}
