﻿namespace BinaryMash.Testing.Data
{
    using System;
    using Domain;

    public class UserBuilder : Builder<User, UserBuilder>
    {

        protected override UserBuilder InstantiateDefaultSubject()
        {
            var account = AccountBuilder.Default.Create();

            Subject = new User
            {
                Id = new Guid("3C63FFF5-E3B5-406E-A7B6-A65C6510F6E7"),
                AccountId = account.Id,
                Email = "test.user@binarymash.com",
                FirstName = "Test",
                LastName = "User",
            };

            Subject.PublicProfile = new UserPublicProfile
            {
                DisplayName = "McTesty",
            };

            return this;
        }

        public static UserBuilder SystemAdmin
        {
            get
            {
                var factory = new UserBuilder
                {
                    Subject = new User
                    {
                        Id = new Guid("0D179DA4-0757-4A2B-9F65-E57808D56576"),
                        AccountId = new Guid("C4083E54-7C50-4D08-B84A-D823FE8EC037"),
                        Email = "philip.m.wood@gmail.com",
                        FirstName = "Philip",
                        LastName = "Wood",
                    }                    
                };

                factory.Subject.PublicProfile = new UserPublicProfile
                {
                    DisplayName = "Bluejam",
                };

                return factory;
            }
        }

        public static UserBuilder DaveSmith
        {
            get
            {
                var factory = new UserBuilder
                {
                    Subject = new User
                    {
                        Id = new Guid("C6851DD9-1F91-4622-B41B-4A1A193E10BE"),
                        AccountId = new Guid("8BBE51BA-5B6D-4FB4-9A52-934C6FEC2E13"),
                        Email = "dave.smith@binarymash.com",
                        FirstName = "Dave",
                        LastName = "Smith",
                    }
                };

                factory.Subject.PublicProfile = new UserPublicProfile
                {
                    DisplayName = "Smudger",
                };

                return factory;
            }
        }

        public static UserBuilder NonExistentUser
        {
            get
            {
                var factory = new UserBuilder
                {
                    Subject = new User
                    {
                        Id = Guid.Empty,
                        AccountId = Guid.Empty,
                        Email = "idontexist@binarymash.com",
                        FirstName = "Doesnt",
                        LastName = "Exist",
                    }
                };

                return factory;
            }
        }

        public UserBuilder WithId(Guid id)
        {
            Subject.Id = id;
            return this;
        }

        public UserBuilder WithEmail(string email)
        {
            Subject.Email = email;
            return this;
        }

        public UserBuilder WithName(string firstName, string lastName)
        {
            Subject.FirstName = firstName;
            Subject.LastName = lastName;
            return this;
        }
    }
}
