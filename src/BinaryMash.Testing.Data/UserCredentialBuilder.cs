﻿namespace BinaryMash.Testing.Data
{
    using Domain;

    public class UserCredentialBuilder : Builder<UserCredential, UserCredentialBuilder>
    {

        protected override UserCredentialBuilder InstantiateDefaultSubject()
        {
            var user = UserBuilder.Default.Create();
            var login = LoginBuilder.Default.Create();

            Subject = new UserCredential
            {
                Email = user.Email,
                HashedPassword = login.HashedPassword
            };

            return this;
        }


    }
}
