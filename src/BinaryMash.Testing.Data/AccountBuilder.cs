﻿namespace BinaryMash.Testing.Data
{
    using System;
    using Domain;

    public class AccountBuilder : Builder<Account, AccountBuilder>
    {

        protected override AccountBuilder InstantiateDefaultSubject()
        {
            Subject = new Account
            {
                Id = new Guid("C4083E54-7C50-4D08-B84A-D823FE8EC037"),
                Name = "This is my account"
            };

            return this;
        }

        protected AccountBuilder AccountForDaveSmith()
        {
            Subject = new Account
            {
                Id = new Guid("8BBE51BA-5B6D-4FB4-9A52-934C6FEC2E13"),
                Name = "Account for Dave Smith"
            };

            return this;
        }
    }
}
