﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryMash.Testing.Data.Requests
{
    using System.IO;
    using System.Reflection;

    public static class RequestLoader
    {
        public static string Get(string path)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "BinaryMash.Testing.Data.Requests."+path+".txt";
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

    }
}
