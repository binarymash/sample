﻿namespace BinaryMash.Testing.Data
{
    public abstract class Builder<TSubject, TBuilder> where TBuilder : Builder<TSubject, TBuilder>, new()        
    {

        public static TBuilder Default
        {
            get
            {
                var builder = new TBuilder();
                builder.InstantiateDefaultSubject();
                return builder;
            }
        }

        public TSubject Create()
        {
            return Subject;
        }

        protected TSubject Subject;

        protected abstract TBuilder InstantiateDefaultSubject();


    }
}
