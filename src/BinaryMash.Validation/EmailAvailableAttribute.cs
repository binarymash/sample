﻿namespace BinaryMash.Validation
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    namespace Checked.Entitites
    {
        public class EmailAvailableAttribute : ValidationAttribute, IClientValidatable
        {
            public override bool IsValid(object value)
            {
                //TODO: implement. This should go to the database to see if the email address exists
                return true;
            }

            public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
            {
                yield return new ModelClientValidationRule
                {
                    ValidationType = "emailavailable",
                    ErrorMessage = ErrorMessageString
                };
            }
        }
    }
}