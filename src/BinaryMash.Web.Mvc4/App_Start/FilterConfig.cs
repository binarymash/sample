﻿using System.Web.Mvc;

namespace BinaryMash.Web.Mvc4
{
    using Membership;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new MyAuthorizeAttribute());
        }
    }
}