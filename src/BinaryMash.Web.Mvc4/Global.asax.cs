﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace BinaryMash.Web.Mvc4
{
    //using Application.UserService.Contract.Events;
    //using Application.UserService.Contract.Messages;
    using System.Web.Optimization;
    using Castle.Windsor;
    using Castle.Windsor.Installer;
    using Domain;
    using Injection;
    using ModelBinders;
    using NServiceBus;

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication, IContainerAccessor
    {
        protected void Application_Start()
        {
            BootstrapWindsor();

            System.Web.Mvc.ModelBinders.Binders.Add(typeof(UserIdentity), new UserIdentityModelBinder<UserIdentity>());

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            BootstrapNServiceBus();
            //BootstrapUserCredentials();

        }

        private void BootstrapWindsor()
        {
            _container = new WindsorContainer()
                .Install(Configuration.FromAppConfig());
        }

        //private void BootstrapUserCredentials()
        //{
        //    var bus = _windsorContainer.Resolve<IBus>();
        //    bus.Subscribe<UserActivated>();
        //    bus.Send(new GetAllUsernamesMessage());
        //}

        private void BootstrapNServiceBus()
        {
            Configure.Serialization.Xml();
            Configure.Transactions.Disable();

            Configure.With()
                .CastleWindsorBuilder(Container)
                .ForMvc()
                .ForWebApi()
                .UseTransport<Msmq>()
                .PurgeOnStartup(true)
                .UnicastBus()
                .RunHandlersUnderIncomingPrincipal(false)
                .CreateBus()
                .Start(() => Configure.Instance.ForInstallationOn<NServiceBus.Installation.Environments.Windows>()
                                      .Install());
        }

        private static IWindsorContainer _container;
        public IWindsorContainer Container { get { return _container; } }
    }
}