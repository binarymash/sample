﻿
namespace BinaryMash.Web.Mvc4
{
    using Agents;
    using Agents.Interfaces;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Membership;

    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            InstallMembership(container);
            InstallAgents(container);
            //InstallAppFabricCache(container);
        }

        private static void InstallMembership(IWindsorContainer container)
        {
            container.Register(Component.For<MyRoleProvider>()
                .LifeStyle.PerWebRequest
                .Named("MyRoleProvider"));
        }
        private static void InstallAgents(IWindsorContainer container)
        {
            container.Register(

                Component.For<IAccountServiceAgent>()
                         .ImplementedBy<AccountServiceAgent>()
                         .LifeStyle.PerWebRequest,

                Component.For<ISessionAgent>()
                         .ImplementedBy<SessionAgent>()
                         .LifeStyle.PerWebRequest
                );
        }

        //private static void InstallAppFabricCache(IWindsorContainer container)
        //{
        //    container.Register(

        //        Component.For<DataCacheFactory>()
        //                 .Instance(new DataCacheFactory())

        //        );
        //}

    }
}