﻿using System;

namespace BinaryMash.Web.Mvc4.Commands
{
    using System.Net;
    using global::Breeze.WebApi;

    public class CommandException : Exception
    {
        public HttpStatusCode HttpCode { get; private set; }

        public EntityInfo EntityInfo { get; private set; }

        public CommandException(string message, HttpStatusCode httpCode) : base(message)
        {
            HttpCode = httpCode;
        }

        public CommandException(string message, HttpStatusCode httpCode, EntityInfo entityInfo)
            : this(message, httpCode)
        {
            EntityInfo = entityInfo;
        }
    
    }
}