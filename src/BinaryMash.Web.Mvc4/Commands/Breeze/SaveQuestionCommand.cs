﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using global::Breeze.WebApi;

    public class SaveQuestionCommand : BreezeUserSessionCommand
    {

        public SaveQuestionCommand(IPrincipal principal, BirdhouseContext cleanContext) : base(principal, cleanContext)
        {
        }

        #region Breeze overrides

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            //confirm that you are always updating/deleting a TodoList/TodoItem that belongs to the current user. 
            //
            //It also assigns the current user to a new TodoList. In a real app, this would be a dispatcher to some helper 
            //classes dedicated to validating specific entity types, a point I slightly elaborate below.

            var entityType = entityInfo.Entity.GetType();
            switch (entityType.FullName)
            {
                case ("BinaryMash.Domain.Question"):
                    return BeforeSaveQuestion(entityInfo);

                case ("BinaryMash.Domain.QuestionVersion"):
                    return BeforeSaveQuestionVersion(entityInfo);

                case ("BinaryMash.Domain.QuestionAndAnswerSection"):
                    return BeforeSaveQuestionAndAnswerSection(entityInfo);

                case ("BinaryMash.Domain.AnswerContentUserSupplied"):
                    return BeforeSaveAnswerContentUserSupplied(entityInfo);

                case ("BinaryMash.Domain.AnswerContentQuestionerSupplied"):
                    return BeforeSaveAnswerContentQuestionerSupplied(entityInfo);

                default:
                    throw new CommandException("Unsupported entity " + entityInfo.Entity.GetType().Name, HttpStatusCode.BadRequest);
            }
        }

        protected override System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> BeforeSaveEntities(System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> saveMap)
        {
            //gives you a chance to examine the entire change-set at once. 
            //
            //This is a great way to make validate the entire change-set as a whole perhaps to ensure that this save request 
            //makes sense as a single transaction. 
            //
            //It's also a good place to do some cross-entity checking. 
            //
            //BeforeSaveEntities(saveMap) is called after calling BeforeSaveEntity(entityInfo) for each entity individually.
            //
            //might be a good dispatch point for delegating validation to dedicated helper classes. I doubt that I would put 
            //all of my validation logic in one big ContextProvider class. I don’t mind that all save activity funnels through 
            //the ContextProvider.SaveChanges gate but I don’t want to do all the work in the ContextProvider itself.            

            //TODO: user has permission to save drafts

            var now = DateTime.UtcNow;

            //Confirm only one QuestionVersion
            if (!saveMap.ContainsKey(typeof (QuestionVersion)))
            {
                throw new CommandException("Request must contain one QuestionVersion", HttpStatusCode.BadRequest);                
            }

            var questionVersions = saveMap[typeof (QuestionVersion)];
            if (questionVersions.Count != 1)
            {
                throw new CommandException("Request must contain one QuestionVersion", HttpStatusCode.BadRequest);                                
            }

            var questionVersion = questionVersions[0];
            var questionVersionEntity = (QuestionVersion)questionVersion.Entity;

            if (questionVersion.EntityState == EntityState.Added)
            {
                BeforeSaveEntitiesWhenAddingQuestionVersion(saveMap, questionVersionEntity, questionVersion, now);
            }
            else if (questionVersion.EntityState == EntityState.Modified)
            {
                BeforeSaveEntitiesWhenModifyingQuestionVersion(saveMap, questionVersionEntity, questionVersion, now);
            }
            else
            {
                throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, questionVersion);
            }

            return base.BeforeSaveEntities(saveMap);
        }

        #endregion

        private void BeforeSaveEntitiesWhenAddingQuestionVersion(Dictionary<Type, List<EntityInfo>> saveMap, QuestionVersion questionVersionEntity, EntityInfo questionVersion, DateTime now)
        {
            //always set these values on a new TestVersion
            questionVersionEntity.Created = now;
            questionVersionEntity.Updated = now;
            questionVersionEntity.UserId = User.PublicProfileId;
            questionVersionEntity.State = QuestionVersionState.Draft;

            //confirm we have a Question
            if (!saveMap.ContainsKey(typeof(Question)))
            {
                throw new CommandException("Request must contain one Question", HttpStatusCode.BadRequest);
            }

            var questions = saveMap[typeof(Question)];
            if (questions.Count != 1)
            {
                throw new CommandException("Request must contain one Question", HttpStatusCode.BadRequest);
            }

            var question = questions[0];
            var questionEntity = (Question)question.Entity;
            if (question.EntityState == EntityState.Added)
            {
                //always set these values on a new Question
                questionEntity.Created = now;
                questionEntity.AccountId = User.AccountId;
                questionEntity.LatestVersion = questionVersionEntity.Version;
                questionEntity.UserId = User.PublicProfileId;

                //Check that full object graph has valid foreign keys
            }
            else if (question.EntityState == EntityState.Modified)
            {
                //Adding a new TestVersion to an existing Test
                question.OriginalValuesMap["LatestDraftGuid"] = "";
                questionEntity.LatestVersion = questionVersionEntity.Version;

                if (questionEntity.Id != questionVersionEntity.QuestionId)
                {
                    throw new CommandException("Question and QuestionVersion must be related", HttpStatusCode.BadRequest);
                }

                //TODO: just get the test not the test version
                var cleanQuestion = CleanContext.Questions.First(q => q.Id == questionEntity.Id);

                if (cleanQuestion.LatestPublishedVersion != cleanQuestion.LatestVersion)
                {
                    //already have an unpublished version for this test    
                    throw new CommandException("Cannot have more than one draft QuestionVersion for a test", HttpStatusCode.BadRequest);
                }
            }

            //Check that full object graph has valid foreign keys

            //check that sections are assigned to the question we were passed in
            if (saveMap.ContainsKey(typeof(QuestionAndAnswerSection)))
            {
                var entityInfo = saveMap[typeof (QuestionAndAnswerSection)].FirstOrDefault(s => 
                    (((QuestionAndAnswerSection) s.Entity).QuestionVersionId != questionVersionEntity.QuestionId) ||
                    (((QuestionAndAnswerSection) s.Entity).QuestionVersionVersion != questionVersionEntity.Version));
                if (entityInfo != null)
                {
                    throw new CommandException("Invalid QuestionVersion on section", HttpStatusCode.BadRequest, entityInfo);
                }
            }

            //check that AnswerContentUserSupplied are not being assigned to an existing section (new question version so must be new section!)
            if (saveMap.ContainsKey(typeof(AnswerContentUserSupplied)))
            {
                var eiOnExistingSection = saveMap[typeof (AnswerContentUserSupplied)].FirstOrDefault(ei => 
                    ((AnswerContent) ei.Entity).QuestionAndAnswerSectionId >= 0);
                if (eiOnExistingSection != null)
                {
                    throw new CommandException("Invalid QuestionAndAnswerSection on AnswerContent", HttpStatusCode.BadRequest, eiOnExistingSection);                    
                }
            }

            //check that AnswerContentQuetionerSupplied are not being assigned to an existing section (new question version so must be new section!)
            if (saveMap.ContainsKey(typeof(AnswerContentQuestionerSupplied)))
            {
                var eiOnExistingSection = saveMap[typeof(AnswerContentQuestionerSupplied)].FirstOrDefault(ei =>
                    ((AnswerContent)ei.Entity).QuestionAndAnswerSectionId >= 0);
                if (eiOnExistingSection != null)
                {
                    throw new CommandException("Invalid QuestionAndAnswerSection on AnswerContent", HttpStatusCode.BadRequest, eiOnExistingSection);
                }
            }
        }

        private void BeforeSaveEntitiesWhenModifyingQuestionVersion(Dictionary<Type, List<EntityInfo>> saveMap, QuestionVersion questionVersionEntity, EntityInfo questionVersion, DateTime now)
        {
            var cleanQuestionVersion = CleanContext.QuestionVersions
                                                   .Include("Sections.AnswerContents")
                                                   .Include("Question")
                                                   .First(q => ((q.QuestionId == questionVersionEntity.QuestionId) && (q.Version == questionVersionEntity.Version)));

            //identity owns this QuestionVersion
            if (cleanQuestionVersion.Question.AccountId != User.AccountId)
            {
                throw new CommandException("User not authorised", HttpStatusCode.Forbidden, questionVersion);
            }

            //Check that full object graph has valid foreign keys

            //check that sections have correct questionId and version
            if (saveMap.ContainsKey(typeof(QuestionAndAnswerSection)))
            {
                var entityInfo = saveMap[typeof(QuestionAndAnswerSection)].FirstOrDefault(s =>
                    (((QuestionAndAnswerSection)s.Entity).QuestionVersionId != questionVersionEntity.QuestionId) ||
                    (((QuestionAndAnswerSection)s.Entity).QuestionVersionVersion != questionVersionEntity.Version));
                if (entityInfo != null)
                {
                    throw new CommandException("Invalid QuestionVersion on section", HttpStatusCode.BadRequest, entityInfo);
                }
            }

            //check that AnswerContentUserSupplied have valid QuestionAndAnswerSectionId
            if (saveMap.ContainsKey(typeof(AnswerContentUserSupplied)))
            {
                var entities = saveMap[typeof(AnswerContentUserSupplied)];
                foreach (var entity in entities)
                {
                    var answerContent = (AnswerContent)entity.Entity;

                    //don't care about new AnswerContents
                    if (answerContent.QuestionAndAnswerSectionId < 0) continue; 

                    //can only update AnswerContents on an existing section on our question version
                    if (cleanQuestionVersion.Sections.All(s => s.Id != answerContent.QuestionAndAnswerSectionId))
                    {
                        throw new CommandException("Invalid section on answer content", HttpStatusCode.BadRequest, entity);
                    }
                }
            }

            //check that AnswerContentQuestionerSupplied have valid QuestionAndAnswerSectionId
            if (saveMap.ContainsKey(typeof(AnswerContentQuestionerSupplied)))
            {
                var entities = saveMap[typeof(AnswerContentQuestionerSupplied)];
                foreach (var entity in entities)
                {
                    var answerContent = (AnswerContent)entity.Entity;

                    //don't care about new AnswerContents
                    if (answerContent.QuestionAndAnswerSectionId < 0) continue; 

                    //can only update AnswerContents on an existing section on our question version
                    if (cleanQuestionVersion.Sections.All(s => s.Id != answerContent.QuestionAndAnswerSectionId))
                    {
                        throw new CommandException("Invalid section on answer content", HttpStatusCode.BadRequest, entity);
                    }
                }
            }
        }

        private bool BeforeSaveQuestion(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities
            var question = (Question) entityInfo.Entity;

            if (entityInfo.EntityState == EntityState.Added)
            {
                //only expected if also adding a TestVersion with version 1
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only expected if also adding a TestVersion with version > 1

                if (entityInfo.OriginalValuesMap.Keys.Any())
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveQuestionVersion(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            var questionVersion = (QuestionVersion) entityInfo.Entity;

            questionVersion.Created = DateTime.UtcNow;
            questionVersion.UserId = User.PublicProfileId;
            
            if (entityInfo.EntityState == EntityState.Added)
            {
                questionVersion.State = QuestionVersionState.Draft;
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only allow updates if state is draft
                if (questionVersion.State != QuestionVersionState.Draft)
                {
                    return false;
                }

                //only field we allow to be changed is Title
                if (entityInfo.OriginalValuesMap.Keys.Any(k => k != "Title"))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                //TODO: if state is published, reject the changes

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveQuestionAndAnswerSection(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Added)
            {
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is Index, Text and Notes
                if (entityInfo.OriginalValuesMap.Keys.Any(k => (k != "Index" && k != "Text" && k != "Notes" && k != "Type")))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            if (entityInfo.EntityState == EntityState.Deleted)
            {
                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveAnswerContentUserSupplied(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Added)
            {
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is MaxLength and Notes
                if (entityInfo.OriginalValuesMap.Keys.Any(k => (k != "MaxLength" && k != "Notes")))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            if (entityInfo.EntityState == EntityState.Deleted)
            {
                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveAnswerContentQuestionerSupplied(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Added)
            {
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is Index, Text and Notes
                if (entityInfo.OriginalValuesMap.Keys.Any(k => (k != "Index" && k != "Text" && k != "Notes")))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            if (entityInfo.EntityState == EntityState.Deleted)
            {
                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

    }
}