﻿namespace BinaryMash.Web.Mvc4.ContextProviders
{
    using System.Security.Principal;
    using Breeze.WebApi;
    using BinaryMash.DataAccess.EF;
    using Breeze.WebApi.EF;
    using Newtonsoft.Json.Linq;

    public abstract class BreezeCommand : EFContextProvider<BirdhouseContext>
    {
        protected IPrincipal Principal { get; private set; }

        //see http://stackoverflow.com/questions/14517945/using-this-context-inside-beforesaveentity
        protected BirdhouseContext CleanContext { get; set; } //TODO: make this readonly

        public JObject SaveBundle { get; set; }

        public SaveResult Run()
        {
            return SaveChanges(SaveBundle);
        }

        protected BreezeCommand(IPrincipal pricipal, BirdhouseContext cleanContext)
        {
            Principal = pricipal;
            CleanContext = cleanContext;
        }

    }
}