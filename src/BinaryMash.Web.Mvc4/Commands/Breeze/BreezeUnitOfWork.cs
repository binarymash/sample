﻿using System;

namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System.Collections.Generic;
    using System.Transactions;
    using Castle.Components.DictionaryAdapter;
    using ContextProviders;
    using global::Breeze.WebApi;

    public static class BreezeUnitOfWork
    {

        public static SaveResult Run(BreezeCommand command)
        {
            SaveResult result;
            try
            {
                using (var transactionScope = new TransactionScope())
                {
                    result = command.Run();
                    transactionScope.Complete();
                }
                //TODO: why is this not throwing an exception if invalid data, eg not set a required field? (login hashedpassword)
            }
            catch (CommandException ex)
            {
                result = new SaveResult
                {
                    Errors = new List<object> {"An unexpected error occurred: " + ex.Message}
                };
            }
            //catch (Exception ex)
            //{
            //    //TODO: write NSB audit here including exception message
            //    result = new SaveResult
            //    {
            //        Errors = new List<object> {"An unexpected error occurred: " + ex.Message}
            //    };
            //};            

            return result;
        }
    }

}