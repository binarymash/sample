﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using System.Data.Entity;
    using ContextProviders;

    public abstract class BreezeSessionCommand : BreezeCommand
    {

        private Session _session;

        protected BreezeSessionCommand(IPrincipal principal, BirdhouseContext cleanContext) : base(principal, cleanContext)
        {
        }

        protected Session Session
        {
            get
            {
                if (_session == null && Principal.Identity.IsAuthenticated)
                {
                    var id = IdentityConverter.ExtractSessionId(Principal.Identity.Name);
                    _session = CleanContext.Logins
                                        .Include(l => l.Session)
                                        .Where(l => l.SessionId == id)
                                        .Select(l => l.Session)
                                        .FirstOrDefault();
                }
                return _session;
            }
        }
    }
}