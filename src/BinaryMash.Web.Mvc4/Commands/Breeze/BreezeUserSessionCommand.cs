﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System;
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using System.Data.Entity;
    using ContextProviders;

    public abstract class BreezeUserSessionCommand : BreezeCommand
    {
        private User _user;
 
        protected BreezeUserSessionCommand(IPrincipal principal, BirdhouseContext cleanContext) : base(principal, cleanContext)
        {
        }

        protected User User
        {
            get
            {
                if (_user == null && Principal.Identity.IsAuthenticated)
                {
                    var id = IdentityConverter.ExtractUserId(Principal.Identity.Name);
                    _user = CleanContext.Logins
                                        .Include(l => l.User)
                                        .Where(l => l.UserId == id)
                                        .Select(l => l.User)
                                        .FirstOrDefault();
                }
                return _user;
            }
        }
    }
}