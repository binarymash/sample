﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using Core.Security.Interfaces;
    using global::Breeze.WebApi;

    public class SaveSessionCommand : BreezeUserSessionCommand
    {

        public SaveSessionCommand(IPrincipal principal, BirdhouseContext cleanContext, IHashingService hashingService)
            : base(principal, cleanContext)
        {
            _hashingService = hashingService;
        }

        #region Breeze overrides

        protected override EntityInfo CreateEntityInfo()
        {
            return base.CreateEntityInfo();
        }

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            //confirm that you are always updating/deleting a TodoList/TodoItem that belongs to the current user. 
            //
            //It also assigns the current user to a new TodoList. In a real app, this would be a dispatcher to some helper 
            //classes dedicated to validating specific entity types, a point I slightly elaborate below.

            var entityType = entityInfo.Entity.GetType();
            switch (entityType.FullName)
            {
                case ("BinaryMash.Domain.Session"):
                    return BeforeSaveSession(entityInfo);
                default:
                    throw new CommandException("Unsupported entity " + entityInfo.Entity.GetType().Name, HttpStatusCode.BadRequest);
            }
        }

        protected override System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> BeforeSaveEntities(System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> saveMap)
        {
            //gives you a chance to examine the entire change-set at once. 
            //
            //This is a great way to make validate the entire change-set as a whole perhaps to ensure that this save request 
            //makes sense as a single transaction. 
            //
            //It's also a good place to do some cross-entity checking. 
            //
            //BeforeSaveEntities(saveMap) is called after calling BeforeSaveEntity(entityInfo) for each entity individually.
            //
            //might be a good dispatch point for delegating validation to dedicated helper classes. I doubt that I would put 
            //all of my validation logic in one big ContextProvider class. I don’t mind that all save activity funnels through 
            //the ContextProvider.SaveChanges gate but I don’t want to do all the work in the ContextProvider itself.            

            //TODO: user has permission to save drafts

            //Confirm only one QuestionVersion
            if (!saveMap.ContainsKey(typeof (Session)))
            {
                throw new CommandException("Request must contain one session", HttpStatusCode.BadRequest);                
            }

            var sessions = saveMap[typeof (Session)];
            if (sessions.Count != 1)
            {
                throw new CommandException("Request must contain one session", HttpStatusCode.BadRequest);                                
            }

            var session = (Session) sessions[0].Entity;
            Session cleanSession = null;

            if (sessions[0].EntityState == EntityState.Added)
            {
                ((Session) sessions[0].Entity).AccountId = User.AccountId;

                //create associated login. Do this here rather than on the client because there is no user variables in it
                var hashRequest = new HashRequest
                    {
                        PlainText = session.Password,
                        SaltSeed = session.Id.ToString()
                    };

                var hashResponse =  _hashingService.Hash(hashRequest);

                var login = new Login{
                        Id = session.Id,
                        Session = session,
                        HashedPassword = hashResponse.Hash,
                        Salt = hashResponse.Salt
                    };

                var loginEi = CreateEntityInfo(login, EntityState.Added);

                saveMap[typeof(Login)] = new List<EntityInfo> {loginEi};
            }
            else
            {
                cleanSession = CleanContext.Sessions
                                           .Include(s => s.Person)
                                           .First(p => p.Id == session.Id);

                //identity owns this session
                if (cleanSession.AccountId != User.AccountId)
                {
                    throw new CommandException("User not authorised", HttpStatusCode.Forbidden, sessions[0]);
                }            
            }

            //Check that full object graph has valid foreign keys...

            var person = CleanContext.People.FirstOrDefault(p => p.Id == session.PersonId);
            if (person.AccountId != User.AccountId)
            {
                //TODO: finish
                
            }


            return base.BeforeSaveEntities(saveMap);
        }

        #endregion

        private bool BeforeSaveSession(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            var session = (Session) entityInfo.Entity;


            if (entityInfo.EntityState == EntityState.Added)
            {
                session.UserId = User.PublicProfileId;
                session.Updated = DateTime.UtcNow; 
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is Title
                if (entityInfo.OriginalValuesMap.Keys.Any(k => ((k != "TestId") && (k != "PersonId") && (k != "TestVersionVersion"))))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                session.UserId = User.PublicProfileId;
                session.Updated = DateTime.UtcNow;
                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private readonly IHashingService _hashingService;
    }
}