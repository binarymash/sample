﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using System.Data.Entity;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using global::Breeze.WebApi;

    public class PublishQuestionCommand : BreezeUserSessionCommand
    {

        public PublishQuestionCommand(IPrincipal principal, BirdhouseContext cleanContext) : base(principal, cleanContext)
        {
        }

        #region Breeze overrides

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            //confirm that you are always updating/deleting a TodoList/TodoItem that belongs to the current user. 
            //
            //It also assigns the current user to a new TodoList. In a real app, this would be a dispatcher to some helper 
            //classes dedicated to validating specific entity types, a point I slightly elaborate below.

            var entityType = entityInfo.Entity.GetType();
            switch (entityType.FullName)
            {
                case ("BinaryMash.Domain.QuestionVersion"):
                    return BeforeSaveQuestionVersion(entityInfo);
                case ("BinaryMash.Domain.Question"):
                    return BeforeSaveQuestion(entityInfo);
                default:
                    throw new CommandException("Unsupported entity " + entityInfo.Entity.GetType().Name, HttpStatusCode.BadRequest);
            }
        }

        protected override System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> BeforeSaveEntities(System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> saveMap)
        {
            //gives you a chance to examine the entire change-set at once. 
            //
            //This is a great way to make validate the entire change-set as a whole perhaps to ensure that this save request 
            //makes sense as a single transaction. 
            //
            //It's also a good place to do some cross-entity checking. 
            //
            //BeforeSaveEntities(saveMap) is called after calling BeforeSaveEntity(entityInfo) for each entity individually.
            //
            //might be a good dispatch point for delegating validation to dedicated helper classes. I doubt that I would put 
            //all of my validation logic in one big ContextProvider class. I don’t mind that all save activity funnels through 
            //the ContextProvider.SaveChanges gate but I don’t want to do all the work in the ContextProvider itself.            


            //TODO: user has permission to save drafts

            //Confirm only one QuestionVersion
            if (!saveMap.ContainsKey(typeof (QuestionVersion)))
            {
                throw new CommandException("Request must contain one QuestionVersion", HttpStatusCode.BadRequest);                
            }

            var questionVersions = saveMap[typeof (QuestionVersion)];
            if (questionVersions.Count != 1)
            {
                throw new CommandException("Request must contain one QuestionVersion", HttpStatusCode.BadRequest);                                
            }

            var questionVersionEi = questionVersions[0];
            var questionVersion = (QuestionVersion)questionVersionEi.Entity;

            questionVersionEi.OriginalValuesMap["State"] = "";
            questionVersion.State = QuestionVersionState.Published;

            //Confirm only one Question
            if (!saveMap.ContainsKey(typeof(Question)))
            {
                throw new CommandException("Request must contain one Question", HttpStatusCode.BadRequest);
            }

            var questions = saveMap[typeof(Question)];
            if (questions.Count != 1)
            {
                throw new CommandException("Request must contain one Question", HttpStatusCode.BadRequest);
            }

            var question = (Question)questions[0].Entity;

            var cleanQuestionVersion = CleanContext.QuestionVersions
                                               .Include(cleanQv => cleanQv.User)
                                               .Include(cleanQv => cleanQv.Question)
                                               .First(cleanQv => ((cleanQv.QuestionId == questionVersion.QuestionId) && (cleanQv.Version == questionVersion.Version)));

            //identity owns this session
            if (cleanQuestionVersion.Question.AccountId != User.AccountId)
            {
                throw new CommandException("User not authorised", HttpStatusCode.Forbidden, questionVersions[0]);
            }

            //TODO: remove this once we have asynch publishing
            if ((cleanQuestionVersion.Question.LatestPublishedVersion == null) ||
                (cleanQuestionVersion.Question.LatestPublishedVersion < questionVersion.Version))
            {
                question.LatestPublishedVersion = questionVersion.Version;
            }
            //TODO: update state on QuestionVersion
            //Check that full object graph has valid foreign keys

            
            return base.BeforeSaveEntities(saveMap);
        }

        #endregion


        private bool BeforeSaveQuestionVersion(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //We dont want any fields to change on a publish
                if (entityInfo.OriginalValuesMap.Keys.Any())
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveQuestion(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //We dont want any fields to change on a publish
                if (entityInfo.OriginalValuesMap.Keys.Any())
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

    }
}