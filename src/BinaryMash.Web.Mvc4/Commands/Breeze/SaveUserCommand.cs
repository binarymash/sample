﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using global::Breeze.WebApi;

    public class SaveUserCommand : BreezeUserSessionCommand
    {

        public SaveUserCommand(IPrincipal principal, BirdhouseContext cleanContext) : base(principal, cleanContext)
        {
        }

        #region Breeze overrides

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            //confirm that you are always updating/deleting a TodoList/TodoItem that belongs to the current user. 
            //
            //It also assigns the current user to a new TodoList. In a real app, this would be a dispatcher to some helper 
            //classes dedicated to validating specific entity types, a point I slightly elaborate below.

            var entityType = entityInfo.Entity.GetType();
            switch (entityType.FullName)
            {
                case ("BinaryMash.Domain.User"):
                    return BeforeSaveUser(entityInfo);
                case ("BinaryMash.Domain.UserPublicProfile"):
                    return BeforeSaveUserPublicProfile(entityInfo);

                default:
                    throw new CommandException("Unsupported entity " + entityInfo.Entity.GetType().Name, HttpStatusCode.BadRequest);
            }
        }

        protected override System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> BeforeSaveEntities(System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> saveMap)
        {
            //gives you a chance to examine the entire change-set at once. 
            //
            //This is a great way to make validate the entire change-set as a whole perhaps to ensure that this save request 
            //makes sense as a single transaction. 
            //
            //It's also a good place to do some cross-entity checking. 
            //
            //BeforeSaveEntities(saveMap) is called after calling BeforeSaveEntity(entityInfo) for each entity individually.
            //
            //might be a good dispatch point for delegating validation to dedicated helper classes. I doubt that I would put 
            //all of my validation logic in one big ContextProvider class. I don’t mind that all save activity funnels through 
            //the ContextProvider.SaveChanges gate but I don’t want to do all the work in the ContextProvider itself.            


            //TODO: user has permission to save drafts

            //Confirm only one User
            if (!saveMap.ContainsKey(typeof (User)))
            {
                throw new CommandException("Request must contain one User", HttpStatusCode.BadRequest);                
            }

            var users = saveMap[typeof (User)];
            if (users.Count != 1)
            {
                throw new CommandException("Request must contain one User", HttpStatusCode.BadRequest);                                
            }

            var userEi = users[0];
            var user = (User) userEi.Entity;

            //Logged in session has permission to change user
            var cleanUser = CleanContext.Users.First(u => u.Id == user.Id);

            //identity owns this user
            if (cleanUser.AccountId != User.AccountId)
            {
                throw new CommandException("User not authorised", HttpStatusCode.Forbidden, userEi);
            }

            //Check that full object graph has valid foreign keys...

            //check that comms channels have correct personId
            if (saveMap.ContainsKey(typeof (UserPublicProfile)))
            {
                var publicProfileEi = saveMap[typeof (UserPublicProfile)][0];
                var publicProfile = (UserPublicProfile) publicProfileEi.Entity;

                //TODO: once we start supporting multiple users per account, we'll need to retrieve the clean public profile to check its ID
                if (publicProfile.Id != cleanUser.PublicProfileId)
                {
                    throw new CommandException("Invalid PersonId on AddressCommunicationChannel", HttpStatusCode.BadRequest, publicProfileEi);
                }
            }
            
            return base.BeforeSaveEntities(saveMap);
        }

        #endregion

        private bool BeforeSaveUser(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is FirstName and LastName
                if (entityInfo.OriginalValuesMap.Keys.Any(k => (
                    (k != "FirstName") && 
                    (k != "LastName"))))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveUserPublicProfile(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is DisplayName
                if (entityInfo.OriginalValuesMap.Keys.Any(k => (k != "DisplayName")))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

    }
}