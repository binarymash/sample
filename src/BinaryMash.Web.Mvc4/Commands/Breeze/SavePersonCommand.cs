﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using global::Breeze.WebApi;

    public class SavePersonCommand : BreezeUserSessionCommand
    {

        public SavePersonCommand(IPrincipal principal, BirdhouseContext cleanContext) : base(principal, cleanContext)
        {
        }

        #region Breeze overrides

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            //confirm that you are always updating/deleting a TodoList/TodoItem that belongs to the current user. 
            //
            //It also assigns the current user to a new TodoList. In a real app, this would be a dispatcher to some helper 
            //classes dedicated to validating specific entity types, a point I slightly elaborate below.

            var entityType = entityInfo.Entity.GetType();
            switch (entityType.FullName)
            {
                case ("BinaryMash.Domain.Person"):
                    return BeforeSavePerson(entityInfo);

                case ("BinaryMash.Domain.EmailChannel"):
                case ("BinaryMash.Domain.PhoneChannel"):
                case ("BinaryMash.Domain.AddressChannel"):
                    return BeforeSaveChannel(entityInfo);

                default:
                    throw new CommandException("Unsupported entity " + entityInfo.Entity.GetType().Name, HttpStatusCode.BadRequest);
            }
        }

        protected override System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> BeforeSaveEntities(System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> saveMap)
        {
            //gives you a chance to examine the entire change-set at once. 
            //
            //This is a great way to make validate the entire change-set as a whole perhaps to ensure that this save request 
            //makes sense as a single transaction. 
            //
            //It's also a good place to do some cross-entity checking. 
            //
            //BeforeSaveEntities(saveMap) is called after calling BeforeSaveEntity(entityInfo) for each entity individually.
            //
            //might be a good dispatch point for delegating validation to dedicated helper classes. I doubt that I would put 
            //all of my validation logic in one big ContextProvider class. I don’t mind that all save activity funnels through 
            //the ContextProvider.SaveChanges gate but I don’t want to do all the work in the ContextProvider itself.            


            //TODO: user has permission to save drafts

            //Confirm only one QuestionVersion
            if (!saveMap.ContainsKey(typeof (Person)))
            {
                throw new CommandException("Request must contain one person", HttpStatusCode.BadRequest);                
            }

            var people = saveMap[typeof (Person)];
            if (people.Count != 1)
            {
                throw new CommandException("Request must contain one person", HttpStatusCode.BadRequest);                                
            }

            var person = (Person) people[0].Entity;
            Person cleanPerson = null;

            if (people[0].EntityState != EntityState.Added)
            {
                cleanPerson = CleanContext.People
                                            .Include("CommunicationChannels")
                                            .First(p => p.Id == person.Id);

                //identity owns this person
                if (cleanPerson.AccountId != User.AccountId)
                {
                    throw new CommandException("User not authorised", HttpStatusCode.Forbidden, people[0]);
                }            
            }

            //Check that full object graph has valid foreign keys...

            //check that comms channels have correct personId
            if (saveMap.ContainsKey(typeof (AddressChannel)))
            {
                var entities = saveMap[typeof (AddressChannel)];
                foreach (var entity in entities)
                {
                    //parent person
                    var channel = (AddressChannel)entity.Entity;
                    if (person.Id != channel.PersonId)
                    {
                        throw new CommandException("Invalid PersonId on AddressCommunicationChannel", HttpStatusCode.BadRequest, entity);
                    }
                }
            }

            if (saveMap.ContainsKey(typeof(EmailChannel)))
            {
                var entities = saveMap[typeof(EmailChannel)];
                foreach (var entity in entities)
                {
                    //parent person
                    var channel = (EmailChannel)entity.Entity;
                    if (person.Id != channel.PersonId)
                    {
                        throw new CommandException("Invalid PersonId on EmailCommunicationChannel", HttpStatusCode.BadRequest, entity);
                    }
                }
            }

            if (saveMap.ContainsKey(typeof(PhoneChannel)))
            {
                var entities = saveMap[typeof(PhoneChannel)];
                foreach (var entity in entities)
                {
                    //parent person
                    var channel = (PhoneChannel)entity.Entity;
                    if (person.Id != channel.PersonId)
                    {
                        throw new CommandException("Invalid PersonId on PhoneCommunicationChannel", HttpStatusCode.BadRequest, entity);
                    }
                }
            }
            
            return base.BeforeSaveEntities(saveMap);
        }

        #endregion

        private bool BeforeSavePerson(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            var person = (Person) entityInfo.Entity;
            person.UserId = User.PublicProfileId;
            person.Updated = DateTime.UtcNow;

            if (entityInfo.EntityState == EntityState.Added)
            {
                person.AccountId = User.AccountId;
                person.Created = DateTime.UtcNow;
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is Title
                if (entityInfo.OriginalValuesMap.Keys.Any(k => ((k != "Name") && (k != "Notes"))))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveChannel(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Added)
            {
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is Name, Value and Type
                if (entityInfo.OriginalValuesMap.Keys.Any(k => (k != "Name" && k != "Value" && k !="Type")))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            if (entityInfo.EntityState == EntityState.Deleted)
            {
                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

    }
}