﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using global::Breeze.WebApi;

    public class SaveTestCommand : BreezeUserSessionCommand
    {

        public SaveTestCommand(IPrincipal principal, BirdhouseContext cleanContext) : base(principal, cleanContext)
        {
        }

        #region Breeze overrides

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            //confirm that you are always updating/deleting a TodoList/TodoItem that belongs to the current user. 
            //
            //It also assigns the current user to a new TodoList. In a real app, this would be a dispatcher to some helper 
            //classes dedicated to validating specific entity types, a point I slightly elaborate below.

            var entityType = entityInfo.Entity.GetType();
            switch (entityType.FullName)
            {
                case ("BinaryMash.Domain.TestVersion"):
                    return BeforeSaveTestVersion(entityInfo);
                case ("BinaryMash.Domain.Test"):
                    return BeforeSaveTest(entityInfo);
                case ("BinaryMash.Domain.QuestionVersionTestVersion"):
                    return BeforeSaveQuestionVersionTestVersion(entityInfo);
                default:
                    throw new CommandException("Unsupported entity " + entityInfo.Entity.GetType().Name, HttpStatusCode.BadRequest);
            }
        }

        protected override System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> BeforeSaveEntities(System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> saveMap)
        {
            //gives you a chance to examine the entire change-set at once. 
            //
            //This is a great way to make validate the entire change-set as a whole perhaps to ensure that this save request 
            //makes sense as a single transaction. 
            //
            //It's also a good place to do some cross-entity checking. 
            //
            //BeforeSaveEntities(saveMap) is called after calling BeforeSaveEntity(entityInfo) for each entity individually.
            //
            //might be a good dispatch point for delegating validation to dedicated helper classes. I doubt that I would put 
            //all of my validation logic in one big ContextProvider class. I don’t mind that all save activity funnels through 
            //the ContextProvider.SaveChanges gate but I don’t want to do all the work in the ContextProvider itself.            

            //TODO: user has permission to save drafts

            var now = DateTime.UtcNow;

            //Confirm only one TestVersion
            if (!saveMap.ContainsKey(typeof (TestVersion)))
            {
                throw new CommandException("Request must contain one TestVersion", HttpStatusCode.BadRequest);                
            }

            var testVersions = saveMap[typeof (TestVersion)];
            if (testVersions.Count != 1)
            {
                throw new CommandException("Request must contain one TestVersion", HttpStatusCode.BadRequest);                                
            }

            var testVersion = testVersions[0];
            var testVersionEntity = (TestVersion) testVersion.Entity;

            if (testVersion.EntityState == EntityState.Added)
            {
                BeforeSaveEntitiesWhenAddingTestVersion(saveMap, testVersionEntity, testVersion, now);
            } 
            else if (testVersion.EntityState == EntityState.Modified)
            {
                BeforeSaveEntitiesWhenModifyingTestVersion(saveMap, testVersionEntity, testVersion, now);
            }
            else
            {
                throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, testVersion);
            }
            
            return base.BeforeSaveEntities(saveMap);
        }

        #endregion

        private void BeforeSaveEntitiesWhenAddingTestVersion(Dictionary<Type, List<EntityInfo>> saveMap, TestVersion testVersionEntity, EntityInfo testVersion, DateTime now)
        {
            //always set these values on a new TestVersion
            testVersionEntity.Created = now;
            testVersionEntity.Updated = now;
            testVersionEntity.UserId = User.PublicProfileId;
            testVersionEntity.State = TestVersionState.Draft;

            //confirm we have a test
            if (!saveMap.ContainsKey(typeof(Test)))
            {
                throw new CommandException("Request must contain one Test", HttpStatusCode.BadRequest);
            }

            var tests = saveMap[typeof(Test)];
            if (tests.Count != 1)
            {
                throw new CommandException("Request must contain one Test", HttpStatusCode.BadRequest);
            }

            var test = tests[0];
            var testEntity = (Test)test.Entity;
            if (test.EntityState == EntityState.Added)
            {
                //always set these values on a new Test
                testEntity.Created = now;
                testEntity.AccountId = User.AccountId;
                testEntity.LatestVersion = testVersionEntity.Version;
                testEntity.UserId = User.PublicProfileId;

                //Check that full object graph has valid foreign keys
            }
            else if (test.EntityState == EntityState.Modified)
            {
                //Adding a new TestVersion to an existing Test
                test.OriginalValuesMap["LatestDraftGuid"] = "";
                testEntity.LatestVersion = testVersionEntity.Version;

                if (testEntity.Id != testVersionEntity.TestId)
                {
                    throw new CommandException("Test and TestVersion must be related", HttpStatusCode.BadRequest);
                }

                //TODO: just get the test not the test version
                var cleanTest = CleanContext.Tests
                                            .Include(t => t.Test)
                                            .Where(t => t.TestId == testEntity.Id)
                                            .Select(t => t.Test)
                                            .First();

                if (cleanTest.LatestPublishedVersion != cleanTest.LatestVersion)
                {
                    //already have an unpublished version for this test    
                    throw new CommandException("Cannot have more than one draft TestVersion for a test",
                                               HttpStatusCode.BadRequest);
                }

                //Check that full object graph has valid foreign keys

            }
            else
            {
                throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, testVersion);
            }
        }

        private void BeforeSaveEntitiesWhenModifyingTestVersion(Dictionary<Type, List<EntityInfo>> saveMap, TestVersion testVersionEntity, EntityInfo testVersion, DateTime now)
        {
            //always set these values when modifying a TestVersion 
            testVersion.OriginalValuesMap["Updated"] = "";
            testVersionEntity.Updated = now;

            testVersion.OriginalValuesMap["UserId"] = "";
            testVersionEntity.UserId = User.PublicProfileId;

            if (saveMap.ContainsKey(typeof(Test)))
            {
                throw new CommandException("Request must not contain a Test", HttpStatusCode.BadRequest);
            }

            //Check that full object graph has valid foreign keys
        }


        private bool BeforeSaveTest(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Added)
            {
                //only expected if also adding a TestVersion with version 1
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only expected if also adding a TestVersion with version > 1

                if (entityInfo.OriginalValuesMap.Keys.Any())
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveTestVersion(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            var test = (TestVersion) entityInfo.Entity;

            if (entityInfo.EntityState == EntityState.Added)
            {
                test.State = TestVersionState.Draft;
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only allow updates if state is draft
                if (test.State != TestVersionState.Draft)
                {
                    return false;
                }

                //only field we allow to be changed is Title
                if (entityInfo.OriginalValuesMap.Keys.Any(k => k != "Name"))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveQuestionVersionTestVersion(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            var qvtv = (QuestionVersionTestVersion)entityInfo.Entity;

            if (entityInfo.EntityState == EntityState.Added)
            {
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is QuestionVersionVersion
                if (entityInfo.OriginalValuesMap.Keys.Any(k => k != "QuestionVersionVersion" && k != "Index"))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            if (entityInfo.EntityState == EntityState.Deleted)
            {
                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

    }
}