﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using System.Data.Entity;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using global::Breeze.WebApi;

    public class PublishTestCommand : BreezeUserSessionCommand
    {

        public PublishTestCommand(IPrincipal principal, BirdhouseContext cleanContext) : base(principal, cleanContext)
        {
        }

        #region Breeze overrides

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            //confirm that you are always updating/deleting a TodoList/TodoItem that belongs to the current user. 
            //
            //It also assigns the current user to a new TodoList. In a real app, this would be a dispatcher to some helper 
            //classes dedicated to validating specific entity types, a point I slightly elaborate below.

            var entityType = entityInfo.Entity.GetType();
            switch (entityType.FullName)
            {
                case ("BinaryMash.Domain.TestVersion"):
                    return BeforeSaveTestVersion(entityInfo);
                case ("BinaryMash.Domain.Test"):
                    return BeforeSaveTest(entityInfo);
                default:
                    throw new CommandException("Unsupported entity " + entityInfo.Entity.GetType().Name, HttpStatusCode.BadRequest);
            }
        }

        protected override System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> BeforeSaveEntities(System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> saveMap)
        {
            //gives you a chance to examine the entire change-set at once. 
            //
            //This is a great way to make validate the entire change-set as a whole perhaps to ensure that this save request 
            //makes sense as a single transaction. 
            //
            //It's also a good place to do some cross-entity checking. 
            //
            //BeforeSaveEntities(saveMap) is called after calling BeforeSaveEntity(entityInfo) for each entity individually.
            //
            //might be a good dispatch point for delegating validation to dedicated helper classes. I doubt that I would put 
            //all of my validation logic in one big ContextProvider class. I don’t mind that all save activity funnels through 
            //the ContextProvider.SaveChanges gate but I don’t want to do all the work in the ContextProvider itself.            

            //TODO: user has permission to save drafts

            //Confirm only one TestVersion
            if (!saveMap.ContainsKey(typeof (TestVersion)))
            {
                throw new CommandException("Request must contain one TestVersion", HttpStatusCode.BadRequest);                
            }

            var testVersions = saveMap[typeof (TestVersion)];
            if (testVersions.Count != 1)
            {
                throw new CommandException("Request must contain one TestVersion", HttpStatusCode.BadRequest);                                
            }

            var testVersionEi = testVersions[0];
            var testVersion = (TestVersion)testVersionEi.Entity;

            testVersionEi.OriginalValuesMap["Published"] = "";
            testVersion.State = TestVersionState.Published;

            //Confirm only one Test
            if (!saveMap.ContainsKey(typeof(Test)))
            {
                throw new CommandException("Request must contain one Test", HttpStatusCode.BadRequest);
            }

            var tests = saveMap[typeof(Test)];
            if (tests.Count != 1)
            {
                throw new CommandException("Request must contain one Test", HttpStatusCode.BadRequest);
            }

            var testEi = tests[0];
            var test = (Test)testEi.Entity;

            var cleanTestVersion = CleanContext.Tests
                                               .Include(cleanTv => cleanTv.User)
                                               .Include(cleanTv => cleanTv.Test)
                                               .First(cleanTv => ((cleanTv.TestId == testVersion.TestId) && (cleanTv.Version == testVersion.Version)));

            //identity owns this session
            if (cleanTestVersion.Test.AccountId != User.AccountId)
            {
                throw new CommandException("User not authorised", HttpStatusCode.Forbidden, testVersions[0]);
            }

            //TODO: remove this once we have asynch publishing
            if ((cleanTestVersion.Test.LatestPublishedVersion == null) || 
                (cleanTestVersion.Test.LatestPublishedVersion < testVersion.Version))
            {
                test.LatestPublishedVersion = testVersion.Version;
            }

            //TODO: update state on TestVersion
            //Check that full object graph has valid foreign keys
            
            return base.BeforeSaveEntities(saveMap);
        }

        #endregion

        private bool BeforeSaveTestVersion(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //We dont want any fields to change on a publish
                if (entityInfo.OriginalValuesMap.Keys.Any())
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveTest(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //We dont want any fields to change on a publish
                if (entityInfo.OriginalValuesMap.Keys.Any())
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

    }
}