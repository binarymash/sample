﻿namespace BinaryMash.Web.Mvc4.Commands.Breeze
{
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using BinaryMash.Domain;
    using ContextProviders;
    using global::Breeze.WebApi;

    public class SaveCandidateQuestionAnswersCommand : BreezeSessionCommand
    {

        public SaveCandidateQuestionAnswersCommand(IPrincipal principal, BirdhouseContext cleanContext)
            : base(principal, cleanContext)
        {
        }

        #region Breeze overrides

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            //confirm that you are always updating/deleting a TodoList/TodoItem that belongs to the current user. 
            //
            //It also assigns the current user to a new TodoList. In a real app, this would be a dispatcher to some helper 
            //classes dedicated to validating specific entity types, a point I slightly elaborate below.

            var entityType = entityInfo.Entity.GetType();
            switch (entityType.FullName)
            {
                case ("BinaryMash.Domain.CandidateQuestionVersionAnswers"):
                    return BeforeSaveCandidateQuestionVersionAnswers(entityInfo);

                case ("BinaryMash.Domain.CandidateSectionAnswers"):
                    return BeforeSaveCandidateSectionAnswers(entityInfo);

                case ("BinaryMash.Domain.CandidateAnswer"):
                    return BeforeSaveCandidateAnswer(entityInfo);

                default:
                    throw new CommandException("Unsupported entity " + entityInfo.Entity.GetType().Name, HttpStatusCode.BadRequest);
            }
        }

        protected override System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> BeforeSaveEntities(System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<EntityInfo>> saveMap)
        {
            //gives you a chance to examine the entire change-set at once. 
            //
            //This is a great way to make validate the entire change-set as a whole perhaps to ensure that this save request 
            //makes sense as a single transaction. 
            //
            //It's also a good place to do some cross-entity checking. 
            //
            //BeforeSaveEntities(saveMap) is called after calling BeforeSaveEntity(entityInfo) for each entity individually.
            //
            //might be a good dispatch point for delegating validation to dedicated helper classes. I doubt that I would put 
            //all of my validation logic in one big ContextProvider class. I don’t mind that all save activity funnels through 
            //the ContextProvider.SaveChanges gate but I don’t want to do all the work in the ContextProvider itself.            

            //TODO: user has permission to save drafts
            //TODO: implement security stuff

            //Confirm only one QuestionVersion
            if (!saveMap.ContainsKey(typeof (CandidateQuestionVersionAnswers)))
            {
                throw new CommandException("Request must contain one CandidateQuestionVersionAnswers", HttpStatusCode.BadRequest);                
            }

            var cqvs = saveMap[typeof (CandidateQuestionVersionAnswers)];
            if (cqvs.Count != 1)
            {
                throw new CommandException("Request must contain one CandidateQuestionVersionAnswers", HttpStatusCode.BadRequest);                                
            }

            var cqva = cqvs[0];
            var cqvaEntity = (CandidateQuestionVersionAnswers)cqva.Entity;

            //session must exist and be accessible to the user identity
            if (Session.Id != cqvaEntity.SessionId)
            {
                throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, cqva);                
            }

            //if (cqva.EntityState == EntityState.Added)
            //{
            //    BeforeSaveEntitiesWhenAddingQuestionVersion(saveMap, cqvEntity, cqv, now);
            //}
            //else if (cqv.EntityState == EntityState.Modified)
            //{
            //    BeforeSaveEntitiesWhenModifyingQuestionVersion(saveMap, cqvEntity, cqv, now);
            //}
            //else
            //{
            //    throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, cqv);
            //}

            return base.BeforeSaveEntities(saveMap);
        }

        #endregion


        private bool BeforeSaveCandidateQuestionVersionAnswers(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Added)
            {
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                if (entityInfo.OriginalValuesMap.Keys.Any())
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveCandidateSectionAnswers(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities
            
            if (entityInfo.EntityState == EntityState.Added)
            {
                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

        private bool BeforeSaveCandidateAnswer(EntityInfo entityInfo)
        {
            //Ownership: implicitly checked via the aggregate root and validation of graph in BeforeSaveEntities
            //FKs: will be checked in BeforeSaveEntities

            if (entityInfo.EntityState == EntityState.Added)
            {
                return true;
            }

            if (entityInfo.EntityState == EntityState.Modified)
            {
                //only field we allow to be changed is Text
                if (entityInfo.OriginalValuesMap.Keys.Any(k => (k != "Text")))
                {
                    throw new CommandException("Fields cannot be modified", HttpStatusCode.BadRequest, entityInfo);
                }

                return true;
            }

            if (entityInfo.EntityState == EntityState.Deleted)
            {
                return true;
            }

            throw new CommandException("Unsupported EntityState", HttpStatusCode.BadRequest, entityInfo);
        }

    }
}