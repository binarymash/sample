﻿namespace BinaryMash.Web.Mvc4.Commands.WebApi
{
    using System.Net;
    using System.Net.Http;
    using System.Security.Principal;
    using System.Web.Http;
    using BinaryMash.Core.Security.Interfaces;
    using BinaryMash.DataAccess.Repositories.Contract;
    using BinaryMash.Domain;

    public class ChangePasswordCommand : WebApiCommand
    {
        public ChangePasswordCommand(IPrincipal principal, ILoginRepository loginRepository, IHashingService hashingService, PasswordChange passwordChange) : base(principal)
        {
            _principal = principal;
            _loginRepository = loginRepository;
            _hashingService = hashingService;
            _passwordChange = passwordChange;
        }

        public override HttpResponseMessage Run()
        {
            if (!_principal.Identity.IsAuthenticated)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            if (!IdentityConverter.IsUser(_principal.Identity.Name))
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);                
            }

            //confirm old password
            var login = _loginRepository.GetUser(_principal.Identity);
            if (login == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            var validateRequest = new ValidateRequest
            {
                Hash = login.HashedPassword,
                PlainText = _passwordChange.CurrentPassword,
                SaltSeed = login.Id.ToString()
            };

            var validateResponse = _hashingService.Validate(validateRequest);
            if (!validateResponse.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            var hashRequest = new HashRequest
                {
                    PlainText = _passwordChange.NewPassword,
                    SaltSeed = IdentityConverter.ExtractUserId(_principal.Identity.Name).ToString()
                };
            
            var hashResponse = _hashingService.Hash(hashRequest);
            login.HashedPassword = hashResponse.Hash;
            login.Salt = hashResponse.Salt;
            _loginRepository.SaveChanges();

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        private readonly PasswordChange _passwordChange;
        private readonly IPrincipal _principal;
        private readonly ILoginRepository _loginRepository;
        private readonly IHashingService _hashingService;
    }
}