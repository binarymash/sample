﻿namespace BinaryMash.Web.Mvc4.Commands.WebApi
{
    using System.Net;
    using System.Net.Http;
    using System.Transactions;
    using System.Web.Http;

    public static class UnitOfWork
    {

        public static HttpResponseMessage Run(WebApiCommand command)
        {
            try
            {
                HttpResponseMessage result;
                using (var transactionScope = new TransactionScope())
                {
                    result = command.Run();
                    transactionScope.Complete();
                }
                return result;
            }
            catch (HttpResponseException ex)
            {
                return new HttpResponseMessage(ex.Response.StatusCode);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }

}