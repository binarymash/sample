﻿namespace BinaryMash.Web.Mvc4.Commands.WebApi
{
    using System.Net.Http;
    using System.Security.Principal;
    using BinaryMash.DataAccess.EF;
    using global::Breeze.WebApi.EF;

    public abstract class WebApiCommand : EFContextProvider<BirdhouseContext>
    {
        protected IPrincipal Principal { get; private set; }

        public abstract HttpResponseMessage Run();

        protected WebApiCommand(IPrincipal pricipal)
        {
            Principal = pricipal;
        }

    }
}