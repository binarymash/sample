﻿define(['services/prettify'], function(prettify) {

    var vm = {
        init: init
    };
    
    return vm;
    
    function init() {
        prettify.run();
    }
    
})