﻿define([
    'jquery',
    'ko',
    'Q',
    'services/dataContext',
    'services/answerTypes',
    'initializers/QuestionVersion/createEdit',
    'initializers/QuestionAndAnswerSection/createEdit',
    'initializers/AnswerContentUserSupplied/createEdit',
    'initializers/AnswerContentQuestionerSupplied/createEdit',
    'services/notifier',
    'validation/integerRangeBreezeValidator',
    'components/asyncTracker'],

    function ($, ko, Q, dataContext, answerTypes,
        questionInitializer, sectionInitializer, userSuppliedInitializer,
        questionerSuppliedInitializer, notifier, integerRange, asyncTracker) {

        var validator = integerRange("AnswerContentUserSupplied", "MaxLength", "'Maximum Length' must be between 1 and 4096", 1, 4096);
        var validators = [validator];
        
        dataContext.manager.metadataStore.registerEntityTypeCtor("QuestionVersion", null, questionInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("QuestionAndAnswerSection", null, sectionInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("AnswerContentUserSupplied", null, userSuppliedInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("AnswerContentQuestionerSupplied", null, questionerSuppliedInitializer);
    
        var vm = {        
            id: ko.observable(),
            antiForgeryToken:ko.observable(),
            question: ko.observable(),
            answerTypes: ko.observableArray(answerTypes),
            initialising: ko.observable(true),
            loadingData: ko.observable(true),
            asyncTracker: asyncTracker,
            referrer: ko.observable('Index'),
            cancelling : ko.observable(false),
            saving: ko.observable(false),
            publishing: ko.observable(false),

            init: init,
            save: save,
            done: done,
            publish: publish,
            cancel:cancel
        };

        vm.disableBtns = ko.computed(function() {
            return vm.cancelling() || vm.saving() || vm.publishing();
        });
        
        return vm;   
        
        function init() {
            vm.id(parseInt($("#id")[0].value));
            vm.antiForgeryToken($("input[name=__RequestVerificationToken]")[0].value);
            ko.applyBindings(vm);
            dataContext.init(validators, vm.antiForgeryToken())
                .then(function() {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoaded", "");
                    initData();
                })
                .fail(function() {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
        }

        function initData() {
            vm.loadingData(true);
            return Q.fcall(function() {
                if (vm.id() === 0) {
                    createData();
                    return null;
                }
                return loadData();
            }).then(function() {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoaded", "");
            }).fail(function(err) {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoadedFailed", "");
            });
        }
        
        function createData() {
            vm.question(dataContext.questionVersions.create());
        }

        function loadData() {
            return dataContext.questionVersions.getLatest(vm.id(), vm.question)
                .then(function() {
                    if (vm.question().State() !== 'Draft') {
                        var newQuestionVersion = vm.question().clone();
                        vm.question(newQuestionVersion);
                    }
                });
        }                         
                
        function save() {
            saveImpl(saveSucceeded, saveFailed);
        }
        
        function done() {
            saveImpl(doneAfterSaveSucceeded, saveFailed);
        }
        
        function saveImpl(succeeded, failed) {
            vm.saving(true);
            if (!dataContext.questionVersions.save(vm.question, succeeded, failed)) {
                vm.saving(false);
                succeeded();
            };
        }
        
        function saveSucceeded() {
            vm.id(vm.question().QuestionId());
            vm.saving(false);
            asyncTracker.push("SaveSucceeded");
            notifier.success("Saved a draft of the question");
        }
        
        function doneAfterSaveSucceeded() {
            $("#publishModal").modal("show");
        }
                
        function saveFailed(error) {
            vm.saving(false);
            asyncTracker.push("SaveFailed");
            if (error.status < 200 || error.status > 299) {
                notifier.error("An error occurred when saving the question. Your changes have NOT been saved");
            } else if (error.entityErrors && error.entityErrors.length !== 0) {
                notifier.error("There are validation errors - please fix these. Your changes have NOT been saved");
            } else {
                notifier.error("An error occurred when saving the question. Your changes have NOT been saved");
            }
        }
        
        function publish() {
            vm.publishing(true);
            if (!dataContext.questionVersions.publish(vm.question, publishSucceeded, publishFailed)) {
                vm.publishing(false);
            }
        }

        function publishSucceeded() {
            return window.location.href = "/Question/Published/" + vm.question().QuestionId();
        }

        function publishFailed(error) {
            vm.publishing(false);
            asyncTracker.push("PublishFailed");
            if (error.entityErrors.length !== 0) {
                notifier.error("There are validation errors - please fix these. Your changes have NOT been saved");
            } else {
                notifier.error("An error occurred when publishing '" + vm.question().Title() + "'. Your changes have NOT been saved");
            }
        }

        function cancel() {
            vm.cancelling(true);
            
            if (vm.question().QuestionId() > 0) {
                return document.location.href = "/Question/Preview/" + vm.question().QuestionId();
            }

            return window.location.href = "/Question";
        }

    }

);