﻿define(['ko',
    'services/dataContext',
    'initializers/QuestionVersion/summary',
    'services/notifier',
    'components/asyncTracker'],

    function (ko, dataContext, questionVersionInitializer, notifier, asyncTracker) {

        var validators = [];

        dataContext.manager.metadataStore.registerEntityTypeCtor("QuestionVersion", null, questionVersionInitializer);

        var vm = {
            id: ko.observable(),
            question: ko.observable(),
            initialising: ko.observable(true),
            asyncTracker: asyncTracker,

            init: init,
        };
        
        return vm;
    
        function init() {
            dataContext.init(validators)
                .then(function () {
                    loadQuestion();
                })
                .fail(function () {
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
            vm.initialising(false);
        }
        
        function loadQuestion() {
            vm.id(parseInt($("#id")[0].value));
            dataContext.questionVersions.getLatest(vm.id(), vm.question)
            .then(function () {                
                ko.applyBindings(vm);
                vm.asyncTracker.push("ContentLoaded", "");
            })
            .fail(function (errors) {
                vm.asyncTracker.push("ContentLoadFailed", "");
            });
        }        
    }
)