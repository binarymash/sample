﻿define(['jquery', 'ko', 'services/dataContext', 'components/asyncTracker'],
    function ($, ko, dataContext, asyncTracker) {

    var vm = {
        email: ko.observable(),
        initialising:ko.observable(true),
        activating: ko.observable(true),
        activated: ko.observable(false),
        activationAttempt: ko.observable(0),
        timedOut:ko.observable(false),
        asyncTracker: asyncTracker,
        maxAttempts: 6,
        speed:5000,

        init: init,
        activate: activate,
        authenticate: authenticate
    };

    vm.throttledActivationAttempt = ko.computed(vm.activationAttempt).extend({ throttle: vm.speed });
    vm.throttledActivationAttempt.subscribe(checkActivation);
                
    return vm;
    
    function init() {
        vm.email($("#email")[0].value);
        ko.applyBindings(vm);
        dataContext.init([])
            .then(function () {
                vm.initialising(false);
                vm.asyncTracker.push("MetadataLoaded", "");
                activate();
            })
            .fail(function () {
                vm.initialising(false);
                vm.asyncTracker.push("MetadataLoadFailed", "");
            });
    }
    
    function activate() {
        vm.timedOut(false);
        vm.activating(true);
        vm.activationAttempt(1);
    }
        
    function checkActivation() {
        if (!vm.activated() && !vm.timedOut()) {
            if (dataContext.users.checkEmailIsAvailable(vm.email())) {
                if (vm.activationAttempt() === vm.maxAttempts) {
                    vm.timedOut(true);
                    vm.activating(false);
                    vm.asyncTracker.push("TimedOut", "");
                } else {
                    vm.activationAttempt(vm.activationAttempt() + 1);
                }
            } else {
                vm.activated(true);
                vm.activating(false);
                vm.asyncTracker.push("Activated", "");
            }
        }
    }
                
    function authenticate() {
        return window.location.href = "/Authentication?email=" + vm.email();
    }
    
});