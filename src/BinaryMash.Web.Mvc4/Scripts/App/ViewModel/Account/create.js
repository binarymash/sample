﻿define(['jquery', 'ko', 'validation/manager', 'validation/booleanRequiredValidator'],
    function ($, ko, validation, booleanRequired) {

    var vm = {
        init: init
    };

    ko.applyBindings(vm);
    
    return vm;
    
    function init() {
        initValidation();
    }
    
    function initValidation() {
        validation.init($("#accountForm"), [booleanRequired]);
    }
    
});