﻿define(['ko','services/dataContext','initializers/Session/summary','components/asyncTracker'],

    function (ko, dataContext, sessionInitializer, asyncTracker) {

        dataContext.manager.metadataStore.registerEntityTypeCtor("Session", null, sessionInitializer);

        var vm = {
            id: ko.observable(),
            session: ko.observable(),
            initialising: ko.observable(true),
            asyncTracker: asyncTracker,

            init: init,
        };
        
        return vm;
    
        function init() {
            dataContext.init([])
                .then(function () {
                    vm.id($("#Id")[0].value);
                    loadSession();
                    vm.initialising(false);
                })
                .fail(function () {
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                    vm.initialising(false);
                });
        }
        
        function loadSession() {
            dataContext.sessions.get(vm.id(), vm.session)
                .then(function () {
                    ko.applyBindings(vm);
                    vm.asyncTracker.push("ContentLoaded", "");
                })
                .fail(function() {
                    vm.asyncTracker.push("ContentLoadFailed", "");
                });           
        }        
    }
)