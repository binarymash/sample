﻿define([
    'jquery', 'ko', 'Q', 'services/dataContext', 'services/notifier',
    'initializers/QuestionVersion/answers', 'initializers/QuestionAndAnswerSection/answers',
    'initializers/AnswerContentUserSupplied/answers', 'initializers/AnswerContentQuestionerSupplied/answers',
    'initializers/CandidateQuestionAnswers/view', 'initializers/CandidateSectionAnswers/view',
    'components/asyncTracker',
    'services/answerMerger',
    'services/prettify'],

    function ($, ko, Q, dataContext, notifier,
        questionInitializer, sectionInitializer,
        userAnswerInitializer, questionerAnswerInitializer,
        cqaInitializer, csaInitializer,
        asyncTracker, mergeAnswers, prettify) {

        var validators = [];

        dataContext.manager.metadataStore.registerEntityTypeCtor("QuestionVersion", null, questionInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("QuestionAndAnswerSection", null, sectionInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("AnswerContentUserSupplied", null, userAnswerInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("AnswerContentQuestionerSupplied", null, questionerAnswerInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("CandidateQuestionVersionAnswers", null, cqaInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("CandidateSectionAnswers", null, csaInitializer);

        var vm = {
            sessionId: ko.observable(),
            session: ko.observable(),
            test: ko.observable(),
            questionIndex : ko.observable(0),
            question: ko.observable(),
            questionAnswer: ko.observable(),
            
            initialising: ko.observable(true),
            loadingData: ko.observable(true),
            loadingQuestion: ko.observable(true),
            asyncTracker : asyncTracker,
            nextQActive: ko.observable(false),
            previousQActive: ko.observable(false),
            error: ko.observable(),

            init: init,
            nextQ: nextQ,
            previousQ: previousQ,
            done:done,
            
            makePretty:makePretty
        };
        
        vm.questionIndex.subscribe(function(questionIndex) {
            loadQuestion(questionIndex);
        });

        vm.questionIndexDisplay = ko.computed(function() {
            return vm.questionIndex() + 1;
        });
        
        vm.prevQuestionIndex = ko.computed(function() {
            return vm.questionIndex() - 1;
        });

        vm.prevQuestionIndexDisplay = ko.computed(function() {
            return vm.prevQuestionIndex() + 1;
        });
        
        vm.nextQuestionIndex = ko.computed(function() {
            return vm.questionIndex() + 1;
        });

        vm.nextQuestionIndexDisplay = ko.computed(function() {
            return vm.nextQuestionIndex() + 1;
        });
        
        vm.lastQuestionIndex = ko.computed(function() {
            if (!vm.test()) {
                return -1;
            }

            return vm.test().QuestionVersions().length - 1;
        });

        vm.allowPrevious = ko.computed(function() {
            return vm.questionIndex() > 0;
        });

        vm.allowNext = ko.computed(function() {
            return vm.questionIndex() < (vm.lastQuestionIndex());
        });
                      
        return vm;   
        
        function init() {
            vm.sessionId($("#sessionId")[0].value);
            ko.applyBindings(vm);
            dataContext.init(validators)
                .then(function() {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoaded", "");
                    return initData();
                })
                .fail(function (error) {
                    vm.question();
                    vm.error(error);
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
        }

        function initData() {
            vm.loadingData(true);
            return Q.fcall(function() {
                return loadData();
            }).then(function() {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoaded", "");
            }).fail(function(error) {
                vm.question();
                vm.error(error);
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoadedFailed", "");
                throw new Error(error);
            });
        }
        
        function loadData() {
            return dataContext.sessions.getWithQuestions(vm.sessionId(), vm.session)
                .then(function () {
                    vm.test(vm.session().TestVersion());
                    return loadQuestion(vm.questionIndex());
                });
        }       
                   
        function loadQuestion(questionIndex) {
            if (questionIndex < 0) {
                vm.questionIndex(0);
            }
            
            if (questionIndex > (vm.lastQuestionIndex() + 1)) {
                vm.questionIndex(vm.lastQuestionIndex() + 1);
            }
            
            if (questionIndex < (vm.lastQuestionIndex() + 1)) {
                vm.loadingQuestion(true);

                var qvtv = vm.test().QuestionVersions()[questionIndex];
                if (!qvtv) {
                    vm.loadingQuestion(false);
                    return false;
                }

                return dataContext.questionVersions.get(qvtv.QuestionId(), qvtv.QuestionVersionVersion(), vm.question, "Sections.AnswerContents")
                    .then(function () {
                        return loadAnswers(vm.question());
                    })
                    .then(function () {
                        return loadQuestionSucceeded();
                    })
                    .fail(function (err) {
                        loadQuestionFailed(err);
                    });
            }
            
            vm.question();            
            return loadQuestionSucceeded();
        }

        function loadAnswers(qv) {
            return dataContext.candidateAnswers.get(vm.sessionId(), qv.QuestionId(), qv.Version(), vm.questionAnswer)
                .then(function () {
                    mergeAnswers(vm.sessionId(), qv, vm.questionAnswer());
                });
        }

                
        function loadQuestionSucceeded() {
            asyncTracker.push("LoadQuestionSucceeded");
            vm.loadingQuestion(false);
        }

        function loadQuestionFailed(error) {
            vm.question();
            vm.error(error);
            asyncTracker.push("LoadQuestionFailed");
            vm.loadingQuestion(false);
        }

        function nextQ() {
            if (vm.loadingQuestion()) {
                return;
            }

            if (vm.questionIndex() > vm.lastQuestionIndex()) {
                return;
            }
            
            vm.questionIndex(vm.nextQuestionIndex());
        }

        function previousQ() {
            if (vm.loadingQuestion()) {
                return;
            }

            if (vm.questionIndex() < 0) {
                return;
            }

            vm.questionIndex(vm.prevQuestionIndex());
        }
        
        function makePretty(dom, model) {
            prettify.run();
        }
        
        function done() {
            return document.location.href = "/Session/Summary/" + vm.sessionId();
        }
    }

);