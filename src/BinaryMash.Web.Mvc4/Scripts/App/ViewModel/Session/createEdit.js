﻿define(['ko',
    'services/dataContext',
    'services/guid',
    'initializers/Session/createEdit',
    'services/notifier',
    'components/asyncTracker',
    'components/koSelect2',
    'validation/integerGtBreezeValidator'],

    function (ko, dataContext, guid, sessionInitializer, notifier, asyncTracker, select2, integerGt) {

        var testVal = integerGt("Session", "TestId", "You must select a test", 1);
        var testVerVal = integerGt("Session", "TestVersionVersion", "You must select the test version", 1);
        var personVal = integerGt("Session", "PersonId", "You must select a candidate", 1);
        var validators = [testVal, testVerVal, personVal];

        dataContext.manager.metadataStore.registerEntityTypeCtor("Session", null, sessionInitializer);

        var vm = {
            id: ko.observable(),
            antiForgeryToken:ko.observable(),

            session: ko.observable(),
            initialising: ko.observable(true),
            loadingData: ko.observable(true),
            asyncTracker: asyncTracker,
            referrer: ko.observable('Index'),
            cancelling : ko.observable(false),
            saving: ko.observable(false),
            publishing: ko.observable(false),

            init: init,
            save: save,
            done: done,
            cancel:cancel
        };

        vm.disableBtns = ko.computed(function () {
            return vm.cancelling() || vm.saving() || vm.publishing();
        });

        return vm;

        function init() {
            vm.id($("#id")[0].value);
            vm.antiForgeryToken($("input[name=__RequestVerificationToken]")[0].value);

            ko.applyBindings(vm);
            dataContext.init(validators, vm.antiForgeryToken())
                .then(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoaded", "");
                    initData();
                })
                .fail(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
        }

        function initData() {
            vm.loadingData(true);
            return Q.fcall(function () {
                if (vm.id() === guid.empty) {
                    createData();
                    return null;
                }
                return loadData();
            }).then(function () {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoaded", "");
            }).fail(function () {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoadedFailed", "");
            });
        }

        function createData() {
            var testId = $("#testId")[0].value;
            var personId = $("#personId")[0].value;
            vm.session(dataContext.sessions.create(testId, personId));
            return vm.session().loadDependencies();
        }

        function loadData() {
            return dataContext.sessions.get(vm.id(), vm.session)
                .then(function () {
                    if (vm.session()) {
                        return dataContext.testVersions.getPublishedVersions(vm.session().TestId(), vm.session().versions);
                    }
                });
        }

        function save() {
            saveImpl(saveSucceeded, saveFailed);
        }

        function done() {
            saveImpl(doneAfterSaveSucceeded, saveFailed);
        }

        function saveImpl(succeeded, failed) {
            vm.saving(true);
            if (!dataContext.sessions.save(vm.session, succeeded, failed)) {
                vm.saving(false);
                succeeded();
            };
        }

        function saveSucceeded() {
            vm.id(vm.session().Id());
            vm.saving(false);
            asyncTracker.push("SaveSucceeded");
            notifier.success("The session has been saved");
        }

        function doneAfterSaveSucceeded() {
            return document.location.href = "/Session/Summary/" + vm.session().Id();
        }

        function saveFailed(error) {
            vm.saving(false);
            asyncTracker.push("SaveFailed");
            if (error.status < 200 || error.status > 299) {
                notifier.error("An error occurred when saving the session. Your changes have NOT been saved");
            } else if (error.entityErrors && error.entityErrors.length !== 0) {
                notifier.error("There are validation errors - please fix these. Your changes have NOT been saved");
            } else {
                notifier.error("An error occurred when saving the session. Your changes have NOT been saved");
            }
        }
        
        function cancel() {
            vm.cancelling(true);
            
            if (vm.session().Id() > 0) {
                return document.location.href = "/Session/Summary/" + vm.session().Id();
            }

            return document.location.href = "/Session";
        }

    }
)