﻿define(['ko',
    'services/dataContext',
    'services/uiUtils',
    'initializers/TestVersion/createEdit',
    'initializers/QuestionVersion/search',
    'initializers/QuestionVersionTestVersion/createEdit',
    'services/notifier',
    'services/guid',
    'components/asyncTracker'],

    function (ko, dataContext, uiUtils, tvInitializer, qvInitializer, qvtvInitializer, notifier, guid, asyncTracker) {

        var validators = [];

        dataContext.manager.metadataStore.registerEntityTypeCtor("TestVersion", null, tvInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("QuestionVersion", null, qvInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("QuestionVersionTestVersion", null, qvtvInitializer);

        var vm = {
            id: ko.observable(),
            antiForgeryToken:ko.observable(),
            test: ko.observable(),
            initialising: ko.observable(true),
            loadingData: ko.observable(true),
            asyncTracker: asyncTracker,
            referrer: ko.observable('Index'),
            cancelling: ko.observable(false),
            saving: ko.observable(false),
            publishing: ko.observable(false),

            init: init,
            save: save,
            done: done,
            publish: publish,
            cancel: cancel
        };
        
        vm.disableBtns = ko.computed(function () {
            return vm.cancelling() || vm.saving() || vm.publishing();
        });

        return vm;
    
        function init() {
            vm.id(parseInt($("#id")[0].value));
            vm.antiForgeryToken($("input[name=__RequestVerificationToken]")[0].value);
            
            ko.applyBindings(vm);
            dataContext.init(validators, vm.antiForgeryToken())
                .then(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoaded", "");
                    initData();
                })
                .fail(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
        }

        function initData() {
            vm.loadingData(true);
            return Q.fcall(function () {
                if (vm.id() === 0) {
                    createData();
                    return null;
                }
                return loadData();
            }).then(function () {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoaded", "");
            }).fail(function () {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoadedFailed", "");
            });
        }

        function createData() {
            vm.test(dataContext.testVersions.create());
        }
        
        function loadData() {
            return dataContext.testVersions.get(vm.id(), vm.test, ["Test","QuestionVersions.QuestionVersion"])
                .then(function() {
                    if (vm.test().State() !== 'Draft') {
                        var newTestVersion = vm.test().clone();
                        vm.test(newTestVersion);
                    }
                });
        }
                
        function save() {
            saveImpl(saveSucceeded, saveFailed);
        }

        function done() {
            saveImpl(doneAfterSaveSucceeded, saveFailed);
        }

        function saveImpl(succeeded, failed) {
            vm.saving(true);
            if (!dataContext.testVersions.save(vm.test, succeeded, failed)) {
                vm.saving(false);
                succeeded();
            };
        }

        function saveSucceeded() {
            vm.id(vm.test().TestId());
            vm.saving(false);
            asyncTracker.push("SaveSucceeded");
            notifier.success("Saved a draft of the test");
        }

        function doneAfterSaveSucceeded() {
            $("#publishModal").modal("show");
        }

        function saveFailed(error) {
            vm.saving(false);
            asyncTracker.push("SaveFailed");
            if (error.status < 200 || error.status > 299) {
                notifier.error("An error occurred when saving " + vm.test().Name() + ". Your changes have NOT been saved");
            } else if (error.entityErrors && error.entityErrors.length !== 0) {
                notifier.error("There are validation errors - please fix these. Your changes have NOT been saved");
            } else {
                notifier.error("An error occurred when saving " + vm.test().Name() + ". Your changes have NOT been saved");
            }
        }
        
        function publish() {
            vm.publishing(true);
            if (!dataContext.testVersions.publish(vm.test, publishSucceeded, publishFailed)) {
                vm.publishing(false);
            }
        }

        function publishSucceeded() {
            return window.location.href = "/Test/Published/" + vm.test().TestId();
        }

        function publishFailed(error) {
            vm.publishing(false);
            asyncTracker.push("PublishFailed");
            if (error.entityErrors.length !== 0) {
                notifier.error("There are validation errors - please fix these. Your changes have NOT been saved");
            } else {
                notifier.error("An error occurred when publishing " + vm.test().Name() + ". Your changes have NOT been saved");
            }
        }
        
        function cancel() {
            vm.cancelling(true);

            if (vm.test().TestId() > 0) {
                return window.location.href = "/Test/Summary/" + vm.test().TestId();
            }

            return window.location.href = "/Test";
        }

    }
)