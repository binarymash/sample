﻿define(['ko',
    'services/dataContext',
    'initializers/TestVersion/summary',
    'services/notifier',
    'components/asyncTracker'],

    function (ko, dataContext, testVersionInitializer, notifier, asyncTracker) {

        var validators = [];

        dataContext.manager.metadataStore.registerEntityTypeCtor("TestVersion", null, testVersionInitializer);

        var vm = {
            id: ko.observable(),
            test: ko.observable(),
            initialising: ko.observable(true),
            asyncTracker: asyncTracker,

            init: init,
        };
        
        return vm;
    
        function init() {
            dataContext.init(validators)
                .then(function () {
                    loadTest();
                })
                .fail(function () {
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
            vm.initialising(false);
        }
        
        function loadTest() {
            vm.id(parseInt($("#id")[0].value));
            dataContext.testVersions.get(vm.id(), vm.test)
            .then(function () {                
                ko.applyBindings(vm);
                vm.asyncTracker.push("ContentLoaded", "");
            })
            .fail(function (errors) {
                vm.asyncTracker.push("ContentLoadFailed", "");
            });
        }        
    }
)