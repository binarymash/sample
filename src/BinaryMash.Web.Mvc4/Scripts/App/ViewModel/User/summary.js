﻿define(['ko',
    'services/dataContext',
    'services/notifier',
    'initializers/User/summary',
    'components/asyncTracker'],

    function (ko, dataContext, notifier, userInitializer, asyncTracker) {

        var validators = [];

        dataContext.manager.metadataStore.registerEntityTypeCtor("User", null, userInitializer);

        var vm = {
            id: ko.observable(),
            user: ko.observable(),
            initialising: ko.observable(true),
            asyncTracker: asyncTracker,

            init: init,
        };
        
        return vm;
    
        function init() {
            vm.id($("#id")[0].value);
            dataContext.init(validators)
                .then(function () {
                    return loadUser();
                })
                .fail(function () {
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
            vm.initialising(false);
        }
        
        function loadUser() {
            return dataContext.users.get(vm.id(), vm.user)
                .then(function () {                
                    ko.applyBindings(vm);
                    vm.asyncTracker.push("ContentLoaded", "");
                })
                .fail(function (errors) {
                    vm.asyncTracker.push("ContentLoadFailed", "");
                });
        }        
    }
)