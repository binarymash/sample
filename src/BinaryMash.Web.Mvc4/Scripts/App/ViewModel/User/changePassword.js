﻿define(['jquery',
    'ko',
    'validation/manager',
    'services/dataContext',
    'services/notifier',
    'initializers/User/createEdit',
    'components/asyncTracker'],

    function ($, ko, validation, dataContext, notifier, userInitializer, asyncTracker) {

        var validators = [];

        dataContext.manager.metadataStore.registerEntityTypeCtor("User", null, userInitializer);
        
        var vm = {
            id: ko.observable(),
            antiForgeryToken: ko.observable(),
            passwordChange: ko.observable({
                Password: ko.observable(),
                PasswordRejected : ko.observable(false),
                NewPassword: ko.observable(),
                NewPasswordConfirmation: ko.observable()
            }),
            initialising: ko.observable(true),
            asyncTracker: asyncTracker,

            cancelling: ko.observable(false),
            saving: ko.observable(false),

            init: init,
            done: done,
            cancel: cancel
        };

        vm.disableBtns = ko.computed(function () {
            return vm.cancelling() || vm.saving();
        });

        ko.applyBindings(vm);

        return vm;
    
        function init() {
            initValidation();
            $('#changePasswordForm').submit(submitForm);
            vm.id($("#id")[0].value);
            vm.antiForgeryToken($("input[name=__RequestVerificationToken]")[0].value);
            dataContext.init(validators)
                .then(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoaded", "");
                })
                .fail(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
        }
        
        function initValidation() {
            validation.init($("#changePasswordForm"), []);
        }
        
        function done() {
            $('#changePasswordForm').submit();
        }

        function submitForm(event) {
            event.preventDefault();
            saveImpl(doneAfterSaveSucceeded, saveFailed);
        }
        
        function saveImpl(succeeded, failed) {
            vm.saving(true);

            var validator = $('#changePasswordForm').validate();
            if (!validator.valid()) {
                failed();
                return;
            }
            
            if (!dataContext.users.savePassword(vm.passwordChange(), vm.antiForgeryToken(), succeeded, failed)) {
                vm.saving(false);
                succeeded();
            }                
            
        }        

        function doneAfterSaveSucceeded() {
            return window.location.href = "/User/Summary/" + vm.id();
        }

        function saveFailed(error) {
            vm.saving(false);
            asyncTracker.push("SaveFailed");

            var validationErrors = false;
            if (!error) {
                validationErrors = true;
            } else if (error.status == 403) {
                //forbidden - probably password rejected
                vm.passwordChange().PasswordRejected(true);
                validationErrors = true;
            }
            
            if (validationErrors) {
                notifier.error("There are validation errors - please fix these. Your changes have NOT been saved");
            } else {
                notifier.error("An error occurred when changing the password. Your changes have NOT been saved");
            }
        }
        
        function cancel() {
            vm.cancelling(true);
            
            return window.location.href = "/User/Summary/" + vm.id();
        }
    }
)