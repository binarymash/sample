﻿define(['ko',
    'services/dataContext',
    'services/notifier',
    'initializers/User/createEdit',
    'initializers/UserPublicProfile/createEdit',
    'components/asyncTracker'],

    function (ko, dataContext, notifier, userInitializer, publicUserInitializer, asyncTracker) {

        var validators = [];

        dataContext.manager.metadataStore.registerEntityTypeCtor("User", null, userInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("UserPublicProfile", null, publicUserInitializer);

        var vm = {
            id: ko.observable(),
            antiForgeryToken:ko.observable(),
            user: ko.observable(),
            initialising: ko.observable(true),
            loadingData: ko.observable(true),
            asyncTracker: asyncTracker,

            cancelling: ko.observable(false),
            saving: ko.observable(false),
            publishing: ko.observable(false),

            init: init,
            done: done,
            cancel: cancel
        };

        vm.disableBtns = ko.computed(function () {
            return vm.cancelling() || vm.saving() || vm.publishing();
        });

        return vm;
    
        function init() {
            vm.id($("#id")[0].value);
            vm.antiForgeryToken($("input[name=__RequestVerificationToken]")[0].value);
            
            ko.applyBindings(vm);
            dataContext.init(validators, vm.antiForgeryToken())
                .then(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoaded", "");
                    initData();
                })
                .fail(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
        }

        function initData() {
            vm.loadingData(true);
            return Q.fcall(function () {
                return loadData();
            }).then(function () {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoaded", "");
            }).fail(function () {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoadedFailed", "");
            });
        }
        
        function loadData() {
            return dataContext.users.get(vm.id(), vm.user);
        }
        
        function done() {
            saveImpl(doneAfterSaveSucceeded, saveFailed);
        }

        function saveImpl(succeeded, failed) {
            vm.saving(true);
            if (!dataContext.users.save(vm.user, succeeded, failed)) {
                vm.saving(false);
                succeeded();
            };
        }
        
        function doneAfterSaveSucceeded() {
            return window.location.href = "/User/Summary/" + vm.user().Id();
        }

        function saveFailed(error) {
            vm.saving(false);
            asyncTracker.push("SaveFailed");
            if (error.status < 200 || error.status > 299) {
                notifier.error("An error occurred when saving " + vm.user().Name() + ". Your changes have NOT been saved");
            } else if (error.entityErrors && error.entityErrors.length !== 0) {
                notifier.error("There are validation errors - please fix these. Your changes have NOT been saved");
            } else {
                notifier.error("An error occurred when saving " + vm.user().Name() + ". Your changes have NOT been saved");
            }
        }
        
        function cancel() {
            vm.cancelling(true);
            
            return window.location.href = "/User/Summary/" + vm.user().Id();
        }
    }
)