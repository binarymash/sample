﻿define(['ko',
    'services/dataContext',
    'initializers/Person/summary',
    'services/notifier',
    'components/asyncTracker'],

    function (ko, dataContext, personInitializer, notifier, asyncTracker) {

        dataContext.manager.metadataStore.registerEntityTypeCtor("Person", null, personInitializer);

        var vm = {
            id: ko.observable(),
            person: ko.observable(),
            initialising: ko.observable(true),
            asyncTracker: asyncTracker,

            init: init,
        };
        
        return vm;
    
        function init() {
            vm.id($("#id")[0].value);
            dataContext.init([])
                .then(function () {
                    loadPerson();
                })
                .fail(function () {
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
            vm.initialising(false);
        }
                
        function loadPerson() {
            dataContext.people.get(vm.id(), vm.person)
            .then(function () {
                ko.applyBindings(vm);
                vm.asyncTracker.push("ContentLoaded", "");
            })
            .fail(function () {
                vm.asyncTracker.push("ContentLoadFailed", "");
            });
        }        
    }
)