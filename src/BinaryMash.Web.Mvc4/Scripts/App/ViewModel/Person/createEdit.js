﻿define(['ko',
    'services/dataContext',
    'initializers/Person/createEdit',
    'initializers/EmailChannel/createEdit',
    'initializers/PhoneChannel/createEdit',
    'initializers/AddressChannel/createEdit',
    'services/notifier',
    'components/asyncTracker',
    'components/koSelect2'],

    function (ko, dataContext, personInitializer,
        ecInitializer, pcInitializer, acInitializer, notifier, asyncTracker, select2) {

        var validators = [];

        dataContext.manager.metadataStore.registerEntityTypeCtor("Person", null, personInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("EmailChannel", null, ecInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("PhoneChannel", null, pcInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("AddressChannel", null, acInitializer);

        var vm = {
            id: ko.observable(),
            antiForgeryToken:ko.observable(),
            person: ko.observable(),
            initialising: ko.observable(true),
            loadingData: ko.observable(true),
            asyncTracker: asyncTracker,
            referrer: ko.observable('Index'),
            cancelling: ko.observable(false),
            saving: ko.observable(false),
            publishing: ko.observable(false),

            init: init,
            save: save,
            done: done,
            cancel: cancel
        };

        vm.disableBtns = ko.computed(function () {
            return vm.cancelling() || vm.saving() || vm.publishing();
        });

        return vm;
    
        function init() {
            vm.id(parseInt($("#id")[0].value));
            vm.antiForgeryToken($("input[name=__RequestVerificationToken]")[0].value);
            ko.applyBindings(vm);
            dataContext.init(validators, vm.antiForgeryToken())
                .then(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoaded", "");
                    initData();
                })
                .fail(function () {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
        }

        function initData() {
            vm.loadingData(true);
            return Q.fcall(function () {
                if (vm.id() === 0) {
                    createData();
                    return null;
                }
                return loadData();
            }).then(function () {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoaded", "");
            }).fail(function () {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoadedFailed", "");
            });
        }

        function createData() {
            vm.person(dataContext.people.create());
        }
        
        function loadData() {
            return dataContext.people.get(vm.id(), vm.person);
        }
        
        function save() {
            saveImpl(saveSucceeded, saveFailed);
        }

        function done() {
            saveImpl(doneAfterSaveSucceeded, saveFailed);
        }

        function saveImpl(succeeded, failed) {
            vm.saving(true);
            if (!dataContext.people.save(vm.person, succeeded, failed)) {
                vm.saving(false);
                succeeded();
            };
        }
        
        function saveSucceeded() {
            vm.id(vm.person().Id());
            vm.saving(false);
            asyncTracker.push("SaveSucceeded");
            notifier.success(vm.person().Name() + " has been saved");
        }

        function doneAfterSaveSucceeded() {
            return window.location.href = "/Person/Summary/" + vm.person().Id();
        }

        function saveFailed(error) {
            vm.saving(false);
            asyncTracker.push("SaveFailed");
            if (error.status < 200 || error.status > 299) {
                notifier.error("An error occurred when saving " + vm.person().Name() + ". Your changes have NOT been saved");
            } else if (error.entityErrors && error.entityErrors.length !== 0) {
                notifier.error("There are validation errors - please fix these. Your changes have NOT been saved");
            } else {
                notifier.error("An error occurred when saving " + vm.person().Name()+". Your changes have NOT been saved");
            }
        }
        
        function cancel() {
            vm.cancelling(true);
            
            if (vm.person().Id() > 0) {
                return window.location.href = "/Person/Summary/" + vm.person().Id();
            }

            return window.location.href = "/Person";
        }
    }
)