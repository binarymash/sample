﻿define([
    'jquery', 'ko', 'Q', 'services/dataContext', 'services/notifier',
    'initializers/CandidateQuestionAnswers/edit', 'initializers/CandidateSectionAnswers/edit',
    'initializers/CandidateAnswer/edit', 'initializers/complex/CandidateQuestionVersion/view',
    'components/asyncTracker',
    'services/candidateAnswerMerger',
    'services/prettify',
    'validation/answerLengthBreezeValidator'],

    function ($, ko, Q, dataContext, notifier,
        questionAnswersInitializer, sectionAnswersInitializer,
        answerInitializer, questionInitializer,
        asyncTracker, mergeAnswers, prettify, answerLength) {

        var validator = answerLength("CandidateAnswer", "Text");
        var validators = [validator];

        dataContext.manager.metadataStore.registerEntityTypeCtor("CandidateQuestionVersionAnswers", null, questionAnswersInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("CandidateSectionAnswers", null, sectionAnswersInitializer);
        dataContext.manager.metadataStore.registerEntityTypeCtor("CandidateAnswer", null, answerInitializer);

        var vm = {
            sessionId: ko.observable(),
            antiForgeryToken: ko.observable(),
            test: ko.observable(),
            questionIndex : ko.observable(0),
            question: ko.observable(),
            questionAnswer: ko.observable(),
            
            initialising: ko.observable(true),
            loadingData: ko.observable(true),
            loadingQuestion: ko.observable(true),
            savingQuestion: ko.observable(false),
            asyncTracker : asyncTracker,
            nextQActive: ko.observable(false),
            previousQActive: ko.observable(false),
            error: ko.observable(),

            init: init,
            goToQ: goToQ,
            nextQ: nextQ,
            previousQ: previousQ,
            review: review,
            
            makePretty:makePretty
        };
        
        vm.questionIndex.subscribe(function(questionIndex) {
            loadQuestion(questionIndex);
        });

        vm.questionIndexDisplay = ko.computed(function() {
            return vm.questionIndex() + 1;
        });
        
        vm.prevQuestionIndex = ko.computed(function() {
            return vm.questionIndex() - 1;
        });

        vm.prevQuestionIndexDisplay = ko.computed(function() {
            return vm.prevQuestionIndex() + 1;
        });
        
        vm.nextQuestionIndex = ko.computed(function() {
            return vm.questionIndex() + 1;
        });

        vm.nextQuestionIndexDisplay = ko.computed(function() {
            return vm.nextQuestionIndex() + 1;
        });
        
        vm.lastQuestionIndex = ko.computed(function() {
            if (!vm.test()) {
                return -1;
            }

            return vm.test().QuestionVersions.length - 1;
        });

        vm.allowPrevious = ko.computed(function() {
            return vm.questionIndex() > 0;
        });

        vm.allowNext = ko.computed(function() {
            return vm.questionIndex() < (vm.lastQuestionIndex() + 1);
        });
        
        vm.showQuestionView = ko.computed(function() {
            return !(vm.questionIndex() < 0 || vm.questionIndex() > vm.lastQuestionIndex());
        });

        vm.showSummaryView = ko.computed(function() {
            return (vm.questionIndex() > vm.lastQuestionIndex());
        });
                
        return vm;   
        
        function init() {
            vm.sessionId($("#sessionId")[0].value);
            vm.antiForgeryToken($("input[name=__RequestVerificationToken]")[0].value);
            ko.applyBindings(vm);
            dataContext.init(validators, vm.antiForgeryToken())
                .then(function() {
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoaded", "");
                    initData();
                })
                .fail(function (error) {
                    vm.question();
                    vm.error(error);
                    vm.initialising(false);
                    vm.asyncTracker.push("MetadataLoadFailed", "");
                });
        }

        function initData() {
            vm.loadingData(true);
            return Q.fcall(function() {
                return loadData();
            }).then(function() {
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoaded", "");
            }).fail(function(error) {
                vm.question();
                vm.error(error);
                vm.loadingData(false);
                vm.asyncTracker.push("ContentLoadedFailed", "");
            });
        }
        
        function loadData() {
            return dataContext.candidateTests.get(vm.test)
                .then(function () {
                    return loadQuestion(vm.questionIndex());
                });
        }       
                   
        function loadQuestion(questionIndex) {
            if (questionIndex < 0) {
                vm.questionIndex(0);
            }
            
            if (questionIndex > (vm.lastQuestionIndex() + 1)) {
                vm.questionIndex(vm.lastQuestionIndex() + 1);
            }
            
            if (questionIndex < (vm.lastQuestionIndex() + 1)) {
                vm.loadingQuestion(true);

                var qv = vm.test().QuestionVersions[questionIndex];
                if (!qv) {
                    vm.loadingQuestion(false);
                    return false;
                }

                return dataContext.candidateTests.getQuestion(qv.Id, qv.Version, vm.question)
                    .then(function () {
                        questionInitializer(vm.question());
                        return loadAnswers(vm.question());
                    })
                    .then(function () {
                        return loadQuestionSucceeded();
                    })
                    .fail(function (err) {
                        loadQuestionFailed(err);
                    });
            }

            vm.question();
            return loadQuestionSucceeded();
        }

        function loadAnswers(qv) {
            return dataContext.candidateAnswers.getForSession(qv.Id, qv.Version, vm.questionAnswer)
                .then(function () {
                    mergeAnswers(vm.sessionId(), qv, vm.questionAnswer());
                });
        }

                
        function loadQuestionSucceeded() {
            asyncTracker.push("LoadQuestionSucceeded");
            vm.loadingQuestion(false);
        }

        function loadQuestionFailed(error) {
            vm.question();
            vm.error(error);
            asyncTracker.push("LoadQuestionFailed");
            vm.loadingQuestion(false);
        }

        function save(onSucceeded, onFailed) {
            vm.savingQuestion(true);
            if (vm.questionIndex() > vm.lastQuestionIndex()) {
                onSucceeded();
            } else if (!dataContext.candidateAnswers.save(vm.question().candidateSupplied, onSucceeded, onFailed)) {
                onSucceeded();                
            }            
        }

        function goToQ(cqv) {
            //only call this if not already on a question, because we won't save anything!
            
            if (vm.loadingQuestion()) {
                return;
            }

            var index = vm.test().QuestionVersions.indexOf(cqv);
            
            if (vm.showQuestionView()) {
                save(gotoAfterSaveSucceeded(index), saveFailed);
            }

            vm.questionIndex(index);
        }

        function nextQ() {
            if (vm.loadingQuestion() || vm.savingQuestion()) {
                return;
            }

            if (vm.questionIndex() > vm.lastQuestionIndex()) {
                return;
            }
            
            save(nextAfterSaveSucceeded, saveFailed);
        }

        function previousQ() {
            if (vm.loadingQuestion() || vm.savingQuestion()) {
                return;
            }

            if (vm.questionIndex() < 0) {
                return;
            }

            save(previousAfterSaveSucceeded, saveFailed);
        }

        function review() {
            if (vm.loadingQuestion()) {
                return;
            }

            save(reviewAfterSaveSucceeded, saveFailed);
        }

        function gotoAfterSaveSucceeded(index) {
            vm.questionIndex(index);
            if (vm.questionIndex() > vm.lastQuestionIndex()) {
                vm.loadingQuestion(false);
            }

            loadQuestion(vm.questionIndex());
        }

        function nextAfterSaveSucceeded() {
            vm.savingQuestion(false);
            vm.questionIndex(vm.nextQuestionIndex());
        }        

        function previousAfterSaveSucceeded() {
            vm.savingQuestion(false);
            vm.questionIndex(vm.prevQuestionIndex());
        }

        function reviewAfterSaveSucceeded() {
            vm.savingQuestion(false);
            vm.questionIndex(vm.lastQuestionIndex() + 1);
        }

        function saveFailed(err) {
            vm.savingQuestion(false);
            asyncTracker.push("SaveFailed");
            if (error.status < 200 || error.status > 299) {
                notifier.error("An error occurred when saving the answer. Your answer have NOT been saved.");
            } else if (error.entityErrors && error.entityErrors.length !== 0) {
                notifier.error("There are validation errors - please fix these. Your answers have NOT been saved");
            } else {
                notifier.error("An error occurred when saving the answer. Your answer have NOT been saved.");
            }
        }
        
        function makePretty(dom, model) {
            prettify.run();
        }
    }

);