﻿define(['breeze', 'Q'], function (breeze, Q) {

    var manager;

    var module = {
        init: init,

        get: get,
        getQuestion: getQuestion,
    };

    return module;

    function init(entityManager) {
        manager = entityManager;
    }

    function get(itemObservable) {
        var query = breeze.EntityQuery.from("CandidateTestVersion_Session");

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            itemObservable(data.results[0]);
        }
    }
    
    function getQuestion(id, version, itemObservable) {

        var query = breeze.EntityQuery
            .from("CandidateQuestionVersion_Session")
            .where(questionIs(id, version));

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            itemObservable(data.results[0]);
        }
    }

    function questionIs(id, version) {
        return breeze.Predicate.create("Id", "==", id)
                               .and("Version", "==", version);
    }
    
    function queryFailed(error) {
        throw new Error(error);
    }


    
})