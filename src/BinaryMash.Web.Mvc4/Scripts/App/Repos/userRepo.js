﻿define(['jquery','config/url'], function($,url) {

    var manager;
    
    var module = {
        init: init,

        checkEmailIsAvailable: checkEmailIsAvailable,
        get: get,
        save: save,
        savePassword: savePassword,
        saveEmail:saveEmail
    };

    return module;
    
    function init(entityManager) {
        manager = entityManager;
    }
    
    function checkEmailIsAvailable(address) {
        var result = true;

        $.ajax({
            url: url.accountAvailable,
            async: false,
            dataType: 'json',
            data: {
                email: address
            },
            success: function (data) {
                result = data;
            },
            error: function (xhr, status, error) {
                result = true;
            }
        });

        return result;
    }
    
    function get(id, itemObservable) {
        var query = breeze.EntityQuery
            .from("Me");

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            var matchingQuestion = null;
            if (data.results.length > 0) {
                matchingQuestion = data.results[0];
            }
            itemObservable(matchingQuestion);
        }
    }

    function save(user, succeededCallback, failedCallback) {
        if (!manager.hasChanges()) {
            return false;
        }

        if (user().entityAspect.entityState === breeze.EntityState.Unchanged) {
            //ensure we send across the person - make our lives easier on the server
            user().entityAspect.setModified();
        }

        var options = new breeze.SaveOptions();
        options.tag = "SaveUser";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function saveEmail(user, succeededCallback, failedCallback) {
        if (!manager.hasChanges()) {
            return false;
        }

        if (user().entityAspect.entityState === breeze.EntityState.Unchanged) {
            //ensure we send across the person - make our lives easier on the server
            user().entityAspect.setModified();
        }

        var options = new breeze.SaveOptions();
        options.tag = "ChangeUserEmail";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function savePassword(passwordChange, antiForgeryToken, succeededCallback, failedCallback) {
        //we don't use breeze for this as there is no direct mapping onto an entity
        var dto = {
            "CurrentPassword": passwordChange.Password(),
            "NewPassword": passwordChange.NewPassword(),
            "NewPasswordConfirmation": passwordChange.NewPasswordConfirmation()
        };
        $.ajax({
            type: 'PUT',
            url: url.password,
            dataType: 'json',
            data: dto,
            success: succeededCallback,
            error: failedCallback,
            headers:{"__RequestVerificationToken": antiForgeryToken}
            
        });
        
        return true;
    }

    function queryFailed(error) {
        throw new Error(error);
    }

})