﻿define(['breeze'], function() {

    var manager;
    
    var module = {
        init: init,
        
        create: create,
        get: get,
        getWithName: getWithName,
        save: save,
    };

    return module;
    
    function init(entityManager) {
        manager = entityManager;
    }
    
    function create() {
        return manager.createEntity('Person');
    }

    function get(id, itemObservable) {
        var query = breeze.EntityQuery
            .from("People")
            .where("Id", "==", id);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            var matchingPerson = null;
            if (data.results.length > 0) {
                matchingPerson = data.results[0];
            }
            itemObservable(matchingPerson);
        }
    }

    function getWithName(skip, take, itemsObservable, countObservable, name) {
        var query = breeze.EntityQuery
            .from("People")
            .where("Name", "contains", name)
            .orderBy("Name")
            .skip(skip)
            .take(take)
            .inlineCount(true);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            itemsObservable(data.results);
            countObservable(data.inlineCount);
        }
    }

    function save(person, succeededCallback, failedCallback) {
        if (!manager.hasChanges()) {
            return false;
        }

        if (person().entityAspect.entityState === breeze.EntityState.Unchanged) {
            //ensure we send across the person - make our lives easier on the server
            person().entityAspect.setModified();
        }

        var options = new breeze.SaveOptions();
        options.tag = "SavePerson";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function queryFailed(error) {
        //TODO: what shall we do eh?
        //log.error('Failed to retrieve data: ' + error);
        //        throw new Error(error);
    }

})