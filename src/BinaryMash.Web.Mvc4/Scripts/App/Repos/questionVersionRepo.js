﻿define(['breeze'], function(breeze) {

    var manager;
    
    var module = {
        init: init,

        create: create,
        get: get,
        getLatest: getLatest,
        getLatestPublished: getLatestPublished,
        getHistoryPublished: getHistoryPublished,
        save: save,
        publish: publish,

    };

    return module;

    function init(entityManager) {
        manager = entityManager;
    }
    
    function create() {
        var question = manager.createEntity("Question", {IsPublic:true});
        var questionVersion = manager.createEntity('QuestionVersion', { State: 1, Version: 1, Question: question });
        questionVersion.addSection();
        return questionVersion;
    }

    function get(id, version, itemObservable) {
        var query = breeze.EntityQuery
            .from("QuestionVersions")
            .where(questionIs(id, version));

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            var matchingQuestion = null;
            if (data.results.length > 0) {
                matchingQuestion = data.results[0];
            }
            itemObservable(matchingQuestion);
        }
    }

    function getLatest(questionId, itemObservable) {
        var query = breeze.EntityQuery
            .from("LatestQuestionVersions")
            .where("QuestionId", "==", questionId);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            var matchingQuestion = null;
            if (data.results.length > 0) {
                matchingQuestion = data.results[0];
            }
            itemObservable(matchingQuestion);
        }
    }

    function getLatestPublished(skip, take, itemsObservable, countObservable, title) {
        var query = breeze.EntityQuery
            .from("LatestPublishedQuestionVersions")
            .where("Title", "contains", title)
            .orderBy("Updated")
            .skip(skip)
            .take(take)
            .inlineCount(true);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            itemsObservable(data.results);
            countObservable(data.inlineCount);
        }
    }

    function getHistoryPublished(skip, take, itemsObservable, countObservable, testId) {
        var query = breeze.EntityQuery
            .from("PublishedQuestionVersions")
            .where("QuestionId", "==", testId)
            .orderBy("Version desc")
            .skip(skip)
            .take(take)
            .inlineCount(true);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            itemsObservable(data.results);
            countObservable(data.inlineCount);
        }
    }

    function save(question, succeededCallback, failedCallback) {
        if (!manager.hasChanges()) {
            return false;
        }

        if (question().entityAspect.entityState === breeze.EntityState.Unchanged) {
            //ensure we send across the QuestionVersion - make our lives easier on the server
            question().entityAspect.setModified();
        }

        if (question().entityAspect.entityState === breeze.EntityState.Added) {
            if (question().Question().entityAspect.entityState === breeze.EntityState.Unchanged) {
                //if we're adding a new QuestionVersion, ensure we are also sending through the Question
                //so we can update the versioning. If this is the first QuestionVersion, Question will be 'Added' already
                question().Question().entityAspect.setModified();
            }
        }
        
        var options = new breeze.SaveOptions();
        options.tag = "SaveQuestion";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function publish(question, succeededCallback, failedCallback) {
        //ensure we send across the questionversion and question - make our lives easier on the server
        question().entityAspect.setModified();
        question().Question().entityAspect.setModified();

        var options = new breeze.SaveOptions();
        options.tag = "PublishQuestion";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function queryFailed(error) {
        throw new Error(error);
    }
    
    function questionIs(id, version) {
        return breeze.Predicate.create("QuestionId", "==", id)
                               .and("Version", "==", version);
    }

})