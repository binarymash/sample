﻿define(['breeze','services/guid'], function (breeze,guid) {

    var manager;

    var module = {
        init: init,

        create: create,
        get: get,
        getWithQuestions:getWithQuestions,
        save: save,
    };

    return module;

    function init(entityManager) {
        manager = entityManager;
    }

    function create(testId, personId) {
        return manager.createEntity('Session', { Id: guid.create(), State: 1, TestId: testId, PersonId: personId, Password: Math.floor((Math.random() * 900000) + 100000) });
    }

    function get(id, itemObservable) {
        var query = breeze.EntityQuery.from("Sessions");
        query = query.where("Id", "==", id);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            var matchingItem = null;
            if (data.results.length > 0) {
                matchingItem = data.results[0];
            }
            itemObservable(matchingItem);
        }
    }

    function getWithQuestions(id, itemObservable) {
        var query = breeze.EntityQuery.from("SessionsAndQuestions");
        query = query.where("Id", "==", id);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            var matchingItem = null;
            if (data.results.length > 0) {
                matchingItem = data.results[0];
            }
            itemObservable(matchingItem);
        }
    }

    function save(session, succeededCallback, failedCallback) {
        if (!manager.hasChanges()) {
            return false;
        }

        if (session().entityAspect.entityState === breeze.EntityState.Unchanged) {
            //ensure we send across the test - make our lives easier on the server
            session().entityAspect.setModified();
        }

        var options = new breeze.SaveOptions();
        options.tag = "SaveSession";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function queryFailed(error) {
        //TODO: what shall we do eh?
        //log.error('Failed to retrieve data: ' + error);
        //throw new Error(error);
    }

})