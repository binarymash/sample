﻿define(['breeze'], function () {

    var manager;

    var module = {
        init: init,

        create: create,
        get: get,
        getWithName: getWithName,
        getPublishedVersions: getPublishedVersions,
        save: save,
        publish: publish,

    };

    return module;

    function init(entityManager) {
        manager = entityManager;
    }

    function create() {
        var test = manager.createEntity('Test');
        return manager.createEntity('TestVersion', { State: 1, Version: 1, Test: test });
    }

    function get(id, itemObservable) {
        var query = breeze.EntityQuery.from("LatestTestVersions")
                                      .where("TestId", "==", id);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            var matchingItem = null;
            if (data.results.length > 0) {
                matchingItem = data.results[0];
            }
            itemObservable(matchingItem);
        }
    }

    function getWithName(skip, take, itemsObservable, countObservable, name) {
        var query = breeze.EntityQuery
            .from("LatestPublishedTestVersions")
            .where("Name", "contains", name)
            .orderBy("Name")
            .skip(skip)
            .take(take)
            .inlineCount(true);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            itemsObservable(data.results);
            countObservable(data.inlineCount);
        }
    }

    function getPublishedVersions(id, itemsObservable) {
        var query = breeze.EntityQuery
            .from("TestVersions")
            .where("TestId", "==", id);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            itemsObservable(data.results);
        }
    }

    function save(test, succeededCallback, failedCallback) {
        if (!manager.hasChanges()) {
            return false;
        }

        if (test().entityAspect.entityState === breeze.EntityState.Unchanged) {
            //ensure we send across the test - make our lives easier on the server
            test().entityAspect.setModified();
        }

        if (test().entityAspect.entityState === breeze.EntityState.Added) {
            if (test().Test().entityAspect.entityState === breeze.EntityState.Unchanged) {
                //if we're adding a new version of a TestVersion, ensure we are also sending through the Test
                //so we can update the versioning. If this is the first TestVersion, Test will be 'Added' already
                test().Test().entityAspect.setModified();
            }
        }

        var options = new breeze.SaveOptions();
        options.tag = "SaveTest";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function publish(test, succeededCallback, failedCallback) {
        //ensure we send across the test - make our lives easier on the server
        test().entityAspect.setModified();
        test().Test().entityAspect.setModified();

        var options = new breeze.SaveOptions();
        options.tag = "PublishTest";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function queryFailed(error) {
        //TODO: what shall we do eh?
        //log.error('Failed to retrieve data: ' + error);
        //throw new Error(error);
    }

})