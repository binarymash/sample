﻿define(['breeze', 'Q'], function (breeze, Q) {

    var manager;

    var module = {
        init: init,

        createQuestion: createQuestion,
        createSection: createSection,
        createAnswer: createAnswer,

        get: get,
        getForSession: getForSession,

        save: save
    };

    return module;

    function init(entityManager) {
        manager = entityManager;
    }
    
    function get(sessionId, questionId, questionVersion, resultObservable) {

        var p = breeze.Predicate.create("SessionId", "==", sessionId)
                                .and("QuestionVersionId", "==", questionId)
                                .and("QuestionVersionVersion", "==", questionVersion);
        
        var query = breeze.EntityQuery
            .from("CandidateQuestionVersionAnswers")
            .where(p);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);
        
        function querySucceeded(data) {
            resultObservable(data.results[0]);
        }
    }
    
    function getForSession(id, version, resultObservable) {

        var p = breeze.Predicate.create("QuestionVersionId", "==", id)
                               .and("QuestionVersionVersion", "==", version);

        var query = breeze.EntityQuery
            .from("CandidateQuestionVersionAnswers_Session")
            .where(p);

        return manager.executeQuery(query)
            .then(querySucceeded)
            .fail(queryFailed);

        function querySucceeded(data) {
            resultObservable(data.results[0]);
        }
    }

    function createQuestion(sessionId, questionId, questionVersion) {
        return manager.createEntity('CandidateQuestionVersionAnswers', { QuestionVersionId: questionId, QuestionVersionVersion: questionVersion, SessionId: sessionId });
    }
    
    function createSection(sectionId) {
        return manager.createEntity('CandidateSectionAnswers', { SectionId: sectionId });
    }

    function createAnswer(answerContentId) {
        return manager.createEntity('CandidateAnswer', { AnswerContentId: answerContentId});
    }

    function save(candidateQuestionVersionAnswers, succeededCallback, failedCallback) {
        if (!manager.hasChanges()) {
            return false;
        }

        if (candidateQuestionVersionAnswers().entityAspect.entityState === breeze.EntityState.Unchanged) {
            //ensure we send across the root - make our lives easier on the server
            candidateQuestionVersionAnswers().entityAspect.setModified();
        }

        var options = new breeze.SaveOptions();
        options.tag = "SaveCandidateQuestionAnswers";
        manager.saveChanges(null, options, succeededCallback, failedCallback);
        return true;
    }

    function queryFailed(error) {
        throw new Error(error);
    }


    
})