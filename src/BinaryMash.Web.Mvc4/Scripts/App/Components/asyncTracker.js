﻿define(['ko', 'components/asyncTrackerItem'], function(ko, Item) {

    var module = {
        items: ko.observableArray([]),
        isVisible: ko.observable(false),
        
        push:push,
        remove: remove,
        removeAll: removeAll
    };

    window.document.asyncTracker = module;
    
    return module;
    
    function push(key, value) {
        var item = new Item(key, value);
        module.items.push(item);
    }
    
    function remove(timestamp, key, value) {
        var length = module.items().length;
        for (var i = 0; i < length; i++) {
            var item = module.items()[i];
            if ((item.timestamp() === timestamp) && (item.key() === key) && (item.value() === value)) {
                module.items.remove(item);
                return true;
            }
        }
        return false;
    }
    
    function removeAll() {
        module.items.removeAll();
    }
        
})