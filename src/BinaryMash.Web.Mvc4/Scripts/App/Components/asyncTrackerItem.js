﻿define(['ko','moment'],function(ko, moment) {

    return function (key, value) {

        return {
            timestamp : ko.observable(moment.utc().valueOf()),
            key: ko.observable(""+key),
            value: ko.observable(value ? (""+value) : ""),
        };
    };

})