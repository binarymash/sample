﻿define(['ko'], function (ko) {

    return function Pager(itemCount, pageSize, numberOfPagesToDisplay) {

        var self = this;

        this.currentPageID = ko.observable(1);
        this.itemCount = ko.observable(itemCount),
        this.pageSize = ko.observable(pageSize),
        this.pages = ko.observableArray();
        this.numberOfPagesToDisplay = ko.observable(numberOfPagesToDisplay);

        this.skip = ko.computed(function () {
            return self.pageSize() * (self.currentPageID() - 1);
        });

        this.lastPageID = ko.computed(function () {
            return Math.ceil(self.itemCount() / self.pageSize());
        });

        self.first = function () {
            self.currentPageID(1);
        };

        this.previous = function () {
            if (self.currentPageID() > 1) {
                self.currentPageID(self.currentPageID() - 1);
            }
        };

        this.next = function () {
            if (self.currentPageID() < self.lastPageID()) {
                self.currentPageID(self.currentPageID() + 1);
            }
        };

        this.last = function () {
            self.currentPageID(self.lastPageID());
        };

        this.pageSelected = function(page) {
            self.currentPageID(page);
        };
        
        this.itemCount.subscribe(function() {
            updatePages();
        });
        
        function updatePages() {
            var tempPages = [];
            if (self.lastPageID() > 0) {
                var split = Math.round((self.numberOfPagesToDisplay() - 1) / 2);
                var shortageAfter = split - (self.lastPageID() - self.currentPageID());
                var idealFirstPage = self.currentPageID() - split - (shortageAfter > 0 ? shortageAfter : 0);

                var firstIndex = (idealFirstPage < 1) ? 1 : idealFirstPage;
                var currentIndex = firstIndex - 1;
                var pagesAdded = 0;
                do {
                    currentIndex++;
                    pagesAdded++;
                    tempPages.push(currentIndex);
                } while ((pagesAdded < self.lastPageID()) && (currentIndex < self.lastPageID()));
            }
            self.pages(tempPages);
            if (self.currentPageID() > self.lastPageID()) {
                self.currentPageID(self.lastPageID());
            }                
        }


    };

})