﻿define(['jquery', 'ko'], function($, ko) {

    ko.bindingHandlers.radioAnswer = {
        init: function(element, valueAccessor) {
            // Get radio button located inside this div
            var radio = $(element).find('input[type="radio"]');

            // When div is clicked, check the radio and trigger radio change event
            $(element).click(function() {
                radio.prop('checked', true);
                radio.change();
            });

            // When radio button is checked, update the viewModel property!!
            $(radio).change(function() {
                if (radio.prop('checked')) // only if it was changed to checked
                {
                    // Set viewModel property to value of the radio button that was clicked
                    var value = valueAccessor();
                    value(radio.val());
                }
            });
        },
        update: function(element, valueAccessor, allBindingsAccessor) {
            var value = valueAccessor();
            var valueUnwrapped = ko.utils.unwrapObservable(value);

            // Get radio button located inside this div
            var radio = $(element).find('input[type="radio"]');

            // Set radio to be checked or unchecked
            var shouldBeChecked = valueUnwrapped == radio.val();
            radio.prop('checked', shouldBeChecked);
        }
    };
 
})