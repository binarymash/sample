﻿define(['ko','services/guid','services/pagedownConverter'], function(ko, guid, converter) {

    return function()
    {
        var self = this;
        
        self.guid = ko.observable(guid.create());

        self.buttonBarId = ko.computed(function () {
            return "wmd-button-bar-" + self.guid();
        });

        self.inputId = ko.computed(function () {
            return "wmd-input-" + self.guid();
        });

        self.previewId = ko.computed(function () {
            return "wmd-preview-" + self.guid();
        });

        self.preview = ko.observable(true);

        self.previewLink = ko.computed(function () {
            return (self.preview() ? "Hide preview of formatted text" : "Show preview of formatted text");
        });
        
        self.init = function() {

            //var help = function () { alert("Do you need help?"); };

            var options = {
                //helpButton: { handler: help },
                strings: { quoteexample: "add your quote here" }
            };

            var editor = new Markdown.Editor(converter.create(), "-" + self.guid(), options);

            editor.run();           
        };
        
        self.togglePreview = function () {
            self.preview(!self.preview());
        };
    };
    
});
