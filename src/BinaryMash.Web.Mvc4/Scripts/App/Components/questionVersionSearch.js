﻿define(['components/pager', 'services/dataContext'], function (Pager, dataContext) {

    return function QuestionVersionSearch(testId) {

        var self = this;

        this.testId = testId;
        this.pager = new Pager(0, 10, 5),
        this.items = ko.observableArray(),
        this.isActive = ko.observable(false),
        this.refreshing = ko.observable(false),        

        this.pager.currentPageID.subscribe(function () {
            refresh();
        });

        this.sortedItems = ko.computed(function() {
            return self.items().sort(function(l, r) {
                return l.Version() == r.Version() ?
                    0 :
                    (l.Version() > r.Version() ? -1 : 1);
            });
        });
        
        this.activate = function() {
            self.isActive(true);
            self.refresh();
        };

        this.disable = function() {
            self.refreshing(false);
            self.isActive(false);
        };

        this.refresh = function() {
            self.refreshing(true);
            self.items([]);
            return dataContext.questionVersions.getHistoryPublished(self.pager.skip(), self.pager.pageSize(), self.items, self.pager.itemCount, self.testId)
                .then(function() {
                    self.refreshing(false);
                })
                .fail(function(error) {
                    self.refreshing(false);
                });
        };

        this.manualRevert = function() {
            self.disable();
        };
    };

})