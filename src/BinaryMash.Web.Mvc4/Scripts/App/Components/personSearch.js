﻿define(['components/pager', 'services/dataContext'], function (Pager, dataContext) {

    var vm = {
        pager: new Pager(0, 10, 5),
        items: ko.observableArray(),
        isActive: ko.observable(false),
        nameFilter: ko.observable(''),
        activeNameFilter:ko.observable(''),
        refreshing: ko.observable(false),
        
        activate: activate,
        disable: disable,
        refresh: refresh,
        filter:filter,
        resetFilter: resetFilter,
        manualRevert:manualRevert
    };

    vm.filterApplied = ko.computed(function() {
        return vm.activeNameFilter() !== '';
    });

    vm.pager.currentPageID.subscribe(function () {
        refresh();
    });
        
    return vm;

    //Private functions
    function activate() {
        vm.isActive(true);
        vm.refresh();
    }

    function disable() {
        vm.refreshing(false);
        vm.isActive(false);
    }
    
    function filter() {
        vm.activeNameFilter(vm.nameFilter());
        refresh();
    }
    
    function resetFilter() {
        vm.nameFilter('');
        filter();
    }
    
    function refresh() {            
        vm.refreshing(true);
        vm.items([]);
        return dataContext.people.getWithName(vm.pager.skip(), vm.pager.pageSize(), vm.items, vm.pager.itemCount, vm.activeNameFilter())
            .then(function () {
                vm.refreshing(false);
            })
            .fail(function (error) {
                vm.refreshing(false);
            });
    }
    
    function manualRevert() {
        vm.disable();        
    }

})