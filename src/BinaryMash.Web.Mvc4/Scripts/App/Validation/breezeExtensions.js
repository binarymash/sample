﻿define(['ko'], function(ko) {

    var module = {
        addHasValidationErrorsProperty: addHasValidationErrorsProperty,
    };

    return module;
  
    function addHasValidationErrorsProperty(entity, property) {

        var prop = ko.observable(false);
        var errorsObservable = ko.observable("");

        var onChange = function () {
            var errs = entity.entityAspect.getValidationErrors(property);
            var hasError = errs.length > 0;
            errorsObservable(hasError ? errs[0].errorMessage : "");
            prop(hasError);
        };

        //onChange();             // check now ...
        entity.entityAspect.validationErrorsChanged.subscribe(onChange); // ... and when errors collection changes

        // observable property is wired up; now add it to the entity
        entity["has" + property + "Errors"] = prop;
        entity[property + "Errors"] = errorsObservable;
    }
    
})