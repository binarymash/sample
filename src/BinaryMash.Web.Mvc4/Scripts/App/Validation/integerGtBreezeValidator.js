﻿define(function() {

    return function(entityName, propertyName, messageTemplate, min) {

        var messageValue = messageTemplate ? messageTemplate : "'%displayName%' must be greater than %min%";
        var minValue = min ? min : 1;

        var validator = new breeze.Validator(
            "indexRange", indexRangeFn,
            {
                messageTemplate: messageValue,
                min: minValue
            }
        );

        return {
            entityName: entityName,
            propertyName: propertyName,
            validator: validator
        };
        
        function indexRangeFn(value, context) {
            if (value == null) return false;
            return value >= context.min;
        };
    };      

})