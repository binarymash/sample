﻿define(function() {

    return function(entityName, propertyName, messageTemplate, min, max) {

        var messageValue = messageTemplate ? messageTemplate : "'%displayName%' must be between %min% and %max%";
        var minValue = min ? min : 1;
        var maxValue = max ? max : 4096;

        var validator = new breeze.Validator(
            "indexRange", indexRangeFn,
            {
                messageTemplate: messageValue,
                min: minValue,
                max: maxValue
            }
        );

        return {
            entityName: entityName,
            propertyName: propertyName,
            validator: validator
        };
        
        function indexRangeFn(value, context) {
            if (value == null) return false;
            return value >= context.min && value <= context.max;
        };
    };      

})