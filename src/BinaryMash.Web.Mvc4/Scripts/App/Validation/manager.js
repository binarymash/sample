﻿define(['jquery'], function($) {
    
    var module = {
        init: init
    };

    return module;
    
    function init(form, validators) {
        uninitialise(form);
        addStyling();
        addCustomValidators(validators);
        reparse(form);
    }
    
    function uninitialise(form) {
        form.removeData("validator");
        form.removeData("unobtrusiveValidation");
    }

    function addStyling() {
        $.validator.setDefaults({
            
            //onkeyup: function(element) {
            //    delay(function() {
            //         $(element).valid();
            //    }, 2000);
            //},
            onkeyup:false,
            onfocusout: false,            
            focusCleanup:true,
            
            
            highlight: function (element, errorClass, validClass) {
                if (element.type === 'radio') {
                    this.findByName(element.name).addClass(errorClass).removeClass(validClass);
                } else {
                    $(element).addClass(errorClass).removeClass(validClass);
                    $(element).closest('.control-group').addClass('error');
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if (element.type === 'radio') {
                    this.findByName(element.name).removeClass(errorClass).addClass(validClass);
                } else {
                    $(element).removeClass(errorClass).addClass(validClass);
                    $(element).closest('.control-group').removeClass('error');
                }
            }
        });
    }
    
    function addCustomValidators(validators) {
        if (validators) {
            $.each(validators, function(index, validator) {
                validator.register($.validator);
            });
        }
    }    
    
    function reparse(form) {
        $.validator.unobtrusive.parse(form);
    }
    
})