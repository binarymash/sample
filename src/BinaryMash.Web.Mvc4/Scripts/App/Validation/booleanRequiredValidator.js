﻿define(function() {

    var module = {
        register:register
    };

    return module;
    
    function register(validator) {
        // we add a custom jquery validation method
        validator.addMethod('booleanRequired', function (value, element, params) {
            return (value === "true");
        }, '');

        // and an unobtrusive adapter
        validator.unobtrusive.adapters.add('booleanrequired', {}, function (options) {
            options.rules['booleanRequired'] = true;
            options.messages['booleanRequired'] = options.message;
        });
    }

})