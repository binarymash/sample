﻿define(function() {

    return function(entityName, propertyName, messageTemplate) {

        var messageValue = messageTemplate ? messageTemplate : "Your answer exceeds the maximum length allowed.";

        var validator = new breeze.Validator(
            "answerLength", answerLengthFn,
            {
                messageTemplate: messageValue
            }
        );

        return {
            entityName: entityName,
            propertyName: propertyName,
            validator: validator
        };
        
        function answerLengthFn(value, context) {
            if (value == null) return true;
            return (value.length <= context.entity.MaxLength());
        };
    };      

})