﻿define(function() {

    var module = {
        dataServiceName: "/breeze/Data",
        accountExists: "/Account/Exists",
        accountAvailable: "/Account/Available",
        password: "/Api/Password"
    };

    return module;
})