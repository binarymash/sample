﻿define(['jquery'], function ($) {

    var vm = {
        run: run
    };

    return vm;
    
    function run() {

        var added = false;

        $("pre code").parent().each(function () {
            if (!$(this).hasClass("prettyprint")) {
                $(this).addClass("prettyprint");
                added = true;
            }
        });

        if (added) {
            prettyPrint();
        }
    }
})