﻿define(['toastr', 'components/asyncTracker'], function(toastr, asyncTracker) {


    toastr.options = {
        "debug": false,
        "positionClass": "toast-bottom-full-width",
        "onclick": null,
        "showDuration": 300,
        "hideDuration": 1000,
        "timeOut": 5000,
        "extendedTimeOut": 1000
    };
    
    var module = {
        success: success,
        warning: warning,
        error: error,
        removeAll: removeAll
    };

    window.document.notifier = module;

    return module;
    
    function success(message) {
        toastr.success(message);
        asyncTracker.push("SuccessNotification", message);
    }
    
    function warning(message) {
        toastr.warning(message);
        asyncTracker.push("WarningNotification", message);
    }

    function error(message) {
        toastr.error(message);
        asyncTracker.push("ErrorNotification", message);
    }
    
    function removeAll() {
        toastr.clear();
        return true;
    }

})