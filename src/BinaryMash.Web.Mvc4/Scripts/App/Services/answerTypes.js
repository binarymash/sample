﻿define(function () {
    
    var AnswerType = function (id, description) {
        this.id = id;
        this.description = description;
    };

    return [new AnswerType('UserSupplied', 'User supplied text'),
        new AnswerType('Radio', 'Radio buttons'),
        new AnswerType('Checkbox', 'Checkboxes')];
})