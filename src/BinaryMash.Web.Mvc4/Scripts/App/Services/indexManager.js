﻿define(function() {

    var module = {
        repack: repack,
        moveFirst: moveFirst,
        moveUp: moveUp,
        moveDown: moveDown,
        moveLast : moveLast
    };

    return module;
    
    function repack(collection) {
        var length = collection.length;
        for (var i = 0; i < length; i++) {
            collection[i].Index(i);
        }
    }
    function moveFirst(item, collection) {
        var indexToMove = item.Index();
        if (indexToMove === 0) {
            return;
        }
        var length = collection.length;
        for (var i = 0; i < length; i++) {
            var s = collection[i];
            if (s.Index() < indexToMove) {
                s.Index(s.Index() + 1);
            }
        }
        item.Index(0);
    }
    
    function moveUp(item, collection) {
        var indexToMove = item.Index();
        if (indexToMove === 0) {
            return;
        }
        var length = collection.length;
        for (var i = 0; i < length; i++) {
            var s = collection[i];
            if (s.Index() === indexToMove - 1) {
                s.Index(indexToMove);
                break;
            }
        }
        item.Index(indexToMove - 1);
    };

    function moveDown(item, collection) {
        var length = collection.length;
        var indexToMove = item.Index();
        if (indexToMove === length - 1) {
            return;
        }
        for (var i = 0; i < length; i++) {
            var s = collection[i];
            if (s.Index() === indexToMove + 1) {
                s.Index(indexToMove);
                break;
            }
        }
        item.Index(indexToMove + 1);
    }

    function moveLast(item, collection) {
        var length = collection.length;
        var indexToMove = item.Index();
        if (indexToMove === length - 1) {
            return;
        }
        for (var i = 0; i < length; i++) {
            var s = collection[i];
            if (s.Index() > indexToMove) {
                s.Index(s.Index() - 1);
            }
        }
        item.Index(length - 1);
    };

})