﻿define(function () {

    return {
        create: create
    };

    function create() {
        var mdConverter = Markdown.getSanitizingConverter();
        mdConverter.hooks.chain("plainLinkText", function (url) {
            return "This is a link to " + url.replace(/^https?:\/\//, "");
        });
        return mdConverter;
    }
})