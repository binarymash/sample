﻿define(['services/dataContext'], function(dataContext) {


    return function(sessionId, qv, cqva) {

        if (!cqva) {
            return;
        }

        qv.candidateSupplied(cqva);

        for (var i = 0; i < qv.Sections().length; i++) {
            attachCandidateSection(qv.Sections()[i], cqva);
        }
    };

    function attachCandidateSection(section, cqva) {
        var csa = cqva.findSection(section.Id());
        if (csa === null) {
            csa = dataContext.candidateAnswers.createSection(section.Id());
            cqva.Sections.push(csa);
        }

        section.candidateSupplied(csa);

        for (var i = 0; i < section.AnswerContents().length; i++) {
            attachCandidateAnswer(section, section.AnswerContents()[i], csa);
        }
    }

    function attachCandidateAnswer(section, answerContent, csa) {
        var ca = csa.findAnswer(answerContent.Id());
        if (ca !== null) {
            answerContent.candidateSupplied(ca);
        }
    }

    
})