﻿define(function () {

    var module = {
      
        buttonClicked:buttonClicked,
        buttonReset:buttonReset
    };

    return module;
    
    function buttonClicked(btn, text) {
        $(btn).button('toggle');
        $(btn).button(text);
    }
    
    function buttonReset(btn) {
        $(btn).button('toggle');
        $(btn).button('reset');
    }

})