﻿define(['jquery', 'breeze', 'config/url',
        'repos/questionVersionRepo',
        'repos/personRepo',
        'repos/testRepo',
        'repos/sessionRepo',
        'repos/userRepo',
        'repos/candidateTestRepo',
        'repos/candidateAnswersRepo'],
    function ($, breeze, url, questionVersions, people, tests, sessions, users, candidateTests, candidateAnswers) {
    
    
    // manager is the service gateway and cache holder
    var manager = new breeze.EntityManager(url.dataServiceName);

    var module = {
        init : init,
        manager: manager,
                                
        revert:revert,
        isReverting: false,

        //repos
        questionVersions: questionVersions,
        people: people,
        testVersions: tests,
        sessions: sessions,
        users: users,
        candidateTests: candidateTests,
        candidateAnswers: candidateAnswers
    };

    return module;

    function init(validators, antiForgeryToken) {
        configureBreeze(antiForgeryToken);
        return manager.fetchMetadata()
            .then(function () {
                initValidation(validators);
                initRepos();
            });
    }    

    function configureBreeze(antiForgeryToken) {
        if (antiForgeryToken) {
            var ajaxAdapter = breeze.config.getAdapterInstance("ajax");
            ajaxAdapter.defaultSettings = {
                headers: {
                    '__RequestVerificationToken': antiForgeryToken
                },
            };
        }
    }
        
    function initValidation(validators) {

        var length = validators.length;
        for (var i = 0; i < length; i++) {
            manager.metadataStore.getEntityType(validators[i].entityName)
                .getProperty(validators[i].propertyName)
                .validators.push(validators[i].validator);
        }
        
        //change default messages: bool, date, duration, guid, integer, integerRange, maxLength, number, required, string, stringLength        
        //breeze.Validator.messageTemplates["required", "funky magic!!"];
        var options = new breeze.ValidationOptions({ validateOnSave: true, validateOnPropertyChange: false, validateOnAttach: false });
        manager.setProperties({validationOptions: options});
    }
    
    function initRepos() {
        module.questionVersions.init(manager);
        module.people.init(manager);
        module.testVersions.init(manager);
        module.sessions.init(manager);
        module.users.init(manager);
        module.candidateTests.init(manager);
        module.candidateAnswers.init(manager);
    }
    
    function revert() {
        module.isReverting = true;
        module.manager.rejectChanges();
        module.isReverting = false;
    }
   
})