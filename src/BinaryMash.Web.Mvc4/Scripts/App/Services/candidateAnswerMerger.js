﻿define(['services/dataContext'], function(dataContext) {


    return function(sessionId, qv, cqva) {

        if (!cqva) {
            cqva = dataContext.candidateAnswers.createQuestion(sessionId, qv.Id, qv.Version);
        }

        qv.candidateSupplied(cqva);

        for (var i = 0; i < qv.Sections.length; i++) {
            attachCandidateSection(qv.Sections[i], cqva);
        }
    };

    function attachCandidateSection(section, cqa) {
        var csa = cqa.findSection(section.Id);
        if (csa === null) {
            csa = dataContext.candidateAnswers.createSection(section.Id);
            cqa.Sections.push(csa);
        }

        section.candidateSupplied(csa);

        for (var i = 0; i < section.AnswerContents.length; i++) {
            attachCandidateAnswer(section, section.AnswerContents[i], csa);
        }
    }

    function attachCandidateAnswer(section, answerContent, csa) {
        var ca = csa.findAnswer(answerContent.Id);
        if (ca === null) {
            if (section.Type == "UserSupplied") {
                //always add for a user supplied. Other types will only be added when selected by user
                ca = dataContext.candidateAnswers.createAnswer(answerContent.Id);
                csa.Answers.push(ca);
            }
        }
        if (ca !== null) {
            if (section.Type == "UserSupplied") {
                ca.MaxLength(answerContent.MaxLength);
            }
            answerContent.candidateSupplied(ca);
        }
    }

    
})