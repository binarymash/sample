﻿define(['ko', 'initializers/complex/CandidateQuestionAndAnswerSection/view'], function (ko, sectionInitializer) {
        
    return function (question) {        

        question.candidateSupplied = ko.observable();

        for (var i = 0; i < question.Sections.length; i++) {
            sectionInitializer(question.Sections[i]);
        }

    };

})