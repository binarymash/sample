﻿define(['ko','services/pageDownConverter'], function(ko,converter) {
    
    return function (content) {

        content.candidateSupplied = ko.observable();        
        content.markdownText = ko.observable(converter.create().makeHtml(content.Text));

    };

})