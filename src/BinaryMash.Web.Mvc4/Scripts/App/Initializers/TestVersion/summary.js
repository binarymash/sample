﻿define(['breeze', 'services/dataContext'], function (breeze, dataContext) {
    
    return function (test) {

        //Observables
        
        test.canEdit = ko.computed(function () {
            return test.State() === 'Draft';
        });

        test.canCreateNewVersion = ko.computed(function () {
            return test.State() === 'Published' && test.Version() === test.Test().LatestVersion();
        });

        test.canSchedule = ko.computed(function () {
            return test.State() === 'Published' && test.Version() === test.Test().LatestVersion();
        });

        //Functions
        
        //Subscriptions
        
        //Bootstrap

    };

})