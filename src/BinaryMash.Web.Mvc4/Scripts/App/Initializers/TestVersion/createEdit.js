﻿define(['breeze', 'services/dataContext', 'validation/breezeExtensions', 'components/questionSearch', 'services/indexManager'],
    function (breeze, dataContext, validation, questionSearch, indexManager) {
    
    return function (test) {

        //Observables

        test.questionSearch = ko.observable(questionSearch);
        
        test.sortedQuestionVersions = ko.computed(function () {
            return test.QuestionVersions().sort(function (l, r) {
                return l.Index() == r.Index() ?
                    0 :
                    (l.Index() > r.Index() ? 1 : -1);
            });
        });
        
        //Functions

        test.questionSelected = function (qv) {
            var qvtv = dataContext.manager.createEntity("QuestionVersionTestVersion", {
                QuestionId: qv.QuestionId(),
                QuestionVersionVersion: qv.Version(),
                QuestionVersion: qv,
                TestId: test.TestId(),
                TestVersionVersion: test.Version(),
                Index:test.QuestionVersions().length                
            });
        };

        test.questionRemoved = function (qvtv) {
            //section.setDeleted();
            //indexManager.repack(question.Sections());
            
            test.QuestionVersions.remove(qvtv);            
            qvtv.entityAspect.setDeleted();
            indexManager.repack(test.sortedQuestionVersions());
        };

        test.allQuestionsRemoved = function (thisTest) {
            var allQvtv = test.QuestionVersions.removeAll();
            var quantity = allQvtv.length;
            for (var i = 0; i < quantity; i++) {
                allQvtv[i].entityAspect.setDeleted();
            }
        };
        
        test.clone = function() {
            //we never clone the Test, just the TestVersion            
            var newTestVersion = dataContext.manager.createEntity('TestVersion', { State: 1, Version: test.Test().LatestVersion() + 1, Test: test.Test() });
            newTestVersion.Name(test.Name());
            var quantity = test.QuestionVersions().length;
            for (var i = 0; i < quantity; i++) {
                newTestVersion.QuestionVersions.push(test.QuestionVersions()[i].clone());
            }
            return newTestVersion;
        };
                        
        //Subscriptions
        
        //Bootstrap
        validation.addHasValidationErrorsProperty(test, "Name");
    };

})