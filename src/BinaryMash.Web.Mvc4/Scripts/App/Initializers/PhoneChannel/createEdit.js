﻿define(['ko', 'breeze', 'services/dataContext', 'validation/breezeExtensions', 'services/phoneTypes'],
    function (ko, breeze, dataContext, validation, defaultTypes) {
    
    return function (channel) {

        //Properties
        
        channel.variant = 2;
        
        //Observables
        
        channel.isSelected = ko.observable(false);
        channel.allTypes = ko.observableArray(defaultTypes.slice(0)); //create copy so we don't end up changing the defaults
        if (channel.Type() === 'Other') {
            channel.allTypes.push({ id: channel.Name(), text: channel.Name() });
        }
        channel.typeSelection = ko.observable(channel.Type() === 'Other' ? channel.Name() : channel.Type());

        //Functions
        
        channel.createChoice = function (term, data) {
            if ($(data).filter(function () {
                return this.text.localeCompare(term) === 0;
            }).length === 0) {
                return { id: term, text: term };
            };
        };

        channel.remove = function () {
            channel.entityAspect.setDeleted();
        };
        
        channel.manualRevert = function () {
            channel.typeSelection(channel.Type() === 'Other' ? channel.Name() : channel.Type());
        };

        //Subscriptions
        
        channel.typeSelection.subscribe(function (value) {

            channel.allTypes.remove(function (i) {
                //remove all non-default types
                return !defaultTypes.some(function (e) {
                    return e.id === i.id;
                });
            });

            for (var i = 0; i < channel.allTypes().length; i++) {
                if (channel.allTypes()[i].id === value) {
                    channel.Type(value);
                    channel.Name("");
                    return;
                }
            }

            channel.allTypes.push({ id: value, text: value });
            channel.Type("Other");
            channel.Name(value);
        });

        //Bootstrap
        validation.addHasValidationErrorsProperty(channel, "Name");
        validation.addHasValidationErrorsProperty(channel, "Value");
        validation.addHasValidationErrorsProperty(channel, "Type");
    };

})