﻿define(['ko'], function (ko) {
    
    return function (session) {

        //Observables

        session.testUrl = ko.observable("/Test/Summary/"+ session.TestId());
        session.personUrl = ko.observable("/Person/Summary/"+session.PersonId());
        
        session.canEditDetails = ko.computed(function () {
            return session.State() === 'Scheduled';
        });

        session.canTakeTestNow = ko.computed(function () {
            return session.State() === 'Scheduled';
        });

        session.canContinueTest = ko.computed(function () {
            return session.State() === 'Started';
        });

        session.canViewAnswers = ko.computed(function () {
            return session.State() === 'Answered' ||
                   session.State() === 'RequiresMarking' ||
                   session.State() === 'Marked' ||
                   session.State() === 'Finished';
        });
        
        //Functions

        //Subscriptions        

        //Bootstrap

    };

})