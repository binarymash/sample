﻿define(['q', 'ko','breeze', 'services/dataContext', 'validation/breezeExtensions','components/testSearch','components/personSearch'],
    function (Q, ko, breeze, dataContext, validation, testSearch, personSearch) {
    
    return function (session) {

        //Observables

        session.testSearch = ko.observable(testSearch);
        session.personSearch = ko.observable(personSearch);
        session.versions = ko.observableArray([]);
        session.loadingTest = ko.observable(false);
        session.loadingPerson = ko.observable(false);
        
        //Functions

        session.manualRevert = function () {
            session.testSearch().manualRevert();
            session.personSearch().manualRevert();
        };

        session.loadDependencies = function() {
            return Q.all([
                session.loadTest(),
                session.loadPerson()
            ]);
        };
        
        //test id
        session.testSelected = function (test) {
            session.TestId(test.TestId());
            session.TestVersion(test);
            session.testSearch().disable();
            session.loadVersions(test.TestId());
        };

        session.loadTest = function () {
            if (session.TestId() === 0) {
                return;
            }
            
            session.loadingTest(true);
            return dataContext.testVersions.get(session.TestId(), session.TestVersion)
                .then(function () {
                    return session.loadVersions()
                        .then(function() {
                            session.loadingTest(false);
                        })
                        .fail(function() {
                            session.loadingTest(false);
                        });
                })
                .fail(function() {
                    session.loadingTest(false);
                });
        };
        
        session.loadVersions = function () {
            return dataContext.testVersions.getPublishedVersions(session.TestId(), session.versions)
                .then(function () {
                    if (session.TestVersion()) {
                        var latestPublished = session.TestVersion().Test().LatestPublishedVersion();
                        session.TestVersionVersion(latestPublished);
                    }
                })
                .fail(function (error) {
                    session.TestVersionVersion();
                });
        };

        //person id
        session.personSelected = function (person) {
            session.PersonId(person.Id());
            session.Person(person);
            session.personSearch().disable();
        };
        
        session.loadPerson = function () {
            if (session.PersonId() === 0) {
                return;
            }

            return dataContext.people.get(session.PersonId(), session.Person)
                .then(function() {
                    session.loadingPerson(false);
                })
                .fail(function() {
                    session.loadingPerson(false);
                });
        };

        //Subscriptions        

        //Bootstrap
        validation.addHasValidationErrorsProperty(session, "TestId");
        validation.addHasValidationErrorsProperty(session, "TestVersionVersion");
        validation.addHasValidationErrorsProperty(session, "PersonId");
    };

})