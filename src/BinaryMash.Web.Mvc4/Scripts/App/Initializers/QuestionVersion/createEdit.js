﻿define(['breeze', 'services/dataContext', 'validation/breezeExtensions', 'services/indexManager'],
    function (breeze, dataContext, validation, indexManager) {
    
    return function (question) {

        //Observables
        
        //Functions

        question.addSection = function() {
            var section = dataContext.manager.createEntity("QuestionAndAnswerSection", {Index:question.Sections().length});
            question.Sections.push(section);
        };

        question.removeSection = function (section) {
            section.setDeleted();
            indexManager.repack(question.Sections());
        };

        question.moveSectionFirst = function (section) {
            indexManager.moveFirst(section, question.Sections());
        };

        question.moveSectionUp = function (section) {
            indexManager.moveUp(section, question.Sections());
        };

        question.moveSectionDown = function (section) {
            indexManager.moveDown(section, question.Sections());
        };

        question.moveSectionLast = function (section) {
            indexManager.moveLast(section, question.Sections());
        };

        question.clone = function () {
            //we never clone the Question, just the QuestionVersion            
            var newQuestionVersion = dataContext.manager.createEntity('QuestionVersion', { State: 1, Version: question.Question().LatestVersion() + 1, Question: question.Question() });
            newQuestionVersion.Title(question.Title());
            var quantity = question.Sections().length;
            for (var i = 0; i < quantity; i++) {
                newQuestionVersion.Sections.push(question.Sections()[i].clone());
            }
            return newQuestionVersion;
        };

        question.initPageDownSection = function (templ,model) {
            model.pageDown().init();
        };

        //Subscriptions
        
        //Bootstrap
        validation.addHasValidationErrorsProperty(question, "Title");
        validation.addHasValidationErrorsProperty(question, "Sections");
    };

})