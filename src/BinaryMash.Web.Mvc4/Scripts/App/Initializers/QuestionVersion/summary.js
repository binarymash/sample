﻿define(['breeze', 'services/dataContext'], function (breeze, dataContext) {
    
    return function (question) {

        //Observables

        question.canEdit = ko.computed(function() {
            return question.State() === 'Draft';
        });

        question.canCreateNewVersion = ko.computed(function() {
            return question.State() === 'Published' && question.Version() === question.Question().LatestVersion();
        });
        
        //Functions

        //Subscriptions

        //Bootstrap

    };

})