﻿define(['ko','moment'], function(ko,moment) {
    
    return function (qv) {

        //Observables
                
        qv.UpdatedDisplay = ko.computed(function () {
            return moment(qv.Updated()).format("LLLL");
        });
        
        //Functions
        
        //Subscriptions
        
        //Bootstrap

    };

})