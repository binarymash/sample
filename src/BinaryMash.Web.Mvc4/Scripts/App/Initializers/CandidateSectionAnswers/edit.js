﻿define(['ko', 'repos/candidateAnswersRepo'], function (ko, answersRepo) {
    
    return function (section) {

        //Observables

        section.changing = ko.observable(false);
        
        section.checkboxAnswerSelected = function(answerContentId) {
            var ca = section.findAnswer(answerContentId);
            return ca !== null;
        };
        
        section.radioAnswerSelected = function () {
            if (section.Answers().length > 0) {
                return section.Answers()[0].AnswerContentId();
            }
            return null;
        };

        //Functions        

        section.checkboxAnswerChanged = function (answerContent, event) {
            if (!section.changing()) {
                section.changing(true);
                var ca = section.findAnswer(answerContent.Id);
                if (event.originalEvent.srcElement.checked === true) {
                    if (ca === null) {
                        var newCa = answersRepo.createAnswer(answerContent.Id);
                        section.Answers.push(newCa);
                    }
                } else {
                    if (ca !== null) {
                        section.Answers.remove(ca);
                        ca.entityAspect.setDeleted();
                    }
                }
                section.changing(false);
            }
        };

        section.radioAnswerChanged = function (answerContent, event) {
            if (!section.changing()) {
                section.changing(true);
                while (section.Answers().length > 0) {
                    var oldAnswer = section.Answers.pop();
                    oldAnswer.entityAspect.setDeleted();
                }

                var newCa = answersRepo.createAnswer(answerContent.Id);
                section.Answers.push(newCa);
                section.changing(false);
            }
        };

        section.findAnswer = function (answerContentId) {
            for (var i = 0; i < section.Answers().length; i++) {
                if (section.Answers()[i].AnswerContentId() == answerContentId) {
                    return section.Answers()[i];
                }
            }
            return null;
        };
                
        //Subscriptions
        
        //Bootstrap
        
    };

})