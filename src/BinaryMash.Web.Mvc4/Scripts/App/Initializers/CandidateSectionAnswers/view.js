﻿define([], function () {
    
    return function (section) {

        //Observables
        
        section.checkboxAnswerSelected = function(answerContentId) {
            var ca = section.findAnswer(answerContentId);
            return ca !== null;
        };
        
        section.radioAnswerSelected = function () {
            if (section.Answers().length > 0) {
                return section.Answers()[0].AnswerContentId();
            }
            return null;
        };

        //Functions        

        section.findAnswer = function (answerContentId) {
            for (var i = 0; i < section.Answers().length; i++) {
                if (section.Answers()[i].AnswerContentId() == answerContentId) {
                    return section.Answers()[i];
                }
            }
            return null;
        };
                
        //Subscriptions
        
        //Bootstrap
        
    };

})