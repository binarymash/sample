﻿define(['services/dataContext','validation/breezeExtensions'], function (dataContext, validation) {
    
    return function (answerContent) {
        
        //Observables
        
        answerContent.isActive = ko.observable(false);

        //Functions

        answerContent.activate = function () {
            answerContent.isActive(true);
        };

        answerContent.inactivate = function () {
            answerContent.isActive(false);
        };

        answerContent.setDeleted = function () {
            answerContent.entityAspect.setDeleted();
        };
        
        answerContent.clone = function () {
            var newAnswerContent = dataContext.manager.createEntity('AnswerContentUserSupplied');
            newAnswerContent.MaxLength(answerContent.MaxLength());
            return newAnswerContent;
        };

        //Subscriptions
        
        //Bootstrap
        validation.addHasValidationErrorsProperty(answerContent, "MaxLength");
        validation.addHasValidationErrorsProperty(answerContent, "Notes");

    };

})