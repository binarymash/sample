﻿define(['services/dataContext','validation/breezeExtensions','components/pageDown'], function (dataContext,validation, PageDown) {

    return function (answerContent) {
        
        //Observables
        
        answerContent.isActive = ko.observable(false);

        answerContent.number = ko.computed(function () {
            return (answerContent.Index() + 1).toString();
        }, answerContent);

        answerContent.pageDown = ko.observable(new PageDown());

        //Functions
        

        answerContent.activate = function () {
            answerContent.isActive(true);
        };

        answerContent.inactivate = function () {
            answerContent.isActive(false);
        };

        answerContent.setDeleted = function () {
            answerContent.entityAspect.setDeleted();
        };

        answerContent.clone = function () {
            var newAnswerContent = dataContext.manager.createEntity('AnswerContentQuestionerSupplied');
            newAnswerContent.Text(answerContent.Text());
            return newAnswerContent;
        };

        //Subscriptions
        
        
        //Bootstrap
        validation.addHasValidationErrorsProperty(answerContent, "Text");
        validation.addHasValidationErrorsProperty(answerContent, "Notes");

    };

})