﻿define(['services/dataContext','validation/breezeExtensions','services/indexManager','components/pageDown'], function(dataContext, validation, indexManager, PageDown) {
    
    return function (section) {

        //Observables
        
        section.number = ko.computed(function () {
            return (section.Index()+1).toString();
        }, section);

        section.activeSpecialismTemplate = ko.observable();

        section.isCloning = ko.observable(false);
        
        section.pageDown = ko.observable(new PageDown());
        
        //Functions        

        section.setActiveAnswerTemplate = function () {
            if (section.Type()) {
                section.activeSpecialismTemplate("answerContainerSpecialism" + section.Type() + "-template");
            } else {
                section.activeSpecialismTemplate(undefined);
            }
        };

        section.addAnswer = function () {
            var entityName = undefined;
            if (section.Type() === "UserSupplied") {
                entityName = "AnswerContentUserSupplied";
            } else if (section.Type() === "Radio" || section.Type() === "Checkbox") {
                entityName = "AnswerContentQuestionerSupplied";
            }
            if (entityName) {
                var answer = dataContext.manager.createEntity(entityName, { Index: section.AnswerContents().length });
                section.AnswerContents.push(answer);
            }
        };

        section.setDeleted = function() {
            while (section.AnswerContents().length > 0) {
                section.AnswerContents()[0].setDeleted();
            }
            section.entityAspect.setDeleted();
        };
        
        section.removeAnswer = function (answer) {
            answer.setDeleted();
            indexManager.repack(section.AnswerContents());
        };

        section.moveAnswerFirst = function (answer) {
            indexManager.moveFirst(answer, section.AnswerContents());
        };

        section.moveAnswerUp = function (answer) {
            indexManager.moveUp(answer, section.AnswerContents());
        };

        section.moveAnswerDown = function (answer) {
            indexManager.moveDown(answer, section.AnswerContents());
        };

        section.moveAnswerLast = function (answer) {
            indexManager.moveLast(answer, section.AnswerContents());
        };

        section.clone = function () {
            var newSection = dataContext.manager.createEntity('QuestionAndAnswerSection');
            newSection.isCloning(true);
            newSection.Index(section.Index());
            newSection.Text(section.Text());
            newSection.Notes(section.Notes());
            newSection.Type(section.Type());
            newSection.setActiveAnswerTemplate();
            var quantity = section.AnswerContents().length;
            for (var i = 0; i < quantity; i++) {
                newSection.AnswerContents.push(section.AnswerContents()[i].clone());
            }
            newSection.isCloning(false);
            return newSection;
        };
  
        section.initPageDownAnswer = function (templ, model) {
            model.pageDown().init();
        };
        
        //Subscriptions
        
        section.Type.subscribe(function () {
            if (section.isCloning()) {
                return;
            }
            if (dataContext.isReverting) {
                //without this block, we blow up on a revert
                section.AnswerContents.removeAll();
                section.setActiveAnswerTemplate();
            } else {
                while (section.AnswerContents().length > 0) {
                    section.AnswerContents()[0].setDeleted();
                }
                section.setActiveAnswerTemplate();
                section.addAnswer();
            }
        });

        //Bootstrap
        
        section.setActiveAnswerTemplate();
        validation.addHasValidationErrorsProperty(section, "Text");
        validation.addHasValidationErrorsProperty(section, "Notes");
        validation.addHasValidationErrorsProperty(section, "Type");
    };

})