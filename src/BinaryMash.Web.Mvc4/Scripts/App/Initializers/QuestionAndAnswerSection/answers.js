﻿define(['services/pageDownConverter'], function (converter) {

    return function (section) {

        section.candidateSupplied = ko.observable();
        section.markdownText = ko.observable(converter.create().makeHtml(section.Text()));

        section.number = ko.computed(function () {
            return (section.Index() + 1).toString();
        }, section);

        section.activeSpecialismTemplate = ko.observable("answerContainerSpecialism" + section.Type() + "-template");


    };

})