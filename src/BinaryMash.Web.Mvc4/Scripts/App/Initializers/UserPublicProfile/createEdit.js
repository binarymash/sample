﻿define(['ko', 'validation/breezeExtensions'], function (ko, validation) {
    return function (user) {

        //Observables

        //Functions

        //Subscriptions

        //Bootstrap
        
        validation.addHasValidationErrorsProperty(user, "DisplayName");
    };

})