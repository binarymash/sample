﻿define(['ko', 'validation/breezeExtensions'], function (ko, validation) {
    return function (user) {

        //Observables

        user.Name = ko.computed(function () {
            return user.FirstName() + " " + user.LastName();
        });

        //Functions

        //Subscriptions

        //Bootstrap

        validation.addHasValidationErrorsProperty(user, "Email");
        validation.addHasValidationErrorsProperty(user, "FirstName");
        validation.addHasValidationErrorsProperty(user, "LastName");
    };

})