﻿define(['breeze', 'moment', 'services/dataContext', 'components/questionVersionSearch'],
    function (breeze, moment, dataContext, VersionSearch) {

        return function (qvtv) {

            //Observables

            qvtv.versions = ko.observableArray([]);
            qvtv.changingVersion = ko.observable(false);
            qvtv.loadingVersions = ko.observable(false);
            qvtv.versionSearch = ko.observable();

            qvtv.UpdatedDisplay = ko.computed(function () {
                return qvtv.QuestionVersion() ? moment(qvtv.QuestionVersion().Updated()).format("LLLL") : '';
            });

            //Functions

            qvtv.clone = function() {
                return dataContext.manager.createEntity("QuestionVersionTestVersion", {
                    QuestionId: qvtv.QuestionId(),
                    QuestionVersionVersion: qvtv.QuestionVersionVersion()
                });
            };

            qvtv.changeVersion = function() {
                qvtv.changingVersion(true);
                var search = new VersionSearch(qvtv.QuestionId());
                qvtv.versionSearch(search);
                qvtv.versionSearch().refresh();
            };

            qvtv.versionSelected = function (qv) {
                qvtv.QuestionVersionVersion(qv.Version());
                qvtv.changingVersion(false);
                qvtv.changeVersionFinished();
            };

            qvtv.changeVersionFinished = function () {
                var versionSearch = qvtv.versionSearch();
                qvtv.versionSearch(undefined);
                delete versionSearch;
            };
            
            //Subscriptions

            //Bootstrap

        };

    })