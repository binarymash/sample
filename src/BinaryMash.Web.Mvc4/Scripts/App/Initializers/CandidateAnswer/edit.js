﻿define(['ko', 'validation/breezeExtensions'], function (ko, validation) {
    
    return function (answer) {

        //Observables
        answer.MaxLength = ko.observable(1);
        
        answer.charactersLeft = ko.computed(function () {
            if (!answer.Text()) {
                return answer.MaxLength();
            }

            return (answer.MaxLength() - answer.Text().length);
        });

        answer.charactersLeftDisplay = ko.computed(function () {
            var remaining = answer.charactersLeft();
            if (remaining < 0) {
                remaining = 0;
            }
            return remaining;
        });

        answer.fewCharactersLeft = ko.computed(function () {
            return answer.charactersLeft() < 50;
        });

        answer.noCharactersLeft = ko.computed(function () {
            return answer.charactersLeft() < 0;
        });

        //Functions        
                
        //Subscriptions
        
        //Bootstrap
        validation.addHasValidationErrorsProperty(answer, "Text");

    };

})