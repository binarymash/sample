﻿define(['breeze', 'services/dataContext', 'validation/breezeExtensions'],
    function (breeze, dataContext, validation) {
    
    return function (person) {

        //Observables
                
        //Functions

        person.addEmail = function () {
            var channel = dataContext.manager.createEntity("EmailChannel");
            person.CommunicationChannels.push(channel);
            channel.isSelected(true);
        };

        person.removeEmail = function (channel) {
            channel.setDeleted();
        };

        person.addPhone = function () {
            var channel = dataContext.manager.createEntity("PhoneChannel");
            person.CommunicationChannels.push(channel);
            channel.isSelected(true);
        };

        person.removePhone = function (channel) {
            channel.setDeleted();
        };

        person.addAddress = function () {
            var channel = dataContext.manager.createEntity("AddressChannel");
            person.CommunicationChannels.push(channel);
            channel.isSelected(true);
        };

        person.removeAddress = function (channel) {
            channel.setDeleted();
        };

        person.manualRevert = function() {
            for (i = 0; i < person.CommunicationChannels().length;i++) {
                person.CommunicationChannels()[i].manualRevert();
            }
        };

        //Subscriptions
        
        //Bootstrap
        validation.addHasValidationErrorsProperty(person, "Name");
        validation.addHasValidationErrorsProperty(person, "CommunicationChannels");
        validation.addHasValidationErrorsProperty(person, "Notes");
    };

})