﻿namespace BinaryMash.Web.Mvc4.Helpers
{
    using System.Web;
    using Domain;

    //Don't trust anything on this cookie!
    public static class ProfileCookie
    {

        public static string CookieName { get { return "BinaryMash.Apt.Profile"; } }

        public static void Add(UserIdentity userIdentity)
        {
            var myCookie = new HttpCookie(CookieName);
            myCookie["Name"] = userIdentity.FirstName + " " + userIdentity.LastName;
            myCookie.Shareable = false;
            myCookie.Secure = false;
            myCookie.HttpOnly = true;
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }

        public static void Remove()
        {
            HttpContext.Current.Response.Cookies.Remove(CookieName);
        }

    }
}