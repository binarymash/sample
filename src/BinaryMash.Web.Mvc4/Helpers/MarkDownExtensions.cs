﻿namespace BinaryMash.Web.Mvc4.Helpers
{
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using StackExchange.DataExplorer.Helpers;

    public static class MarkDownExtensions
    {
        public static MvcHtmlString Markdown(this HtmlHelper helper, string rawText)
        {
            var transformed = HtmlUtilities.RawToCooked(rawText);
            var safe = HtmlUtilities.Safe(transformed);
            //var safe2 = HtmlUtilities.BalanceTags(safe); //TODO: check
            return new MvcHtmlString(safe);
        }

        public static string MarkdownSummary(this HtmlHelper helper, string rawText, int summaryLength, bool ellipsis = true)
        {
            var transformed = HtmlUtilities.RawToCooked(rawText);
            var safe = HtmlUtilities.Safe(transformed);
            var snipped = HtmlUtilities.RemoveTags(safe);
            if (snipped.Length > 200)
            {
                snipped = snipped.Substring(0, 200);
                if (ellipsis)
                {
                    snipped += "...";
                }
            }

            return snipped;
        }

        public static Regex rxExtractLanguage = new Regex("^({{(.+)}}[\r\n])", RegexOptions.Compiled);

        //public static string FormatCodeSyntaxHighlighter(MarkdownDeep.Markdown m, string code)
        //{
        //    // Try to extract the language from the first line
        //    var match = rxExtractLanguage.Match(code);
        //    string language = null;

        //    if (match.Success)
        //    {
        //        // Save the language
        //        var g = (Group)match.Groups[2];
        //        language = g.ToString();

        //        // Remove the first line
        //        code = code.Substring(match.Groups[1].Length);
        //    }

        //    // If not specified, look for a link definition called "default_syntax" and
        //    // grab the language from its title
        //    if (language == null)
        //    {
        //        var d = m.GetLinkDefinition("default_syntax");
        //        if (d != null)
        //            language = d.title;
        //    }

        //    // Common replacements
        //    if (language == "C#")
        //        language = "cs";
        //    if (language == "C++")
        //        language = "cpp";

        //    // Wrap code in pre/code tags and add PrettyPrint attributes if necessary
        //    if (string.IsNullOrEmpty(language))
        //    {
        //        return string.Format("<pre><code>{0}</code></pre>\n", code);
        //    }

        //    var newCode = string.Format("<pre><code class=\"{0}\">{1}</code></pre>\n", language.ToLowerInvariant(), code);
        //    return newCode;
        //}
    }
}