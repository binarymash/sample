﻿namespace BinaryMash.Web.Mvc4.Helpers
{
    using System.Diagnostics;
    using System.Web.Mvc;

    public static class VersionHelper
    {
        private static readonly object Lock = new object();
        private static string _versionInfo;

        public static string AssemblyInformationalVersion(this HtmlHelper htmlHelper)
        {
            if (_versionInfo == null)
            {
                lock (Lock)
                {
                    if (_versionInfo == null)
                    {
                        _versionInfo = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion;
                    }
                }
            }
            return _versionInfo;
        }
    }
}