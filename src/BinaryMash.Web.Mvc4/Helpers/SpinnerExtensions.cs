﻿namespace BinaryMash.Web.Mvc4.Helpers
{
    using System.Globalization;
    using System.Web.Mvc;

    public static class SpinnerExtensions
    {
        public static MvcHtmlString Spinner(this HtmlHelper helper, string dataBind)
        {
            
            return new MvcHtmlString(string.Format(CultureInfo.InvariantCulture, 
                "<div class=\"text-center\" data-bind=\"{0}\"><div><img src=\"{1}\" /></div><div>Please wait a moment...</div></div>", 
                dataBind, UrlHelper.GenerateContentUrl("~/Content/images/spiffygif_60x60.gif", helper.ViewContext.HttpContext)));
        }
    }
}

    