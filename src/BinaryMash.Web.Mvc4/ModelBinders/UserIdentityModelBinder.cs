﻿namespace BinaryMash.Web.Mvc4.ModelBinders
{
    using System;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Security;
    using BinaryMash.Domain;

    /// <summary>
    /// Binder to pull the UserData out for any actions that may want it.
    /// See http://www.danharman.net/2011/07/07/storing-custom-data-in-forms-authentication-tickets/
    /// </summary>
    public class UserIdentityModelBinder<T> : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.Model != null)
            {
                throw new InvalidOperationException("Cannot update instances");
            }

            if (controllerContext.RequestContext.HttpContext.Request.IsAuthenticated)
            {
                var cookie = controllerContext
                    .RequestContext
                    .HttpContext
                    .Request
                    .Cookies[FormsAuthentication.FormsCookieName];

                if (null == cookie)
                {
                    return null;
                }

                var decrypted = FormsAuthentication.Decrypt(cookie.Value);

                if (!string.IsNullOrEmpty(decrypted.UserData))
                {
                    return Json.Decode<UserIdentity>(decrypted.UserData);
                }
            }

            return null;
        }
    }
}