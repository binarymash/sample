﻿namespace BinaryMash.Web.Mvc4.Models.Session
{
    using System;

    public class CreateEdit
    {
        public Guid Id { get; set; }
        public int TestId { get; set; }
        public int TestVersion { get; set; }
        public int PersonId { get; set; }
    }
}