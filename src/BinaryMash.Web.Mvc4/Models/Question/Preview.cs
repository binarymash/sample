﻿namespace BinaryMash.Web.Mvc4.Models.Question
{
    using System;
    using Domain;

    public class Preview
    {
        public bool IsAuthenticated { get; set; }
        public Guid? UserAccountId { get; set; }
        public QuestionVersion QuestionVersion { get; set; }
        public bool CanEdit()
        {
            if (!UserAccountId.HasValue)
            {
                return false;
            }

            return UserAccountId == QuestionVersion.Question.AccountId;
        }
    }
}