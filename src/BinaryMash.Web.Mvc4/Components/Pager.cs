﻿namespace BinaryMash.Web.Mvc4.Components
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class Pager
    {
        public int NumberOfItems { get; private set; }
        public int PageSize { get; private set; }
        public int NumberOfPages { get; private set; }
        public int CurrentPageNumber { get; private set; }
        public int NumberOfPagesToDisplay { get; private set; }
        public string BaseUrl { get; private set; }
        public ICollection<int> Pages { get; private set; }

        public Pager(int currentPageNumber, int numberOfItems, int pageSize, string baseUrl, int numberOfPagesToDisplay)
        {
            NumberOfItems = numberOfItems;
            PageSize = pageSize;
            BaseUrl = baseUrl;
            NumberOfPagesToDisplay = numberOfPagesToDisplay;

            NumberOfPages = Convert.ToInt32(Math.Ceiling((double)NumberOfItems / PageSize));
            CurrentPageNumber = currentPageNumber > NumberOfPages ? NumberOfPages : currentPageNumber;

            InitialisePages();
        }

        public int PreviousPage
        {
            get
            {
                if (NumberOfPages == 0)
                {
                    return 0;
                }

                return CurrentPageNumber == 1 ? 1 : CurrentPageNumber - 1;
            }
        }

        public int NextPage
        {
            get
            {
                if (NumberOfPages == 0)
                {
                    return 0;
                }

                return CurrentPageNumber == NumberOfPages ? CurrentPageNumber : CurrentPageNumber + 1;
            }
        }

        private void InitialisePages()
        {
            Pages = new Collection<int>();

            if (NumberOfPages > 0)
            {
                var split = Convert.ToInt32(Math.Round((double) (NumberOfPagesToDisplay - 1)/2, MidpointRounding.ToEven));
                var shortageAfter = split - (NumberOfPages - CurrentPageNumber);
                var idealFirstPage = CurrentPageNumber - split - (shortageAfter > 0 ? shortageAfter : 0);

                var firstIndex = (idealFirstPage < 1) ? 1 : idealFirstPage;
                var currentIndex = firstIndex - 1;
                var pagesAdded = 0;
                do
                {
                    currentIndex++;
                    pagesAdded++;
                    Pages.Add(currentIndex);
                } while ((pagesAdded < NumberOfPagesToDisplay) && (currentIndex < NumberOfPages));
            }
        }

    }
}