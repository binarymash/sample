﻿using System;

namespace BinaryMash.Web.Mvc4.Membership
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Security;
    using DataAccess.Repositories.Contract;
    using Domain;

    public class MyRoleProvider : RoleProvider
    {
        public MyRoleProvider(ILoginRepository loginRepository)
        {
            _loginRepository = loginRepository;

            //Initialise never called from Windsor. Looks like we need to initialise to set the Name property.
            base.Initialize("MyRoleProvider", new NameValueCollection());
        }

        public override bool IsUserInRole(string identity, string roleName)
        {

            return GetRolesForUser(identity).Any(p => string.Equals(p, roleName, StringComparison.OrdinalIgnoreCase));
        }

        public override string[] GetRolesForUser(string loginId)
        {
            var id = IdentityConverter.ExtractUserId(loginId);

            var user = _loginRepository.GetAll()
                                       .Include(l => l.User)
                                       .Where(l => l.Id == id)
                                       .Select(l => l.User)
                                       .FirstOrDefault();

            var userPermissions = user == null ? 0 : user.Permissions;
            
            if (userPermissions == 0)
            {
                return new string[]{};
            }

            var allPermissions = Enum.GetValues(typeof (UserPermissionType));

            var userPermissionNames = 
                from object permission
                    in allPermissions
                where (userPermissions & (int) permission) == (int) permission
                select Enum.GetName(typeof (UserPermissionType), (UserPermissionType) permission);
            ;

            return userPermissionNames.ToArray();
        }

        #region not implementing

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override sealed string ApplicationName { get; set; }

        #endregion

        private readonly ILoginRepository _loginRepository;
    }
}
