﻿namespace BinaryMash.Web.Mvc4.Membership
{
    using System;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Domain;

    [Flags]
    public enum AuthorizationSessionType
    {
        Anonymous = 1,
        User = 2,
        Candidate = 4
    }

    /// <summary>
    /// We derive from AuthorizeAttribute so that this filter runs before caching
    /// Needs adding in FilterConfig.cs
    /// </summary>
    public class MyAuthorizeAttribute : AuthorizeAttribute
    {

        public string Controller { get; set; }
        public string Action { get; set; }
        public AuthorizationSessionType AllowedSessionTypes { get; set; }

        public MyAuthorizeAttribute()
        {
            AllowedSessionTypes = AuthorizationSessionType.User;
            Controller = "Authentication";
            Action = "Index";
        }

        public AuthorizationSessionType SessionType(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            IPrincipal user = httpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
                return AuthorizationSessionType.Anonymous;
            }

            if (IdentityConverter.IsSession(user.Identity.Name))            
            {
                return AuthorizationSessionType.Candidate;
            }

            return AuthorizationSessionType.User;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var sessionType = SessionType(filterContext.HttpContext);
            if ((sessionType == AuthorizationSessionType.Anonymous) ||
                (AllowedSessionTypes & sessionType) == sessionType)
            {
                base.OnAuthorization(filterContext);
                return;
            }

            //not in the allowed list of types, so immediately unauthorized
            HandleUnauthorizedRequest(filterContext);
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            RouteValueDictionary rvd;

            var sessionType = SessionType(filterContext.HttpContext);
            if (sessionType == AuthorizationSessionType.Candidate)
            {
                rvd = new RouteValueDictionary(
                    new
                    {
                        controller = "Candidate", 
                        action = "Welcome"
                    });    
            }
            else
            {
                rvd = new RouteValueDictionary(
                    new
                    {
                        controller = Controller,
                        action = Action
                    });
            }

            filterContext.Result = new RedirectToRouteResult(rvd);
        }

    }
}