﻿namespace BinaryMash.Web.Mvc4.Injection
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using NServiceBus;
    using NServiceBus.ObjectBuilder;

    public class NServiceBusMvcDependencyResolverAdapter : IDependencyResolver
    {
        readonly IBuilder _builder;

        public NServiceBusMvcDependencyResolverAdapter(IBuilder builder)
        {
            _builder = builder;
        }

        public object GetService(Type serviceType)
        {
            if (Configure.Instance.Configurer.HasComponent(serviceType))
            {
                return _builder.Build(serviceType);
            }

            return null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _builder.BuildAll(serviceType);
        }

    }
}

