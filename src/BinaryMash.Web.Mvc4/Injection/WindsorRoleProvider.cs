﻿namespace BinaryMash.Web.Mvc4.Injection
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Web.Security;
    using Castle.Windsor;

    //See http://bugsquash.blogspot.co.uk/2010/11/windsor-managed-membershipproviders.html
    public abstract class WindsorRoleProvider : RoleProvider
    {
        private string _providerId;

        public abstract IWindsorContainer GetContainer();

        private RoleProvider GetProvider()
        {
            try
            {
                var provider = GetContainer().Resolve<RoleProvider>(_providerId, new Hashtable());
                if (provider == null)
                {
                    throw new Exception(string.Format("Component '{0}' does not inherit RoleProvider", _providerId));
                }

                return provider;
            }
            catch (Exception e)
            {
                throw new Exception("Error resolving RoleProvider " + _providerId, e);
            }
        }

        private T WithProvider<T>(Func<RoleProvider, T> f)
        {
            var provider = GetProvider();
            try
            {
                return f(provider);
            }
            finally
            {
                GetContainer().Release(provider);
            }
        }

        private void WithProvider(Action<RoleProvider> f)
        {
            var provider = GetProvider();
            try
            {
                f(provider);
            }
            finally
            {
                GetContainer().Release(provider);
            }
        }

        #region RoleProvider implementation

        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            _providerId = config["providerId"];
            if (string.IsNullOrWhiteSpace(_providerId))
            {
                throw new Exception("Please configure the providerId from the membership provider " + name);
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            WithProvider(p => p.AddUsersToRoles(usernames, roleNames));
        }

        public override void CreateRole(string roleName)
        {
            WithProvider(p => p.CreateRole(roleName));
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            return WithProvider(p => p.DeleteRole(roleName, throwOnPopulatedRole));
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return WithProvider(p => p.FindUsersInRole(roleName, usernameToMatch));
        }

        public override string[] GetAllRoles()
        {
            return WithProvider(p => p.GetAllRoles());
        }

        public override string[] GetRolesForUser(string username)
        {
            return WithProvider(p => p.GetRolesForUser(username));
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return WithProvider(p => p.GetUsersInRole(roleName));
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            WithProvider(p => p.RemoveUsersFromRoles(usernames, roleNames));
        }

        public override bool RoleExists(string roleName)
        {
            return WithProvider(p => p.RoleExists(roleName));
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            return WithProvider(p => p.IsUserInRole(username, roleName));
        }

        public override string ApplicationName
        {
            get { return WithProvider(p => p.ApplicationName); }
            set { WithProvider(p => p.ApplicationName = value); }
        }

        public override string Name
        {
            get { return WithProvider(p => p.Name); }
        }

        public override string Description
        {
            get { return WithProvider(p => p.Description); }
        }


        #endregion

    }

}