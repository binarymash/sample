﻿namespace BinaryMash.Web.Mvc4.Injection
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dependencies;
    using NServiceBus;
    using NServiceBus.ObjectBuilder;

    public class NServiceBusHttpDependencyResolverAdapter : IDependencyResolver
    {

        private IBuilder _builder;

        public NServiceBusHttpDependencyResolverAdapter(IBuilder builder)
        {
            _builder = builder;
        }

        public object GetService(Type serviceType)
        {
            if (typeof (IHttpController).IsAssignableFrom(serviceType))
            {
                return _builder.Build(serviceType);
            }

            return null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (typeof (IHttpController).IsAssignableFrom(serviceType))
            {
                return _builder.BuildAll(serviceType);
            }

            return new List<object>();
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public void Dispose()
        {
            /*No-op*/
        }
    }
}

