﻿namespace BinaryMash.Web.Mvc4.Injection
{
    using System;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dispatcher;
    using System.Web.Mvc;
    using NServiceBus;

    public static class ConfigureHttpDependecyInjection
    {

        public static Configure ForWebApi(this Configure configure)
        {
            // Register our controller activator with NSB
            configure.Configurer.RegisterSingleton(typeof(IHttpControllerActivator),
                                                   new NServiceBusHttpControllerActivator());

            // Find every controller class so that we can register it
            var controllers = Configure.TypesToScan
                                       .Where(t => typeof(IHttpController).IsAssignableFrom(t));

            // Register each controller class with the NServiceBus container
            foreach (Type type in controllers)
            {
                configure.Configurer.ConfigureComponent(type, DependencyLifecycle.InstancePerCall);
            }

            GlobalConfiguration.Configuration.DependencyResolver = new NServiceBusHttpDependencyResolverAdapter(configure.Builder);

            return configure;
        }

    }
}
