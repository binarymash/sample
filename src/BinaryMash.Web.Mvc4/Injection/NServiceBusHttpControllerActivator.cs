﻿namespace BinaryMash.Web.Mvc4.Injection
{

    using System;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dispatcher;

    /// <summary>
    /// http://ahchurch.blogspot.co.uk/2013/01/using-nservicebus-with-aspnet-mvc-webapi.html
    /// </summary>
    public class NServiceBusHttpControllerActivator : IHttpControllerActivator
    {
        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            return GlobalConfiguration.Configuration.Services
                                      .GetService(controllerType) as IHttpController;
        }
    }    
    
}