﻿namespace BinaryMash.Web.Mvc4.Injection
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using NServiceBus;

    public static class ConfigureMvcDependecyInjection
    {

        public static Configure ForMvc(this Configure configure)
        {
            // Register our controller activator with NSB
            configure.Configurer.RegisterSingleton(typeof(IControllerActivator),
                                                   new NServiceBusMvcControllerActivator());

            // Find every controller class so that we can register it
            var controllers = Configure.TypesToScan
                                       .Where(t => typeof(IController).IsAssignableFrom(t));

            // Register each controller class with the NServiceBus container
            foreach (Type type in controllers)
            {
                configure.Configurer.ConfigureComponent(type, DependencyLifecycle.InstancePerCall);
            }

            DependencyResolver.SetResolver(new NServiceBusMvcDependencyResolverAdapter(configure.Builder));

            return configure;
        }

    }
}
