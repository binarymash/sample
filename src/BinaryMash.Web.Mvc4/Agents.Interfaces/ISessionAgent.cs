﻿namespace BinaryMash.Web.Mvc4.Agents.Interfaces
{
    using System;
    using System.Security.Principal;

    public interface ISessionAgent
    {
        bool AuthenticateSession(Guid id, string password);
        bool StartSession(IIdentity identity);
        bool FinishSession(IIdentity identity);

    }
}
