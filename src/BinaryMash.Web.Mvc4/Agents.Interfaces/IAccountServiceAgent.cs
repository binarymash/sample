﻿namespace BinaryMash.Web.Mvc4.Agents.Interfaces
{
    using System;
    using Domain;

    public interface IAccountServiceAgent
    {
        bool AuthenticateUser(string email,string plainTextPassword);
        UserIdentity GetUserIdentity(string email);
        bool UserIsInUse(string email);

        bool CreateUser(Guid accountId, Guid userId, string email, string firstName, string lastName, string plaintextPassword);
        void ValidateUser(Guid userValidationSagaId);
        
        bool ForgottenPassword(string email);
        bool ResetPassword(Guid sagaId, string email, string plaintextPassword);

    }
}
