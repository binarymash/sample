﻿using System.Web.Mvc;

namespace BinaryMash.Web.Mvc4.Controllers
{
    using System;
    using System.Web;
    using System.Web.Security;
    using Domain;

    public class UserController : BaseUserController
    {
        public ActionResult Home()
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            string cookiePath = ticket.CookiePath;
            DateTime expiration = ticket.Expiration;
            bool expired = ticket.Expired;
            bool isPersistent = ticket.IsPersistent;
            DateTime issueDate = ticket.IssueDate;
            string name = ticket.Name;
            string userData = ticket.UserData;
            int version = ticket.Version;

            return View();
        }

        public ActionResult Summary()
        {
            return View("Summary", null, IdentityConverter.ExtractUserId(User.Identity.Name));
        }

        public ActionResult Edit(Guid id)
        {
            return View("Edit", null, id);
        }

        public ActionResult ChangeEmail(Guid id)
        {
            return View("ChangeEmail", null, id);
        }

        public ActionResult ChangePassword()
        {
            return View("ChangePassword", null, IdentityConverter.ExtractId(User.Identity.Name));
        }
    
    }
}
