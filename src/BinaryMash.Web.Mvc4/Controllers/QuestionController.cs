﻿namespace BinaryMash.Web.Mvc4.Controllers
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;
    using Components;
    using DataAccess.Repositories.Contract;

    public class QuestionController : BaseUserController
    {

        #region ctor

        public QuestionController(IQuestionVersionRepository questionVersionRepository, ILoginRepository loginRepository)
        {
            _questionVersionRepository = questionVersionRepository;
            _loginRepository = loginRepository;
        }

        #endregion

        #region Index

        [AllowAnonymous]
        public ActionResult Index(int? page, int? pageSize)
        {
            if (!page.HasValue)
            {
                page = 1;
            }

            if (!pageSize.HasValue)
            {
                pageSize = 15;
            }
            else
            {
                if (pageSize > 30)
                {
                    pageSize = 30;
                }
            }

            var query = _questionVersionRepository.Latest(User.Identity)
                                      .Include(q => q.Sections)
                                      .Include("Sections.AnswerContents")
                                      .Include(q => q.User);

            int numberOfQuestions = query.Count();

            var questions = query.OrderByDescending(q => q.Created)
                                .Skip((page.Value - 1) * pageSize.Value)
                                .Take(pageSize.Value)
                                .ToList();

            ViewBag.Pager = new Pager(page.Value, numberOfQuestions, pageSize.Value, "", 7);

            return View("Index", questions);
        }

        #endregion

        #region About

        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        #endregion

        #region Tagged

        [AllowAnonymous]
        public ActionResult Tagged(string id, int? page, int? pageSize)
        {
            ViewBag.Tag = id;

            if (!page.HasValue)
            {
                page = 1;
            }

            if (!pageSize.HasValue)
            {
                pageSize = 15;
            }
            else
            {
                if (pageSize > 30)
                {
                    pageSize = 30;
                }
            }

            var query = _questionVersionRepository.Tagged(User.Identity, id)
                                                  .Include(qv => qv.Sections)
                                                  .Include(q => q.User);


            //Total number of questions on the tag
            int numberOfQuestions = query.Count();

            //Paged questions we care about
            var questions = query.OrderByDescending(q => q.Updated)
                                 .Skip((page.Value - 1) * pageSize.Value)
                                 .Take(pageSize.Value)
                                 .ToList();

            ViewBag.Pager = new Pager(page.Value, numberOfQuestions, pageSize.Value, "", 7);

            return View("TaggedIndex", questions);
        }

        #endregion

        #region Create

        [AllowAnonymous]
        //[MyAuthorize(Roles = "ManageQuestions", Controller="Question", Action="CreateUnauthenticated")]
        public ActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("CreateUnauthenticated");
            }

            //TODO: check that user is allowed to create questions

            return View("Create", 0);
        }

        #endregion

        #region Edit

        public ActionResult Edit(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("CreateUnauthenticated");
            }

            return View("Create", id);
        }

        #endregion

        #region Preview

        [AllowAnonymous]
        public ActionResult Preview(int id)
        {
            //TODO: use title as id?

            var question = _questionVersionRepository.Latest(User.Identity, id)
                                                     .Include(qv => qv.Question.Tags.Select(t => t.Tag))
                                                     .Include(q => q.User)
                                                     .Include(q => q.Sections.Select(s => s.AnswerContents))
                                                     .FirstOrDefault();

            if (question == null)
            {
                //couldn't find QuestionVersion    
                return View("NotFound");
            }

            Guid? accountId = null;
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                var user = _loginRepository.Get(HttpContext.User.Identity).Select(l => l.User).FirstOrDefault();
                if (user != null)
                {
                    accountId = user.AccountId;
                }
            }

            var model = new Models.Question.Preview
            {
                IsAuthenticated = User.Identity.IsAuthenticated,
                QuestionVersion = question,
                UserAccountId = accountId
            };

            return View(model);
        }

        #endregion

        #region Preview

        [AllowAnonymous]
        public ActionResult Published(int id)
        {
            return View("Published", id);
        }

        #endregion

        #region Summary

        [AllowAnonymous]
        public ActionResult Summary(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("NotFound");
            }

            return View("Summary", id);
        }

        #endregion

        [AllowAnonymous]
        public ActionResult CreateUnauthenticated()
        {
            return View();
        }

        private readonly IQuestionVersionRepository _questionVersionRepository;
        private readonly ILoginRepository _loginRepository;

    }
}
