﻿using System.Web.Mvc;

namespace BinaryMash.Web.Mvc4.Controllers
{
    using System;
    using System.Globalization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Data.Entity;
    using System.Linq;
    using Components;
    using DataAccess.Repositories.Contract;
    using Agents.Interfaces;
    using Domain;

    public class SessionController : BaseUserController
    {

        #region ctor

        public SessionController(ISessionRepository sessionRepository, ISessionAgent sessionAgent)
        {
            _sessionRepository = sessionRepository;
            _sessionAgent = sessionAgent;
        }

        #endregion

        [AllowAnonymous]
        public ActionResult Index(int? page, int? pageSize)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Unauthenticated");
            }

            if (!page.HasValue)
            {
                page = 1;
            }

            if (!pageSize.HasValue)
            {
                pageSize = 15;
            }
            else
            {
                if (pageSize > 30)
                {
                    pageSize = 30;
                }
            }


            var query = _sessionRepository.All(User.Identity)
                                          .Include("Person")
                                          .Include("User")
                                          .Include("TestVersion");

            int numberOfSessions = query.Count();

            var sessions = query.OrderByDescending(s => s.Id)
                                .Skip((page.Value - 1) * pageSize.Value)
                                .Take(pageSize.Value)
                                .ToList();

            ViewBag.Pager = new Pager(page.Value, numberOfSessions, pageSize.Value, "", 7);

            return View("Index", sessions);
        }

        #region About

        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        #endregion

        #region Create

        [AllowAnonymous]
        //TODO: user is authorised to create
        public ActionResult Create(int? person, int? test, int? testVersion)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Unauthenticated");
            }

            var model = new Models.Session.CreateEdit
            {
                Id = Guid.Empty,
                TestId = test ?? 0,
                TestVersion = testVersion ?? 0,
                PersonId = person ?? 0
            };

            return View("CreateEdit", model);
        }

        #endregion

        #region Edit

        public ActionResult Edit(Guid id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Unauthenticated");
            }

            var model = new Models.Session.CreateEdit
            {
                Id = id,
            };

            return View("CreateEdit", model);
        }

        #endregion

        #region Summary

        public ActionResult Summary(Guid id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Unauthenticated");
            }

            return View("Summary", id);
        }

        #endregion

        #region TakeNow

        /// <summary>
        /// Take the test directly from when logged in as a user on the site
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TakeNow(Guid id, string password)
        {
            if (_sessionAgent.AuthenticateSession(id, password))
            {
                if (User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.SignOut();
                }

                FormsAuthentication.SetAuthCookie(IdentityConverter.CreateIdentityForSession(id), false);
                return RedirectToAction("Welcome", "Candidate");
            }

            return View("Unauthenticated");
        }

        #endregion
        
        #region Answers

        /// <summary>
        /// View the candidates answers
        /// </summary>
        /// <param name="id">The sessuib id.</param>
        /// <returns></returns>
        public ActionResult Answers(Guid id)
        {
            return View("Answers", null, id.ToString());
        }

        #endregion

        private readonly ISessionRepository _sessionRepository;
        private readonly ISessionAgent _sessionAgent;

    }
}
