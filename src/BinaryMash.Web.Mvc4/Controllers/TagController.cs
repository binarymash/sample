﻿namespace BinaryMash.Web.Mvc4.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using Components;
    using DataAccess.Repositories.Contract;

    [AllowAnonymous]
    public class TagController : BaseUserController
    {

        public TagController(ITagRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }

        public ActionResult Index(int? page, int? pageSize)
        {
            if (!page.HasValue)
            {
                page = 1;
            }

            if (!pageSize.HasValue)
            {
                pageSize = 48;
            }
            else
            {
                if (pageSize > 48)
                {
                    pageSize = 48;
                }
            }


            var query = _tagRepository.All(User.Identity);

            int numberOfTags = query.Count();

            var tags = query
                .OrderBy(q => q.Name)
                .Skip((page.Value - 1) * pageSize.Value)
                .Take(pageSize.Value)
                .ToList();

            ViewBag.Pager = new Pager(page.Value, numberOfTags, pageSize.Value, "", 7);

            return View("Index", tags);            
        }

        #region About

        public ActionResult About()
        {
            return View();
        }

        #endregion

        public ActionResult Details(string id)
        {
            var tag = _tagRepository.Named(User.Identity, id).FirstOrDefault();

            if (tag == null)
            {
                //couldn't find tag 
                return View("NotFound");
            }

            return View(tag);            
        }

        private readonly ITagRepository _tagRepository;
    }
}
