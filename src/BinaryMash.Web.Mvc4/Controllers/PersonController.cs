﻿using System.Web.Mvc;

namespace BinaryMash.Web.Mvc4.Controllers
{
    using System.Data.Entity;
    using System.Linq;
    using Components;
    using DataAccess.Repositories.Contract;

    public class PersonController : BaseUserController
    {

        #region ctor

        public PersonController(IPersonRepository personRepository) : base()
        {
            _personRepository = personRepository;
        }

        #endregion

        [AllowAnonymous]
        public ActionResult Index(int? page, int? pageSize)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Unauthenticated");
            }

            if (!page.HasValue)
            {
                page = 1;
            }

            if (!pageSize.HasValue)
            {
                pageSize = 15;
            }
            else
            {
                if (pageSize > 30)
                {
                    pageSize = 30;
                }
            }

            var query = _personRepository.All(User.Identity)
                                         .Include(p => p.User);

            int numberOfPeople = query.Count();

            var people = query.OrderByDescending(p => p.Name)
                                .Skip((page.Value - 1) * pageSize.Value)
                                .Take(pageSize.Value)
                                .ToList();

            ViewBag.Pager = new Pager(page.Value, numberOfPeople, pageSize.Value, "", 7);

            return View("Index", people);
        }

        #region About

        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        #endregion

        #region Create

        [AllowAnonymous]
        //TODO: user is authorised to create
        public ActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Unauthenticated");
            }

            //TODO: check that user is allowed to create questions

            return View("CreateEdit", 0);
        }

        #endregion

        #region Edit

        public ActionResult Edit(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Unauthenticated");
            }

            return View("CreateEdit", id);
        }

        #endregion

        #region Summary

        public ActionResult Summary(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("Unauthenticated");
            }

            return View("Summary", id);
        }

        #endregion

        private readonly IPersonRepository _personRepository;
    }
}
