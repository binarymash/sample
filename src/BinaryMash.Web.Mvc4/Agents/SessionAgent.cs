﻿namespace BinaryMash.Web.Mvc4.Agents
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Security.Principal;
    using System.Transactions;
    using Core.Security.Interfaces;
    using DataAccess.Repositories.Contract;
    using Domain;
    using Interfaces;
    using NServiceBus;

    public class SessionAgent : ISessionAgent
    {

        public SessionAgent(IHashingService hashingService, ILoginRepository loginRepository)
        {
            _hashingService = hashingService;
            _loginRepository = loginRepository;
        }

        public bool AuthenticateSession(Guid id, string password)
        {
            var sessionCredential = _loginRepository.GetSessionCredential(id);
            if (sessionCredential == null)
            {
                //user is not active
                return false;
            }

            var validateResponse = _hashingService.Validate(
                new ValidateRequest
                {
                    Hash = sessionCredential.HashedPassword,
                    PlainText = password,
                    SaltSeed = id.ToString()
                });

            return validateResponse.IsValid;
        }

        public bool StartSession(IIdentity identity)
        {
            try
            {
                using (var scope = new TransactionScope())
                {
                    var session = _loginRepository.Get(identity).Include(l => l.Session).Select(l => l.Session).FirstOrDefault();
                    if (session == null)
                    {
                        //TODO: No login or session found. Should never happen, but what if it does?
                        return false;
                    }

                    if (session.State == SessionState.Scheduled)
                    {
                        session.State = SessionState.Started;
                        _loginRepository.SaveChanges();
                        //TODO: send sessionstarted event to bus - eg, might want to email user
                    }
                    else if (session.State != SessionState.Started)
                    {
                        //Session is in wrong state to be started
                        //TODO: more informative error to the user
                        return false;
                    }
                    scope.Complete();
                }
            }
            catch
            {
                //TODO: log error
                return false;
            }

            return true;
        }

        public bool FinishSession(IIdentity identity)
        {
            try
            {
                using (var scope = new TransactionScope())
                {
                    var session = _loginRepository.Get(identity).Include(l => l.Session).Select(l => l.Session).FirstOrDefault();
                    if (session == null)
                    {
                        //TODO: No login or session found. Should never happen, but what if it does?
                        return false;
                    }

                    if (session.State == SessionState.Started)
                    {
                        session.State = SessionState.Answered;
                        _loginRepository.SaveChanges();
                        //TODO: send sessionstarted event to bus - eg, might want to email user
                    }
                    else
                    {
                        //Session is in wrong state to be finished
                        return false;
                    }
                    scope.Complete();
                }
            }
            catch
            {
                //TODO: log error
                return false;
            }

            return true;
        }

        private readonly IHashingService _hashingService;
        private readonly ILoginRepository _loginRepository;

    }
}