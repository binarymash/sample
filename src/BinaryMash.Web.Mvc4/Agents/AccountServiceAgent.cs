﻿namespace BinaryMash.Web.Mvc4.Agents
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Transactions;
    using Application.UserService.Contract.Commands;
    using Application.UserService.Contract.Messages;
    using Core.Security.Interfaces;
    using DataAccess.Repositories.Contract;
    using Domain;
    using Interfaces;
    using NServiceBus;

    public class AccountServiceAgent : IAccountServiceAgent
    {

        public AccountServiceAgent(IBus bus, IHashingService hashingService, IUserRepository userRepository, ILoginRepository loginRepository, IResetPasswordSagaTrackerRepository resetPasswordSagaTrackerRepository)
        {
            _bus = bus;
            _hashingService = hashingService;
            _userRepository = userRepository;
            _loginRepository = loginRepository;
            _resetPasswordSagaTrackerRepository = resetPasswordSagaTrackerRepository;
        }

        public bool CreateUser(Guid accountId, Guid userId, string email, string firstName, string lastName, string plaintextPassword)
        {
            if (UserIsInUse(email))
            {
                return false;
            }
            
            var hashRequest = new HashRequest
            {
                SaltSeed = userId.ToString(), //really the login ID. //TODO: if we ever change the user ID so it doesn't match login ID, this will also have to change
                PlainText = plaintextPassword
            };

            var hashResponse = _hashingService.Hash(hashRequest);

            var registerUserMessage = new RegisterUserCommand
            {
                AccountId = accountId,
                UserId = userId,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                HashedPassword = hashResponse.Hash,
                Salt = hashResponse.Salt
            };

            _bus.Send(registerUserMessage);

            return true;
        }

        public bool AuthenticateUser(string email, string plainTextPassword)
        {
            var userCredential = _loginRepository.GetUserCredential(email);
            if (userCredential == null)
            {
                //user is not active
                return false;
            }

            var validateResponse = _hashingService.Validate(
                new ValidateRequest
                {
                    Hash = userCredential.HashedPassword,
                    PlainText = plainTextPassword,
                    SaltSeed = userCredential.Id.ToString()
                });

            if (validateResponse.IsValid)
            {
                _bus.Send(new UserLogInCommand { Id = Guid.Empty, Email = email, Time = DateTime.UtcNow });
            }

            return validateResponse.IsValid;
        }

        public UserIdentity GetUserIdentity(string email)
        {
            return _loginRepository.GetUserIdentity(email);
        }

        public void ValidateUser(Guid userValidationSagaId)
        {
            var message = new UserValidationReceivedMessage {UserValidationSagaId = userValidationSagaId};
            _bus.Send(message);
        }

        public bool UserIsInUse(string email)
        {
            return _userRepository.Get(email) != null;
        }

        public bool ForgottenPassword(string email)
        {
            var login = _loginRepository.GetAll()
                                        .Include(l => l.User)
                                        .FirstOrDefault(l => l.User.Email == email);
            if (login == null)
            {
                return false;
            }

            var openPasswordResetWindowCommand = new OpenPasswordResetWindowCommand
            {
                LoginId = login.Id,
                Email = email,
                FirstName = login.User.FirstName
            };

            _bus.Send(openPasswordResetWindowCommand);

            return true;
        }

        public bool ResetPassword(Guid sagaId, string email, string plaintextPassword)
        {
            var passwordResetWindow = _resetPasswordSagaTrackerRepository.GetAll()
                                                                 .Include(s => s.Login.User)
                                                                 .FirstOrDefault(s => (
                                                                     (s.PasswordResetWindowSagaId == sagaId) &&
                                                                     (s.Login.User.Email == email)));
            
            if (passwordResetWindow == null)
            {
                //either the saga never exited, or it has timed out
                return false;
            }

            if (passwordResetWindow.Used || passwordResetWindow.Expires < DateTime.UtcNow)
            {
                return false;
            }

            try
            {
                using (var scope = new TransactionScope())
                {
                    var hashRequest = new HashRequest
                    {
                        SaltSeed = passwordResetWindow.LoginId.ToString(),
                        PlainText = plaintextPassword
                    };

                    var hashResponse = _hashingService.Hash(hashRequest);

                    passwordResetWindow.Login.Salt = hashResponse.Salt;
                    passwordResetWindow.Login.HashedPassword = hashResponse.Hash;

                    passwordResetWindow.Used = true;
                    _resetPasswordSagaTrackerRepository.SaveChanges();

                    var closePasswordResetWindowCommand = new ClosePasswordResetWindowCommand
                    {
                        PasswordResetWindowSagaId = sagaId
                    };

                    _bus.Send(closePasswordResetWindowCommand);

                    scope.Complete();
                }

                return true;
            }
            catch 
            {
                //TODO: log exception
                return false;
            }
        }

        private readonly IHashingService _hashingService;
        private readonly IUserRepository _userRepository;
        private readonly ILoginRepository _loginRepository;
        private readonly IResetPasswordSagaTrackerRepository _resetPasswordSagaTrackerRepository;
        private readonly IBus _bus;

    }
}