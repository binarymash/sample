﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System;
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface ISessionRepository : IRepository<Session>
    {
        IQueryable<Session> All(IIdentity identity);
        Session GetUnauthenticated(Guid id);
    }
}
