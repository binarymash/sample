﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface ITagRepository : IRepository<Tag>
    {
        IQueryable<Tag> All(IIdentity identity);
        IQueryable<Tag> Named(IIdentity identity, string name);

    }
}
