﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    public interface IRepositoryFactory
    {
        IPersonRepository People { get; }
        IQuestionVersionRepository Questions { get; }
        ISessionRepository Sessions { get; }
        ITagRepository Tags { get; }
        ITestVersionRepository TestVersions { get; set; }
    }
}
