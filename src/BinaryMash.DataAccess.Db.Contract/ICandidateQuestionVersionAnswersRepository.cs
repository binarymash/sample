﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface ICandidateQuestionVersionAnswersRepository : IRepository<CandidateQuestionVersionAnswers>
    {
        IQueryable<CandidateQuestionVersionAnswers> All(IIdentity identity);
        IQueryable<CandidateQuestionVersionAnswers> AllFromCandidateSession(IIdentity identity);
    }
}
