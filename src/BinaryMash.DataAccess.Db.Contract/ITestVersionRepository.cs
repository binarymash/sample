﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface ITestVersionRepository : IRepository<TestVersion>
    {
        IQueryable<TestVersion> All(IIdentity identity);

        IQueryable<TestVersion> Published(IIdentity identity);
        
        IQueryable<TestVersion> Latest(IIdentity identity);
        IQueryable<TestVersion> Latest(IIdentity identity, IQueryable<TestVersion> query); 
        
        TestVersion Latest(IIdentity identity, int id);
        
        IQueryable<TestVersion> LatestPublished(IIdentity identity);        
    }
}
