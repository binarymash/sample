﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface IQuestionVersionRepository : IRepository<QuestionVersion>
    {
        IQueryable<QuestionVersion> All(IIdentity identity);

        IQueryable<QuestionVersion> Published(IIdentity identity);
        
        IQueryable<QuestionVersion> Latest(IIdentity identity);
        IQueryable<QuestionVersion> Latest(IIdentity identity, int id);
        IQueryable<QuestionVersion> Latest(IIdentity identity, IQueryable<QuestionVersion> query);         
        
        IQueryable<QuestionVersion> LatestPublished(IIdentity identity);
        
        IQueryable<QuestionVersion> Tagged(IIdentity identity, string name);

    }
}
