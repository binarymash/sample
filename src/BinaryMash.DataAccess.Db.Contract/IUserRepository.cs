﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface IUserRepository : IRepository<User>
    {
        IQueryable<User> Me(IIdentity identity);
        User Get(string email);
        IQueryable<User> All(IIdentity identity);
    }
}
