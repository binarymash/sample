﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface IResetPasswordSagaTrackerRepository : IRepository<PasswordResetWindow>
    {
        void Delete(Guid id);
    }
}
