﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface IPersonRepository : IRepository<Person>
    {
        IQueryable<Person> All(IIdentity identity);
    }
}
