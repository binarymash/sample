﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface ICandidateQuestionVersionRepository : IRepository<CandidateQuestionVersion>
    {
        IQueryable<CandidateQuestionVersion> All(IIdentity identity);
    }
}
