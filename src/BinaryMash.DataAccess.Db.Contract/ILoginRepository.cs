﻿namespace BinaryMash.DataAccess.Repositories.Contract
{
    using System;
    using System.Linq;
    using System.Security.Principal;
    using BinaryMash.Core.EF;
    using BinaryMash.Domain;

    public interface ILoginRepository : IRepository<Login>
    {
        IQueryable<Login> Get(IIdentity identity);
        Login GetUser(IIdentity identity);

        UserCredential GetUserCredential(string email);
        SessionCredential GetSessionCredential(Guid id);
        UserIdentity GetUserIdentity(string email);

    }
}
