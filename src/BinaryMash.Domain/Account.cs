﻿namespace BinaryMash.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;

    public class Account
    {

        [Key]
        public virtual Guid Id { get; set; }
        
        public virtual string Name { get; set; }
        
        public virtual ICollection<User> Users
        {
            get { return _users ?? (_users = new Collection<User>()); }
            protected set { _users = value; }
        }

        private ICollection<User> _users;
    }
}
