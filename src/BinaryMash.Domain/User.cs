﻿namespace BinaryMash.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class User
    {        
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid AccountId { get; set; }

        public Account Account { get; set; }

        [Required(ErrorMessage = "Email address is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Must be a valid email address")]
        [StringLength(64, ErrorMessage = "There is a 64 character limit on email addresses")] //yeah, the actual max length is 320 (or maybe 254?) but we don't care http://stackoverflow.com/questions/1297272/how-long-should-sql-email-fields-be
        [MaxLength(64, ErrorMessage = "There is a 64 character limit on email addresses")]
        [Display(Name = "Email address")]
        [EmailAddress(ErrorMessage = "Must be a valid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [StringLength(32, ErrorMessage = "There is a 32 character limit on first name")]
        [MaxLength(32, ErrorMessage = "There is a 32 character limit on first name")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [StringLength(32, ErrorMessage = "There is a 32 character limit on last name")]
        [MaxLength(32, ErrorMessage = "There is a 32 character limit on last name")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [ForeignKey("PublicProfile")]
        public int PublicProfileId { get; set; }

        public UserPublicProfile PublicProfile { get; set; }

        public long Permissions { get; set; }

        public ICollection<QuestionVersion> Questions
        {
            get { return _questions ?? (_questions = new Collection<QuestionVersion>()); }
            protected set { _questions = value; }
        }

        public ICollection<Session> Sessions
        {
            get { return _sessions ?? (_sessions = new Collection<Session>()); }
            protected set { _sessions = value; }
        }

        private ICollection<QuestionVersion> _questions;
        private ICollection<Session> _sessions;
    }
}
