﻿namespace BinaryMash.DataAccess.EF
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using Domain;

    public class BirdhouseContext : DbContext
    {

        public BirdhouseContext() : base("Birdhouse")
        {            
        }
        
        public DbSet<DummyObjectForPreCompiledView> Dummy { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionVersion> QuestionVersions { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TestVersion> Tests { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<CandidateQuestionVersionAnswers> CandidateQuestionVersionAnswers { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<Login> Logins { get; set; }
        public DbSet<PasswordResetWindow> ResetPasswords { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            SetConventions(modelBuilder);
            SetConfiguration(modelBuilder);

            base.OnModelCreating(modelBuilder);        
        }

        private void SetConventions(DbModelBuilder modelBuilder)
        {            
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //Introducing FOREIGN KEY constraint 'FK_dbo.QuestionAudit_dbo.Account_AccountId' on table 'QuestionAudit' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints.
            //I'm never going to explicitly delete, so let's remove the convention which causes this
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        private void SetConfiguration(DbModelBuilder modelBuilder)
        {

            //TPT Hierarchies
            modelBuilder.Entity<AnswerContentQuestionerSupplied>()
                .ToTable("AnswerContentQuestionerSupplied");

            modelBuilder.Entity<AnswerContentUserSupplied>()
                .ToTable("AnswerContentUserSupplied");

            modelBuilder.Entity<AddressChannel>()
                .ToTable("AddressChannel");

            modelBuilder.Entity<EmailChannel>()
                .ToTable("EmailChannel");

            modelBuilder.Entity<PhoneChannel>()
                .ToTable("PhoneChannel");

            base.OnModelCreating(modelBuilder);
        
        }

    }
}
