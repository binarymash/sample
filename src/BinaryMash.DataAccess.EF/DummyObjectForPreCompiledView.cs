﻿namespace BinaryMash.DataAccess.EF
{
    using System.ComponentModel.DataAnnotations;

    public class DummyObjectForPreCompiledView
    {
        [Key]
        public int Id { get; set; }

        //This is in here so that the precompiled view can be found, as it is not in the same assembly as the domain model.... 
        //This object must be added to the DBContext
        //See http://stackoverflow.com/questions/11543990/why-are-my-ef-code-first-pregenerated-views-having-no-effect/12060962#12060962
    }
}
