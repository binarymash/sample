namespace BinaryMash.DataAccess.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DummyObjectForPreCompiledView",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        Email = c.String(nullable: false, maxLength: 64),
                        FirstName = c.String(nullable: false, maxLength: 32),
                        LastName = c.String(nullable: false, maxLength: 32),
                        PublicProfileId = c.Int(nullable: false),
                        Permissions = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .ForeignKey("dbo.UserPublicProfile", t => t.PublicProfileId)
                .Index(t => t.AccountId)
                .Index(t => t.PublicProfileId);
            
            CreateTable(
                "dbo.UserPublicProfile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionVersion",
                c => new
                    {
                        QuestionId = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        State = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 128),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => new { t.QuestionId, t.Version })
                .ForeignKey("dbo.Question", t => t.QuestionId)
                .ForeignKey("dbo.UserPublicProfile", t => t.UserId)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.QuestionId)
                .Index(t => t.UserId)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Question",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountId = c.Guid(nullable: false),
                        LatestPublishedVersion = c.Int(),
                        LatestVersion = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        IsPublic = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .ForeignKey("dbo.UserPublicProfile", t => t.UserId)
                .Index(t => t.AccountId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.QuestionTag",
                c => new
                    {
                        QuestionTagId = c.Int(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionTagId)
                .ForeignKey("dbo.Question", t => t.QuestionId)
                .ForeignKey("dbo.Tag", t => t.TagId)
                .Index(t => t.QuestionId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        TagId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.TagId);
            
            CreateTable(
                "dbo.QuestionAndAnswerSection",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionVersionId = c.Int(nullable: false),
                        QuestionVersionVersion = c.Int(nullable: false),
                        Index = c.Int(nullable: false),
                        Notes = c.String(maxLength: 1024),
                        Text = c.String(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionVersion", t => new { t.QuestionVersionId, t.QuestionVersionVersion })
                .Index(t => new { t.QuestionVersionId, t.QuestionVersionVersion });
            
            CreateTable(
                "dbo.AnswerContent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionAndAnswerSectionId = c.Int(nullable: false),
                        Notes = c.String(maxLength: 1024),
                        Index = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.QuestionAndAnswerSection", t => t.QuestionAndAnswerSectionId)
                .Index(t => t.QuestionAndAnswerSectionId);
            
            CreateTable(
                "dbo.QuestionVersionTestVersion",
                c => new
                    {
                        QuestionVersionTagVersionId = c.Int(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        QuestionVersionVersion = c.Int(nullable: false),
                        TestId = c.Int(nullable: false),
                        TestVersionVersion = c.Int(nullable: false),
                        Index = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionVersionTagVersionId)
                .ForeignKey("dbo.QuestionVersion", t => new { t.QuestionId, t.QuestionVersionVersion })
                .ForeignKey("dbo.TestVersion", t => new { t.TestId, t.TestVersionVersion })
                .Index(t => new { t.QuestionId, t.QuestionVersionVersion })
                .Index(t => new { t.TestId, t.TestVersionVersion });
            
            CreateTable(
                "dbo.TestVersion",
                c => new
                    {
                        TestId = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        State = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.TestId, t.Version })
                .ForeignKey("dbo.Test", t => t.TestId)
                .ForeignKey("dbo.UserPublicProfile", t => t.UserId)
                .Index(t => t.TestId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Test",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountId = c.Guid(nullable: false),
                        LatestPublishedVersion = c.Int(),
                        LatestVersion = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .ForeignKey("dbo.UserPublicProfile", t => t.UserId)
                .Index(t => t.AccountId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Session",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TestId = c.Int(nullable: false),
                        TestVersionVersion = c.Int(nullable: false),
                        PersonId = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        Password = c.String(nullable: false),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TestVersion", t => new { t.TestId, t.TestVersionVersion })
                .ForeignKey("dbo.Person", t => t.PersonId)
                .ForeignKey("dbo.UserPublicProfile", t => t.UserId)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => new { t.TestId, t.TestVersionVersion })
                .Index(t => t.PersonId)
                .Index(t => t.UserId)
                .Index(t => t.AccountId)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        AccountId = c.Guid(nullable: false),
                        UserId = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .ForeignKey("dbo.UserPublicProfile", t => t.UserId)
                .Index(t => t.AccountId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.CommunicationChannel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.PersonId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.CandidateQuestionVersionAnswers",
                c => new
                    {
                        QuestionVersionId = c.Int(nullable: false),
                        QuestionVersionVersion = c.Int(nullable: false),
                        Id = c.Int(nullable: false, identity: true),
                        SessionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Session", t => t.SessionId)
                .ForeignKey("dbo.QuestionVersion", t => new { t.QuestionVersionId, t.QuestionVersionVersion })
                .Index(t => t.SessionId)
                .Index(t => new { t.QuestionVersionId, t.QuestionVersionVersion });
            
            CreateTable(
                "dbo.CandidateSectionAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionVersionId = c.Int(nullable: false),
                        SectionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CandidateQuestionVersionAnswers", t => t.QuestionVersionId)
                .ForeignKey("dbo.QuestionAndAnswerSection", t => t.SectionId)
                .Index(t => t.QuestionVersionId)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.CandidateAnswer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SectionAnswersId = c.Int(nullable: false),
                        AnswerContentId = c.Int(nullable: false),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CandidateSectionAnswers", t => t.SectionAnswersId)
                .ForeignKey("dbo.AnswerContent", t => t.AnswerContentId)
                .Index(t => t.SectionAnswersId)
                .Index(t => t.AnswerContentId);
            
            CreateTable(
                "dbo.UserPermission",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Login",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        HashedPassword = c.String(nullable: false),
                        Salt = c.String(nullable: false),
                        UserId = c.Guid(),
                        SessionId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .ForeignKey("dbo.Session", t => t.SessionId)
                .Index(t => t.UserId)
                .Index(t => t.SessionId);
            
            CreateTable(
                "dbo.PasswordResetWindow",
                c => new
                    {
                        PasswordResetWindowSagaId = c.Guid(nullable: false),
                        LoginId = c.Guid(nullable: false),
                        Expires = c.DateTime(nullable: false),
                        Used = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PasswordResetWindowSagaId)
                .ForeignKey("dbo.Login", t => t.LoginId)
                .Index(t => t.LoginId);
            
            CreateTable(
                "dbo.AnswerContentQuestionerSupplied",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Text = c.String(nullable: false, maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AnswerContent", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.AnswerContentUserSupplied",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        MaxLength = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AnswerContent", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.AddressChannel",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CommunicationChannel", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.EmailChannel",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CommunicationChannel", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.PhoneChannel",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CommunicationChannel", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.PhoneChannel", new[] { "Id" });
            DropIndex("dbo.EmailChannel", new[] { "Id" });
            DropIndex("dbo.AddressChannel", new[] { "Id" });
            DropIndex("dbo.AnswerContentUserSupplied", new[] { "Id" });
            DropIndex("dbo.AnswerContentQuestionerSupplied", new[] { "Id" });
            DropIndex("dbo.PasswordResetWindow", new[] { "LoginId" });
            DropIndex("dbo.Login", new[] { "SessionId" });
            DropIndex("dbo.Login", new[] { "UserId" });
            DropIndex("dbo.UserPermission", new[] { "UserId" });
            DropIndex("dbo.CandidateAnswer", new[] { "AnswerContentId" });
            DropIndex("dbo.CandidateAnswer", new[] { "SectionAnswersId" });
            DropIndex("dbo.CandidateSectionAnswers", new[] { "SectionId" });
            DropIndex("dbo.CandidateSectionAnswers", new[] { "QuestionVersionId" });
            DropIndex("dbo.CandidateQuestionVersionAnswers", new[] { "QuestionVersionId", "QuestionVersionVersion" });
            DropIndex("dbo.CandidateQuestionVersionAnswers", new[] { "SessionId" });
            DropIndex("dbo.CommunicationChannel", new[] { "PersonId" });
            DropIndex("dbo.Person", new[] { "UserId" });
            DropIndex("dbo.Person", new[] { "AccountId" });
            DropIndex("dbo.Session", new[] { "User_Id" });
            DropIndex("dbo.Session", new[] { "AccountId" });
            DropIndex("dbo.Session", new[] { "UserId" });
            DropIndex("dbo.Session", new[] { "PersonId" });
            DropIndex("dbo.Session", new[] { "TestId", "TestVersionVersion" });
            DropIndex("dbo.Test", new[] { "UserId" });
            DropIndex("dbo.Test", new[] { "AccountId" });
            DropIndex("dbo.TestVersion", new[] { "UserId" });
            DropIndex("dbo.TestVersion", new[] { "TestId" });
            DropIndex("dbo.QuestionVersionTestVersion", new[] { "TestId", "TestVersionVersion" });
            DropIndex("dbo.QuestionVersionTestVersion", new[] { "QuestionId", "QuestionVersionVersion" });
            DropIndex("dbo.AnswerContent", new[] { "QuestionAndAnswerSectionId" });
            DropIndex("dbo.QuestionAndAnswerSection", new[] { "QuestionVersionId", "QuestionVersionVersion" });
            DropIndex("dbo.QuestionTag", new[] { "TagId" });
            DropIndex("dbo.QuestionTag", new[] { "QuestionId" });
            DropIndex("dbo.Question", new[] { "UserId" });
            DropIndex("dbo.Question", new[] { "AccountId" });
            DropIndex("dbo.QuestionVersion", new[] { "User_Id" });
            DropIndex("dbo.QuestionVersion", new[] { "UserId" });
            DropIndex("dbo.QuestionVersion", new[] { "QuestionId" });
            DropIndex("dbo.User", new[] { "PublicProfileId" });
            DropIndex("dbo.User", new[] { "AccountId" });
            DropForeignKey("dbo.PhoneChannel", "Id", "dbo.CommunicationChannel");
            DropForeignKey("dbo.EmailChannel", "Id", "dbo.CommunicationChannel");
            DropForeignKey("dbo.AddressChannel", "Id", "dbo.CommunicationChannel");
            DropForeignKey("dbo.AnswerContentUserSupplied", "Id", "dbo.AnswerContent");
            DropForeignKey("dbo.AnswerContentQuestionerSupplied", "Id", "dbo.AnswerContent");
            DropForeignKey("dbo.PasswordResetWindow", "LoginId", "dbo.Login");
            DropForeignKey("dbo.Login", "SessionId", "dbo.Session");
            DropForeignKey("dbo.Login", "UserId", "dbo.User");
            DropForeignKey("dbo.UserPermission", "UserId", "dbo.User");
            DropForeignKey("dbo.CandidateAnswer", "AnswerContentId", "dbo.AnswerContent");
            DropForeignKey("dbo.CandidateAnswer", "SectionAnswersId", "dbo.CandidateSectionAnswers");
            DropForeignKey("dbo.CandidateSectionAnswers", "SectionId", "dbo.QuestionAndAnswerSection");
            DropForeignKey("dbo.CandidateSectionAnswers", "QuestionVersionId", "dbo.CandidateQuestionVersionAnswers");
            DropForeignKey("dbo.CandidateQuestionVersionAnswers", new[] { "QuestionVersionId", "QuestionVersionVersion" }, "dbo.QuestionVersion");
            DropForeignKey("dbo.CandidateQuestionVersionAnswers", "SessionId", "dbo.Session");
            DropForeignKey("dbo.CommunicationChannel", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Person", "UserId", "dbo.UserPublicProfile");
            DropForeignKey("dbo.Person", "AccountId", "dbo.Account");
            DropForeignKey("dbo.Session", "User_Id", "dbo.User");
            DropForeignKey("dbo.Session", "AccountId", "dbo.Account");
            DropForeignKey("dbo.Session", "UserId", "dbo.UserPublicProfile");
            DropForeignKey("dbo.Session", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Session", new[] { "TestId", "TestVersionVersion" }, "dbo.TestVersion");
            DropForeignKey("dbo.Test", "UserId", "dbo.UserPublicProfile");
            DropForeignKey("dbo.Test", "AccountId", "dbo.Account");
            DropForeignKey("dbo.TestVersion", "UserId", "dbo.UserPublicProfile");
            DropForeignKey("dbo.TestVersion", "TestId", "dbo.Test");
            DropForeignKey("dbo.QuestionVersionTestVersion", new[] { "TestId", "TestVersionVersion" }, "dbo.TestVersion");
            DropForeignKey("dbo.QuestionVersionTestVersion", new[] { "QuestionId", "QuestionVersionVersion" }, "dbo.QuestionVersion");
            DropForeignKey("dbo.AnswerContent", "QuestionAndAnswerSectionId", "dbo.QuestionAndAnswerSection");
            DropForeignKey("dbo.QuestionAndAnswerSection", new[] { "QuestionVersionId", "QuestionVersionVersion" }, "dbo.QuestionVersion");
            DropForeignKey("dbo.QuestionTag", "TagId", "dbo.Tag");
            DropForeignKey("dbo.QuestionTag", "QuestionId", "dbo.Question");
            DropForeignKey("dbo.Question", "UserId", "dbo.UserPublicProfile");
            DropForeignKey("dbo.Question", "AccountId", "dbo.Account");
            DropForeignKey("dbo.QuestionVersion", "User_Id", "dbo.User");
            DropForeignKey("dbo.QuestionVersion", "UserId", "dbo.UserPublicProfile");
            DropForeignKey("dbo.QuestionVersion", "QuestionId", "dbo.Question");
            DropForeignKey("dbo.User", "PublicProfileId", "dbo.UserPublicProfile");
            DropForeignKey("dbo.User", "AccountId", "dbo.Account");
            DropTable("dbo.PhoneChannel");
            DropTable("dbo.EmailChannel");
            DropTable("dbo.AddressChannel");
            DropTable("dbo.AnswerContentUserSupplied");
            DropTable("dbo.AnswerContentQuestionerSupplied");
            DropTable("dbo.PasswordResetWindow");
            DropTable("dbo.Login");
            DropTable("dbo.UserPermission");
            DropTable("dbo.CandidateAnswer");
            DropTable("dbo.CandidateSectionAnswers");
            DropTable("dbo.CandidateQuestionVersionAnswers");
            DropTable("dbo.CommunicationChannel");
            DropTable("dbo.Person");
            DropTable("dbo.Session");
            DropTable("dbo.Test");
            DropTable("dbo.TestVersion");
            DropTable("dbo.QuestionVersionTestVersion");
            DropTable("dbo.AnswerContent");
            DropTable("dbo.QuestionAndAnswerSection");
            DropTable("dbo.Tag");
            DropTable("dbo.QuestionTag");
            DropTable("dbo.Question");
            DropTable("dbo.QuestionVersion");
            DropTable("dbo.UserPublicProfile");
            DropTable("dbo.User");
            DropTable("dbo.Account");
            DropTable("dbo.DummyObjectForPreCompiledView");
        }
    }
}
