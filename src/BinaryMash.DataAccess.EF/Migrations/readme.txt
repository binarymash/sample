﻿updatTo update a existing EF migration (must be the most recent migration)...

Add-Migration <MigrationName> -Force

For each existing EF migration, create the sql with...

Update-Database -Script -TargetMigration:<migration name>

...for example...

Update-Database -Script -TargetMigration:201306081424116_InitialCreate

..then add this into the roundhouse up folder