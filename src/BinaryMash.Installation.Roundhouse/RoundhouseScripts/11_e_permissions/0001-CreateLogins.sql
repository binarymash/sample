﻿--ensures that website login is created on database server
IF NOT EXISTS (
  SELECT name  
  FROM master.sys.server_principals
  WHERE name = 'IIS APPPOOL\AptitudeApplicationPool')
BEGIN
    CREATE LOGIN [IIS APPPOOL\AptitudeApplicationPool] 
	FROM WINDOWS
END
GO

--ensures that services login is created on database server
IF NOT EXISTS (
  SELECT name  
  FROM master.sys.server_principals
  WHERE name = 'NT AUTHORITY\SYSTEM')
BEGIN
    CREATE LOGIN [NT AUTHORITY\SYSTEM] 
	FROM WINDOWS
END
GO

