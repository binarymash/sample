﻿--Create database user and map to login
IF NOT EXISTS (
  SELECT * 
  FROM sys.database_principals
  WHERE name = 'IIS APPPOOL\AptitudeApplicationPool')
BEGIN
  PRINT 'Creating database user IIS APPPOOL\AptitudeApplicationPool...'
  CREATE USER [IIS APPPOOL\AptitudeApplicationPool]
  FROM LOGIN [IIS APPPOOL\AptitudeApplicationPool]
END
GO

IF NOT EXISTS (
  SELECT * 
  FROM sys.database_principals
  WHERE name = 'NT AUTHORITY\SYSTEM')
BEGIN
  PRINT 'Creating database user NT AUTHORITY\SYSTEM...'
  CREATE USER [NT AUTHORITY\SYSTEM]
  FROM LOGIN [NT AUTHORITY\SYSTEM]
END
GO

-- Give permissions to logins
-- sp_addrolemember is deprecated in sqlserver 2012. In future, use alter role
sp_addrolemember 'db_datareader', 'IIS APPPOOL\AptitudeApplicationPool'
GO
sp_addrolemember 'db_datawriter', 'IIS APPPOOL\AptitudeApplicationPool'
GO

sp_addrolemember 'db_datareader', 'NT AUTHORITY\SYSTEM'
GO
sp_addrolemember 'db_datawriter', 'NT AUTHORITY\SYSTEM'
GO
