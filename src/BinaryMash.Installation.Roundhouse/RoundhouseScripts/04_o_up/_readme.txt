﻿The UP folder and subfolders is where you put your schema changes and 
database insert scripts. This is a one time only folder. That means 
once you have run a script here, if RH detects the file has changed 
(even so much as a space) it will shut down and report errors. Of 
course there is a configuration that will allow you to just warn on one 
time script changes (check the configuration section of the 
documentation).

RH runs files in the order it finds them. The convention is to start all 
scripts with a nice long numeric sequence.


What goes in the Up Folder?

DDL (schema changes - database structure)
DML (inserts/updates/deletes)