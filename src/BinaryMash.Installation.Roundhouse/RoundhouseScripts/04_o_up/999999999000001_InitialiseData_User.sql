﻿INSERT INTO [dbo].[Account]
           ([Id]
           ,[Name])
     VALUES
           ('c4083e54-7c50-4d08-b84a-d823fe8ec037'
           ,'Test account')
GO

SET IDENTITY_INSERT [dbo].[UserPublicProfile] ON
GO

INSERT INTO [dbo].[UserPublicProfile]
           ([Id]
		   ,[Displayname])
     VALUES
           (1
		   ,'Bluejam')
GO

SET IDENTITY_INSERT [dbo].[UserPublicProfile] OFF
GO

INSERT INTO [dbo].[User]
           ([Id]
           ,[FirstName]
           ,[LastName]
           ,[Email]
           ,[AccountId]
		   ,[Permissions]
		   ,[PublicProfileId])
     VALUES
           ('0d179da4-0757-4a2b-9f65-e57808d56576'
           ,'Philip'
           ,'Wood'
           ,'philip.m.wood@gmail.com'
           ,'c4083e54-7c50-4d08-b84a-d823fe8ec037'
		   ,0xffff
		   ,1)
GO

INSERT INTO [dbo].[Login]
           ([Id]
		   ,[HashedPassword]
           ,[Salt]
           ,[UserId])
     VALUES
           ('0d179da4-0757-4a2b-9f65-e57808d56576'
		   ,'24UvnpXBXBf9gYPtoigt9kZZPLZI4zWnFHnA=='
           ,'h+Hg6iVeZmwxbeBhBX7EUP/3klPWx4SPsA=='
		   ,'0d179da4-0757-4a2b-9f65-e57808d56576')
GO