﻿INSERT INTO [dbo].[Account]
           ([Id]
           ,[Name])
     VALUES
           ('8BBE51BA-5B6D-4FB4-9A52-934C6FEC2E13'
           ,'Account for Dave Smith')
GO

SET IDENTITY_INSERT [dbo].[UserPublicProfile] ON
GO

INSERT INTO [dbo].[UserPublicProfile]
           ([Id]
		   ,[Displayname])
     VALUES
           (2
		   ,'Smudger')
GO

SET IDENTITY_INSERT [dbo].[UserPublicProfile] OFF
GO

INSERT INTO [dbo].[User]
           ([Id]
           ,[FirstName]
           ,[LastName]
           ,[Email]
           ,[AccountId]
		   ,[Permissions]
		   ,[PublicProfileId])
     VALUES
           ('C6851DD9-1F91-4622-B41B-4A1A193E10BE'
           ,'Dave'
           ,'Smith'
           ,'dave.smith@binarymash.com'
           ,'8BBE51BA-5B6D-4FB4-9A52-934C6FEC2E13'
		   ,0xffff,
		   2)
GO

INSERT INTO [dbo].[Login]
           ([Id]
		   ,[HashedPassword]
           ,[Salt]
           ,[UserId])
     VALUES
           ('C6851DD9-1F91-4622-B41B-4A1A193E10BE'
		   ,'24mzfrb/oEvhhyYCTF8C6rH7oWowdtxpUmiA==' --passpasspass
           ,'QTrs4U3+pY6CpBPyUzuWKDAn070P9p876g=='
		   ,'C6851DD9-1F91-4622-B41B-4A1A193E10BE')
GO