﻿Stored procedures are found in a sprocs folder and subfolders. If you 
have any that need to run prior to others, make sure they are 
alphabetically first before the dependent scripts.

https://github.com/chucknorris/roundhouse/wiki/GettingStarted
31-08-12