﻿namespace BinaryMash.Core.EF
{
    using System;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;

    /// <summary>
    /// From John Papa SPA
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EFRepository<T> : IRepository<T> where T: class
    {

        public EFRepository(DbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentException("A DbContext must be specified", "dbContext");
            }

            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }



        public IQueryable<T> GetAll()
        {
            return DbSet;
        }

        //public T GetById(Guid id)
        //{
        //    return DbSet.Find(id);
        //}

        public void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public void SaveChanges()
        {
            DbContext.SaveChanges();
        }

        //public void Update(T entity)
        //{
        //    DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
        //    if (dbEntityEntry.State == EntityState.Detached)
        //    {
        //        DbSet.Attach(entity);
        //    }
        //    dbEntityEntry.State = EntityState.Modified;
        //}

        //public void Delete(T entity)
        //{
        //    DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
        //    if (dbEntityEntry.State != EntityState.Deleted)
        //    {
        //        dbEntityEntry.State = EntityState.Deleted;
        //    }
        //    else
        //    {
        //        DbSet.Attach(entity);
        //        DbSet.Remove(entity);
        //    }
        //}

        //public void Delete(Guid id)
        //{
        //    var entity = GetById(id);
        //    if (entity == null)
        //    {
        //        return;
        //    }

        //    Delete(entity);
        //}

        protected DbContext DbContext { get; set; }
        protected DbSet<T> DbSet { get; set; }

    }
}
