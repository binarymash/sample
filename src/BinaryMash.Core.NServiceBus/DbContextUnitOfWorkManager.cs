﻿namespace BinaryMash.Core.NServiceBus
{
    using System;
    using System.Data.Entity;
    using global::NServiceBus.UnitOfWork;

    /// <summary>
    /// Manages an Entity Framework DbContext unit of work.
    /// 
    /// NSB IoC child containers will deal with the lifetime of the DbContext; if using 
    /// Castle Windsor then DbContext should be registered with Lifestyle.Scoped. This
    /// will ensure that the unit of work, and all handlers in the same process, use the
    /// same instance of DbContext.
    /// 
    /// See also the following implementations:
    /// NHibernate: https://github.com/NServiceBus/NServiceBus/blob/master/src/nhibernate/UnitOfWork/NServiceBus.UnitOfWork.NHibernate/UnitOfWorkManager.cs
    /// RavenDB: http://andreasohlund.net/2011/11/22/a-nservicebus-unit-of-work-implementation-for-ravendb
    /// </summary>
    public class DbContextUnitOfWorkManager : IManageUnitsOfWork
    {
        public DbContextUnitOfWorkManager(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Begin()
        {
        }

        public void End(Exception ex = null)
        {
            if (ex == null)
            {
                _dbContext.SaveChanges();
            }
        }

        private readonly DbContext _dbContext;

    }
}