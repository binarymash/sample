﻿
namespace BinaryMash.Core.Security
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Hashing;
    using Interfaces;

    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(

                Component.For<IAlgorithmFactory>()
                    .ImplementedBy<AlgorithmFactory>(),

                Component.For<IHashingService>()
                    .ImplementedBy<HashingService>(),

                Component.For<ISaltFactory>()
                    .ImplementedBy<SaltFactory>()
                );
        }
    }
}
