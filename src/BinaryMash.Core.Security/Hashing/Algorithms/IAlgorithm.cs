﻿namespace BinaryMash.Core.Security.Hashing.Algorithms
{
    public interface IAlgorithm
    {
        string Hash(string plainText, string salt);
        string Id { get; }
    }
}
