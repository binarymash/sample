namespace BinaryMash.Core.Security.Hashing
{
    public interface ISaltFactory
    {
        string Create(string seed);
    }
}