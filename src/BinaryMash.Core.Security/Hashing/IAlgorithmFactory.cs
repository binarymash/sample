namespace BinaryMash.Core.Security.Hashing
{
    using Algorithms;

    public interface IAlgorithmFactory
    {
        IAlgorithm Default { get; }
        IAlgorithm Create(string hash);
    }
}