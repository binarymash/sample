﻿namespace BinaryMash.Core.Security.Interfaces
{
    public interface IHashingService
    {
        HashResponse Hash(HashRequest request);
        ValidateResponse Validate(ValidateRequest request);
    }
}
