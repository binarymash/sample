﻿namespace BinaryMash.Core.Security.Interfaces
{
    public class HashResponse
    {
        public string Salt { get; set; }
        public string Hash { get; set; }
    }
}
