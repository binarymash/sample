﻿namespace BinaryMash.Core.Security.Interfaces
{
    public class ValidateRequest
    {
        public string PlainText { get; set; }
        public string SaltSeed { get; set; }
        public string Hash { get; set; }
    }
}
