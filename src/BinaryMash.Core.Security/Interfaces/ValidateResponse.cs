﻿namespace BinaryMash.Core.Security.Interfaces
{
    public class ValidateResponse
    {
        public ValidateResponse()
        {
            Status = StatusCode.None;
            IsValid = false;
        }

        public StatusCode Status { get; set; }
        public bool IsValid { get; set; }
    }
}
