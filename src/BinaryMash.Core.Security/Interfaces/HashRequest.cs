﻿
namespace BinaryMash.Core.Security.Interfaces
{
    public class HashRequest
    {
        public string SaltSeed { get; set; }
        public string PlainText { get; set; }
    }
}
