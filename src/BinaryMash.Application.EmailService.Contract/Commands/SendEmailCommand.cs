﻿namespace BinaryMash.Application.EmailService.Contract.Commands
{
    using NServiceBus;

    public class SendEmailCommand : ICommand
    {
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public string ToAddress { get; set; }
        public string Body { get; set; }
    }
}
