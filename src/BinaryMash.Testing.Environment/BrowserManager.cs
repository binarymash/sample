﻿namespace BinaryMash.Testing.Environment
{
    using System;
    using Coypu;
    using Coypu.Drivers.Selenium;

    public class BrowserManager : IDisposable
    {
        public BrowserSession Session;

        public BrowserManager()
        {
            var sessionConfiguration = new SessionConfiguration
            {
                AppHost = "localhost",
                Port = 64023,
                SSL = false,
                Driver = typeof(SeleniumWebDriver),
                Browser = Coypu.Drivers.Browser.Chrome
            };

            Session = new BrowserSession(sessionConfiguration);            
        }

        protected void Dispose(bool alsoDisposeManagedObjects)
        {
            if (alsoDisposeManagedObjects)
            {
                if (Session != null)
                {
                    Session.Dispose();
                    Session = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~BrowserManager()
        {
            Dispose(false);
        }
    }
}
