﻿namespace BinaryMash.Testing.Environment
{
    //TODO: nuget ref to roundhouse?
    using System;
    using System.Diagnostics;
    using System.Linq;
    using Data;
    using DataAccess.EF;

    public class DatabaseManager : IDisposable
    {

        public void InstallDatabase()
        {
            using (var process = new Process())
            {
                process.StartInfo = new ProcessStartInfo("dk.exe",
                                                         "execute /environment:DEV /roles:Db /deployment:BinaryMash.Build.Installation.Deployments.Install,BinaryMash.Build.Installation --silent")
                    {
                        WindowStyle = ProcessWindowStyle.Hidden
                    };

                {
                    //TODO: for some reason, if we're not running in shellExecute mode then the process never finished
                    //RedirectStandardOutput = true,
                    //RedirectStandardError = true,
                    //UseShellExecute = false
                }
                ;

                //var standardOutput = new StringBuilder();
                //var errorOutput = new StringBuilder();

                Trace.WriteLine(System.Environment.CurrentDirectory);

                var started = process.Start();
                if (!started)
                {
                    //TODO: logging of output
                    throw new Exception("Failed to reset database");
                }

                var finished = process.WaitForExit(30000);
                if (!finished)
                {
                    throw new Exception("Timed out waiting for database DropCreate to complete");
                }
                //while (!process.HasExited)
                //{
                //    System.Threading.Thread.Sleep(100);
                //}

                //standardOutput.Append(process.StandardOutput.ReadToEnd());
                //errorOutput.Append(process.StandardError.ReadToEnd()); 

                //var r = standardOutput.ToString();
                //var s = errorOutput.ToString();
            }
        }

        public void BackupDatabase()
        {
            using (var process = new Process())
            {
                process.StartInfo = new ProcessStartInfo("dk.exe",
                                                         "execute /environment:DEV /roles:DbSqlCmd /deployment:BinaryMash.Build.Installation.Deployments.Backup,BinaryMash.Build.Installation --silent")
                {
                    WindowStyle = ProcessWindowStyle.Hidden
                };

                Trace.WriteLine(System.Environment.CurrentDirectory);

                var started = process.Start();
                if (!started)
                {
                    //TODO: logging of output
                    throw new Exception("Failed to backup database");
                }

                var finished = process.WaitForExit(30000);
                if (!finished)
                {
                    throw new Exception("Timed out waiting for database backup to complete");
                }
            }
        }

        public void RestoreDatabase()
        {
            using (var process = new Process())
            {
                process.StartInfo = new ProcessStartInfo("dk.exe",
                                                         "execute /environment:DEV /roles:DbSqlCmd /deployment:BinaryMash.Build.Installation.Deployments.Restore,BinaryMash.Build.Installation --silent")
                {
                    WindowStyle = ProcessWindowStyle.Hidden
                };

                Trace.WriteLine(System.Environment.CurrentDirectory);

                var started = process.Start();
                if (!started)
                {
                    //TODO: logging of output
                    throw new Exception("Failed to restore database");
                }

                var finished = process.WaitForExit(30000);
                if (!finished)
                {
                    throw new Exception("Timed out waiting for database restore to complete");
                }
            }
        }

        public void SeedData()
        {
            using (var dbContext = new BirdhouseContext())
            {
                //Ah! The problems we had here with entities being "remembered" was because the factory for tags was creating static entities 
                //so even though we were using a new dbContext, the entity remembeed its previous references

                var tags = TagFactory.All;
                foreach (var tag in tags)
                {
                    dbContext.Tags.Add(tag);
                }

                dbContext.SaveChanges();

                var questions = QuestionFactory.All(dbContext);
                foreach (var question in questions)
                {
                    dbContext.Questions.Add(question);
                }

                var people = PersonFactory.All(dbContext);
                foreach (var person in people)
                {
                    dbContext.People.Add(person);
                }

                dbContext.SaveChanges();

                var testVersions = TestFactory.AllVersions(dbContext);
                foreach (var tv in testVersions)
                {
                    dbContext.Tests.Add(tv);
                }

                dbContext.SaveChanges();

                var sessionLogin = LoginBuilder.DefaultSession(dbContext).Create();
                dbContext.Logins.Add(sessionLogin);

                dbContext.SaveChanges();
            }   
        }

        //public void SeedBulkData()
        //{
        //    //Tags
        //    SeedTags(100);

        //    for (var i = 1; i < 5; i++)
        //    {
        //        SeedQuestions(i, 100);
        //    }            
        //}

        //protected void SeedQuestions(int iteration, int quantity)
        //{
        //    using (var dbContext = new BirdhouseContext())
        //    {
        //        var questionsOnContext = dbContext.QuestionVersions;
        //        var dbTimeSpan = new TimeSpan();

        //        var time1 = DateTime.UtcNow;

        //        var questions = QuestionFactory.Bulk(dbContext, (quantity * iteration - 1) + 1, quantity, dbTimeSpan);

        //        var time2 = DateTime.UtcNow;

        //        foreach (var question in questions)
        //        {
        //            questionsOnContext.Add(question);
        //        }

        //        var time3 = DateTime.UtcNow;

        //        dbContext.ChangeTracker.DetectChanges();
        //        dbContext.SaveChanges();

        //        var time4 = DateTime.UtcNow;

        //        dbTimeSpan = dbTimeSpan.Add(time3.Subtract(time2));
        //        dbTimeSpan = dbTimeSpan.Add(time4.Subtract(time3));
        //        System.Diagnostics.Trace.WriteLine(string.Format("questions saved ({0}): {1} {2} {3} {4}", iteration, time4.Subtract(time1), dbTimeSpan, time3.Subtract(time2), time4.Subtract(time3)));
        //    }
        //}

        protected void SeedTags(int quantity)
        {
            using (var dbContext = new BirdhouseContext())
            {
                var tagsOnContext = dbContext.Tags;
                var time1 = DateTime.UtcNow;

                var tags = TagFactory.Bulk(quantity);
                foreach (var tag in tags)
                {
                    tagsOnContext.Add(tag);
                }
                
                var time2 = DateTime.UtcNow;

                dbContext.SaveChanges();

                var time3 = DateTime.UtcNow;

                System.Diagnostics.Trace.WriteLine(string.Format("tags saved: {0} {1}", time2.Subtract(time1), time3.Subtract(time2)));
            }
        }

        public BirdhouseContext Context
        {
            get{return new BirdhouseContext();}
        }

        protected void Dispose(bool alsoDisposeManagedObjects)
        {
            if (alsoDisposeManagedObjects)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~DatabaseManager()
        {
            Dispose(false);
        }

    }
}
