﻿namespace BinaryMash.Testing.Environment
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Net.Mail;
    using EmbeddedMail;

    public class EmailManager : IDisposable
    {
        protected ICollection<MailMessage> ReadMessages;
 
        public EmbeddedSmtpServer SmtpServer
        {
            get { return _smtpServer; }
        }

        public IEnumerable<MailMessage> Messages
        {
            get { return _smtpServer.ReceivedMessages(); }            
        }

        public EmailManager()
        {
            SetUpSmtpServer();            
            ReadMessages = new Collection<MailMessage>();
        }

        public MailMessage GetFirstEmail()
        {
            return GetFirstEmail(null);
        }

        public MailMessage GetFirstEmail(string subject)
        {
            for (var i = 0; i < 120; i++)
            {
                if (_smtpServer.ReceivedMessages().Except(ReadMessages).Any())
                {
                    var message = (subject == null) ? 
                        _smtpServer.ReceivedMessages().Except(ReadMessages).First() : 
                        _smtpServer.ReceivedMessages().Except(ReadMessages).First(m => m.Subject == subject);

                    ReadMessages.Add(message);
                    
                    return message;
                }

                System.Threading.Thread.Sleep(500);
            }

            return null;
        }

        private void SetUpSmtpServer()
        {
            _smtpServer = new EmbeddedSmtpServer(25);
            _smtpServer.Start();
        }

        protected void Dispose(bool alsoDisposeManagedObjects)
        {            
            if (alsoDisposeManagedObjects)
            {
                if (_smtpServer != null)
                {
                    _smtpServer.Stop();
                    _smtpServer.Dispose();
                    _smtpServer = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~EmailManager()
        {
            Dispose(false);
        }

        private EmbeddedSmtpServer _smtpServer;
    }
}
