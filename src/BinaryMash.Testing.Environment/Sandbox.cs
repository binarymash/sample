﻿namespace BinaryMash.Testing.Environment
{
    using System;

    public class Sandbox : IDisposable
    {
        public NServiceBusManager NServiceBus { get; private set; }
        public EmailManager Email { get; private set; }
        public DatabaseManager Database { get; private set; }
        public BrowserManager Browser { get; private set; }

        public Sandbox()
        {
            NServiceBus = new NServiceBusManager();
            Email = new EmailManager();
            Database = new DatabaseManager();
            Database.RestoreDatabase();
            Browser = new BrowserManager();
        }

        protected void Dispose(bool alsoFreeManagedObjects)
        {
            //if we have unmanaged resources, free them now

            if (alsoFreeManagedObjects)
            {
                Browser.Dispose();
                NServiceBus.Dispose();
                Email.Dispose();
                Database.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~Sandbox()
        {
            Dispose(false);
        }

    }
}
