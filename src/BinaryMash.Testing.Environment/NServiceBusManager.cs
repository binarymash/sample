﻿namespace BinaryMash.Testing.Environment
{
    using System;
    using System.Linq;
    using System.Messaging;
    using Raven.Abstractions.Data;
    using Raven.Abstractions.Indexing;
    using Raven.Client.Document;
    using Raven.Client.Indexes;

    public class NServiceBusManager : IDisposable
    {
        public NServiceBusManager()
        {
            CleanMessageQueues();
            CleanSagas();
        }
        
        public void CleanMessageQueues()
        {
            var queues = from queue in MessageQueue.GetPrivateQueuesByMachine(".")
                         where queue.QueueName.StartsWith(QueueNameFilter, StringComparison.OrdinalIgnoreCase)
                         select queue;

            foreach (var queue in queues)
            {
                queue.Purge();
            }
        }

        public void CleanSagas()
        {
            //TODO: remove hard-coded url
            using (var documentStore = new DocumentStore {Url = "http://localhost:8080"})
            {
                documentStore.Initialize();
                
                documentStore.DatabaseCommands
                    .ForDatabase("BinaryMash.Application.EmailService")
                    .DeleteByIndex("Raven/DocumentsByEntityName", new IndexQuery());
                
                documentStore.DatabaseCommands
                    .ForDatabase("BinaryMash.Application.UserService")
                    .DeleteByIndex("Raven/DocumentsByEntityName", new IndexQuery());
            }
        }

        protected void Dispose(bool alsoDisposeManagedObjects)
        {
            if (alsoDisposeManagedObjects)
            {
                CleanMessageQueues();
                CleanSagas();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~NServiceBusManager()
        {
            Dispose(false);
        }

        private const string QueueNameFilter = "private$\\BinaryMash.";

    }

    public class AllDocumentsById : AbstractIndexCreationTask
    {
        public override IndexDefinition CreateIndexDefinition()
        {
            return new IndexDefinition
            {
                Name = "AllDocumentsById",
                Map = "from doc in docs let DocId = doc[\"@metadata\"][\"@id\"] select new {DocId};"
            };
        }
    }
}
