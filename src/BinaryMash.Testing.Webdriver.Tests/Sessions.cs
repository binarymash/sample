﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using System;
    using System.Globalization;
    using Data;
    using NUnit.Framework;
    using Pages.Web;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    public class Sessions : SandboxedTest
    {
        [Test]
        public void WhenUnauthenticated_UsersAreToldToRegister()
        {
            var testsPage = new IndexPage(Sandbox).Menu.Sessions;
            testsPage.Summary.ShouldBe("Test sessions are only available to registered users"); 
        }

        [Test]
        public void WhenAuthenticated_ASummaryOfSessionsIsShown()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var sessionsPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Sessions;

            sessionsPage.Summary.ShouldBe("There are 1 sessions");


            var sessions = sessionsPage.Sessions;
            sessions.Count.ShouldBe(1);
             
            var session = sessions[0];
            session.Header.ShouldBe("Kiki Nortey - This is version 2 of test 1");
            session.State.ShouldBe("Scheduled"); 
            session.Updated.ShouldBe("18/07/2013 09:58:11");
            session.UpdatedBy.ShouldBe("Bluejam");
        }

        [Test]
        public void WhenUnauthenticated_AUserCannotCreateASession()
        {
            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);

            var unauthenticatedPage = userManuallyNavigates.ToCreateSessionExpectingUnauthenticated;
            unauthenticatedPage.Summary.ShouldBe("Test sessions are only available to registered users");
        }

        //[Test]
        //public void WhenANewSessionIsScheduled_ItHasNoQuestionsOrAnything()
        //{
        //    var user = UserBuilder.SystemAdmin.Create();

        //    var createTestPage = new IndexPage(Sandbox)
        //        .LogIn()
        //        .LogInAs(user.Email, "testpass")
        //        .Menu.Tests
        //        .CreateANewTest;

        //    createTestPage.Summary.ShouldBe("Create test");

        //    createTestPage.Name.ShouldBe("");

        //    var questions = createTestPage.Questions;
        //    questions.Count.ShouldBe(0);
        //}

        [Test]
        public void WhenEditingAnExistingSessionOnSomeoneElsesAccount_IAmToldTheSessionIsNotFound()
        {
            var user = UserBuilder.DaveSmith.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "passpasspass");

            var editTest = new UserManuallyNavigates(Sandbox).ToEditSession(Guid.Parse("B04AB6E0-4BE7-410D-BFC0-62511AE77B10"));
            editTest.Summary.ShouldBe("Sorry, we could not find this session.");
        }

        [Test]
        [Ignore]
        public void WhenEditingAnExistingSession()
        {
            var user = UserBuilder.DaveSmith.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "passpasspass");

            var sessionsPage = homePage.Menu.Sessions;
            var sessionSummary = sessionsPage.Sessions[0].ViewSummary();
            var editPage = sessionSummary.EditDetails;

            //TODO: xxx
            editPage.Save();
            editPage.UserHasBeenNotifiedThatSavingWasSuccessful.ShouldBe(true);
        }

        [Test]
        public void WhenEditingASessionWhichDoesNotExist_IAmToldTheSessionIsNotFound()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editTest = new UserManuallyNavigates(Sandbox).ToEditSession(Guid.Parse("99999999-9999-9999-9999-999999999999"));
            editTest.Summary.ShouldBe("Sorry, we could not find this session.");
        }

    }
}
