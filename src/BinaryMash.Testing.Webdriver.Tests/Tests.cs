﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using System.Linq;
    using Data;
    using NUnit.Framework;
    using Pages.Web;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    public class Tests : SandboxedTest
    {
        [Test]
        public void WhenUnauthenticated_UsersAreToldToRegister()
        {
            var testsPage = new IndexPage(Sandbox).Menu.Tests;
            testsPage.Summary.ShouldBe("Tests are only available to registered users"); 
        }

        [Test]
        public void WhenAuthenticated_ASummaryOfTestsIsShown()
        {
            var expectedTest = TestFactory.Test1(Sandbox.Database.Context);
            var expectedTestVersion = expectedTest.Versions.First(t => t.Version == expectedTest.LatestVersion);
            var user = UserBuilder.SystemAdmin.Create();

            var testsPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Tests;

            testsPage.Summary.ShouldBe("There are 1 tests");

            var tests = testsPage.Tests;
            tests.Count.ShouldBe(1);

            var test = tests[0];
            test.Name.ShouldBe(expectedTestVersion.Name);
            //test.Created.ShouldBe(expectedTestVersion.Created.ToString(CultureInfo.InvariantCulture));
            test.Updated.ShouldBe(expectedTestVersion.Updated.ToString("dd/MM/yyyy HH:mm:ss"));
            test.UpdatedBy.ShouldBe("Bluejam");
        }

        [Test]
        public void WhenUnauthenticated_AUserCannotCreateATest()
        {
            var unauthenticatedPage = new UserManuallyNavigates(Sandbox).ToCreateTestExpectingUnauthenticated;
            unauthenticatedPage.Summary.ShouldBe("Tests are only available to registered users");
        }

        [Test]
        public void WhenANewTestIsCreated_ItHasNoQuestionsOrAnything()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var createTestPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Tests
                .CreateANewTest;

            createTestPage.Summary.ShouldBe("Create Test");

            createTestPage.Name.ShouldBe("");

            var questions = createTestPage.Questions;
            questions.Count.ShouldBe(0);
        }

        [Test]
        public void WhenEditingAnExistingTestOnSomeoneElsesAccount_IAmToldTheTestIsNotFound()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editTest = new UserManuallyNavigates(Sandbox).ToEditTest(2, false);
            editTest.Summary.ShouldBe("Sorry, we could not find this test.");
        }

        [Test]
        public void WhenEditingAnTestWhichDoesNotExist_IAmToldTheTestIsNotFound()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editTest = new UserManuallyNavigates(Sandbox).ToEditTest(9999, false);
            editTest.Summary.ShouldBe("Sorry, we could not find this test.");
        }

        [Test]
        public void WhenICreateANewQuestionAndSaveSeveralTimesWithoutPublishing_Only1NewTestIsCreated()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var testsPage = homePage.Menu.Tests;
            var originalNumberOfTests = testsPage.NumberOfTests;

            var createTestPage = homePage.Menu.Tests
                                         .CreateANewTest
                                         .SetName("This is my new test")
                                         .Save
                                         .SetName("This is my new test with a new name")
                                         .Save;

            var summaryPage = createTestPage.Done().DontPublish();

            testsPage = summaryPage.Menu.Tests;

            testsPage.NumberOfTests.ShouldBe(originalNumberOfTests+1);
        }

        [Test]
        public void WhenICreateSaveAndPublishInASingleGo_ANewTestIsPublished()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var testsPage = homePage.Menu.Tests;
            var originalNumberOfTests = testsPage.NumberOfTests;

            var createTestPage = homePage.Menu.Tests
                         .CreateANewTest
                         .SetName("This is my new test")
                         .Save;

            createTestPage.UserHasBeenNotifiedThatSavingWasSuccessful.ShouldBe(true);
            
            var publishDialog = createTestPage.Done();

            var publishedPage = publishDialog.Publish();
            publishedPage.Summary.ShouldBe("The test has been published");

            testsPage = publishedPage.Menu.Tests;

            testsPage.NumberOfTests.ShouldBe(originalNumberOfTests + 1);
        }

        [Test]
        public void WhenICreateSaveEditAndCancelInASingleGo_ImTakenToTheSummaryPageWithTheDraftFirstSave()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var createTestPage = homePage.Menu.Tests
                         .CreateANewTest
                         .SetName("This is my new test edit 1")
                         .Save;

            createTestPage.UserHasBeenNotifiedThatSavingWasSuccessful.ShouldBe(true);

            var summaryPage = createTestPage.SetName("This is my new test edit 2")
                                           .CancelExpectingSummary();

            summaryPage.Name.ShouldBe("This is my new test edit 1");
        }
    
    }

}
