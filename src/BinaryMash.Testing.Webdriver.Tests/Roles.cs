﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using Data;
    using NUnit.Framework;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    [Ignore("Not implemented")]
    public class Roles : SandboxedTest
    {

        [Test]
        public void WhenUnauthenticated_UsersAreToldToRegister()
        {
            var rolesPage = new IndexPage(Sandbox).Menu.Roles;
            rolesPage.Summary.ShouldBe("Roles are only available to registered users");
        }

        [Test]
        public void WhenAuthenticated_ASummaryOfRolesIsShown()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var rolesPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Roles;

            rolesPage.Summary.ShouldBe("There are no roles");
        }

    }
}
