﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Data;
    using Domain;
    using NUnit.Framework;
    using Pages.Web;
    using Pages.Web.Question;
    using Shouldly;
    using IndexPage = Pages.Web.Home.IndexPage;

    [TestFixture]
    public class Questions : SandboxedTest
    {

        #region Tagged

        [Test]
        public void SearchingForATagWhichDoesNotExist()
        {
            var questionsPage = new UserManuallyNavigates(Sandbox).ToQuestionsTaggedWith("ThisTagDoesNotExist");
            questionsPage.NumberOfQuestions.ShouldBe(0);
        }

        [Test]
        public void SearchingForATagWhichDoesExistAndHasNoQuestions()
        {
            var questionsPage = new UserManuallyNavigates(Sandbox).ToQuestionsTaggedWith("Javascript");
            questionsPage.NumberOfQuestions.ShouldBe(0);
            questionsPage.Pager.IsDisplayed.ShouldBe(false);
        }

        [Test]
        public void SearchingForATagWhichDoesExistAndHas2PagesOfQuestions()
        {
            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var questionsPage = userManuallyNavigates.ToQuestionsTaggedWith("c%23");
            questionsPage.NumberOfQuestions.ShouldBe(16);
            questionsPage.Pager.IsDisplayed.ShouldBe(true);

            var visiblePages = questionsPage.Pager.DisplayedPages;
            visiblePages.Count.ShouldBe(2);
            visiblePages.ShouldContain(1);
            visiblePages.ShouldContain(2);            
            questionsPage.Pager.CurrentPageNumber.ShouldBe(1);

            questionsPage.Pager.FirstPageLinkIsEnabled.ShouldBe(false);
            questionsPage.Pager.PreviousPageLinkIsEnabled.ShouldBe(false);
            questionsPage.Pager.NextPageLinkIsEnabled.ShouldBe(true);
            questionsPage.Pager.LastPageLinkIsEnabled.ShouldBe(true);

            //user 1
            var user = UserBuilder.SystemAdmin.Create();
            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            questionsPage = userManuallyNavigates.ToQuestionsTaggedWith("c%23");
            questionsPage.NumberOfQuestions.ShouldBe(18);
            questionsPage.Pager.IsDisplayed.ShouldBe(true);
            questionsPage.Menu.LogOut();

            //user 2
            user = UserBuilder.DaveSmith.Create();
            homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "passpasspass");

            questionsPage = userManuallyNavigates.ToQuestionsTaggedWith("c%23");
            questionsPage.NumberOfQuestions.ShouldBe(17);
            questionsPage.Pager.IsDisplayed.ShouldBe(true);
            questionsPage.Menu.LogOut();

        }

        [Test]
        public void SearchingForATagWhichDoesExistAndHas1PageOfQuestions_TheCorrectNumberOfPublicAndPrivateQuestionsAreDisplayed()
        {
            //unauthenticated
            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var questionsPage = userManuallyNavigates.ToQuestionsTaggedWith(".net");
            questionsPage.NumberOfQuestions.ShouldBe(10);
            questionsPage.Pager.IsDisplayed.ShouldBe(false);

            //user 1
            var user = UserBuilder.SystemAdmin.Create();
            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            questionsPage = userManuallyNavigates.ToQuestionsTaggedWith(".net");
            questionsPage.NumberOfQuestions.ShouldBe(12);
            questionsPage.Pager.IsDisplayed.ShouldBe(false);
            questionsPage.Menu.LogOut();

            //user 2
            user = UserBuilder.DaveSmith.Create();
            homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "passpasspass");

            questionsPage = userManuallyNavigates.ToQuestionsTaggedWith(".net");
            questionsPage.NumberOfQuestions.ShouldBe(11);
            questionsPage.Pager.IsDisplayed.ShouldBe(false);
            questionsPage.Menu.LogOut();
        }

        #endregion

        #region Create

        [Test]
        public void WhenUnauthenticated_AUserCannotCreateAQuestion()
        {
            var unauthenticatedPage = new UserManuallyNavigates(Sandbox).ToCreateQuestionExpectingUnauthenticated;
            unauthenticatedPage.Summary.ShouldBe("Questions can only be created by registered users");
        }

        [Test]
        public void WhenAuthenticated_AUserCanCreateAQuestion()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var createQuestionPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Questions
                .WriteANewQuestion;

            createQuestionPage.Summary.ShouldBe("Create A New Question");
        }

        [Test]
        public void WhenANewQuestionIsCreated_ItHasADefaultSectionAndUnsetAnswerType()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var createQuestionPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Questions
                .WriteANewQuestion;

            createQuestionPage.Title.ShouldBe("");
            
            var qAndAsections = createQuestionPage.Sections;            
            qAndAsections.Count.ShouldBe(1);
            qAndAsections[0].Text.ShouldBe("");
            qAndAsections[0].Notes.ShouldBe("");
            qAndAsections[0].SelectedAnswerType.ShouldBe(AnswerContainerType.Unset);
        }

        [Test]
        public void WhenCreatingAQuestionAndNotSettingFields_ErrorsAreDisplayed()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var createQuestionPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Questions
                .WriteANewQuestion
                .SaveExpectingError;

            createQuestionPage.UserHasBeenNotifiedThatSavingTheDraftWasUnsuccessful.ShouldBe(true);

            var errors = createQuestionPage.ErrorMessages;

            errors.Count.ShouldBe(3);
            errors.ShouldContain("'Title' is required");
            errors.ShouldContain("'Text' is required");
            errors.ShouldContain("'Type' is required");

            var section = createQuestionPage.Sections[0];
            section.SelectAnswerType(AnswerContainerType.UserSupplied);
            createQuestionPage = createQuestionPage.SaveExpectingError;
            createQuestionPage.UserHasBeenNotifiedThatSavingTheDraftWasUnsuccessful.ShouldBe(true);
            errors = createQuestionPage.ErrorMessages;

            errors.Count.ShouldBe(3);
            errors.ShouldContain("'Title' is required");
            errors.ShouldContain("'Text' is required");
            errors.ShouldContain("'Maximum Length' must be between 1 and 4096");

            section.SelectAnswerType(AnswerContainerType.Checkbox);
            createQuestionPage = createQuestionPage.SaveExpectingError;
            errors = createQuestionPage.ErrorMessages;

            errors.Count.ShouldBe(3);
            errors.ShouldContain("'Title' is required");
            errors.Count(e => e == "'Text' is required").ShouldBe(2); 

            section.SelectAnswerType(AnswerContainerType.Radio);
            createQuestionPage = createQuestionPage.SaveExpectingError;
            createQuestionPage.UserHasBeenNotifiedThatSavingTheDraftWasUnsuccessful.ShouldBe(true);

            errors = createQuestionPage.ErrorMessages;

            errors.Count.ShouldBe(3);
            errors.ShouldContain("'Title' is required");
            errors.Count(e => e == "'Text' is required").ShouldBe(2); 

        }

        #endregion

        #region Save draft

        [Test]
        public void WhenSavingADraftQuestionWithUserSuppliedAnswer_ICanThenSeeAPreviewOfTheQuestion()
        {
            const string title = "Question testing creation with user supplied text";
            const string questionText = "This is text for the first question section";
            const string questionNotes = "These are notes for the first question section";
            const string maxLength = "2048";
            const string answerNotes = "These are notes for the user supplied answer in the first question section";

            var user = UserBuilder.SystemAdmin.Create();

            var createQuestionPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Questions
                .WriteANewQuestion;

            createQuestionPage
                 .SetTitle(title);

            var section = createQuestionPage.Sections[0]
                 .SetText(questionText)
                 .SetNotes(questionNotes)
                 .SelectAnswerType(AnswerContainerType.UserSupplied);

            var answerContent = (AnswerContentUserSuppliedEditComponent)section.AnswerContents[0];
            answerContent.SetMaxLength(maxLength)
                .SetNotes(answerNotes);

            createQuestionPage = createQuestionPage.Save;

            createQuestionPage.UserHasBeenNotifiedThatSavingTheDraftWasSuccessful.ShouldBe(true);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);

            var previewPage = userManuallyNavigates.ToPreviewOfQuestion(createQuestionPage.QuestionId);
            previewPage.Title.ShouldBe(title);
            var sections = previewPage.Sections;
            sections.Count.ShouldBe(1);
            sections[0].Text.ShouldBe(questionText);
            sections[0].Notes.ShouldBe(questionNotes);
            var answerPreviewContainer = (AnswerContentUserSuppliedPreviewComponent)sections[0].AnswerContent;
            answerPreviewContainer.AnswerPlaceholder.ShouldBe(string.Format(CultureInfo.InvariantCulture, "The candidate's answer can be up to {0} characters", maxLength));
            answerPreviewContainer.Notes.ShouldBe(answerNotes);
        }

        #endregion

        #region Edit

        [Test]
        public void WhenAuthenticated_ICanEditAQuestionOnMyOwnAccount()
        {
            var user = UserBuilder.DaveSmith.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "passpasspass");

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);

            var createEditPage = userManuallyNavigates.ToEditQuestion(7);

            //TODO: finish
        }

        [Test]
        public void WhenEditingAPublishedQuestion_TheDataIsSavedToTheDatabaseAndIsVisibleInThePreview()
        {
            const string title = "This is my new title";
            const string questionText = "This is my new question text";
            const string questionNotes = "This is my new question notes";
            const string maxLength = "1234";
            const string answerNotes = "These are my new answer notes";

            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editQuestionPage = new UserManuallyNavigates(Sandbox).ToEditQuestion(5);

            editQuestionPage
                 .SetTitle(title);

            var section = editQuestionPage.Sections[0]
                 .SetText(questionText)
                 .SetNotes(questionNotes)
                 .SelectAnswerType(AnswerContainerType.UserSupplied);

            var answerContent = (AnswerContentUserSuppliedEditComponent)section.AnswerContents[0];
            answerContent.SetMaxLength(maxLength)
                .SetNotes(answerNotes);

            editQuestionPage = editQuestionPage.Save;
            editQuestionPage.UserHasBeenNotifiedThatSavingTheDraftWasSuccessful.ShouldBe(true);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var previewPage = userManuallyNavigates.ToPreviewOfQuestion(editQuestionPage.QuestionId);
            previewPage.Title.ShouldBe(title);
            var sections = previewPage.Sections;
            sections.Count.ShouldBe(1);
            sections[0].Text.ShouldBe(questionText);
            sections[0].Notes.ShouldBe(questionNotes);
            var answerPreviewContainer = (AnswerContentUserSuppliedPreviewComponent)sections[0].AnswerContent;
            answerPreviewContainer.AnswerPlaceholder.ShouldBe(string.Format(CultureInfo.InvariantCulture, "The candidate's answer can be up to {0} characters", maxLength));
            answerPreviewContainer.Notes.ShouldBe(answerNotes);
        }


        [Test]
        public void WhenIChangeAPublishedQuestionButThenCancel_ImTakenToPreviewAndNothingHasChanged()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);

            var previewPage = userManuallyNavigates.ToPreviewOfQuestion(3);
            var originalVersion = previewPage.Version;

            var editQuestionPage = userManuallyNavigates.ToEditQuestion(3);

            //lets get the original data
            var originalTitle = editQuestionPage.Title;
            editQuestionPage.Sections.Count.ShouldBe(1);
            var originalSection = editQuestionPage.Sections[0];
            var originalSection1Text = originalSection.Text;
            var originalSection1Notes = originalSection.Notes;
            var originalSection1Type = originalSection.SelectedAnswerType;
            originalSection.AnswerContents.Count.ShouldBe(4);
            var originalAnswer1 = (AnswerContentQuestionerSuppliedEditComponent)originalSection.AnswerContents[0];
            var originalAnswer1Text = originalAnswer1.Text;
            var originalAnswer1Notes = originalAnswer1.Notes;
            var originalAnswer2 = (AnswerContentQuestionerSuppliedEditComponent)originalSection.AnswerContents[1];
            var originalAnswer2Text = originalAnswer2.Text;
            var originalAnswer2Notes = originalAnswer2.Notes;
            var originalAnswer3 = (AnswerContentQuestionerSuppliedEditComponent)originalSection.AnswerContents[2];
            var originalAnswer3Text = originalAnswer3.Text;
            var originalAnswer3Notes = originalAnswer3.Notes;
            var originalAnswer4 = (AnswerContentQuestionerSuppliedEditComponent)originalSection.AnswerContents[3];
            var originalAnswer4Text = originalAnswer4.Text;
            var originalAnswer4Notes = originalAnswer4.Notes;

            //ok, lets change everything
            editQuestionPage
                .SetTitle(originalTitle + "abc");

            //change section
            var section = editQuestionPage.Sections[0];
            section.SetText(originalSection1Text + "abc")
                .SetNotes(originalSection1Notes+"abc")
                .SelectAnswerType(AnswerContainerType.UserSupplied);

            var contents = (AnswerContentUserSuppliedEditComponent)section.AnswerContents[0];
            contents.SetMaxLength("123");
            contents.SetNotes(originalAnswer1Notes + "abc");

            //now cancel
            previewPage = editQuestionPage.CancelExpectingPreview();

            //check preview version
            previewPage.Version.ShouldBe(originalVersion);

            //check everything matches what we first started with
            editQuestionPage = userManuallyNavigates.ToEditQuestion(3);
            editQuestionPage.Title.ShouldBe(originalTitle);
            editQuestionPage.Sections.Count.ShouldBe(1);
            var newSection = editQuestionPage.Sections[0];
            newSection.Text.ShouldBe(originalSection1Text);
            newSection.Notes.ShouldBe(originalSection1Notes);
            newSection.SelectedAnswerType.ShouldBe(originalSection1Type);
            newSection.AnswerContents.Count.ShouldBe(4);
            var newAnswer1 = (AnswerContentQuestionerSuppliedEditComponent)newSection.AnswerContents[0];
            newAnswer1.Text.ShouldBe(originalAnswer1Text);
            newAnswer1.Notes.ShouldBe(originalAnswer1Notes);
            var newAnswer2 = (AnswerContentQuestionerSuppliedEditComponent)newSection.AnswerContents[1];
            newAnswer2.Text.ShouldBe(originalAnswer2Text);
            newAnswer2.Notes.ShouldBe(originalAnswer2Notes);
            var newAnswer3 = (AnswerContentQuestionerSuppliedEditComponent)newSection.AnswerContents[2];
            newAnswer3.Text.ShouldBe(originalAnswer3Text);
            newAnswer3.Notes.ShouldBe(originalAnswer3Notes);
            var newAnswer4 = (AnswerContentQuestionerSuppliedEditComponent)newSection.AnswerContents[3];
            newAnswer4.Text.ShouldBe(originalAnswer4Text);
            newAnswer4.Notes.ShouldBe(originalAnswer4Notes);
        }

        [Test]
        public void WhenICancelEditsOnANewUnsavedQuestion_ImTakenToTheIndexAndNothingHasChanged()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var indexPage = homePage.Menu.Questions;
            var originalCount = indexPage.NumberOfQuestions;

            var createQuestionPage = indexPage.WriteANewQuestion;

            //lets get the original data
            var originalTitle = createQuestionPage.Title;
            createQuestionPage.Sections.Count.ShouldBe(1);
            var originalSection = createQuestionPage.Sections[0];
            var originalSection1Text = originalSection.Text;
            var originalSection1Notes = originalSection.Notes;
            var originalSection1Type = originalSection.SelectedAnswerType;
            originalSection.AnswerContents.Count.ShouldBe(0);

            //ok, lets change everything
            createQuestionPage.SetTitle(originalTitle + "abc");

            //change section
            var section = createQuestionPage.Sections[0];
            section.SetText("abc")
                .SetNotes(originalSection1Notes + "abc")
                .SelectAnswerType(AnswerContainerType.Checkbox);

            var contents = (AnswerContentQuestionerSuppliedEditComponent)section.AnswerContents[0];
            contents.SetText("abc");
            contents.SetNotes("abc");

            //now cancel
            indexPage = createQuestionPage.CancelExpectingIndex();
            indexPage.NumberOfQuestions.ShouldBe(originalCount);
        }

        [Test]
        public void WhenICreateSaveEditAndCancelInASingleGo_ImTakenToThePreviewOfTheDraftFirstSave()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var createQuestionPage = new UserManuallyNavigates(Sandbox).ToCreateQuestion;

            //lets set the original data
            const string originalTitle = "my edit 1";
            const string originalSection1Text = "abc";
            const string originalSection1Notes = "def";
            const AnswerContainerType originalSection1Type = AnswerContainerType.Checkbox;
            const string originalAnswer1Text = "ghi";
            const string originalAnswer1Notes = "jkl";

            createQuestionPage.SetTitle(originalTitle);

            var section = createQuestionPage.Sections[0];
            section.SetText(originalSection1Text)
                .SetNotes(originalSection1Notes)
                .SelectAnswerType(originalSection1Type);

            var contents = (AnswerContentQuestionerSuppliedEditComponent)section.AnswerContents[0];
            contents.SetText(originalAnswer1Text);
            contents.SetNotes(originalAnswer1Notes);

            createQuestionPage.Save.UserHasBeenNotifiedThatSavingTheDraftWasSuccessful.ShouldBe(true);

            //change stuff
            createQuestionPage.SetTitle(originalTitle + "2");

            section = createQuestionPage.Sections[0];
            section.SetText(originalSection1Text+"2")
                .SetNotes(originalSection1Notes + "2")
                .SelectAnswerType(AnswerContainerType.Radio);

            contents = (AnswerContentQuestionerSuppliedEditComponent)section.AnswerContents[0];
            contents.SetText(originalAnswer1Text+"2");
            contents.SetNotes(originalAnswer1Notes+"2");

            section.AddAnswer();
            contents = (AnswerContentQuestionerSuppliedEditComponent)section.AnswerContents[1];
            contents.SetText(originalAnswer1Text + "2b");
            contents.SetNotes(originalAnswer1Notes + "2b");

            //now cancel
            var previewPage = createQuestionPage.CancelExpectingPreview();

            //check everything matches what we first started with
            previewPage.Version.ShouldBe(1);
            previewPage.State.ShouldBe("Draft");
            previewPage.Title.ShouldBe(originalTitle);
            previewPage.Sections.Count.ShouldBe(1);
            var newSection = previewPage.Sections[0];
            newSection.Text.ShouldBe(originalSection1Text);
            newSection.Notes.ShouldBe(originalSection1Notes);
            var answer = newSection.AnswerContent;

            //TODO: implement
            //answer.ShouldBe(originalSection1Type);
            //newSection.AnswerContents.Count.ShouldBe(1);
            //var answerContent = (AnswerContentQuestionerSuppliedEditComponent)newSection.AnswerContents[0];
            //answerContent.Text.ShouldBe(originalAnswer1Text);
            //answerContent.Notes.ShouldBe(originalAnswer1Notes);
        }

        [Test]
        public void WhenICreateSaveAndPublishInASingleGo_ANewTestIsPublished()
        {
            //test data
            const string originalTitle = "my edit 1";
            const string originalSection1Text = "abc";
            const string originalSection1Notes = "def";
            const AnswerContainerType originalSection1Type = AnswerContainerType.Checkbox;
            const string originalAnswer1Text = "ghi";
            const string originalAnswer1Notes = "jkl";

            //load questions
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var indexPage = homePage.Menu.Questions;
            var originalNumberOfQuestions = indexPage.NumberOfQuestions;
            indexPage.QuestionsWithTitle(originalTitle).Count.ShouldBe(0);

            //create new question
            var createQuestionPage = new UserManuallyNavigates(Sandbox).ToCreateQuestion;

            createQuestionPage.SetTitle(originalTitle);

            var section = createQuestionPage.Sections[0];
            section.SetText(originalSection1Text)
                .SetNotes(originalSection1Notes)
                .SelectAnswerType(originalSection1Type);

            var contents = (AnswerContentQuestionerSuppliedEditComponent)section.AnswerContents[0];
            contents.SetText(originalAnswer1Text);
            contents.SetNotes(originalAnswer1Notes);

            createQuestionPage.Save.UserHasBeenNotifiedThatSavingTheDraftWasSuccessful.ShouldBe(true);

            var publishDialog = createQuestionPage.Done();

            var publishedPage = publishDialog.Publish();

            //asserts
            publishedPage.Summary.ShouldBe("The question has been published.");

            indexPage = publishedPage.Menu.Questions;
            indexPage.NumberOfQuestions.ShouldBe(originalNumberOfQuestions+1);
            indexPage.QuestionsWithTitle("my edit 1").Count.ShouldBe(1);
        }

        #endregion

        #region Index

        [Test]
        public void WhenAQuestionIsPrivate_ItIsOnlyVisibleInTheQuestionsSearchWhenOnTheAcountThatOwnsIt()
        {
            var user1 = UserBuilder.SystemAdmin.Create();
            var user2 = UserBuilder.DaveSmith.Create();
            const string title6_user1 = "This is a sample of a private question owned by philip.m.wood@gmail.com";
            const string title7_user2 = "This is a private question owned by dave.smith@binarymash.com";
            const string title9_user1 = "Another private question owned by philip.m.wood@gmail.com - v2";

            //Private questions are not visible when unauthenticated

            var questionsIndexPage = new IndexPage(Sandbox).Menu.Questions;

            questionsIndexPage.NumberOfQuestions.ShouldBe(18);
            var questionSummaries = questionsIndexPage.Questions;
            questionSummaries.Count(q => q.Title == title6_user1).ShouldBe(0);
            questionSummaries.Count(q => q.Title == title7_user2).ShouldBe(0);
            questionSummaries.Count(q => q.Title == title9_user1).ShouldBe(0);

            //When authenticated on user 1, user 2's private questions are not visible

            questionsIndexPage = new IndexPage(Sandbox).LogIn()
                                                       .LogInAs(user1.Email, "testpass")
                                                       .Menu.Questions;

            questionsIndexPage.NumberOfQuestions.ShouldBe(20);

            questionSummaries = questionsIndexPage.Questions;
            questionSummaries.Count(q => q.Title == title6_user1).ShouldBe(1);
            questionSummaries.Count(q => q.Title == title7_user2).ShouldBe(0);
            questionSummaries.Count(q => q.Title == title9_user1).ShouldBe(1);

            var indexPage = questionsIndexPage.LogOut();

            //When authenticated on user 2, user 1's private questions are not visible

            questionsIndexPage = indexPage.LogIn()
                                          .LogInAs(user2.Email, "passpasspass")
                                          .Menu.Questions;

            questionsIndexPage.NumberOfQuestions.ShouldBe(19);

            questionSummaries = questionsIndexPage.Questions;
            questionSummaries.Count(q => q.Title == title6_user1).ShouldBe(0);
            questionSummaries.Count(q => q.Title == title7_user2).ShouldBe(1);
            questionSummaries.Count(q => q.Title == title9_user1).ShouldBe(0);
        }

        [Test]
        public void WhenAQuestionIsPublic_ItIsVisibleInTheQuestionsSearchRegardlessOfAuthentication()
        {
            var user1 = UserBuilder.SystemAdmin.Create();
            var user2 = UserBuilder.DaveSmith.Create();
            const string title8_user2 = "This is a sample of a public question owned by dave.smith@binarymash.com";
            const string title10_user1 = "This is a sample of a public question owned by philip.m.wood@gmail.com";

            //When authenticated, all public questions are visible

            var questionsIndexPage = new IndexPage(Sandbox).Menu.Questions;

            questionsIndexPage.NumberOfQuestions.ShouldBe(18);
            var questionSummaries = questionsIndexPage.Questions;
            questionSummaries.Count(q => q.Title == title8_user2).ShouldBe(1);
            questionSummaries.Count(q => q.Title == title10_user1).ShouldBe(1);

            //When authenticated on user 1, all public questions are visible

            questionsIndexPage = new IndexPage(Sandbox).LogIn()
                                                       .LogInAs(user1.Email, "testpass")
                                                       .Menu.Questions;

            questionsIndexPage.NumberOfQuestions.ShouldBe(20);

            questionSummaries = questionsIndexPage.Questions;
            questionSummaries.Count(q => q.Title == title8_user2).ShouldBe(1);
            questionSummaries.Count(q => q.Title == title10_user1).ShouldBe(1);

            var indexPage = questionsIndexPage.LogOut();

            //When authenticated on user 2, all public questions are visible

            questionsIndexPage = indexPage.LogIn()
                                          .LogInAs(user2.Email, "passpasspass")
                                          .Menu.Questions;

            questionsIndexPage.NumberOfQuestions.ShouldBe(19);

            questionSummaries = questionsIndexPage.Questions;
            questionSummaries.Count(q => q.Title == title8_user2).ShouldBe(1);
            questionSummaries.Count(q => q.Title == title10_user1).ShouldBe(1);
        }

        #endregion

        #region Preview

        [Test]
        public void WhenAQuestionIsPrivate_ThePreviewCanOnlyBeNavigatedToDirectlyWhenOnTheAcountThatOwnsIt()
        {
            var user1 = UserBuilder.SystemAdmin.Create();
            var user2 = UserBuilder.DaveSmith.Create();
            const string title6_user1 = "This is a sample of a private question owned by philip.m.wood@gmail.com";
            const string title7_user2 = "This is a private question owned by dave.smith@binarymash.com";
            const string title9_user1 = "Another private question owned by philip.m.wood@gmail.com - v2";

            //Private questions are not visible when unauthenticated

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox); 

            var previewNotFoundPage = userManuallyNavigates.ToPreviewOfQuestionExpectingNotFound(6);
            previewNotFoundPage.Summary.ShouldBe("Sorry, we couldn't find the question you were looking for.");
            previewNotFoundPage.ContainsContent(title6_user1).ShouldBe(false);

            previewNotFoundPage = userManuallyNavigates.ToPreviewOfQuestionExpectingNotFound(7);
            previewNotFoundPage.Summary.ShouldBe("Sorry, we couldn't find the question you were looking for.");
            previewNotFoundPage.ContainsContent(title7_user2).ShouldBe(false);

            previewNotFoundPage = userManuallyNavigates.ToPreviewOfQuestionExpectingNotFound(9);
            previewNotFoundPage.Summary.ShouldBe("Sorry, we couldn't find the question you were looking for.");
            previewNotFoundPage.ContainsContent(title9_user1).ShouldBe(false);

            //When authenticated on user 1, user 2's private questions are not visible

            var homePage = new IndexPage(Sandbox).LogIn()
                                                 .LogInAs(user1.Email, "testpass");

            var previewPage = userManuallyNavigates.ToPreviewOfQuestion(6);
            previewPage.ContainsContent("Sorry, we couldn't find the question you were looking for.").ShouldBe(false);
            previewPage.Title.ShouldBe(title6_user1);

            previewNotFoundPage = userManuallyNavigates.ToPreviewOfQuestionExpectingNotFound(7);
            previewNotFoundPage.Summary.ShouldBe("Sorry, we couldn't find the question you were looking for.");
            previewNotFoundPage.ContainsContent(title7_user2).ShouldBe(false);

            previewPage = userManuallyNavigates.ToPreviewOfQuestion(9);
            previewPage.ContainsContent("Sorry, we couldn't find the question you were looking for.").ShouldBe(false);
            previewPage.Title.ShouldBe(title9_user1);

            var indexPage = homePage.LogOut();

            //When authenticated on user 2, user 1's private questions are not visible

            indexPage.LogIn()
                     .LogInAs(user2.Email, "passpasspass");

            previewNotFoundPage = userManuallyNavigates.ToPreviewOfQuestionExpectingNotFound(6);
            previewNotFoundPage.Summary.ShouldBe("Sorry, we couldn't find the question you were looking for.");
            previewNotFoundPage.ContainsContent(title6_user1).ShouldBe(false);

            previewPage = userManuallyNavigates.ToPreviewOfQuestion(7);
            previewPage.ContainsContent("Sorry, we couldn't find the question you were looking for.").ShouldBe(false);
            previewPage.Title.ShouldBe(title7_user2);

            previewNotFoundPage = userManuallyNavigates.ToPreviewOfQuestionExpectingNotFound(9);
            previewNotFoundPage.Summary.ShouldBe("Sorry, we couldn't find the question you were looking for.");
            previewNotFoundPage.ContainsContent(title9_user1).ShouldBe(false);

        }

        [Ignore]
        [Test]
        public void WhenISaveANewDraftVersionOfAnExistingQuestion_OnlyTheLatestVersionIsVisibleInTheIndex()
        {
           //visible only on users's index. SHould not be visible on other users or when unauthenticated   
        }

        [Ignore]
        [Test]
        public void WhenIPublishANewVersionOfAnExistingQuestion_OnlyTheLatestVersionIsVisibleInTheIndex()
        {
            //visible only on users's index, on another user's index and when unauthenticated.
        }

        #endregion

    }
}
