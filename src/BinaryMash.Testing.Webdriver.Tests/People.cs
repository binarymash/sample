﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using Data;
    using Domain;
    using NUnit.Framework;
    using Pages.Web;
    using Pages.Web.Person;
    using Shouldly;
    using System.Linq;
    using IndexPage = Pages.Web.Home.IndexPage;

    [TestFixture]
    public class People : SandboxedTest
    {

        [Test]
        public void WhenUnauthenticated_UsersAreToldToRegister()
        {
            var peoplePage = new IndexPage(Sandbox).Menu.People;
            peoplePage.Summary.ShouldBe("People are only available to registered users.");
        }

        [Test]
        public void WhenAuthenticated_ASummaryOfPeopleIsShown()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var peoplePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.People;

            peoplePage.Summary.ShouldBe("There are 3 people");

            var people = peoplePage.People;
            people.Count.ShouldBe(3);

            var person = people[0];
            person.Name.ShouldBe("Elspeth Wray");
            person.Updated.ShouldBe("09/07/2013 10:28:41");
            person.UpdatedBy.ShouldBe("Bluejam");

            person = people[1];
            person.Name.ShouldBe("Kiki Nortey");
            person.Updated.ShouldBe("09/07/2013 10:14:05");
            person.UpdatedBy.ShouldBe("Bluejam");

            person = people[2];
            person.Name.ShouldBe("Louise Main");
            person.Updated.ShouldBe("09/07/2013 10:18:13");
            person.UpdatedBy.ShouldBe("Bluejam");
        }

        [Test]
        public void WhenUnauthenticated_AUserCannotCreateAPerson()
        {
            var createPersonPage = new UserManuallyNavigates(Sandbox).ToCreatePersonExpectingUnauthenticated;
            createPersonPage.Summary.ShouldBe("People are only available to registered users.");
        }

        [Test]
        public void WhenANewPersonIsCreated_ItHasNoCommunicationChannelsOrAnything()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var createPersonPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.People
                .AddANewPerson;

            createPersonPage.Summary.ShouldBe("Create A New Person");

            createPersonPage.Name.ShouldBe("");
            createPersonPage.Notes.ShouldBe("");

            var channels = createPersonPage.CommunicationChannels;
            channels.Count.ShouldBe(0);
        }

        [Test]
        public void WhenCreatingAPersonAndNotSettingFields_ErrorsAreDisplayed()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var createPersonPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.People
                .AddANewPerson
                .SaveExpectingError;

            createPersonPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);

            var errors = createPersonPage.ErrorMessages;

            errors.Count.ShouldBe(1);
            errors.ShouldContain("'Name' is required");

            createPersonPage = createPersonPage.AddEmail()
                                               .SaveExpectingError;

            createPersonPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);

            errors = createPersonPage.ErrorMessages;

            errors.Count.ShouldBe(3);
            errors.ShouldContain("'Name' is required");
            errors.ShouldContain("'Type' is required");
            errors.ShouldContain("'Value' is required");

            createPersonPage = createPersonPage.AddPhone()
                                               .SaveExpectingError;

            createPersonPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);

            errors = createPersonPage.ErrorMessages;

            errors.Count.ShouldBe(5);
            errors.ShouldContain("'Name' is required");
            errors.Count(e => e == "'Type' is required").ShouldBe(2);
            errors.Count(e => e == "'Value' is required").ShouldBe(2);

            createPersonPage = createPersonPage.AddAddress()
                                               .SaveExpectingError;

            createPersonPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);

            errors = createPersonPage.ErrorMessages;

            errors.Count.ShouldBe(7);
            errors.ShouldContain("'Name' is required");
            errors.Count(e => e == "'Type' is required").ShouldBe(3);
            errors.Count(e => e == "'Value' is required").ShouldBe(3);
        }

        [Test]
        public void WhenChangingCommunicationChannelTypes_TheCorrectOptionsAreShownInTheDropdown()
        {

            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editPerson = new UserManuallyNavigates(Sandbox).ToEditPerson(1);

            var emailChannel = editPerson.EmailChannels.Find(c => c.TypeAsString == "testing2");
            var options = emailChannel.TypeOptions;
            options.Count().ShouldBe(3);
            options.Any(o => o == "Home").ShouldBe(true);
            options.Any(o => o == "Work").ShouldBe(true);
            options.Any(o => o == "testing2").ShouldBe(true);
            emailChannel.SelectedTypeOption.ShouldBe("testing2");

            emailChannel.SetType("abc");

            emailChannel = editPerson.EmailChannels.Find(c => c.TypeAsString == "abc");
            options = emailChannel.TypeOptions;
            options.Count().ShouldBe(3);
            options.Any(o => o == "Home").ShouldBe(true);
            options.Any(o => o == "Work").ShouldBe(true);
            options.Any(o => o == "abc").ShouldBe(true);
            emailChannel.SelectedTypeOption.ShouldBe("abc");

            emailChannel.SetType("def");

            emailChannel = editPerson.EmailChannels.Find(c => c.TypeAsString == "def");
            options = emailChannel.TypeOptions;
            options.Count().ShouldBe(3);
            options.Any(o => o == "Home").ShouldBe(true);
            options.Any(o => o == "Work").ShouldBe(true);
            options.Any(o => o == "def").ShouldBe(true);
            emailChannel.SelectedTypeOption.ShouldBe("def");


        }

        [Test]
        public void WhenEditingAnExistingPerson_TheDataIsSavedToTheDatabaseAndIsVisibleInThePreview()
        {
            const string name = "This is my new name";
            const string notes = "These are my new notes";

            //Email data

            //will be deleted
            const string oldEmail1 = "kiki.nortey@binarymash.com";
            const string oldEmailType1 = "Home";

            //will be changed
            const string oldEmail2 = "testing@binarymash.com";
            const string newEmail2 = "testing_newEmail2@binarymash.com";
            const string oldEmailType2 = "testing";
            const string newEmailType2 = "Work";

            //will be deleted
            const string oldEmail3 = "testing2@binarymash.com";
            const string oldEmailType3 = "testing2";

            //type will be changed
            const string oldEmail4 = "testing3@binarymash.com";
            const string oldEmailType4 = "testing3";
            const string newEmailType4 = "Home";

            //will be added
            const string newEmail5 = "testing4@binarymash.com";
            const string newEmailType5 = "testing4";

            //will be added
            const string newEmail6 = "testing5@binarymash.com";
            const string newEmailType6 = "Home";


            //PhoneChannel data

            //will be deleted
            const string oldPhone1 = "+441234567890";
            const string oldPhoneType1 = "Mobile";

            //will be changed
            const string oldPhone2 = "+4456789012";
            const string newPhone2 = "+4456789099";
            const string oldPhoneType2 = "My number 1";
            const string newPhoneType2 = "My number 1 modified";

            //will be deleted
            const string oldPhone3 = "+44545566665";
            const string oldPhoneType3 = "My number 2";

            //type will be changed
            const string oldPhone4 = "00000000000";
            const string oldPhoneType4 = "My number 3";
            const string newPhoneType4 = "Work";

            //will be added
            const string newPhone5 = "22222222222";
            const string newPhoneType5 = "testing4";

            //will be added
            const string newPhone6 = "33333333333";
            const string newPhoneType6 = "Home";

            //will be unchanged
            const string oldPhone7 = "+443344556677";
            const string oldPhoneType7 = "Home";


            //AddressChannel data

            //will be deleted
            const string oldAddress1 = "1 Some Place, Any Street, Any Town";
            const string oldAddressType1 = "Home";

            //will be changed
            const string oldAddress2 = "abcdefgh";
            const string newAddress2 = "My new address";
            const string oldAddressType2 = "Somewhere";
            const string newAddressType2 = "Work";

            //will be deleted
            const string oldAddress3 = "gghhiijj";
            const string oldAddressType3 = "Somewhere else";

            //type will be changed
            const string oldAddress4 = "aa bb cc dd";
            const string oldAddressType4 = "Mum's house";
            const string newAddressType4 = "Home";

            //will be added
            const string newAddress5 = "abc";
            const string newAddressType5 = "Yet another address";

            //will be added
            const string newAddress6 = "xyz";
            const string newAddressType6 = "Home";


            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editPerson = new UserManuallyNavigates(Sandbox).ToEditPerson(1);

            editPerson.SetName(name)
                      .SetNotes(notes);

            //Email channels

            //delete a default email
            editPerson.EmailChannels
                      .Find(c => c.Value == oldEmail1 && c.TypeAsString == oldEmailType1)
                      .Remove();

            //switch a custom email to a default email
            editPerson.EmailChannels
                      .Find(c => c.Value == oldEmail2 && c.TypeAsString == oldEmailType2)
                      .SetType(newEmailType2)
                      .SetValue(newEmail2);

            //delete a custom email
            editPerson.EmailChannels
                      .Find(c => c.Value == oldEmail3 && c.TypeAsString == oldEmailType3)
                      .Remove();

            //switch a default email to a custom email
            editPerson.EmailChannels
                      .Find(c => c.Value == oldEmail4 && c.TypeAsString == oldEmailType4)
                      .SetType(newEmailType4);

            //add a custom email
            editPerson.EmailChannels.Count().ShouldBe(2);
            var emailChannel = editPerson.AddEmail().EmailChannels[2];
            emailChannel.SetType(newEmailType5).SetValue(newEmail5);

            //add a default email
            editPerson.EmailChannels.Count().ShouldBe(3);
            emailChannel = editPerson.AddEmail().EmailChannels[3];
            emailChannel.SetType(newEmailType6).SetValue(newEmail6);


            //Phone channels

            //delete a default phone
            editPerson.PhoneChannels
                      .Find(c => c.Value == oldPhone1 && c.TypeAsString == oldPhoneType1)
                      .Remove();

            //switch a custom phone to a default phone
            editPerson.PhoneChannels
                      .Find(c => c.Value == oldPhone2 && c.TypeAsString == oldPhoneType2)
                      .SetType(newPhoneType2)
                      .SetValue(newPhone2);

            //delete a custom phone
            editPerson.PhoneChannels
                      .Find(c => c.Value == oldPhone3 && c.TypeAsString == oldPhoneType3)
                      .Remove();

            //switch a default phone to a custom phone
            editPerson.PhoneChannels
                      .Find(c => c.Value == oldPhone4 && c.TypeAsString == oldPhoneType4)
                      .SetType(newPhoneType4);

            //add a custom phone
            editPerson.PhoneChannels.Count().ShouldBe(3);
            var phoneChannel = editPerson.AddPhone().PhoneChannels[3];
            phoneChannel.SetType(newPhoneType5).SetValue(newPhone5);

            //add a default phone
            editPerson.PhoneChannels.Count().ShouldBe(4);
            phoneChannel = editPerson.AddPhone().PhoneChannels[4];
            phoneChannel.SetType(newPhoneType6).SetValue(newPhone6);


            //Address channels

            //delete a default address
            editPerson.AddressChannels
                      .Find(c => c.Value == oldAddress1 && c.TypeAsString == oldAddressType1)
                      .Remove();

            //switch a custom address to a default address
            editPerson.AddressChannels
                      .Find(c => c.Value == oldAddress2 && c.TypeAsString == oldAddressType2)
                      .SetType(newAddressType2)
                      .SetValue(newAddress2);

            //delete a custom address
            editPerson.AddressChannels
                      .Find(c => c.Value == oldAddress3 && c.TypeAsString == oldAddressType3)
                      .Remove();

            //switch a default address to a custom address
            editPerson.AddressChannels
                      .Find(c => c.Value == oldAddress4 && c.TypeAsString == oldAddressType4)
                      .SetType(newAddressType4);

            //add a custom address
            editPerson.AddressChannels.Count().ShouldBe(2);
            var addressChannel = editPerson.AddAddress().AddressChannels[2];
            addressChannel.SetType(newAddressType5).SetValue(newAddress5);

            //add a default address
            editPerson.AddressChannels.Count().ShouldBe(3);
            addressChannel = editPerson.AddAddress().AddressChannels[3];
            addressChannel.SetType(newAddressType6).SetValue(newAddress6);

            editPerson.Save();

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            editPerson = userManuallyNavigates.ToEditPerson(1);
            editPerson.Name.ShouldBe(name);
            editPerson.Notes.ShouldBe(notes);

            var emailChannels = editPerson.EmailChannels;
            emailChannels.Count.ShouldBe(4);
            emailChannels.Any(c => c.Value == newEmail2 && c.TypeAsString == newEmailType2).ShouldBe(true);
            emailChannels.Any(c => c.Value == oldEmail4 && c.TypeAsString == newEmailType4).ShouldBe(true);
            emailChannels.Any(c => c.Value == newEmail5 && c.TypeAsString == newEmailType5).ShouldBe(true);
            emailChannels.Any(c => c.Value == newEmail6 && c.TypeAsString == newEmailType6).ShouldBe(true);

            var phoneChannels = editPerson.PhoneChannels;
            phoneChannels.Count.ShouldBe(5);
            phoneChannels.Any(c => c.Value == newPhone2 && c.TypeAsString == newPhoneType2).ShouldBe(true);
            phoneChannels.Any(c => c.Value == oldPhone4 && c.TypeAsString == newPhoneType4).ShouldBe(true);
            phoneChannels.Any(c => c.Value == newPhone5 && c.TypeAsString == newPhoneType5).ShouldBe(true);
            phoneChannels.Any(c => c.Value == newPhone6 && c.TypeAsString == newPhoneType6).ShouldBe(true);
            phoneChannels.Any(c => c.Value == oldPhone7 && c.TypeAsString == oldPhoneType7).ShouldBe(true);

            var addressChannels = editPerson.AddressChannels;
            addressChannels.Count.ShouldBe(4);
            addressChannels.Any(c => c.Value == newAddress2 && c.TypeAsString == newAddressType2).ShouldBe(true);
            addressChannels.Any(c => c.Value == oldAddress4 && c.TypeAsString == newAddressType4).ShouldBe(true);
            addressChannels.Any(c => c.Value == newAddress5 && c.TypeAsString == newAddressType5).ShouldBe(true);
            addressChannels.Any(c => c.Value == newAddress6 && c.TypeAsString == newAddressType6).ShouldBe(true);
        
        }

        [Test]
        public void WhenEditingAnExistingPersonOnSomeoneElsesAccount_IAmToldThePersonIsNotFound()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editPerson = new UserManuallyNavigates(Sandbox).ToEditPerson(4);
            editPerson.Summary.ShouldBe("Sorry, we could not find this person.");
        }

        [Test]
        public void WhenEditingAnExistingPersonWhichDoesNotExist_IAmToldThePersonIsNotFound()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editPerson = new UserManuallyNavigates(Sandbox).ToEditPerson(9999);
            editPerson.Summary.ShouldBe("Sorry, we could not find this person.");
        }

        [Test]
        public void WhenICancelEditsOnANewPerson_ImTakenToThePersonIndex()
        {
            const string name = "Dave Person";
            const string notes = "These are some notes";
            const string email1 = "dave@binarymash.com";
            const string phone1 = "+4412345123456";
            const string address1 = "address 1";

            var user = UserBuilder.SystemAdmin.Create();

            var people = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.People;

            var originalNumberOfPeople = people.NumberOfPeople;

            var editPerson = people.AddANewPerson;

            editPerson.SetName(name)
                      .SetNotes(notes);

            editPerson.AddEmail();
            var emailChannels = editPerson.EmailChannels;
            emailChannels[0].SetValue(email1);
            emailChannels[0].SetType(EmailType.Other);

            editPerson.AddPhone();
            var phoneChannels = editPerson.PhoneChannels;
            phoneChannels[0].SetValue(phone1);
            phoneChannels[0].SetType("My custom phone");

            editPerson.AddAddress();
            var addressChannels = editPerson.AddressChannels;
            addressChannels[0].SetValue(address1);
            addressChannels[0].SetType(AddressType.Home);

            people = editPerson.CancelExpectingIndex();
            people.NumberOfPeople.ShouldBe(originalNumberOfPeople);
        }

        [Test]
        public void WhenICancelEditsOnAnExistingPerson_ImTakenToTheSummaryPageAndNothingHasChanged()
        {
            const string oldName = "Kiki Nortey";
            const string newName = "This is my new name";
            const string oldNotes = "";
            const string newNotes = "These are my new notes";

            //Email data

            //will be deleted
            const string oldEmail1 = "kiki.nortey@binarymash.com";
            const string oldEmailType1 = "Home";

            //will be changed
            const string oldEmail2 = "testing@binarymash.com";
            const string newEmail2 = "testing_newEmail2@binarymash.com";
            const string oldEmailType2 = "testing";
            const string newEmailType2 = "Work";

            //will be deleted
            const string oldEmail3 = "testing2@binarymash.com";
            const string oldEmailType3 = "testing2";

            //type will be changed
            const string oldEmail4 = "testing3@binarymash.com";
            const string oldEmailType4 = "testing3";
            const string newEmailType4 = "Home";

            //will be added
            const string newEmail5 = "testing4@binarymash.com";
            const string newEmailType5 = "testing4";

            //will be added
            const string newEmail6 = "testing5@binarymash.com";
            const string newEmailType6 = "Home";


            //PhoneChannel data

            //will be deleted
            const string oldPhone1 = "+441234567890";
            const string oldPhoneType1 = "Mobile";

            //will be changed
            const string oldPhone2 = "+4456789012";
            const string newPhone2 = "+4456789099";
            const string oldPhoneType2 = "My number 1";
            const string newPhoneType2 = "My number 1 modified";

            //will be deleted
            const string oldPhone3 = "+44545566665";
            const string oldPhoneType3 = "My number 2";

            //type will be changed
            const string oldPhone4 = "00000000000";
            const string oldPhoneType4 = "My number 3";
            const string newPhoneType4 = "Work";

            //will be added
            const string newPhone5 = "22222222222";
            const string newPhoneType5 = "testing4";

            //will be added
            const string newPhone6 = "33333333333";
            const string newPhoneType6 = "Home";

            //will be unchanged
            const string oldPhone7 = "+443344556677";
            const string oldPhoneType7 = "Home";


            //AddressChannel data

            //will be deleted
            const string oldAddress1 = "1 Some Place, Any Street, Any Town";
            const string oldAddressType1 = "Home";

            //will be changed
            const string oldAddress2 = "abcdefgh";
            const string newAddress2 = "My new address";
            const string oldAddressType2 = "Somewhere";
            const string newAddressType2 = "Work";

            //will be deleted
            const string oldAddress3 = "gghhiijj";
            const string oldAddressType3 = "Somewhere else";

            //type will be changed
            const string oldAddress4 = "aa bb cc dd";
            const string oldAddressType4 = "Mum's house";
            const string newAddressType4 = "Home";

            //will be added
            const string newAddress5 = "abc";
            const string newAddressType5 = "Yet another address";

            //will be added
            const string newAddress6 = "xyz";
            const string newAddressType6 = "Home";


            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass");

            var editPerson = new UserManuallyNavigates(Sandbox).ToEditPerson(1);


            //check that we start with correct data
            editPerson.Name.ShouldBe(oldName);
            editPerson.Notes.ShouldBe(oldNotes);

            var emailChannels = editPerson.EmailChannels;
            emailChannels.Count().ShouldBe(4);
            emailChannels.Any(c => c.Value == oldEmail1 && c.TypeAsString == oldEmailType1).ShouldBe(true);
            emailChannels.Any(c => c.Value == oldEmail2 && c.TypeAsString == oldEmailType2).ShouldBe(true);
            emailChannels.Any(c => c.Value == oldEmail3 && c.TypeAsString == oldEmailType3).ShouldBe(true);
            emailChannels.Any(c => c.Value == oldEmail4 && c.TypeAsString == oldEmailType4).ShouldBe(true);

            var phoneChannels = editPerson.PhoneChannels;
            phoneChannels.Count().ShouldBe(5);
            phoneChannels.Any(c => c.Value == oldPhone1 && c.TypeAsString == oldPhoneType1).ShouldBe(true);
            phoneChannels.Any(c => c.Value == oldPhone2 && c.TypeAsString == oldPhoneType2).ShouldBe(true);
            phoneChannels.Any(c => c.Value == oldPhone3 && c.TypeAsString == oldPhoneType3).ShouldBe(true);
            phoneChannels.Any(c => c.Value == oldPhone4 && c.TypeAsString == oldPhoneType4).ShouldBe(true);
            phoneChannels.Any(c => c.Value == oldPhone7 && c.TypeAsString == oldPhoneType7).ShouldBe(true);

            var addressChannels = editPerson.AddressChannels;
            addressChannels.Count().ShouldBe(4);
            addressChannels.Any(c => c.Value == oldAddress1 && c.TypeAsString == oldAddressType1).ShouldBe(true);
            addressChannels.Any(c => c.Value == oldAddress2 && c.TypeAsString == oldAddressType2).ShouldBe(true);
            addressChannels.Any(c => c.Value == oldAddress3 && c.TypeAsString == oldAddressType3).ShouldBe(true);
            addressChannels.Any(c => c.Value == oldAddress4 && c.TypeAsString == oldAddressType4).ShouldBe(true);

            editPerson.SetName(newName)
                      .SetNotes(newNotes);

            //Email channels

            //delete a default email
            editPerson.EmailChannels
                      .Find(c => c.Value == oldEmail1 && c.TypeAsString == oldEmailType1)
                      .Remove();

            //switch a custom email to a default email
            editPerson.EmailChannels
                      .Find(c => c.Value == oldEmail2 && c.TypeAsString == oldEmailType2)
                      .SetType(newEmailType2)
                      .SetValue(newEmail2);

            //delete a custom email
            editPerson.EmailChannels
                      .Find(c => c.Value == oldEmail3 && c.TypeAsString == oldEmailType3)
                      .Remove();

            //switch a default email to a custom email
            editPerson.EmailChannels
                      .Find(c => c.Value == oldEmail4 && c.TypeAsString == oldEmailType4)
                      .SetType(newEmailType4);

            //add a custom email
            editPerson.EmailChannels.Count().ShouldBe(2);
            var emailChannel = editPerson.AddEmail().EmailChannels[2];
            emailChannel.SetType(newEmailType5).SetValue(newEmail5);

            //add a default email
            editPerson.EmailChannels.Count().ShouldBe(3);
            emailChannel = editPerson.AddEmail().EmailChannels[3];
            emailChannel.SetType(newEmailType6).SetValue(newEmail6);


            //Phone channels

            //delete a default phone
            editPerson.PhoneChannels
                      .Find(c => c.Value == oldPhone1 && c.TypeAsString == oldPhoneType1)
                      .Remove();

            //switch a custom phone to a default phone
            editPerson.PhoneChannels
                      .Find(c => c.Value == oldPhone2 && c.TypeAsString == oldPhoneType2)
                      .SetType(newPhoneType2)
                      .SetValue(newPhone2);

            //delete a custom phone
            editPerson.PhoneChannels
                      .Find(c => c.Value == oldPhone3 && c.TypeAsString == oldPhoneType3)
                      .Remove();

            //switch a default phone to a custom phone
            editPerson.PhoneChannels
                      .Find(c => c.Value == oldPhone4 && c.TypeAsString == oldPhoneType4)
                      .SetType(newPhoneType4);

            //add a custom phone
            editPerson.PhoneChannels.Count().ShouldBe(3);
            var phoneChannel = editPerson.AddPhone().PhoneChannels[3];
            phoneChannel.SetType(newPhoneType5).SetValue(newPhone5);

            //add a default phone
            editPerson.PhoneChannels.Count().ShouldBe(4);
            phoneChannel = editPerson.AddPhone().PhoneChannels[4];
            phoneChannel.SetType(newPhoneType6).SetValue(newPhone6);


            //Address channels

            //delete a default address
            editPerson.AddressChannels
                      .Find(c => c.Value == oldAddress1 && c.TypeAsString == oldAddressType1)
                      .Remove();

            //switch a custom address to a default address
            editPerson.AddressChannels
                      .Find(c => c.Value == oldAddress2 && c.TypeAsString == oldAddressType2)
                      .SetType(newAddressType2)
                      .SetValue(newAddress2);

            //delete a custom address
            editPerson.AddressChannels
                      .Find(c => c.Value == oldAddress3 && c.TypeAsString == oldAddressType3)
                      .Remove();

            //switch a default address to a custom address
            editPerson.AddressChannels
                      .Find(c => c.Value == oldAddress4 && c.TypeAsString == oldAddressType4)
                      .SetType(newAddressType4);

            //add a custom address
            editPerson.AddressChannels.Count().ShouldBe(2);
            var addressChannel = editPerson.AddAddress().AddressChannels[2];
            addressChannel.SetType(newAddressType5).SetValue(newAddress5);

            //add a default address
            editPerson.AddressChannels.Count().ShouldBe(3);
            addressChannel = editPerson.AddAddress().AddressChannels[3];
            addressChannel.SetType(newAddressType6).SetValue(newAddress6);

            var summaryPage = editPerson.CancelExpectingSummary();

            //check that we're back to what we started with
            summaryPage.Name.ShouldBe(oldName);
        }

        [Test]
        public void WhenCreatingANewPerson_ICanSaveAndReEditAndSaveAgain()
        {
            const string name = "Dave Person";
            const string notes = "These are some notes";
            const string email1 = "dave@binarymash.com";
            const string emailType1 = "Personal";
            const string email2 = "another.new.email@binarymash.com";
            const string emailType2 = "Work";
            const string phone1 = "+4412345123456";
            const string phoneType1 = "Mum's House";
            const string phone2 = "+4412345111111";
            const string phoneType2 = "Home";
            const string address1 = "address 1";
            const string addressType1 = "Home";
            const string address2 = "address 2";
            const string addressType2 = "Mum's House";

            var user = UserBuilder.SystemAdmin.Create();

            var editPersonPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.People
                .AddANewPerson;

            editPersonPage.SetName(name)
                      .SetNotes(notes);

            editPersonPage.AddEmail();
            var emailChannels = editPersonPage.EmailChannels;
            emailChannels[0].SetValue(email1)
                .SetType(emailType1);

            editPersonPage.AddPhone();
            var phoneChannels = editPersonPage.PhoneChannels;
            phoneChannels[0].SetValue(phone1)
                .SetType(phoneType1);

            editPersonPage.AddAddress();
            var addressChannels = editPersonPage.AddressChannels;
            addressChannels[0].SetValue(address1)
                              .SetType(addressType1);

            editPersonPage.Save();

            editPersonPage.AddEmail();
            emailChannels = editPersonPage.EmailChannels;
            emailChannels[1].SetValue(email2)
                            .SetType(emailType2);

            editPersonPage.AddPhone();
            phoneChannels = editPersonPage.PhoneChannels;
            phoneChannels[1].SetValue(phone2)
                            .SetType(phoneType2);

            editPersonPage.AddAddress();
            addressChannels = editPersonPage.AddressChannels;
            addressChannels[1].SetValue(address2)
                              .SetType(addressType2);

            var summaryPage = editPersonPage.Save().Done();
            summaryPage.Name.ShouldBe(name);
            //TODO: other fields once they've been added to the summary page

            editPersonPage = summaryPage.EditDetails;                       
            editPersonPage.Name.ShouldBe(name);
            editPersonPage.Notes.ShouldBe(notes);

            emailChannels = editPersonPage.EmailChannels;
            emailChannels.Count.ShouldBe(2);
            emailChannels.Any(c => (c.Value == email1 && c.TypeAsString == emailType1)).ShouldBe(true);
            emailChannels.Any(c => (c.Value == email2 && c.TypeAsString == emailType2)).ShouldBe(true);

            phoneChannels = editPersonPage.PhoneChannels;
            phoneChannels.Count.ShouldBe(2);
            phoneChannels.Any(c => (c.Value == phone1 && c.TypeAsString == phoneType1)).ShouldBe(true);
            phoneChannels.Any(c => c.Value == phone2 && c.TypeAsString == phoneType2).ShouldBe(true);

            addressChannels = editPersonPage.AddressChannels;
            addressChannels.Count.ShouldBe(2);
            addressChannels.Any(c => (c.Value == address1 && c.TypeAsString == addressType1)).ShouldBe(true);
            addressChannels.Any(c => (c.Value == address2 && c.TypeAsString == addressType2)).ShouldBe(true);
        }

    }
}
