﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using Data;
    using NUnit.Framework;
    using Pages.Web;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    public class ChangeUserEmailAddress : SandboxedTest
    {
        [Test]
        public void WhenAuthenticated_ICanChangeMyEmail()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";
            const string newEmail = "mynewaddress@binarymash.com";

            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, password);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var changeEmailPage = userManuallyNavigates.ToUserSummary(user.Id).ChangeEmail;
            var userSummaryPage = changeEmailPage.SetEmail(newEmail).Done;
            userSummaryPage.Email.ShouldBe(newEmail);
            var homePage = userSummaryPage.LogOut();

            //can't log in with original email
            var loginPage = homePage.LogIn().LogInAsExpectingError(user.Email, password);
            var errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);

            //can log in with new email
            userHomePage = loginPage.LogInAs(newEmail, password);
            errorMessages = userHomePage.ErrorMessages;
            errorMessages.Count.ShouldBe(0);
        }

        [Test]
        public void WhenICancelAnEmailAddressChange_TheAddressIsNotChanged()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";
            const string newEmail = "mynewaddress@binarymash.com";

            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, password);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var changeEmailPage = userManuallyNavigates.ToUserSummary(user.Id).ChangeEmail;
            var userSummaryPage = changeEmailPage.SetEmail(newEmail).Cancel;
            userSummaryPage.Email.ShouldBe(user.Email);
            var homePage = userSummaryPage.LogOut();

            //can't log in with new email
            var loginPage = homePage.LogIn().LogInAsExpectingError(newEmail, password);
            var errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);

            //can log in with original email
            userHomePage = loginPage.LogInAs(user.Email, password);
            errorMessages = userHomePage.ErrorMessages;
            errorMessages.Count.ShouldBe(0);            
        }

        [Test]
        public void WhenITryToChangeToInvalidEmailAddresses_IGetErrors()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, password);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var userSummaryPage = userManuallyNavigates.ToUserSummary(user.Id);                       

            //empty
            var changeEmailPage = userSummaryPage.ChangeEmail.SetEmail("").DoneExpectingErrors;
            changeEmailPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);
            var errorMessages = changeEmailPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("'Email' is required"); //TODO: better message

            //not an email
            changeEmailPage = changeEmailPage.SetEmail("abc").DoneExpectingErrors;
            changeEmailPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);
            errorMessages = changeEmailPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("Must be a valid email address");

            //too long (we have 64 character limit. This is 65)
            changeEmailPage = changeEmailPage.SetEmail("p234567890123456789012345678901234567890123456789@d2345678901.com").DoneExpectingErrors;
            changeEmailPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);
            errorMessages = changeEmailPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("'Email' must be a string with 64 characters or less"); //TODO: better message

            //already exists
            var user2 = UserBuilder.DaveSmith.Create();
            changeEmailPage = changeEmailPage.SetEmail(user2.Email).DoneExpectingErrors;
            changeEmailPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);
            errorMessages = changeEmailPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("This email address is not available");

            userSummaryPage = changeEmailPage.Cancel;
            userSummaryPage.Email.ShouldBe(user.Email);
            

            //can still log in with original email
            userHomePage = userSummaryPage.LogOut()
                                          .LogIn()
                                          .LogInAs(user.Email, password);
            errorMessages = userHomePage.ErrorMessages;
            errorMessages.Count.ShouldBe(0);
        }

        [Test]
        [Ignore("TODO: implement")]
        public void WhenAuthenticated_ICannotChangeAnotherAccountsEmail()
        {
        }

        [Test]
        public void WhenTakingATestSession_ICannotChangeEmailAddresses()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, password);

            var sessions = userHomePage.Menu.Sessions;
            var session = sessions.Sessions[0];
            var sessionSummary = session.ViewSummary();
            var confirmationDialog = sessionSummary.TakeTheTestNowOnThisDevice;
            var candidateWelcomePage = confirmationDialog.ConfirmToTakeTheTest;
            candidateWelcomePage.Message.ShouldBe("Hello, Kiki Nortey");

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            candidateWelcomePage = userManuallyNavigates.ToChangeUserEmailAddressExpectingCandidateWelcome(user.Id);
            candidateWelcomePage.Message.ShouldBe("Hello, Kiki Nortey");        
        }

        [Test]
        public void WhenUnauthenticated_IAmRedirectedToLogin()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var loginPage = userManuallyNavigates.ToChangeUserEmailAddressExpectingUnauthenticated(user.Id);
            loginPage.WaitForPageWithTitle("Login");
            
            loginPage.LogInAs(user.Email, password);

            var changeEmailPage = userManuallyNavigates.ToChangeUserEmailAddress(user.Id);
            changeEmailPage.WaitForPageWithTitle("Change Email");
        }
    }
}
