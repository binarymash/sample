﻿namespace BinaryMash.Testing.Webdriver.Tests.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    public static class MarkdownXssSamples
    {
        public static ICollection<string> AllXssStrings
        {
            get
            {
                var collection = new Collection<string>();

                var propertyInfos = typeof(MarkdownXssSamples).GetProperties().Where(p => Attribute.IsDefined(p, typeof(XssStringAttribute)));
                foreach (var propertyInfo in propertyInfos)
                {
                    var s = propertyInfo.GetValue(null, null) as string;
                    if (!string.IsNullOrEmpty(s))
                    {
                        collection.Add(s);
                    }
                }

                return collection;
            }
        }

        //http://michelf.ca/blog/2010/markdown-and-xss/
        [XssString]
        public static string HrefContainingJavascript
        {
            get
            {
                return
                    "> hello <a name=\"n\"" + System.Environment.NewLine +
                    "> href=\"javascript:alert('xss')\">*you*</a>";
            }
        }

        [XssString]
        public static string LinkContainingJavascript1
        {
            get { return "[some text](javascript:alert('xss'))"; }
        }

        [XssString]
        public static string LinkContainingJavascript2
        {
            get { return "[Example](javascript://alert%28%22xss%22%29)"; }
        }
    }
}
