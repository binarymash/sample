﻿namespace BinaryMash.Testing.Webdriver.Tests.Utilities
{
    using NUnit.Framework;
    using Pages.Utilities;
    using Shouldly;

    [TestFixture]
    public class TestHrefUrlExtractor
    {
        [Test]
        public void Test()
        {
            var url = HrefUrlExtractor.From("this is some text <a something href=\"http://localhost/blah\" blah>Test</a>");
            url.ShouldBe("http://localhost/blah");
        }

        [Test]
        public void TestWithUtf8()
        {
            var url = HrefUrlExtractor.From("this is some text <a something href=3D\"http://localhost/blah\" blah>Test</a>");
            url.ShouldBe("http://localhost/blah");
        }

        [Test]
        public void NoLinkReturnsNull()
        {
            var url = HrefUrlExtractor.From("this is some text!");
            url.ShouldBe(null);
        }
    
    }
}
