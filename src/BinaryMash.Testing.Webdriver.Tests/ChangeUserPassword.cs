﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using Data;
    using NUnit.Framework;
    using Pages.Models;
    using Pages.Web;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    public class ChangeUserPassword : SandboxedTest
    {
        [Test]
        public void WhenAuthenticated_ICanChangeMyPassword()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";
            const string newPassword = "abcdefgh123";

            var userSummaryPage = new IndexPage(Sandbox).LogIn()
                                  .LogInAs(user.Email, password)
                                  .Menu.MyAccount;

            var changePasswordPage = userSummaryPage.ChangePassword;
            userSummaryPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password,NewPassword = newPassword, NewPasswordConfirmation = newPassword
                                                    })
                                                .Done;

            //cannot log in with the new password
            var loginPage = userSummaryPage.LogOut().LogIn().LogInAsExpectingError(user.Email, password);
            var errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("The email address or password was incorrect. Please check your details and try again.");

            //can log in with the old password
            var userHomePage = loginPage.LogInAs(user.Email, newPassword);
            userHomePage.ErrorMessages.Count.ShouldBe(0);
        }

        [Test]
        public void WhenICancel_ThePasswordIsNotChanged()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";
            const string newPassword = "abcdefgh123";

            var userSummaryPage = new IndexPage(Sandbox).LogIn()
                                  .LogInAs(user.Email, password)
                                  .Menu.MyAccount;

            var changePasswordPage = userSummaryPage.ChangePassword;
            changePasswordPage.ErrorMessages.Count.ShouldBe(0);

            userSummaryPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password,
                                                        NewPassword = newPassword,
                                                        NewPasswordConfirmation = newPassword
                                                    })
                                                .Cancel;

            //cannot log in with the new password
            var loginPage = userSummaryPage.LogOut().LogIn().LogInAsExpectingError(user.Email, newPassword);
            var errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("The email address or password was incorrect. Please check your details and try again.");

            //can log in with the old password
            var userHomePage = loginPage.LogInAs(user.Email, password);
            userHomePage.ErrorMessages.Count.ShouldBe(0);
        }

        [Test]
        public void WhenIGetTheCurrentPasswordWrong_ThePasswordIsNotChanged()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";
            const string newPassword = "abcdefgh123";

            var userSummaryPage = new IndexPage(Sandbox).LogIn()
                                  .LogInAs(user.Email, password)
                                  .Menu.MyAccount;

            var changePasswordPage = userSummaryPage.ChangePassword;
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password+"a",
                                                        NewPassword = newPassword,
                                                        NewPasswordConfirmation = newPassword
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            var errorMessages = changePasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("The current password is incorrect");

            //cannot log in with the new password
            var loginPage = changePasswordPage.Cancel
                                              .LogOut()
                                              .LogIn()
                                              .LogInAsExpectingError(user.Email, newPassword);

            errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("The email address or password was incorrect. Please check your details and try again.");

            //can log in with the old password
            var userHomePage = loginPage.LogInAs(user.Email, password);
            userHomePage.ErrorMessages.Count.ShouldBe(0);
        }

        [Test]
        [Ignore("TODO: implement")]
        public void WhenAuthenticated_ICannotChangeAnotherAccountsEmail()
        {
        }

        [Test]
        public void WhenITryToChangePasswordWithInvalidData_IGetErrors()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";
            const string newPassword = "abcdefgh123";

            var userSummaryPage = new IndexPage(Sandbox).LogIn()
                                  .LogInAs(user.Email, password)
                                  .Menu.MyAccount;

            var changePasswordPage = userSummaryPage.ChangePassword;

            //blank currentPassword
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = "",
                                                        NewPassword = newPassword,
                                                        NewPasswordConfirmation = newPassword
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            var errors = changePasswordPage.ErrorMessages;
            errors.Count.ShouldBe(1);
            errors.ShouldContain("Current password is required");

            //blank newPassword
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password,
                                                        NewPassword = "",
                                                        NewPasswordConfirmation = newPassword
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            errors = changePasswordPage.ErrorMessages;
            errors.Count.ShouldBe(2);
            errors.ShouldContain("New password is required");
            errors.ShouldContain("New password confirmation must match the new password");

            //blank newPasswordConfirmation
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password,
                                                        NewPassword = newPassword,
                                                        NewPasswordConfirmation = ""
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            errors = changePasswordPage.ErrorMessages;
            errors.Count.ShouldBe(1);
            errors.ShouldContain("New password confirmation is required");

            //blank newPassword and newPasswordConfirmation
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password,
                                                        NewPassword = "",
                                                        NewPasswordConfirmation = ""
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            errors = changePasswordPage.ErrorMessages;
            errors.Count.ShouldBe(2);
            errors.ShouldContain("New password is required");
            errors.ShouldContain("New password confirmation is required");

            //blank everything
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = "",
                                                        NewPassword = "",
                                                        NewPasswordConfirmation = ""
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            errors = changePasswordPage.ErrorMessages;
            errors.Count.ShouldBe(3);
            errors.ShouldContain("Current password is required");
            errors.ShouldContain("New password is required");
            errors.ShouldContain("New password confirmation is required");

            //newPassword too short
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password,
                                                        NewPassword = "12345678",
                                                        NewPasswordConfirmation = "12345678"
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            errors = changePasswordPage.ErrorMessages;
            errors.Count.ShouldBe(1);
            errors.ShouldContain("Your new password must be at least 9 characters, and at most 32 characters, in length");

            //newPassword too long
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password,
                                                        NewPassword = "123456789012345678901234567890123",
                                                        NewPasswordConfirmation = "123456789012345678901234567890123"
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            errors = changePasswordPage.ErrorMessages;
            errors.Count.ShouldBe(1);
            errors.ShouldContain("Your new password must be at least 9 characters, and at most 32 characters, in length");

            //password confirmation mismatch
            changePasswordPage = changePasswordPage.SetData(
                                                    new ChangeUserPasswordPageModel
                                                    {
                                                        CurrentPassword = password,
                                                        NewPassword = newPassword,
                                                        NewPasswordConfirmation = newPassword+"a"
                                                    })
                                                .DoneExpectingErrors;

            changePasswordPage.UserHasBeenNotifiedThatThereAreValidationErrorsAndTheChangeWasNotSaved.ShouldBe(true);
            errors = changePasswordPage.ErrorMessages;
            errors.Count.ShouldBe(1);
            errors.ShouldContain("New password confirmation must match the new password");

            //cannot still log in with original password
            var loginPage = changePasswordPage.Cancel
                                              .LogOut()
                                              .LogIn()
                                              .LogInAsExpectingError(user.Email, newPassword);

            var errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("The email address or password was incorrect. Please check your details and try again.");

            //can log in with the old password
            var userHomePage = loginPage.LogInAs(user.Email, password);
            userHomePage.ErrorMessages.Count.ShouldBe(0);
        }

        [Test]
        public void WhenTakingATestSessionAndBrowsingToChangeUserPasswordPage_IAmRedirectedToCandidateWelcome()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, password);

            var sessions = userHomePage.Menu.Sessions;
            var session = sessions.Sessions[0];
            var sessionSummary = session.ViewSummary();
            var confirmationDialog = sessionSummary.TakeTheTestNowOnThisDevice;
            var candidateWelcomePage = confirmationDialog.ConfirmToTakeTheTest;
            candidateWelcomePage.Message.ShouldBe("Hello, Kiki Nortey");

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            candidateWelcomePage = userManuallyNavigates.ToChangeUserPasswordExpectingCandidateWelcome();
            candidateWelcomePage.Message.ShouldBe("Hello, Kiki Nortey");
        }

        [Test]
        public void WhenUnauthenticated_IAmRedirectedToLogin()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var loginPage = userManuallyNavigates.ToChangeUserPasswordExpectingUnauthenticated();
            loginPage.WaitForPageWithTitle("Login");

            loginPage.LogInAs(user.Email, password);

            var changeEmailPage = userManuallyNavigates.ToChangeUserPassword();
            changeEmailPage.WaitForPageWithTitle("Change Password");
        }

    }
}
