﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using System;
    using Data;
    using NUnit.Framework;
    using Pages.Models;
    using Pages.Web;
    using Pages.Web.Account;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    public class ResetPassword : SandboxedTest
    {
        [Test]
        public void WhenIHaveForgottenMyPassword_ICanResetItAndLogIn()
        {
            var user = UserBuilder.DaveSmith.Create();
            const string oldPassword = "passpasspass";
            const string newPassword1 = "thisismynewpassword";
            const string newPassword2 = "thisismynewpassword2";
            
            //can log in with old password
            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, oldPassword);

            userHomePage.ErrorMessages.Count.ShouldBe(0);
            
            var homePage = userHomePage.LogOut();

            //can't log in yet with new password
            var loginPage = homePage.LogIn()
                                    .LogInAsExpectingError(user.Email, newPassword1);

            loginPage.ErrorMessages.Count.ShouldBe(1);

            //request reset
            var resettingPasswordPage = loginPage.ForgottenPassword
                                                 .Reset(user.Email);

            resettingPasswordPage.Message.ShouldBe("An email has been sent to dave.smith@binarymash.com; please follow the instructions in this email to confirm the password reset. You must act within the next 10 minutes to confirm the reset. After this time, the reset request will expire.");

            var resetPasswordEmail = resettingPasswordPage.ReadResetPasswordEmail;
            resetPasswordEmail.Message.From.Address.ShouldBe("accounts@binarymash.com");
            resetPasswordEmail.Message.To.Count.ShouldBe(1);
            resetPasswordEmail.Message.To[0].Address.ShouldBe(user.Email);

            var resetPasswordPage = resetPasswordEmail.ClickLink();
            resetPasswordEmail.Message.Dispose();

            var resetUrl = Sandbox.Browser.Session.Location;

            //reset password
            var confirmationOfPasswordResetPage = resetPasswordPage.Reset(
                new ResetPasswordPageModel
                {
                    Email = user.Email,
                    Password = newPassword1,
                    PasswordConfirmation = newPassword1
                });

            confirmationOfPasswordResetPage.Message.ShouldBe("Your password has been reset.");

            //login with new password
            var accountPage = confirmationOfPasswordResetPage.LogIn
                                                             .LogInAs(user.Email, newPassword1);

            accountPage.ErrorMessages.Count.ShouldBe(0);
            homePage = accountPage.LogOut();

            //can't log in with old password
            loginPage = homePage.LogIn().LogInAsExpectingError(user.Email, oldPassword);
            loginPage.ErrorMessages.Count.ShouldBe(1);

            //cannot reset again on the same url
            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            userManuallyNavigates.To(resetUrl);
            resetPasswordPage = new ResetPasswordPage(Sandbox);
            resetPasswordPage.ResetExpectingErrors(
                new ResetPasswordPageModel
                {
                    Email = user.Email,
                    Password = newPassword2,
                    PasswordConfirmation = newPassword2
                });

            var errorMessages = resettingPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("Could not reset the password for this email address. Please check your details and try again.");

            //can't log in with second new password
            loginPage = new Pages.Web.Authentication.IndexPage(Sandbox);
            loginPage = loginPage.LogInAsExpectingError(user.Email, newPassword2);
            loginPage.ErrorMessages.Count.ShouldBe(1);

            //login with first new password
            loginPage.LogInAs(user.Email, newPassword1);
            loginPage.ErrorMessages.Count.ShouldBe(0);

        
        }

        [Test]
        public void WhenIHaveForgottenMyPasswordAndDontSpecifyAValidEmailFormat_IGetAnError()
        {
            //request reset
            var forgottenPassword = new IndexPage(Sandbox).LogIn()
                                                          .ForgottenPassword;

            //empty email
            forgottenPassword = forgottenPassword.ResetExpectingErrors("");
            var errorMessages = forgottenPassword.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages[0].ShouldBe("Email address is required");

            //invalid format
            forgottenPassword = forgottenPassword.ResetExpectingErrors("abc");
            errorMessages = forgottenPassword.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages[0].ShouldBe("Must be a valid email address");
        }

        [Test]
        public void WhenISpecifyANonExistentEmailWhenRequestingAPasswordReset_IGetAnError()
        {
            var user = UserBuilder.NonExistentUser.Create();

            //request reset
            var resettingPasswordPage = new IndexPage(Sandbox).LogIn()
                                                              .ForgottenPassword
                                                              .ResetExpectingErrors(user.Email);

            var errorMessages = resettingPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages[0].ShouldBe("We could not reset the password for this email address. Please check your details and try again.");
        }

        [Test]
        public void WhenIAttemptConcurrentResetsOnTheSameEmail_BothCanBeActedOn()
        {
            var user = UserBuilder.DaveSmith.Create();
            const string newPassword1 = "thisismynewpassword1";
            const string newPassword2 = "thisismynewpassword2";

            //can't log in yet with new password
            var loginPage = new IndexPage(Sandbox).LogIn()
                                    .LogInAsExpectingError(user.Email, newPassword1);

            loginPage.ErrorMessages.Count.ShouldBe(1);

            loginPage = new IndexPage(Sandbox).LogIn()
                                    .LogInAsExpectingError(user.Email, newPassword2);

            loginPage.ErrorMessages.Count.ShouldBe(1);

            //request reset 1
            var resettingPasswordPage = loginPage.ForgottenPassword
                                                 .Reset(user.Email);

            resettingPasswordPage.Message.ShouldBe("An email has been sent to dave.smith@binarymash.com; please follow the instructions in this email to confirm the password reset. You must act within the next 10 minutes to confirm the reset. After this time, the reset request will expire.");

            //request reset 2
            resettingPasswordPage = new IndexPage(Sandbox).LogIn()
                                                          .ForgottenPassword
                                                          .Reset(user.Email);

            resettingPasswordPage.Message.ShouldBe("An email has been sent to dave.smith@binarymash.com; please follow the instructions in this email to confirm the password reset. You must act within the next 10 minutes to confirm the reset. After this time, the reset request will expire.");

            //act on request 1
            var resetPasswordEmail = resettingPasswordPage.ReadResetPasswordEmail;
            resetPasswordEmail.Message.From.Address.ShouldBe("accounts@binarymash.com");
            resetPasswordEmail.Message.To.Count.ShouldBe(1);
            resetPasswordEmail.Message.To[0].Address.ShouldBe(user.Email);

            var resetPasswordPage = resetPasswordEmail.ClickLink();
            resetPasswordEmail.Message.Dispose();

            //reset password
            var confirmationOfPasswordResetPage = resetPasswordPage.Reset(
                new ResetPasswordPageModel
                {
                    Email = user.Email,
                    Password = newPassword1,
                    PasswordConfirmation = newPassword1
                });

            confirmationOfPasswordResetPage.Message.ShouldBe("Your password has been reset.");

            //login with new password
            var accountPage = confirmationOfPasswordResetPage.LogIn
                                                             .LogInAs(user.Email, newPassword1);

            accountPage.ErrorMessages.Count.ShouldBe(0);
            accountPage.LogOut();

            //act on request 2
            resetPasswordEmail = resettingPasswordPage.ReadResetPasswordEmail;
            resetPasswordEmail.Message.From.Address.ShouldBe("accounts@binarymash.com");
            resetPasswordEmail.Message.To.Count.ShouldBe(1);
            resetPasswordEmail.Message.To[0].Address.ShouldBe(user.Email);

            resetPasswordPage = resetPasswordEmail.ClickLink();
            resetPasswordEmail.Message.Dispose();

            //reset password
            confirmationOfPasswordResetPage = resetPasswordPage.Reset(
                new ResetPasswordPageModel
                {
                    Email = user.Email,
                    Password = newPassword2,
                    PasswordConfirmation = newPassword2
                });

            confirmationOfPasswordResetPage.Message.ShouldBe("Your password has been reset.");

            //login with new password
            accountPage = confirmationOfPasswordResetPage.LogIn
                                                         .LogInAs(user.Email, newPassword2);

            accountPage.ErrorMessages.Count.ShouldBe(0);
            var homePage = accountPage.LogOut();

            //can't log in with the first new password
            loginPage = homePage.LogIn().LogInAsExpectingError(user.Email, newPassword1);
            loginPage.ErrorMessages.Count.ShouldBe(1);

        }

        [Test]
        public void WhenIAttemptAPasswordResetOnAResetWindowWhichDoesntExist_IGetAnError()
        {
            var user = UserBuilder.DaveSmith.Create();
            const string oldPassword = "passpasspass";
            const string newPassword = "thisismynewpassword";

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var resetPasswordPage = userManuallyNavigates.ToResetPassword(Guid.NewGuid());

            //reset password
            resetPasswordPage = resetPasswordPage.ResetExpectingErrors(
                new ResetPasswordPageModel
                {
                    Email = user.Email,
                    Password = newPassword,
                    PasswordConfirmation = newPassword
                });

            var errorMessages = resetPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages[0].ShouldBe("Could not reset the password for this email address. Please check your details and try again.");

            //can't log in with the first new password
            var loginPage = new IndexPage(Sandbox).LogIn();
            loginPage = loginPage.LogInAsExpectingError(user.Email, newPassword);
            loginPage.ErrorMessages.Count.ShouldBe(1);

            //can log in with old password
            var accountPage = loginPage.LogInAs(user.Email, oldPassword);
            accountPage.ErrorMessages.Count.ShouldBe(0);
        }

        [Test]
        public void WhenIHaveAPasswordResetWindowAndSpecifyInvalidInputs_IGetErrors()
        {
            var user = UserBuilder.DaveSmith.Create();
            const string oldPassword = "passpasspass";
            const string newPassword = "thisismynewpassword";

            //request reset
            var resettingPasswordPage = new IndexPage(Sandbox).LogIn()
                                                              .ForgottenPassword
                                                              .Reset(user.Email);

            resettingPasswordPage.Message.ShouldBe("An email has been sent to dave.smith@binarymash.com; please follow the instructions in this email to confirm the password reset. You must act within the next 10 minutes to confirm the reset. After this time, the reset request will expire.");

            var resetPasswordEmail = resettingPasswordPage.ReadResetPasswordEmail;
            resetPasswordEmail.Message.From.Address.ShouldBe("accounts@binarymash.com");
            resetPasswordEmail.Message.To.Count.ShouldBe(1);
            resetPasswordEmail.Message.To[0].Address.ShouldBe(user.Email);

            var resetPasswordPage = resetPasswordEmail.ClickLink();
            resetPasswordEmail.Message.Dispose();

            //no data
            resetPasswordPage = resetPasswordPage.ResetExpectingErrors(new ResetPasswordPageModel());
            var errorMessages = resetPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(3);
            errorMessages.ShouldContain("Email address is required");
            errorMessages.ShouldContain("New password is required");
            errorMessages.ShouldContain("New password confirmation is required");

            //missing data
            resetPasswordPage = resetPasswordPage.ResetExpectingErrors(
                new ResetPasswordPageModel
                {
                    Email=user.Email
                });

            errorMessages = resetPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(2);
            errorMessages.ShouldContain("New password is required");
            errorMessages.ShouldContain("New password confirmation is required");

            resetPasswordPage = resetPasswordPage.ResetExpectingErrors(
                new ResetPasswordPageModel
                {
                    Password = newPassword,
                });

            errorMessages = resettingPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("New password confirmation is required");

            resetPasswordPage = resetPasswordPage.ResetExpectingErrors(
                new ResetPasswordPageModel
                {
                    Email = "",
                });

            errorMessages = resetPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(2);
            errorMessages.ShouldContain("Email address is required");
            errorMessages.ShouldContain("New password confirmation is required");

            //passwords mismatch
            resetPasswordPage = resetPasswordPage.ResetExpectingErrors(
                new ResetPasswordPageModel
                {
                    Email = user.Email,
                    Password = newPassword,
                    PasswordConfirmation = newPassword+"a"
                });

            errorMessages = resetPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("Password confirmation must match the password");

            //wrong email
            resetPasswordPage = resetPasswordPage.ResetExpectingErrors(
                new ResetPasswordPageModel
                {
                    Email = user.Email+"j",
                    Password = newPassword,
                    PasswordConfirmation = newPassword
                });

            errorMessages = resetPasswordPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("Could not reset the password for this email address. Please check your details and try again.");


            //can't log in with the first new password
            var loginPage = new IndexPage(Sandbox).LogIn();
            loginPage = loginPage.LogInAsExpectingError(user.Email, newPassword);
            loginPage.ErrorMessages.Count.ShouldBe(1);

            //can log in with old password
            var accountPage = loginPage.LogInAs(user.Email, oldPassword);
            accountPage.ErrorMessages.Count.ShouldBe(0);
        }

    }
}
