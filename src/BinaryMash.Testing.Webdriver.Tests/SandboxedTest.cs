﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using NUnit.Framework;
    using Testing.Environment;

    public abstract class SandboxedTest
    {

        protected Sandbox Sandbox;

        [TestFixtureSetUp]
        public virtual void TestFixtureSetUp()
        {
        }

        [SetUp]
        public virtual void SetUp()
        {
            Sandbox = new Sandbox();

        }

        [TearDown]
        public virtual void TearDown()
        {
            Sandbox.Dispose();
        }

        public void WaitForEvent()
        {
            //TODO: do something more clever here?
            System.Threading.Thread.Sleep(10000);
        }
    }
}
