﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Data;
    using Domain;
    using NUnit.Framework;
    using Pages.Web;
    using Pages.Web.Question;
    using Shouldly;
    using Utilities;
    using IndexPage = Pages.Web.Home.IndexPage;

    [TestFixture]
    public class XssTestingForQuestions : SandboxedTest
    {

        #region OWASP XSS stuff

        [Test]
        [Ignore("TODO: finish this....")]
        public void WhenCreatingQuestionThatContainsPotentialXssAccordingToOwasp_ISeeNoAlerts()
        {
            const string title = "Question testing creation with user supplied text";
            const string questionText = "This is text for the first question section";
            const string questionNotes = "These are notes for the first question section";
            const string maxLength = "2048";
            const string answerNotes = "These are notes for the user supplied answer in the first question section";

            var user = UserBuilder.SystemAdmin.Create();

            var createQuestionPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Questions
                .WriteANewQuestion;

            createQuestionPage
                 .SetTitle(title);

            var section = createQuestionPage.Sections[0]
                 .SetText(questionText)
                 .SetNotes(questionNotes)
                 .SelectAnswerType(AnswerContainerType.UserSupplied);

            var answerContent = (AnswerContentUserSuppliedEditComponent)section.AnswerContents[0];
            answerContent.SetMaxLength(maxLength)
                .SetNotes(answerNotes);

            var xssSamples = OWaspXssSamples.AllXssStrings;
            var sectionIndex = 0;
            foreach (var xssSample in xssSamples)
            {
                sectionIndex++;
                section = createQuestionPage.AddSection().Sections[sectionIndex];
                section.SetText(xssSample)
                       .SelectAnswerType(AnswerContainerType.Radio);

                var usAnswerContent = (AnswerContentQuestionerSuppliedEditComponent)section.AnswerContents[0];
                usAnswerContent.SetText(xssSample);
            }

            createQuestionPage = createQuestionPage.Save;

            createQuestionPage.UserHasBeenNotifiedThatSavingTheDraftWasSuccessful.ShouldBe(true);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);

            var previewPage = userManuallyNavigates.ToPreviewOfQuestion(createQuestionPage.QuestionId);
            previewPage.BrowserSession.HasNoDialog("").ShouldBe(true);
            previewPage.BrowserSession.HasNoContent("This is remote text via xss.js"); 
            
            previewPage.Title.ShouldBe(title);
            var sections = previewPage.Sections;
            sections.Count.ShouldBe(sectionIndex+1);
        }

        #endregion

        [Test]
        public void WhenCreatingQuestionsThatContainsMarkdownSpecificXss_ISeeNoAlerts()
        {
            const string title = "Question testing creation with user supplied text";
            const string questionText = "This is text for the first question section";
            const string questionNotes = "These are notes for the first question section";
            const string maxLength = "2048";
            const string answerNotes = "These are notes for the user supplied answer in the first question section";

            var user = UserBuilder.SystemAdmin.Create();

            var createQuestionPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAs(user.Email, "testpass")
                .Menu.Questions
                .WriteANewQuestion;

            createQuestionPage
                 .SetTitle(title);

            var section = createQuestionPage.Sections[0]
                 .SetText(questionText)
                 .SetNotes(questionNotes)
                 .SelectAnswerType(AnswerContainerType.UserSupplied);

            var answerContent = (AnswerContentUserSuppliedEditComponent)section.AnswerContents[0];
            answerContent.SetMaxLength(maxLength)
                .SetNotes(answerNotes);

            var xssSamples = MarkdownXssSamples.AllXssStrings;
            var sectionIndex = 0;
            foreach (var xssSample in xssSamples)
            {
                sectionIndex++;
                section = createQuestionPage.AddSection().Sections[sectionIndex];
                section.SetText(xssSample)
                       .SelectAnswerType(AnswerContainerType.Radio);

                var usAnswerContent = (AnswerContentQuestionerSuppliedEditComponent)section.AnswerContents[0];
                usAnswerContent.SetText(xssSample);
            }

            createQuestionPage = createQuestionPage.Save;

            createQuestionPage.UserHasBeenNotifiedThatSavingTheDraftWasSuccessful.ShouldBe(true);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);

            var previewPage = userManuallyNavigates.ToPreviewOfQuestion(createQuestionPage.QuestionId);
            previewPage.BrowserSession.HasNoDialog("").ShouldBe(true);
            previewPage.BrowserSession.HasNoContent("This is remote text via xss.js");

            previewPage.Title.ShouldBe(title);
            var sections = previewPage.Sections;
            sections.Count.ShouldBe(sectionIndex + 1);
        }

    }
}
