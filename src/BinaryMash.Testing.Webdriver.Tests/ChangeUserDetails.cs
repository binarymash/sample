﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using Data;
    using NUnit.Framework;
    using Pages.Models;
    using Pages.Web;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    public class ChangeUserDetails : SandboxedTest
    {
        [Test]
        public void WhenAuthenticated_ICanChangeMyDetails()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";
            
            const string newFirstName = "abc";
            const string newLastName = "def";
            const string newDisplayName = "ghi";

            new IndexPage(Sandbox).LogIn()
                                  .LogInAs(user.Email, password);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var changeDetailsPage = userManuallyNavigates.ToUserSummary(user.Id)
                                                         .ChangeDetails;
            var userSummaryPage = changeDetailsPage.SetData(
                                                        new ChangeUserDetailsPageModel
                                                        {
                                                            FirstName = newFirstName,
                                                            LastName = newLastName,
                                                            DisplayName = newDisplayName
                                                        })
                                                    .Done;

            userSummaryPage.Name.ShouldBe(newFirstName + " " + newLastName);
            userSummaryPage.DisplayName.ShouldBe(newDisplayName);

            //confirm summary still shows new data after logging out and in
            userSummaryPage.LogOut().LogIn().LogInAs(user.Email, password);

            userSummaryPage = userManuallyNavigates.ToUserSummary(user.Id);
            userSummaryPage.Name.ShouldBe(newFirstName + " " + newLastName);
            userSummaryPage.DisplayName.ShouldBe(newDisplayName);
        }

        [Test]
        public void WhenICancel_TheDetailAreNotChanged()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            const string newFirstName = "abc";
            const string newLastName = "def";
            const string newDisplayName = "ghi";

            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, password);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var changeDetailsPage = userManuallyNavigates.ToUserSummary(user.Id).ChangeDetails;
            var userSummaryPage = changeDetailsPage.SetData(new ChangeUserDetailsPageModel
                {
                    DisplayName = newDisplayName,
                    FirstName = newFirstName,
                    LastName = newLastName
                }).Cancel;

            userSummaryPage.DisplayName.ShouldBe(user.PublicProfile.DisplayName); 
            userSummaryPage.Name.ShouldBe(user.FirstName + " " + user.LastName);

            var homePage = userSummaryPage.LogOut();

            //confirm summary still shows original data after logging out and in
            homePage.LogIn().LogInAs(user.Email, password);

            userSummaryPage = userManuallyNavigates.ToUserSummary(user.Id);
            userSummaryPage.DisplayName.ShouldBe(user.PublicProfile.DisplayName); 
            userSummaryPage.Name.ShouldBe(user.FirstName + " " + user.LastName);
        }

        [Test]
        [Ignore("TODO: implement")]
        public void WhenAuthenticated_ICannotChangeAnotherAccountsEmail()
        {
        }

        [Test]
        public void WhenITryToChangeToInvalidData_IGetErrors()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, password);

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var userSummaryPage = userManuallyNavigates.ToUserSummary(user.Id);
            var changeDetailsPage = userSummaryPage.ChangeDetails;

            //empty
            changeDetailsPage = changeDetailsPage.SetData(
                new ChangeUserDetailsPageModel
                {
                    DisplayName = "",   
                    FirstName = "", 
                    LastName = ""
                }).DoneExpectingErrors;
            changeDetailsPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);
            var errorMessages = changeDetailsPage.ErrorMessages;
            errorMessages.Count.ShouldBe(3);
            errorMessages.ShouldContain("'DisplayName' is required"); //TODO: better message 
            errorMessages.ShouldContain("'FirstName' is required"); //TODO: better message
            errorMessages.ShouldContain("'LastName' is required"); //TODO: better message

            //too long
            changeDetailsPage = changeDetailsPage.SetData(
                new ChangeUserDetailsPageModel
                {
                    DisplayName = "123456789012345678901234567890123", //max 32
                    FirstName = "123456789012345678901234567890123", //max 32
                    LastName = "123456789012345678901234567890123" //max 32
                }).DoneExpectingErrors;
            changeDetailsPage.UserHasBeenNotifiedThatSavingWasUnsuccessful.ShouldBe(true);
            errorMessages = changeDetailsPage.ErrorMessages;
            errorMessages.Count.ShouldBe(3);
            errorMessages.ShouldContain("'DisplayName' must be a string with 32 characters or less"); //TODO: better message
            errorMessages.ShouldContain("'FirstName' must be a string with 32 characters or less"); //TODO: better message
            errorMessages.ShouldContain("'LastName' must be a string with 32 characters or less"); //TODO: better message

            userSummaryPage = changeDetailsPage.Cancel;
            userSummaryPage.Name.ShouldBe(user.FirstName + " " + user.LastName);
            userSummaryPage.DisplayName.ShouldBe(user.PublicProfile.DisplayName);
        }

        [Test]
        public void WhenTakingATestSessionAndBrowsingToChangeUserDetailsPage_IAmRedirectedToCandidateWelcome()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            var userHomePage = new IndexPage(Sandbox).LogIn()
                                                     .LogInAs(user.Email, password);

            var sessions = userHomePage.Menu.Sessions;
            var session = sessions.Sessions[0];
            var sessionSummary = session.ViewSummary();
            var confirmationDialog = sessionSummary.TakeTheTestNowOnThisDevice;
            var candidateWelcomePage = confirmationDialog.ConfirmToTakeTheTest;
            candidateWelcomePage.Message.ShouldBe("Hello, Kiki Nortey");

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            candidateWelcomePage = userManuallyNavigates.ToChangeUserDetailsExpectingCandidateWelcome(user.Id);
            candidateWelcomePage.Message.ShouldBe("Hello, Kiki Nortey");
        }

        [Test]
        public void WhenUnauthenticated_IAmRedirectedToLogin()
        {
            var user = UserBuilder.SystemAdmin.Create();
            const string password = "testpass";

            var userManuallyNavigates = new UserManuallyNavigates(Sandbox);
            var loginPage = userManuallyNavigates.ToChangeUserDetailsExpectingUnauthenticated(user.Id);
            loginPage.WaitForPageWithTitle("Login");

            loginPage.LogInAs(user.Email, password);

            var changeEmailPage = userManuallyNavigates.ToChangeUserDetails(user.Id);
            changeEmailPage.WaitForPageWithTitle("Edit User");
        }

    }
}
