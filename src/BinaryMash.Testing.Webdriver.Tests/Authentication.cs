﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using Data;
    using NUnit.Framework;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    public class Authentication : SandboxedTest
    {

        [Test]
        public void UnauthenticatedUserIsRedirectedToLogin()
        {
            Sandbox.Browser.Session.Visit("User/Home");
            Sandbox.Browser.Session.Location.AbsolutePath.ShouldBe("/Authentication");
        }

        [Test]
        public void UnauthenticatedUserCanSeeHomePage()
        {
            var homePage = new IndexPage(Sandbox);
            homePage.BrowserSession.Location.AbsolutePath.ShouldBe("/");
            homePage.Menu.ShowsUserIdentity.ShouldBe(false);
            homePage.Menu.ShowsLogIn.ShouldBe(true);
            homePage.Menu.ShowsLogOut.ShouldBe(false);
        }

        [Test]
        public void NominalLogInAndOut()
        {
            var login = (Login_TestResourceExtension)LoginBuilder.SystemAdmin.Create();
            var user = UserBuilder.SystemAdmin.Create();

            var homePage = new IndexPage(Sandbox);
            homePage.Menu.ShowsUserIdentity.ShouldBe(false);
            homePage.Menu.ShowsLogIn.ShouldBe(true);
            homePage.Menu.ShowsLogOut.ShouldBe(false);

            var userPage = homePage.LogIn()
                .LogInAs(user.Email, login.PlainPassword);

            //TODO: replace this 
            //userPage.Menu.ShowsUserIdentity.ShouldBe(true);
            //userPage.Menu.UserIdentity.ShouldBe(user.Email);
            userPage.Menu.ShowsLogIn.ShouldBe(false);
            userPage.Menu.ShowsLogOut.ShouldBe(true);

            homePage = userPage.LogOut();
            homePage.Menu.ShowsUserIdentity.ShouldBe(false);
            homePage.Menu.ShowsLogIn.ShouldBe(true);
            homePage.Menu.ShowsLogOut.ShouldBe(false);

        }

        [Test]
        public void GivenThatFieldsAreEmpty()
        {
            var loginPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAsExpectingError(string.Empty, string.Empty);

            var errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(2);
            errorMessages.ShouldContain("Email is required");
            errorMessages.ShouldContain("Password is required");

            loginPage.LogInAsExpectingError(string.Empty, "blah");

            errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("Email is required");

            var user = UserBuilder.Default.Create();

            loginPage.LogInAsExpectingError(user.Email, string.Empty);

            errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("Password is required");
        }

        [Test]
        public void GivenThatTheWrongCredentialsAreUsed()
        {
            var login = (Login_TestResourceExtension)LoginBuilder.SystemAdmin.Create();
            var user = UserBuilder.SystemAdmin.Create();

            //incorrect password
            var loginPage = new IndexPage(Sandbox)
                .LogIn()
                .LogInAsExpectingError(user.Email, login.PlainPassword + "1");

            var errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages[0].ShouldBe("The email address or password was incorrect. Please check your details and try again.");           

            //incorrect email
            loginPage.LogInAsExpectingError(user.Email + "1", login.PlainPassword);

            errorMessages = loginPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages[0].ShouldBe("The email address or password was incorrect. Please check your details and try again.");

        }

    }
}
