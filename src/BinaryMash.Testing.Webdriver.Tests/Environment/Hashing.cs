﻿namespace BinaryMash.Testing.Webdriver.Tests.Environment
{
    using System.Globalization;
    using Core.Security;
    using Core.Security.Hashing;
    using Core.Security.Interfaces;
    using NUnit.Framework;

    [TestFixture]
    [Category("Environment")]
    public class Hashing
    {
        [Test]
        public void Run()
        {
            string saltSeed = "C6851DD9-1F91-4622-B41B-4A1A193E10BE".ToLower(CultureInfo.InvariantCulture);
            const string plainText = "passpasspass";

            var hashingService = new HashingService(new AlgorithmFactory(), new SaltFactory());
            var hash = hashingService.Hash(
                new HashRequest
                {
                    PlainText = plainText, 
                    SaltSeed = saltSeed
                });

        }
    }
}
