﻿namespace BinaryMash.Testing.Webdriver.Tests.Environment
{
    using NUnit.Framework;

    [TestFixture]
    [Ignore("Run this to force a reset on the environment")]
    [Category("Environment")]
    [Category("ResetSandbox")]
    public sealed class ResetSandbox : SandboxedTest
    {
        [Test]
        public void Run()
        {
        }
    }

}
