﻿namespace BinaryMash.Testing.Webdriver.Tests.Environment
{
    using BinaryMash.Testing.Environment;
    using NUnit.Framework;

    [TestFixture]
    //[Ignore("Run this to initialise the database")]
    [Category("Environment")]
    [Category("InitialiseDatabase")]
    public class InitialiseDatabase
    {
        [Test]
        public void Run()
        {
            var dbManager = new DatabaseManager();
            dbManager.InstallDatabase();
            dbManager.SeedData();
            dbManager.BackupDatabase();
        }
    }

}
