﻿namespace BinaryMash.Testing.Webdriver.Tests
{
    using Data;
    using NUnit.Framework;
    using Pages.Models;
    using Pages.Web.Home;
    using Shouldly;

    [TestFixture]
    public class AccountCreation : SandboxedTest
    {

        [Test]
        public void CreateAccountAndLogIn()
        {          
            var user = UserBuilder.Default.Create();
            const string plainTextPassword = "thisismypassword";

            var accountCreatingPage = new IndexPage(Sandbox)
                .SignUp()
                .Create(
                    new CreateAccountPageModel
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        Password = plainTextPassword,  
                        PasswordConfirmation = plainTextPassword,
                        AcceptTermsAndConditions = true
                    });

            accountCreatingPage.Message.ShouldBe("Your account has been created, but it must be activated before you can use it. An email has been sent to test.user@binarymash.com; please follow the instructions in this email to activate your account.");

            var validateUserEmail = accountCreatingPage.ReadValidateUserEmail;
            validateUserEmail.Message.From.Address.ShouldBe("accounts@binarymash.com");
            validateUserEmail.Message.To.Count.ShouldBe(1);
            validateUserEmail.Message.To[0].Address.ShouldBe(user.Email);

            var accountActivatePage = validateUserEmail.ClickLink();
            validateUserEmail.Message.Dispose();

            accountActivatePage.ActivatingMessage.ShouldBe("Your account is activating.");
            accountActivatePage.WaitForActivation();
            accountActivatePage.ActivatedMessage.ShouldBe("Your account is now active.");

            var accountPage = accountActivatePage.LogIn().LogInAs(user.Email, plainTextPassword);

            accountPage.ErrorMessages.Count.ShouldBe(0);

        }

        [Test]
        public void CreateAccountAndLogInWhenLongNames()
        {
            const string firstName = "1234567890123456789012345";
            const string lastName = "abcdefghijabcdefghijabcdef";
            const string email = "abcdefgh@binarymash.com";

            var expectedDisplayName = "1234567890123456789012345 abcdef";
            const string plainTextPassword = "thisismypassword";

            var accountCreatingPage = new IndexPage(Sandbox)
                .SignUp()
                .Create(
                    new CreateAccountPageModel
                    {
                        FirstName = firstName,
                        LastName = lastName,
                        Email = email,
                        Password = plainTextPassword,
                        PasswordConfirmation = plainTextPassword,
                        AcceptTermsAndConditions = true
                    });

            accountCreatingPage.Message.ShouldBe("Your account has been created, but it must be activated before you can use it. An email has been sent to abcdefgh@binarymash.com; please follow the instructions in this email to activate your account.");

            var validateUserEmail = accountCreatingPage.ReadValidateUserEmail;
            validateUserEmail.Message.From.Address.ShouldBe("accounts@binarymash.com");
            validateUserEmail.Message.To.Count.ShouldBe(1);
            validateUserEmail.Message.To[0].Address.ShouldBe(email);

            var accountActivatePage = validateUserEmail.ClickLink();
            validateUserEmail.Message.Dispose();

            accountActivatePage.ActivatingMessage.ShouldBe("Your account is activating.");
            accountActivatePage.WaitForActivation();
            accountActivatePage.ActivatedMessage.ShouldBe("Your account is now active.");

            var accountPage = accountActivatePage.LogIn().LogInAs(email, plainTextPassword);

            accountPage.ErrorMessages.Count.ShouldBe(0);
            var userSummary = accountPage.Menu.MyAccount;
            userSummary.DisplayName.ShouldBe(expectedDisplayName);
            userSummary.Name.ShouldBe(firstName + " " + lastName);

        }

        [Test]
        public void GivenThatTheEmailAlreadyExists()
        {
            var user = UserBuilder.SystemAdmin.Create();

            var accountCreatedPage = new IndexPage(Sandbox)
                .SignUp()
                .CreateExpectingErrors(
                    new CreateAccountPageModel
                    {
                        FirstName = "Dave",
                        LastName = "Smith",
                        Email = user.Email,
                        Password = "this is a password",
                        PasswordConfirmation = "this is a password",
                        AcceptTermsAndConditions = true
                    });

            var errorMessages = accountCreatedPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages[0].ShouldBe("A user already exists with this email address");
        }

        [Test]
        public void AnyEmptyFieldGivesAnError()
        {
            var user = UserBuilder.Default.Create();

            var createAccountPage = new IndexPage(Sandbox)
                .SignUp();

            //no errors when we first load the page
            createAccountPage.ErrorMessages.Count.ShouldBe(0);

            createAccountPage.CreateExpectingErrors(new CreateAccountPageModel());

            //errors after we submit when nothing set
            var errorMessages = createAccountPage.ErrorMessages;
            errorMessages.Count.ShouldBe(6);
            errorMessages.ShouldContain("First name is required");
            errorMessages.ShouldContain("Last name is required");
            errorMessages.ShouldContain("Email address is required");
            errorMessages.ShouldContain("Password is required");
            errorMessages.ShouldContain("Password confirmation is required");
            errorMessages.ShouldContain("Terms and conditions must be accepted");

            //fill in email
            createAccountPage.CreateExpectingErrors(new CreateAccountPageModel
            {
                Email = user.Email
            });
            errorMessages = createAccountPage.ErrorMessages;
            errorMessages.Count.ShouldBe(5);
            errorMessages.ShouldContain("First name is required");
            errorMessages.ShouldContain("Last name is required");
            errorMessages.ShouldContain("Password is required");
            errorMessages.ShouldContain("Password confirmation is required");
            errorMessages.ShouldContain("Terms and conditions must be accepted");

            //fill in first name
            createAccountPage.CreateExpectingErrors(new CreateAccountPageModel
            {
                FirstName = user.FirstName 
            });
            errorMessages = createAccountPage.ErrorMessages;
            errorMessages.Count.ShouldBe(4);
            errorMessages.ShouldContain("Last name is required");
            errorMessages.ShouldContain("Password is required");
            errorMessages.ShouldContain("Password confirmation is required");
            errorMessages.ShouldContain("Terms and conditions must be accepted");

            //fill in last name
            createAccountPage.CreateExpectingErrors(new CreateAccountPageModel
            {
                LastName = user.LastName 
            });
            errorMessages = createAccountPage.ErrorMessages;
            errorMessages.Count.ShouldBe(3);
            errorMessages.ShouldContain("Password is required");
            errorMessages.ShouldContain("Password confirmation is required");
            errorMessages.ShouldContain("Terms and conditions must be accepted");

            //accept terms
            createAccountPage.CreateExpectingErrors(new CreateAccountPageModel
            {
                AcceptTermsAndConditions = true
            });
            errorMessages = createAccountPage.ErrorMessages;
            errorMessages.Count.ShouldBe(2);
            errorMessages.ShouldContain("Password is required");
            errorMessages.ShouldContain("Password confirmation is required");

            //fill in password
            //accept terms
            createAccountPage.CreateExpectingErrors(new CreateAccountPageModel
            {
                Password = "some password"
            });
            errorMessages = createAccountPage.ErrorMessages;
            errorMessages.Count.ShouldBe(1);
            errorMessages.ShouldContain("Password confirmation is required");
        }

    }
}
