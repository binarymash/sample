﻿namespace BinaryMash.Build.Installation.Components
{
    using System.Collections.Generic;
    using dropkick.Configuration.Dsl;
    using dropkick.Configuration.Dsl.NServiceBusHost;

    public static class NServiceBusGenericHost
    {
        public static void Install(ProtoServer s, SystemSettings settings, string windowsServiceFolder, string windowsServiceName, string windowsServiceDescription, IEnumerable<string> msmqs)
        {
            NServiceBusMsmq.Install(s, msmqs, true);

            s.NServiceBusHost(host =>
            {
                host.Action(Action.Install);
                host.ExeName("NServiceBus.Host.exe");
                host.LocatedAt(windowsServiceFolder);
                host.Profiles(settings.NServiceBusProfiles);
                host.ServiceDescription(windowsServiceDescription);
                host.ServiceName(windowsServiceName);
                host.ServiceDisplayName(windowsServiceName);            
            });

        }

        public static void Uninstall(ProtoServer s, SystemSettings settings, string windowsServiceFolder, string windowsServiceName)
        {
            s.NServiceBusHost(host =>
            {
                host.Action(Action.Uninstall);
                host.ExeName("NServiceBus.Host.exe");
                host.LocatedAt(windowsServiceFolder);
                host.ServiceName(windowsServiceName);
            });
            
        }
    }
}
