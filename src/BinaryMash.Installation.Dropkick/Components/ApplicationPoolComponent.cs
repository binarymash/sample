﻿
namespace BinaryMash.Build.Installation.Components
{
    using Microsoft.Web.Administration;

    public class ApplicationPoolComponent
    {

        public static ApplicationPoolComponent Install(string name)
        {
            var factory = Uninstall(name);
            factory.Subject = factory.ServerManager.ApplicationPools.Add(name);
            return factory;
        }

        public static ApplicationPoolComponent Uninstall(string name)
        {
            var factory = new ApplicationPoolComponent();
            factory.Subject = factory.ServerManager.ApplicationPools[name];
            if (factory.Subject != null)
            {
                factory.ServerManager.ApplicationPools.Remove(factory.Subject);
            }

            return factory;            
        }

        private ApplicationPoolComponent()
        {
            ServerManager = new ServerManager();
        }

        public ApplicationPool Commit()
        {
            ServerManager.CommitChanges();
            return Subject;
        }

        protected readonly ServerManager ServerManager;

        protected ApplicationPool Subject;

        public ApplicationPoolComponent WithVersion(string version)
        {
            Subject.ManagedRuntimeVersion = version;
            return this;
        }

        public ApplicationPoolComponent WithPipelineMode(ManagedPipelineMode pipelineMode)
        {
            Subject.ManagedPipelineMode = pipelineMode;
            return this;
        }

    }
}
