﻿namespace BinaryMash.Build.Installation.Components
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using dropkick.Configuration.Dsl;
    using dropkick.Configuration.Dsl.Msmq;

    public static class NServiceBusMsmq
    {
        public static void Install(ProtoServer s, IEnumerable<string> msmqs, bool forServer)
        {
            msmqs.ToList().ForEach(msmq => Install(s, msmq, forServer));
        }

        public static void Install(ProtoServer s, string serviceName, bool forServer)
        {
            Install(s, serviceName);
            Install(s, string.Format(CultureInfo.InvariantCulture, "{0}.error", serviceName));
            Install(s, string.Format(CultureInfo.InvariantCulture, "{0}.retries", serviceName));

            if (forServer)
            {
                Install(s, string.Format(CultureInfo.InvariantCulture, "{0}.subscriptions", serviceName));
                Install(s, string.Format(CultureInfo.InvariantCulture, "{0}.timeouts", serviceName));
            }
        }

        private static void Install(ProtoServer s, string msmqName)
        {
            s.Msmq().PrivateQueue(msmqName).Transactional();
        }
    }
}
