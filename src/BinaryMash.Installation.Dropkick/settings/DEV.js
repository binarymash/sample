﻿{
    DbName : "Aptitude_Dev",
    DbSqlFilesPath : "RoundhouseScripts",
    DbRestorePath : "\\\\127.0.0.1\\Databases\\local.bak",
    DbRecoveryMode : "Simple",
    RoundhousEMode : "DropCreate",
    SqlCmdPath: "C:\\Program Files\\Microsoft SQL Server\\110\\Tools\\Binn\\sqlcmd.exe",
    DbServer: "localhost\\sqlexpress",
    DbBackupPath:"E:\\dev\\Aptitude\\Db\\Backups\\Aptitude_Dev.bak",     
    
    WebSiteDeploymentDirectory : "E:\\dev\\Aptitude\\Artifacts\\WebSite",
    WebSiteName : "Aptitude",
    WebSitePort : "64023",

    NServiceBusDeploymentDirectory : "E:\\dev\\Aptitude\\Artifacts\\Services",
    NServiceBusProfiles : ""
}