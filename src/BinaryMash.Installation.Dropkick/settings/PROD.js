﻿{
    DbName : "Aptitude",
    DbSqlFilesPath : "RoundhouseScripts",
    DbRestorePath : "\\\\127.0.0.1\\Databases\\local.bak",
    DbRecoveryMode : "Simple",
    RoundhousEMode : "DropCreate",
    SqlCmdPath: "",    
    DbServer:"",
    DbBackupPath:"",      
 
    WebSiteDeploymentDirectory : "c:\\apt-install-web",
    WebSiteName : "Aptitude",
    WebSitePort : "80",
    
    NServiceBusDeploymentDirectory : "c:\\Apt-Install-Services",
    NServiceBusProfiles : ""
}