//ReSharper disable ConvertToLambdaExpression
// ==============================================================================
// 
// ACuriousMind and FerventCoder Copyright � 2011 - Released under the Apache 2.0 License
// 
// Copyright 2007-2008 The Apache Software Foundation.
//  
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use 
// this file except in compliance with the License. You may obtain a copy of the 
// License at 
//
//     http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.
// ==============================================================================

namespace BinaryMash.Build.Installation.Deployments
{
    using Components;
    using dropkick.Configuration.Dsl;
    using dropkick.Configuration.Dsl.Iis;

    public class Uninstall : Deployment<Uninstall, SystemSettings>
    {

        #region Constructors

        public Uninstall()
        {
            Define(settings =>
            {
                DeploymentStepsFor(AppServer, 
                    s =>
                    {
                        //Email Service
                        var targetDirectory = System.IO.Path.Combine(settings.NServiceBusDeploymentDirectory, "UserService");
                        NServiceBusGenericHost.Uninstall(s, settings, targetDirectory, "BinaryMash.Application.UserService");
                            
                        targetDirectory = System.IO.Path.Combine(settings.NServiceBusDeploymentDirectory, "EmailService");
                        NServiceBusGenericHost.Uninstall(s, settings, targetDirectory, "BinaryMash.Application.EmailService");
                    }
                );

                DeploymentStepsFor(WebServer,
                    s =>
                    {
                        s.Iis7Site("Aptitude").Uninstall
                            .VirtualDirectory(string.Empty);
                    }
                ); 
            });
        }

        #endregion

        #region Properties

        //order is important
        public static Role Db { get; set; }
        public static Role AppServer { get; set; }
        public static Role WebServer { get; set; }

        #endregion

    }
}