//ReSharper disable ConvertToLambdaExpression

namespace BinaryMash.Build.Installation.Deployments
{
    using System.IO;
    using dropkick.Configuration.Dsl;
    using dropkick.Configuration.Dsl.CommandLine;

    public class Backup : Deployment<Backup, SystemSettings>
    {

        #region Constructors

        public Backup()
        {
            Define(settings =>
            {

                DeploymentStepsFor(DbSqlCmd, 
                    s =>
                        {
                            var fi = new FileInfo(Settings.DbBackupPath);
                            fi.Delete();

                            var argsString = string.Format(
                                "-S {0} " +
                                "-Q \"" +
                                "BACKUP DATABASE [{1}] " +
                                "TO DISK='{2}'" +
                                "\"",
                                Settings.DbServer, Settings.DbName, Settings.DbBackupPath);

                            s.CommandLine(Settings.SqlCmdPath).Args(argsString);
                        }
                );

            });
        }

        #endregion

        #region Properties

        //order is important
        public static Role DbSqlCmd { get; set; }

        #endregion

    }
}