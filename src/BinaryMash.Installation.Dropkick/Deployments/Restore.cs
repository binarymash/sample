//ReSharper disable ConvertToLambdaExpression

namespace BinaryMash.Build.Installation.Deployments
{
    using dropkick.Configuration.Dsl;
    using dropkick.Configuration.Dsl.CommandLine;

    public class Restore : Deployment<Restore, SystemSettings>
    {

        #region Constructors

        public Restore()
        {
            Define(settings =>
            {

                DeploymentStepsFor(DbSqlCmd, 
                    s =>
                        {
                            var args = string.Format(
                                "-S {0} " +
                                "-Q \"" +
                                "ALTER DATABASE {1} SET SINGLE_USER WITH ROLLBACK IMMEDIATE; " +
                                "RESTORE DATABASE [{1}] " +
                                "FROM DISK='{2}' " +
                                "WITH REPLACE" +
                                "\"",
                                Settings.DbServer, Settings.DbName, Settings.DbBackupPath);

                            s.CommandLine(Settings.SqlCmdPath).Args(args);
                        }

                );

            });
        }

        #endregion

        #region Properties

        //order is important
        public static Role DbSqlCmd { get; set; }

        #endregion

    }
}