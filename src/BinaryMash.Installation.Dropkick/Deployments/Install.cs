//ReSharper disable ConvertToLambdaExpression

namespace BinaryMash.Build.Installation.Deployments
{
    using System;
    using System.Globalization;
    using Components;
    using dropkick.Configuration.Dsl;
    using dropkick.Configuration.Dsl.Files;
    using dropkick.Configuration.Dsl.Iis;
    using dropkick.Configuration.Dsl.RoundhousE;
    using dropkick.Configuration.Dsl.WinService;

    public class Install : Deployment<Install, SystemSettings>
    {

        #region Constructors

        public Install()
        {
            Define(settings =>
            {

                DeploymentStepsFor(Db, 
                    s =>
                    {
                        s.RoundhousE()
                            .ForEnvironment(settings.Environment)
                            .OnDatabase(settings.DbName)
                            .WithScriptsFolder(settings.DbSqlFilesPath)
                            .WithDatabaseRecoveryMode(settings.DbRecoveryMode)
                            .WithRestorePath(settings.DbRestorePath)
                            .WithRepositoryPath("https://binarymash@bitbucket.org/binarymash/aptitude.git")
                            .WithVersionFile("_BuildInfo.xml")
                            .WithAlterDatabaseFolder("01_e_alterDatabase")
                            .WithRunAfterCreateDatabaseFolder("02_o_runAfterCreateDatabase")
                            .WithRunBeforeUpFolder("03_a_runBeforeUp")
                            .WithUpFolder("04_o_up")
                            .WithRunFirstAfterUpFolder("05_a_runFirstAfterUp")
                            .WithFunctionsFolder("06_a_functions")
                            .WithViewsFolder("07_a_views")
                            .WithSprocsFolder("08_a_sprocs")
                            .WithIndexesFolder("09_a_indexes")
                            .WithRunAfterOtherAnyTimeScriptsFolder("10_a_runAfterOtherAnyTimeScripts")
                            .WithPermissionsFolder("11_e_permissions")                            
                            .WithRoundhousEMode(settings.RoundhousEMode);
                    }
                );

                DeploymentStepsFor(AppServer, 
                    s =>
                    {
                        //Email Service
                        var baseDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) ?? string.Empty;
                        var binariesSourceDirectory = System.IO.Path.Combine(baseDirectory, "Application", "UserService");
                        var configSourceDirectory = System.IO.Path.Combine(baseDirectory, "Config", settings.Environment, "UserService");
                        var targetDirectory = System.IO.Path.Combine(settings.NServiceBusDeploymentDirectory, "UserService");

                        s.CopyDirectory(a => a.Include(binariesSourceDirectory))
                            .To(targetDirectory)
                            .DeleteDestinationBeforeDeploying();

                        s.CopyDirectory(a => a.Include(configSourceDirectory))
                            .To(targetDirectory);

                        NServiceBusGenericHost.Install(s, settings, targetDirectory,
                            "BinaryMash.Application.UserService",
                            "Provides user services for Aptitude",
                            new[]
                            {
                                "binarymash.application.userservice",
                                "binarymash.application.emailservice",
                                "binarymash.web.mvc"        
                            });

                        s.WinService("BinaryMash.Application.UserService").Start();

                        //User Service
                        baseDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) ?? string.Empty;
                        binariesSourceDirectory = System.IO.Path.Combine(baseDirectory, "Application", "EmailService");
                        configSourceDirectory = System.IO.Path.Combine(baseDirectory, "Config", settings.Environment, "EmailService");
                        targetDirectory = System.IO.Path.Combine(settings.NServiceBusDeploymentDirectory, "EmailService");

                        s.CopyDirectory(a => a.Include(binariesSourceDirectory))
                            .To(targetDirectory)
                            .DeleteDestinationBeforeDeploying();

                        s.CopyDirectory(a => a.Include(configSourceDirectory))
                            .To(targetDirectory);

                        NServiceBusGenericHost.Install(s, settings, targetDirectory,
                            "BinaryMash.Application.EmailService",
                            "Provides email services for Aptitude",
                            new string[]{});

                        s.WinService("BinaryMash.Application.EmailService").Start();
                    }
                );


                DeploymentStepsFor(WebServer, 
                    s =>
                        {
                            var baseDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) ?? string.Empty;
                            var binariesSourceDirectory = System.IO.Path.Combine(baseDirectory, "WebSite");
                            var configSourceDirectory = System.IO.Path.Combine(baseDirectory, "Config", settings.Environment, "Web");

                            s.CopyDirectory(a => a.Include(binariesSourceDirectory))
                                .To(settings.WebSiteDeploymentDirectory)
                                .DeleteDestinationBeforeDeploying();

                            s.CopyDirectory(a => a.Include(configSourceDirectory))
                                .To(settings.WebSiteDeploymentDirectory);

                            s.Iis7Site(settings.WebSiteName, settings.WebSiteDeploymentDirectory, Convert.ToInt32(settings.WebSitePort, CultureInfo.InvariantCulture)).Install
                                .VirtualDirectory(string.Empty)
                                    .SetAppPoolTo("AptitudeApplicationPool", pool =>
                                        {
                                            pool.SetRuntimeToV4();
                                            pool.Enable32BitAppOnWin64();
                                        })
                                    .SetPathTo(settings.WebSiteDeploymentDirectory);

                        }
                );
            });
        }

        #endregion

        #region Properties

        //order is important
        public static Role Db { get; set; }
        public static Role AppServer { get; set; }
        public static Role WebServer { get; set; }

        #endregion

    }
}