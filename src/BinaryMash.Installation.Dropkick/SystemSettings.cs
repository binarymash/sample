// ==============================================================================
// 
// ACuriousMind and FerventCoder Copyright � 2011 - Released under the Apache 2.0 License
// 
// Copyright 2007-2008 The Apache Software Foundation.
//  
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use 
// this file except in compliance with the License. You may obtain a copy of the 
// License at 
//
//     http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.
// ==============================================================================

namespace BinaryMash.Build.Installation
{
    using dropkick.Configuration;
    using dropkick.Tasks.RoundhousE;

    public class SystemSettings : DropkickConfiguration
    {
        
        #region Properties
        
        //database stuff 
        public string DbName { get; set; }
        public string DbSqlFilesPath { get; set; }
        public string DbRestorePath { get; set; }
        public DatabaseRecoveryMode DbRecoveryMode { get; set; }
        public RoundhousEMode RoundhousEMode { get; set; }
        public string SqlCmdPath { get; set; }
        public string DbServer { get; set; }
        public string DbBackupPath { get; set; }

        //WebSite
        public string WebSiteDeploymentDirectory { get; set; }
        public string WebSiteName { get; set; }
        public string WebSitePort { get; set; }

        //NSB Hosts
        public string NServiceBusDeploymentDirectory { get; set; }
        public string NServiceBusProfiles { get; set; }        

        #endregion

    }
}