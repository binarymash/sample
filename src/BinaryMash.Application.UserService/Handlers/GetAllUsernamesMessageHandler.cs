﻿namespace BinaryMash.Application.UserService.Handlers
{
    using System.Linq;
    using Contract.Messages;
    using DataAccess.Repositories.Contract;
    using Domain;
    using Microsoft.ApplicationServer.Caching;
    using NServiceBus;

    public class GetAllUsernamesMessageHandler : IHandleMessages<GetAllUsernamesMessage>
    {

        public IUserRepository UserRepository { get; set; }

        public DataCacheFactory DataCacheFactory { get; set; }

        public void Handle(GetAllUsernamesMessage message)
        {
            ////todo: create if it doesn't exist
            //var cache = DataCacheFactory.GetCache("authentication");

            //var allUserCredentials = UserRepository.GetAll()
            //    .Select(s => new UserCredential {Email = s.Email, HashedPassword = s.HashedPassword});

            //foreach (var credential in allUserCredentials)
            //{
            //    cache.Put(credential.Email, credential);
            //}

        }
    }
}
