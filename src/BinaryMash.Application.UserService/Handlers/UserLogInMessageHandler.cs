﻿namespace BinaryMash.Application.UserService.Handlers
{
    using Contract.Commands;
    using EmailService.Contract.Commands;
    using NServiceBus;

    public class UserLogInMessageHandler : IHandleMessages<UserLogInCommand>
    {
        public UserLogInMessageHandler(IBus bus)
        {
            _bus = bus;
        }

        public void Handle(UserLogInCommand message)
        {
            var command = new SendEmailCommand
            {
                FromAddress = "notification@binarymash.com",
                Subject = "User logged in",
                Body = string.Format("{0}: {1}", message.Time, message.Email),
                ToAddress = "philip.m.wood@gmail.com" //TODO: config or in db?
            };

            _bus.Send(command);
        }

        private readonly IBus _bus;
    }
}
