﻿
namespace BinaryMash.Application.UserService
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Core.NServiceBus;
    using DataAccess.Repositories.Contract;
    using Microsoft.ApplicationServer.Caching;
    using NServiceBus.UnitOfWork;

    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            InstallNsbUnitOfWork(container);
            //InstallAppFabricCache(container);
        }

        private static void InstallNsbUnitOfWork(IWindsorContainer container)
        {
            container.Register(

                //NSB child container will ensure that these effectively give us 
                //a singleton per unit of work. 
                //See http://andreasohlund.net/2010/10/12/support-for-child-containers-in-nservicebus/

                Component.For<IManageUnitsOfWork>()
                       .ImplementedBy<DbContextUnitOfWorkManager>()
                       .LifeStyle.Scoped()

                );
        }

        private static void InstallAppFabricCache(IWindsorContainer container)
        {
            container.Register(

                Component.For<DataCacheFactory>()
                    .Instance(new DataCacheFactory())

                );
        }
    }
}
