﻿namespace BinaryMash.Application.UserService.Sagas
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using Contract.Commands;
    using Contract.Messages;
    using DataAccess.Repositories.Contract;
    using Domain;
    using EmailService.Contract.Commands;
    using NServiceBus;
    using NServiceBus.Saga;

    public class PasswordResetWindowSaga : Saga<PasswordResetWindowSagaData>, 
        IAmStartedByMessages<OpenPasswordResetWindowCommand>,
        IHandleMessages<ClosePasswordResetWindowCommand>,
        IHandleTimeouts<PasswordResetWindowTimeout>
    {

        public ILoginRepository LoginRepository { get; set; }
        public IResetPasswordSagaTrackerRepository ResetPasswordSagaTrackerRepository { get; set; }

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<ClosePasswordResetWindowCommand>(m => m.PasswordResetWindowSagaId).ToSaga(s => s.Id);
        }

        public void Handle(OpenPasswordResetWindowCommand resetPasswordCommand)
        {
            Data.Email = resetPasswordCommand.Email;
            Data.LoginId = resetPasswordCommand.LoginId;
            var expires = DateTime.UtcNow.AddMinutes(10);

            var tracker = new PasswordResetWindow
            {
                PasswordResetWindowSagaId = Data.Id,
                LoginId = Data.LoginId,
                Expires = expires,
                Used = false
            };

            ResetPasswordSagaTrackerRepository.Add(tracker);

            var url = string.Format("http://{0}/Account/ResetPassword/{1}",
                                    ConfigurationManager.AppSettings["WebHost"],
                                    Data.Id);


            var command = new SendEmailCommand
            {
                FromAddress = "accounts@binarymash.com",
                Subject = "Password Reset",
                Body = string.Format("<p>Hello {0}.</p> <p>We received a request to reset your password. Please click <a href=\"{1}\">here</a>, or paste the link below into your browser, to complete the reset process.</p><p>{1}</p>", 
                    resetPasswordCommand.FirstName, 
                    url),
                ToAddress = resetPasswordCommand.Email
            };

            Bus.Send(command);

            RequestTimeout<PasswordResetWindowTimeout>(expires);
        }

        public void Handle(ClosePasswordResetWindowCommand closeResetPasswordWindowCommand)
        {
            ResetPasswordSagaTrackerRepository.Delete(Data.Id); 
            MarkAsComplete();            
        }

        public void Timeout(PasswordResetWindowTimeout message)
        {
            ResetPasswordSagaTrackerRepository.Delete(Data.Id);
            MarkAsComplete();
        }
    }
}
