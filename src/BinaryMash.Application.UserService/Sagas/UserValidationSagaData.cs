﻿namespace BinaryMash.Application.UserService.Sagas
{
    using System;
    using NServiceBus.Saga;

    public class UserValidationSagaData : IContainSagaData
    {
        public Guid Id { get; set; }

        public string Originator { get; set; }

        public string OriginalMessageId { get; set; }

        [Unique]
        public string Email { get; set; }

        public Guid UserRegistrationSagaId { get; set; }
    }
}
