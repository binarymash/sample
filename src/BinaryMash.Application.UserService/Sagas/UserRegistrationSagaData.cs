﻿namespace BinaryMash.Application.UserService.Sagas
{
    using System;
    using NServiceBus.Saga;

    public class UserRegistrationSagaData : IContainSagaData
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
        //[Unique]
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ValidationAttempts { get; set; }
    }
}
