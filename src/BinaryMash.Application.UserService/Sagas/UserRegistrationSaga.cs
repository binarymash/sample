﻿namespace BinaryMash.Application.UserService.Sagas
{
    using Contract.Commands;
    //using Contract.Events;
    using Contract.Messages;
    using DataAccess.Repositories.Contract;
    using Domain;
    using NServiceBus;
    using NServiceBus.Saga;

    public class UserRegistrationSaga : Saga<UserRegistrationSagaData>, 
        IAmStartedByMessages<RegisterUserCommand>,
        IHandleMessages<UserValidatedMessage>
    {

        public ILoginRepository LoginRepository { get; set; }

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<UserValidatedMessage>(m => m.UserRegistrationSagaId).ToSaga(s => s.Id);
        }

        public void Handle(RegisterUserCommand registerUserCommand)
        {
            Data.Email = registerUserCommand.Email;
            Data.HashedPassword = registerUserCommand.HashedPassword;
            Data.FirstName = registerUserCommand.FirstName;
            Data.LastName = registerUserCommand.LastName;
            Data.ValidationAttempts = 0;
            Data.UserId = registerUserCommand.UserId;
            Data.AccountId = registerUserCommand.AccountId;
            Data.Salt = registerUserCommand.Salt;

            var command = new ValidateUserCommand
            {
                Email = Data.Email,
                FirstName = Data.FirstName,
                UserRegistrationSagaId = Data.Id
            };
            Bus.Send(command);
        }

        public void Handle(UserValidatedMessage userValidatedMessage)
        {

            var account = new Account
            {
                Id = Data.AccountId,
                Name = string.Empty
            };

            var publicProfile = new UserPublicProfile
            {
                DisplayName = Data.FirstName+" "+Data.LastName
            };

            if (publicProfile.DisplayName.Length > 32)
            {
                publicProfile.DisplayName = publicProfile.DisplayName.Substring(0, 32);
            }

            var user = new User
            {
                Id = Data.UserId,
                Account = account,
                Email = Data.Email,
                FirstName = Data.FirstName,
                LastName = Data.LastName,
                PublicProfile = publicProfile,
                Permissions = 0xffff
            };

            var login = new Login
            {
                Id = Data.UserId,
                HashedPassword = Data.HashedPassword,
                Salt = Data.Salt,
                User = user
            };

            LoginRepository.Add(login);

            //Bus.Publish(new UserActivated
            //{
            //    UserCredential = new UserCredential
            //    {
            //        Email = Data.Email,
            //        HashedPassword = Data.HashedPassword
            //    }
            //});

            MarkAsComplete();            
        }

    }
}
