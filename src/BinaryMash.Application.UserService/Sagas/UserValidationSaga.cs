﻿namespace BinaryMash.Application.UserService.Sagas
{
    using System;
    using System.Configuration;
    using System.Web;
    using Contract.Commands;
    using Contract.Messages;
    using EmailService.Contract.Commands;
    using NServiceBus;
    using NServiceBus.Saga;

    public class UserValidationSaga : Saga<UserValidationSagaData>, 
        IAmStartedByMessages<ValidateUserCommand>,
        IHandleMessages<UserValidationReceivedMessage>,
        IHandleTimeouts<UserValidationNotReceivedMessage>
    {

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<UserValidationReceivedMessage>(m => m.UserValidationSagaId).ToSaga(s => s.Id);
        }

        public void Handle(ValidateUserCommand message)
        {
            Data.Email = message.Email;
            Data.UserRegistrationSagaId = message.UserRegistrationSagaId;
            var url = string.Format("http://{0}/Account/Activate/{1}?email={2}",
                                    ConfigurationManager.AppSettings["WebHost"],
                                    Data.Id,
                                    HttpUtility.UrlEncode(Data.Email));

            var command = new SendEmailCommand
            {
                FromAddress = "accounts@binarymash.com",
                Subject = "Activate your account",
                Body = string.Format("<p>Hello {0}.</p> <p>Your account needs to be activated before it can be used. Please click <a href=\"{1}\">here</a>, or paste the link below into your browser.</p><p>{1}</p>", message.FirstName, url),
                ToAddress = message.Email
            };
            Bus.Send(command);
            
            RequestTimeout<UserValidationNotReceivedMessage>(TimeSpan.FromDays(1));
        }

        public void Handle(UserValidationReceivedMessage message)
        {
            var userValidatedMessage = new UserValidatedMessage {UserRegistrationSagaId = Data.UserRegistrationSagaId};
            ReplyToOriginator(userValidatedMessage);
            MarkAsComplete();
        }

        public void Timeout(UserValidationNotReceivedMessage state)
        {
            var userNotValidatedMessage = new UserNotValidatedMessage { UserRegistrationSagaId = Data.UserRegistrationSagaId };
            ReplyToOriginator(userNotValidatedMessage);
            MarkAsComplete();            
        }

    }
}
