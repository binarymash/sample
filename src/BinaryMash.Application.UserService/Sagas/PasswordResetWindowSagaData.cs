﻿namespace BinaryMash.Application.UserService.Sagas
{
    using System;
    using NServiceBus.Saga;

    public class PasswordResetWindowSagaData : IContainSagaData
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public Guid LoginId { get; set; }
        public string Email { get; set; }
    }
}
